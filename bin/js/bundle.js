var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const engine_1 = require("./framework/runtime/engine");
/**
 * @author Sun
 * @time 2019-08-11 19:05
 * @project SFramework_LayaAir
 * @description 游戏启动入口
 *
 */
class Main {
    constructor() {
        engine_1.Engine.$.run();
    }
}
//激活启动类
new Main();
},{"./framework/runtime/engine":30}],2:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MainScene = void 0;
const scene_base_1 = require("../../framework/manager/ui/scene-base");
var LyScene = scene_base_1.CustomScene.LyScene;
/**
* @author Sun
* @time 2019-08-11 11:20
* @project SFramework_LayaAir
* @description 主场景
*
*/
class MainScene extends LyScene {
    constructor() {
        super();
        this.needLoadRes
            .add("res/bg/123.png", Laya.Loader.IMAGE);
    }
}
exports.MainScene = MainScene;
},{"../../framework/manager/ui/scene-base":28}],3:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GameSetting = void 0;
const singleton_1 = require("../../framework/core/singleton");
const config_1 = require("../../framework/setting/config");
const json_template_1 = require("../../framework/manager/json/json-template");
const enum_1 = require("../../framework/setting/enum");
/**
 * @author Sun
 * @time 2019-10-16 21:28
 * @project SFramework_LayaAir
 * @description 手动修改的游戏配置 （不直接修改framework 保持框架的整洁）
 */
class GameSetting extends singleton_1.Singleton {
    constructor() {
        super();
    }
    static get $() {
        if (!this.instance)
            this.instance = new GameSetting();
        return this.instance;
    }
    init() {
        //手动配置Json文件 json 必须执行在ConfigRes之前
        config_1.ConfigData.$.jsonTemplateList = [
            new json_template_1.JsonTemplate("res/data/InviteData.json", enum_1.enumJsonDefine.invite),
            new json_template_1.JsonTemplate("res/data/LevelData.json", enum_1.enumJsonDefine.level),
            new json_template_1.JsonTemplate("res/data/OfflineData.json", enum_1.enumJsonDefine.offline),
            new json_template_1.JsonTemplate("res/data/TurntableData.json", enum_1.enumJsonDefine.lottery),
        ];
        //手动配置loading资源
        config_1.ConfigRes.$.defaultLoadRes
            .add("res/loading/img_loading_bg.png", Laya.Loader.IMAGE)
            .add("res/loading/progress_loading.png", Laya.Loader.IMAGE)
            .add("res/loading/img_8r.png", Laya.Loader.IMAGE);
        //手动配置主页资源
        config_1.ConfigRes.$.defaultMainRes
            .add("res/atlas/res/main/effect.atlas", Laya.Loader.ATLAS)
            .add("res/atlas/res/com.atlas", Laya.Loader.ATLAS)
            .add("res/com/img_lottery_border.png", Laya.Loader.IMAGE)
            .add("res/com/img_lottery_content.png", Laya.Loader.IMAGE)
            .add("res/main/bg/bg.png", Laya.Loader.IMAGE);
    }
}
exports.GameSetting = GameSetting;
GameSetting.instance = null;
},{"../../framework/core/singleton":12,"../../framework/manager/json/json-template":19,"../../framework/setting/config":31,"../../framework/setting/enum":32}],4:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LotteryView = void 0;
const layaMaxUI_1 = require("../../../ui/layaMaxUI");
var lotteryUI = layaMaxUI_1.ui.view.com.lotteryUI;
const json_manager_1 = require("../../../framework/manager/json/json-manager");
const enum_1 = require("../../../framework/setting/enum");
const math_1 = require("../../../framework/util/math");
const log_1 = require("../../../framework/core/log");
/**
 * @author Sun
 * @time 2019-08-12 17:31
 * @project SFramework_LayaAir
 * @description 转盘模板
 *
 */
class LotteryView extends lotteryUI {
    constructor() {
        super();
        /****************************************主页面属性设置****************************************/
        /** Des:倍率 */
        this.rewardMul = 2;
        /** Des:转盘数据 */
        this.lotteryData = null;
    }
    static get $() {
        if (this.instance == null)
            this.instance = new LotteryView();
        return this.instance;
    }
    onAwake() {
        super.onAwake();
        this.init();
    }
    close() {
        super.close();
    }
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /****************************************主页面初始数据****************************************/
    init() {
        this.lotteryData = json_manager_1.JsonManager.$.getTable(enum_1.enumJsonDefine.lottery);
        this.btnConfirm.on(Laya.Event.CLICK, this, this.onBtnStart);
    }
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /****************************************主页面点击事件****************************************/
    onBtnStart() {
        let random = math_1.UtilMath.random(1, 100);
        for (let i = 0; i < 6; i++) {
            if (this.lotteryData[i].rangeMin <= random && random <= this.lotteryData[i].rangeMax) {
                this.rewardMul = this.lotteryData[i].reward;
                this.onTurning(i);
                break;
            }
        }
    }
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /*****************************************转盘动画显示*****************************************/
    onTurning(reward = 0) {
        //关闭关闭按钮显示
        this.btnClose.visible = false;
        //禁用转盘按钮
        this.btnConfirm.mouseEnabled = false;
        //转盘动画
        let aCount = Object.keys(this.lotteryData).length;
        let cIndex = reward;
        let perDeg = 360 / aCount;
        let curDeg = (360 - perDeg * (cIndex - 1)) + math_1.UtilMath.randRangeInt(-perDeg / 2, perDeg / 2);
        this.imgContext.rotation = 0;
        let dstRotation = 3600 + curDeg;
        Laya.Tween.to(this.imgContext, {
            rotation: dstRotation,
        }, 6000, Laya.Ease.strongOut, Laya.Handler.create(this, () => {
            this.btnConfirm.mouseEnabled = true;
            this.btnClose.visible = true;
            log_1.Log.log("倍率：" + this.rewardMul);
        }), 0, false, false);
    }
}
exports.LotteryView = LotteryView;
},{"../../../framework/core/log":10,"../../../framework/manager/json/json-manager":18,"../../../framework/setting/enum":32,"../../../framework/util/math":38,"../../../ui/layaMaxUI":43}],5:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BgView = void 0;
const layaMaxUI_1 = require("../../../ui/layaMaxUI");
var bgUI = layaMaxUI_1.ui.view.main.bgUI;
/**
 * @author Sun
 * @time 2019-08-11 11:23
 * @project SFramework_LayaAir
 * @description
 *
 */
class BgView extends bgUI {
    constructor() {
        super();
    }
    static get $() {
        if (!this.instance)
            this.instance = new BgView();
        return this.instance;
    }
    onAwake() {
        super.onAwake();
        this.Init();
        this.suitInit();
    }
    /**
     * 初始化一次
     */
    Init() {
        this.initOnce();
        // //数据监听
        // this.addDataWatch(DataDefine.UserInfo);
        if (Laya.Browser.onWeiXin) {
            this.initLink();
        }
    }
    /**
     * 适配
     */
    suitInit() {
        this.width = Laya.stage.width;
        this.height = Laya.stage.height;
    }
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /*****************************************页面初始数据*****************************************/
    /** Des:构造是初始化一次 */
    initOnce() {
    }
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /***************************************外部连接进入判断***************************************/
    /** Des:判断进入连接信息 */
    initLink() {
    }
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /*****************************************页面事件相关*****************************************/
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /****************************************数据改变的监听****************************************/
    /**
     * 刷新数据
     */
    onData(data) {
    }
}
exports.BgView = BgView;
},{"../../../ui/layaMaxUI":43}],6:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.D3View = void 0;
const layaMaxUI_1 = require("../../../ui/layaMaxUI");
var d3UI = layaMaxUI_1.ui.view.main.d3UI;
const load3d_1 = require("../../../framework/util/load3d");
const config_1 = require("../../../framework/setting/config");
/**
 * @author Sun
 * @time 2019-08-11 12:03
 * @project SFramework_LayaAir
 * @description 3D场景层
 *
 */
class D3View extends d3UI {
    constructor() {
        super();
    }
    static get $() {
        if (!this.instance)
            this.instance = new D3View();
        return this.instance;
    }
    onAwake() {
        super.onAwake();
        this.Init();
        this.suitInit();
    }
    /**
     * 初始化一次
     */
    Init() {
        this.initOnce();
        // //数据监听
        // this.addDataWatch(DataDefine.UserInfo);
    }
    /**
     * 每次弹出初始化一次
     */
    popupInit() {
        this.initAll();
    }
    /**
     * 适配
     */
    suitInit() {
        this.width = Laya.stage.width;
        this.height = Laya.stage.height;
    }
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /*****************************************页面初始数据*****************************************/
    /** Des:构造是初始化一次 */
    initOnce() {
    }
    /** Des:每次弹出初始化 */
    initAll() {
    }
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /***************************************外部连接进入判断***************************************/
    /** Des:判断进入连接信息 */
    initLink() {
    }
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /*****************************************页面事件相关*****************************************/
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /****************************************3D场景加载完成回调****************************************/
    /**
     * 加载3D场景
     */
    load3DScene(area, callBack) {
        load3d_1.UtilLoad3D.loadScene(config_1.Config3D.$.scenePath, area, callBack);
    }
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /****************************************数据改变的监听****************************************/
    /**
     * 刷新数据
     */
    onData(data) {
    }
}
exports.D3View = D3View;
},{"../../../framework/setting/config":31,"../../../framework/util/load3d":37,"../../../ui/layaMaxUI":43}],7:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EffectView = void 0;
const layaMaxUI_1 = require("../../../ui/layaMaxUI");
var effectUI = layaMaxUI_1.ui.view.main.effectUI;
var Browser = Laya.Browser;
const lottery_view_1 = require("../component-view/lottery-view");
class EffectView extends effectUI {
    constructor() {
        super();
    }
    static get $() {
        if (!this.instance)
            this.instance = new EffectView();
        return this.instance;
    }
    onAwake() {
        super.onAwake();
        this.Init();
        this.suitInit();
    }
    /**
     * 初始化一次
     */
    Init() {
        this.initOnce();
        // //数据监听
        // this.addDataWatch(DataDefine.UserInfo);
        if (Browser.onWeiXin) {
            this.initLink();
        }
    }
    /**
     * 每次弹出初始化一次
     */
    popupInit() {
        this.initAll();
    }
    /**
     * 适配
     */
    suitInit() {
        this.width = Laya.stage.width;
        this.height = Laya.stage.height;
    }
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /*****************************************页面初始数据*****************************************/
    /** Des:构造是初始化一次 */
    initOnce() {
        this.btnLucky.on(Laya.Event.CLICK, this, () => {
            let view = lottery_view_1.LotteryView.$;
            view.popupDialog();
        });
    }
    /** Des:每次弹出初始化 */
    initAll() {
    }
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /***************************************外部连接进入判断***************************************/
    /** Des:判断进入连接信息 */
    initLink() {
    }
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /*****************************************页面点击事件*****************************************/
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /****************************************数据改变的监听****************************************/
    /**
     * 刷新数据
     */
    onData(data) {
    }
}
exports.EffectView = EffectView;
},{"../../../ui/layaMaxUI":43,"../component-view/lottery-view":4}],8:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GameView = void 0;
const layaMaxUI_1 = require("../../../ui/layaMaxUI");
var Browser = Laya.Browser;
var gameUI = layaMaxUI_1.ui.view.main.gameUI;
/**
 * @author Sun
 * @time 2019-08-11 18:08
 * @project SFramework_LayaAir
 * @description 主页
 *
 */
class GameView extends gameUI {
    constructor() {
        super();
    }
    static get $() {
        if (!this.instance)
            this.instance = new GameView();
        return this.instance;
    }
    onAwake() {
        super.onAwake();
        this.Init();
        this.suitInit();
    }
    /**
     * 初始化一次
     */
    Init() {
        this.initOnce();
        // //数据监听
        // this.addDataWatch(DataDefine.UserInfo);
        if (Browser.onWeiXin) {
            this.initLink();
        }
    }
    /**
     * 每次弹出初始化一次
     */
    popupInit() {
        this.initAll();
    }
    /**
     * 适配
     */
    suitInit() {
    }
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /*****************************************页面初始数据*****************************************/
    /** Des:构造是初始化一次 */
    initOnce() {
    }
    /** Des:每次弹出初始化 */
    initAll() {
    }
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /***************************************外部连接进入判断***************************************/
    /** Des:判断进入连接信息 */
    initLink() {
    }
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /*****************************************页面点击事件*****************************************/
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /****************************************数据改变的监听****************************************/
    /**
     * 刷新数据
     */
    onData(data) {
    }
}
exports.GameView = GameView;
class Greeter {
    constructor(message) {
        this.greeting = message;
    }
    greet() {
        return "Hello, " + this.greeting;
    }
}
},{"../../../ui/layaMaxUI":43}],9:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoadingView = void 0;
const layaMaxUI_1 = require("../../../ui/layaMaxUI");
var loadingUI = layaMaxUI_1.ui.view.main.loadingUI;
const bg_view_1 = require("./bg-view");
const d3_view_1 = require("./d3-view");
const config_1 = require("../../../framework/setting/config");
const number_1 = require("../../../framework/util/number");
const enum_1 = require("../../../framework/setting/enum");
const game_view_1 = require("./game-view");
const effect_view_1 = require("./effect-view");
const event_data_1 = require("../../../framework/manager/event/event-data");
const res_manager_1 = require("../../../framework/manager/res/res-manager");
class LoadingView extends loadingUI {
    /*****************************************页面属性管理*****************************************/
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /*****************************************页面生命周期*****************************************/
    constructor() {
        super();
    }
    onAwake() {
        // super.onAwake();
        this.Init();
        this.suitInit();
    }
    /**
   * 加载页面启动项
   */
    onStart() {
        //加载主场景所需要的资源信息
        res_manager_1.ResManager.$.loadGroup(config_1.ConfigRes.$.defaultMainRes, new event_data_1.EventFunc(this, this.onProgress), new event_data_1.EventFunc(this, this.onCompleted));
        this.lblLoading.text = "游戏登录中...";
    }
    /**
     * 加载完成回调
     * @param success
     */
    onCompleted(success) {
        //Bg页面
        let bgView = bg_view_1.BgView.$;
        Laya.stage.addChild(bgView);
        if (config_1.ConfigGame.$.dimension == enum_1.enumDimension.Dim3) {
            //3D页面
            let d3View = d3_view_1.D3View.$;
            Laya.stage.addChild(d3View);
            d3View.load3DScene(this, this.showView);
        }
        else {
            this.showView();
        }
    }
    showView() {
        //主页
        let gameView = game_view_1.GameView.$;
        Laya.stage.addChild(gameView);
        //效果页
        let effectView = effect_view_1.EffectView.$;
        Laya.stage.addChild(effectView);
        //结束销毁加载页
        this.destroy();
    }
    /**
     * 加载进度
     * @param progress
     */
    onProgress(progress) {
        let fixed = number_1.UtilNumber.toFixed(progress * 100, 0);
        this.lblLoading.text = fixed + "%";
        this.pro_Loading.value = fixed / 100;
    }
    /**
     * 初始化一次
     */
    Init() {
        this.initOnce();
    }
    /**
     * 每次弹出初始化一次
     */
    popupInit() {
        this.initAll();
    }
    /**
     * 适配
     */
    suitInit() {
        this.width = Laya.stage.width;
        this.height = Laya.stage.height;
        this.img_bg.width = this.width;
        this.img_bg.height = this.height;
        this.img_bg.x = 0;
        this.img_bg.y = 0;
    }
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /*****************************************页面初始数据*****************************************/
    /** Des:构造是初始化一次 */
    initOnce() {
    }
    /** Des:每次弹出初始化 */
    initAll() {
    }
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /***************************************外部连接进入判断***************************************/
    /** Des:判断进入连接信息 */
    initLink() {
    }
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /*****************************************页面点击事件*****************************************/
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /****************************************数据改变的监听****************************************/
    /**
     * 刷新数据
     */
    onData(data) {
    }
    /********************************************——**********************************************/
    ///////////////////////////////////////////-分界线-///////////////////////////////////////////
    /******************************************销毁自身******************************************/
    destroy() {
        // this.removeSelf();
        // ResManager.$.releaseGroup(ConfigRes.$.defaultLoadRes);
    }
}
exports.LoadingView = LoadingView;
},{"../../../framework/manager/event/event-data":15,"../../../framework/manager/res/res-manager":22,"../../../framework/setting/config":31,"../../../framework/setting/enum":32,"../../../framework/util/number":40,"../../../ui/layaMaxUI":43,"./bg-view":5,"./d3-view":6,"./effect-view":7,"./game-view":8}],10:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Log = void 0;
const config_1 = require("../setting/config");
/**
* @author Sun
* @time 2019-08-09 15:59
* @project SFramework_LayaAir
* @description 输出信息管理
*/
class Log {
    static debug(...args) {
        if (config_1.ConfigDebug.$.isDebug)
            console.debug("[debug]", args.toString());
    }
    static info(...args) {
        if (config_1.ConfigDebug.$.isDebug)
            console.info("[info]", args.toString());
    }
    static warn(...args) {
        if (config_1.ConfigDebug.$.isDebug)
            console.warn("[warn]", args.toString());
    }
    static error(...args) {
        if (config_1.ConfigDebug.$.isDebug)
            console.error("[error]", args.toString());
    }
    static exception(...args) {
        if (config_1.ConfigDebug.$.isDebug)
            console.exception("[exce]", args.toString());
    }
    static log(...args) {
        if (config_1.ConfigDebug.$.isDebug)
            console.log("[log]", args.toString());
    }
    /**打印设备信息*/
    static printDeviceInfo() {
        if (config_1.ConfigDebug.$.isDebug && navigator) {
            let agentStr = navigator.userAgent;
            let start = agentStr.indexOf("(");
            let end = agentStr.indexOf(")");
            if (start < 0 || end < 0 || end < start) {
                return;
            }
            let infoStr = agentStr.substring(start + 1, end);
            let device, system, version;
            let infos = infoStr.split(";");
            if (infos.length == 3) {
                //如果是三个的话， 可能是android的， 那么第三个是设备号
                device = infos[2];
                //第二个是系统号和版本
                let system_info = infos[1].split(" ");
                if (system_info.length >= 2) {
                    system = system_info[1];
                    version = system_info[2];
                }
            }
            else if (infos.length == 2) {
                system = infos[0];
                device = infos[0];
                version = infos[1];
            }
            else {
                system = navigator.platform;
                device = navigator.platform;
                version = infoStr;
            }
            Log.info(system, device, version);
        }
    }
}
exports.Log = Log;
},{"../setting/config":31}],11:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ObjectPool = void 0;
const log_1 = require("./log");
/**
 * @author Sun
 * @time 2019-08-09 23:25
 * @project SFramework_LayaAir
 * @description  对象池
 *
 */
class ObjectPool {
    /**
     * 获取一个对象，不存在则创建
     * @param classDef  类名
     */
    static get(classDef) {
        let sign = "dc." + classDef.name;
        let obj = Laya.Pool.getItem(sign);
        if (!obj) {
            if (!Laya.ClassUtils.getRegClass(sign)) {
                log_1.Log.debug("[pools]注册对象池:" + sign);
                Laya.ClassUtils.regClass(sign, classDef);
            }
            obj = Laya.ClassUtils.getInstance(sign);
        }
        if (obj && obj["init"])
            obj.init();
        return obj;
    }
    /**
     * 回收对象
     * @param obj  对象实例
     */
    static recover(obj) {
        if (!obj)
            return;
        let proto = Object.getPrototypeOf(obj);
        let clazz = proto["constructor"];
        let sign = "dc." + clazz.name;
        obj.close();
        Laya.Pool.recover(sign, obj);
    }
}
exports.ObjectPool = ObjectPool;
},{"./log":10}],12:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Singleton = void 0;
const log_1 = require("./log");
/**
* @author Sun
* @time 2019-08-09 15:57
* @project SFramework_LayaAir
* @description 单例工具类
*/
class Singleton {
    constructor() {
        let clazz = this["constructor"];
        if (!clazz) {
            console.warn("Not support constructor!");
            log_1.Log.warn("Not support constructor!");
            return;
        }
        // 防止重复实例化
        if (Singleton.classKeys.indexOf(clazz) != -1)
            throw new Error(this + "Only instance once!");
        else {
            Singleton.classKeys.push(clazz);
            Singleton.classValues.push(this);
        }
    }
}
exports.Singleton = Singleton;
Singleton.classKeys = [];
Singleton.classValues = [];
},{"./log":10}],13:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimeDelay = exports.TimeDelayData = void 0;
/**
 * @author Sun
 * @time 2019-08-09 23:31
 * @project SFramework_LayaAir
 * @description  事件任务属性
 *
 */
class TimeDelayData {
    set(interval, repeat, callback, thisObj, param) {
        this.interval = interval;
        this.repeat = repeat;
        this.callback = callback;
        this.param = param;
        this.thisObj = thisObj;
    }
}
exports.TimeDelayData = TimeDelayData;
/**
* @author Sun
* @time 2019-08-09 23:25
* @project SFramework_LayaAir
* @description  时间控制核心类
*
*/
const singleton_1 = require("./singleton");
class TimeDelay extends singleton_1.Singleton {
    constructor() {
        super();
        /**当前事件执行的次数 */
        this.repeat = 0;
        this.items = new Array();
        this.toAdd = new Array();
        this.toRemove = new Array();
        this.pool = new Array();
        this.lastTime = 0;
        this.deltaTime = 0;
        Laya.timer.frameLoop(0.01, this, this.update);
    }
    static get $() {
        if (this.mInstance == null) {
            this.mInstance = new TimeDelay();
        }
        return this.mInstance;
    }
    /**
     * 从池子中获取data类
     */
    getFromPool() {
        let t;
        if (this.pool.length > 0) {
            t = this.pool.pop();
        }
        else
            t = new TimeDelayData();
        return t;
    }
    /**
     * data类放回池子
     * @param t
     */
    returnToPool(t) {
        t.set(0, 0, null, null, null);
        t.elapsed = 0;
        t.deleted = false;
        this.pool.push(t);
    }
    exists(callback, thisObj) {
        let t = this.toAdd.find((value, index, obj) => {
            return value.callback == callback && value.thisObj == thisObj;
        });
        if (t != null) {
            return true;
        }
        t = this.items.find((value, index, obj) => {
            return value.callback == callback && value.thisObj == thisObj;
        });
        if (t != null && !t.deleted) {
            return true;
        }
        return false;
    }
    add(interval, repeat, callback, thisObj, callbackParam = null) {
        let t;
        t = this.items.find((value, index, obj) => {
            return value.callback == callback && value.thisObj == thisObj;
        });
        if (t == null) {
            t = this.toAdd.find((value, index, obj) => {
                return value.callback == callback && value.thisObj == thisObj;
            });
        }
        if (t == null) {
            t = this.getFromPool();
            this.toAdd.push(t);
        }
        t.set(interval, repeat, callback, thisObj, callbackParam);
        t.deleted = false;
        t.elapsed = 0;
    }
    addUpdate(callback, thisObj, callbackParam = null) {
        this.add(0.001, 0, callback, thisObj, callbackParam);
    }
    remove(callback, thisObj) {
        let findindex = -1;
        let t = this.toAdd.find((value, index, obj) => {
            if (value.callback == callback && value.thisObj == thisObj) {
                findindex = index;
                return true;
            }
            else {
                return false;
            }
        });
        if (t != null) {
            this.toAdd.splice(findindex, 1);
            this.returnToPool(t);
        }
        t = this.items.find((value, index, obj) => {
            return value.callback == callback && value.thisObj == thisObj;
        });
        if (t != null)
            t.deleted = true;
    }
    start() {
        this.lastTime = Laya.timer.currTimer;
    }
    update() {
        this.deltaTime = (Laya.timer.currTimer - this.lastTime) / 1000;
        this.lastTime = Laya.timer.currTimer;
        for (let index = 0; index < this.items.length; index++) {
            let t = this.items[index];
            if (t.deleted) {
                this.toRemove.push(t);
                continue;
            }
            t.elapsed += this.deltaTime;
            if (t.elapsed < t.interval) {
                continue;
            }
            t.elapsed = 0;
            if (t.repeat > 0) {
                t.repeat--;
                if (t.repeat == 0) {
                    t.deleted = true;
                    this.toRemove.push(t);
                }
            }
            this.repeat = t.repeat;
            if (t.callback != null) {
                try {
                    t.callback.call(t.thisObj, t.param);
                }
                catch (error) {
                    t.deleted = true;
                }
            }
        }
        let len = this.toRemove.length;
        while (len) {
            let t = this.toRemove.pop();
            let index = this.items.indexOf(t);
            if (t.deleted && index != -1) {
                this.items.splice(index, 1);
                this.returnToPool(t);
            }
            len--;
        }
        len = this.toAdd.length;
        while (len) {
            let t = this.toAdd.pop();
            this.items.push(t);
            len--;
        }
    }
}
exports.TimeDelay = TimeDelay;
TimeDelay.mInstance = null;
},{"./singleton":12}],14:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataManager = void 0;
const event_node_1 = require("../event/event-node");
/**
 * @author Sun
 * @time 2019-08-09 15:51
 * @project SFramework_LayaAir
 * @description 数据管理类
 */
class DataManager extends event_node_1.EventNode {
    constructor() {
        super();
        this.datas = new Map();
    }
    static get $() {
        if (!this.instance)
            this.instance = new DataManager();
        return this.instance;
    }
    setup() {
    }
    update() {
    }
    destroy() {
        this.datas.clear();
    }
    register(data) {
        this.datas.set(data.cmd, data);
        return this;
    }
    get(cmd) {
        return this.datas.get(cmd);
    }
}
exports.DataManager = DataManager;
DataManager.instance = null;
},{"../event/event-node":17}],15:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventFunc = exports.EventData = void 0;
/**
* @author Sun
* @time 2019-08-12 17:13
* @project SFramework_LayaAir
* @description 事件数据定义类
*/
class EventData {
    constructor(cmd, obj = null, isStop = false) {
        this.isStop = false;
        this.cmd = cmd;
        this.data = obj;
        this.isStop = false;
    }
    /**
     * 快速创建事件数据
     * @param cmd
     * @param data
     * @param isStop
     */
    static create(cmd, data = null, isStop = false) {
        return new EventData(cmd, data, isStop);
    }
    stop() {
        this.isStop = true;
    }
}
exports.EventData = EventData;
/**
* @author Sun
* @time 2019-01-20 00:24
* @project SFramework_LayaAir
* @description 事件回调函数定义
*/
class EventFunc {
    constructor(thisObj, callBack) {
        this.m_this = thisObj;
        this.m_cb = callBack;
    }
    invoke(...args) {
        this.m_cb.call(this.m_this, ...args);
    }
}
exports.EventFunc = EventFunc;
},{}],16:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventManager = void 0;
const event_node_1 = require("./event-node");
/**
* @author Sun
* @time 2019-01-18 16:20
* @project SFramework_LayaAir
* @description 事件管理器
*/
class EventManager extends event_node_1.EventNode {
    constructor() {
        super();
    }
    static get $() {
        if (!this.instance)
            this.instance = new EventManager();
        return this.instance;
    }
    setup() {
        event_node_1.EventContext.eventNodes.clear();
    }
    update() {
    }
    destroy() {
        event_node_1.EventContext.eventNodes.clear();
    }
    /**
     * 移除一个消息监听节点
     * @param node
     */
    remove(node) {
        node.removeEventListenerAll();
        event_node_1.EventContext.eventNodes.delete(node);
    }
    /**
     * 给所有本地消息节点通知消息
     * @param ed
     */
    dispatchEventLocalAll(ed) {
        event_node_1.EventContext.eventNodes.forEach((en) => {
            en.dispatchEvent(ed);
        });
    }
    /**
     * 给所有本地消息节点通知消息
     * @param cmd
     * @param data
     */
    dispatchEventLocalAllByCmd(cmd, data = null) {
        event_node_1.EventContext.eventNodes.forEach((en) => {
            en.dispatchEventByCmd(cmd, data);
        });
    }
    /**
     * 添加一个消息监听器
     * @param type 消息类型
     * @param callBack 回调函数
     * @param target 作用对象
     * @param priority 消息的优先级
     * @param once 是否只监听一次
     */
    addListener(type, callBack, target, priority = 0, once = false) {
        event_node_1.EventNode.addGlobalListener(type, callBack, target, priority, once);
    }
    /**
     * 移除一个消息监听器
     * @param type 消息id
     * @param callBack 回调函数
     * @param target 作用的对象
     */
    removeListener(type, callBack, target) {
        event_node_1.EventNode.removeGlobalListener(type, callBack, target);
    }
    /**
     * 是否存在这个监听消息
     * @param type 消息类型
     * @param callBack 回调类型
     * @param target 回调对象
     */
    hasListener(type, callBack, target) {
        event_node_1.EventNode.hasGlobalListener(type, callBack, target);
    }
    /**
     * 派发消息
     * @param cmd 消息id
     * @param data 消息内容
     */
    dispatchEventByCmd(cmd, data = null) {
        event_node_1.EventNode.dispatchGlobalByCmd(cmd, data);
    }
}
exports.EventManager = EventManager;
EventManager.instance = null;
},{"./event-node":17}],17:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventContext = exports.EventNode = void 0;
const event_data_1 = require("./event-data");
const log_1 = require("../../core/log");
const singleton_1 = require("../../core/singleton");
/**
* @author Sun
* @time 2019-01-18 16:20
* @project SFramework_LayaAir
* @description 所有需要监控事件节点的基类
*/
class EventNode extends singleton_1.Singleton {
    constructor() {
        super();
        // ==================================================
        // ==============  Local Event Section ==============
        // ==================================================
        this.m_eventData = new Array();
        this.m_eventDict = {};
        EventContext.eventNodes.set(this, this);
    }
    static createGlobalData(cmd, data) {
        let ed;
        if (EventNode.m_globalEventData.length > 0) {
            ed = EventNode.m_globalEventData.pop();
            ed.cmd = cmd;
            ed.data = data;
            ed.isStop = false;
        }
        else {
            ed = new event_data_1.EventData(cmd, data);
        }
        return ed;
    }
    static returnGlobalEventData(ed) {
        ed.data = null;
        ed.cmd = null;
        ed.isStop = false;
        EventNode.m_globalEventData.push(ed);
    }
    /**
     * 添加一个消息监听器
     * @param type 消息类型
     * @param callBack 回调函数
     * @param target 作用对象
     * @param priority 消息的优先级
     * @param once 是否只监听一次
     */
    static addGlobalListener(type, callBack, target, priority = 0, once = false) {
        type = type.toString();
        let info = {
            type: type,
            callBack: callBack,
            target: target,
            priority: priority,
            once: once
        };
        let array = EventNode.m_globalEventDict[type];
        let has = false;
        let pos = 0;
        if (array != null) {
            array.forEach(element => {
                if (element.target == target && element.callBack == callBack) {
                    has = true;
                }
                if (element.priority > info.priority) {
                    pos++;
                }
            });
        }
        else {
            array = new Array();
            EventNode.m_globalEventDict[type] = array;
        }
        if (has) {
            // console.error("重复注册消息：type=" + type);
            log_1.Log.error("重复注册消息：type=" + type);
        }
        else {
            array.splice(pos, 0, info);
        }
    }
    /**
     * 移除一个消息监听器
     * @param type 消息id
     * @param callBack 回调函数
     * @param target 作用的对象
     */
    static removeGlobalListener(type, callBack, target) {
        type = type.toString();
        let info = null;
        let array = EventNode.m_globalEventDict[type];
        if (array != null) {
            let infoIndex = -1;
            array.every((value, index, array) => {
                if (value.target == target && value.callBack == callBack) {
                    infoIndex = index;
                    info = value;
                    return false;
                }
                return true;
            });
            if (infoIndex != -1) {
                array.splice(infoIndex, 1);
            }
        }
    }
    /**
     * 是否存在这个监听消息
     * @param type 消息类型
     * @param callBack 回调类型
     * @param target 回调对象
     */
    static hasGlobalListener(type, callBack, target) {
        let flag = false;
        let array = EventNode.m_globalEventDict[type];
        if (array) {
            // @ts-ignore
            let index = array.findIndex((obj, index, any) => {
                return obj.target == target && obj.callBack == callBack;
            });
            flag = index != -1;
        }
        return flag;
    }
    /**
     * 派发消息
     * @param ed 派发的消息内容
     */
    static dispatchGlobal(ed) {
        EventNode._dispatchGlobal(ed);
    }
    /**
     * 派发消息
     * @param cmd 消息id
     * @param data 消息内容
     */
    static dispatchGlobalByCmd(cmd, data = null) {
        let ed = EventNode.createGlobalData(cmd, data);
        EventNode._dispatchGlobal(ed);
        if (ed != null) {
            EventNode.returnGlobalEventData(ed);
        }
    }
    static _dispatchGlobal(ed) {
        let array = EventNode.m_globalEventDict[ed.cmd];
        if (array != null) {
            for (let i = 0; i < array.length; i++) {
                let info = array[i];
                if (info.callBack != null) {
                    info.callBack.call(info.target, ed);
                }
                if (info.once) {
                    array.splice(i--, 1);
                }
                if (ed.isStop) {
                    break;
                }
            }
        }
    }
    createEventData(cmd, data) {
        let ed;
        if (this.m_eventData.length > 0) {
            ed = this.m_eventData.pop();
            ed.cmd = cmd;
            ed.data = data;
            ed.isStop = false;
        }
        else {
            ed = new event_data_1.EventData(cmd, data);
        }
        return ed;
    }
    returnEventData(ed) {
        ed.data = null;
        ed.cmd = null;
        ed.isStop = false;
        this.m_eventData.push(ed);
    }
    /**
     * 添加一个消息监听器
     * @param type 消息类型
     * @param callBack 回调函数
     * @param target 作用对象
     * @param priority 消息的优先级
     * @param once 是否只监听一次
     */
    addEventListener(type, callBack, target, priority = 0, once = false) {
        type = type.toString();
        let info = {
            type: type,
            callBack: callBack,
            target: target,
            priority: priority,
            once: once
        };
        let array = this.m_eventDict[type];
        let has = false;
        let pos = 0;
        if (array != null) {
            array.forEach(element => {
                if (element.target == target && element.callBack == callBack) {
                    has = true;
                }
                if (element.priority > info.priority) {
                    pos++;
                }
            });
        }
        else {
            array = new Array();
            this.m_eventDict[type] = array;
        }
        if (has) {
            // console.error("重复注册消息：type=" + type);
            log_1.Log.error("重复注册消息：type=" + type);
            return null;
        }
        else {
            array.splice(pos, 0, info);
            return info;
        }
    }
    /**
     * 移除一个消息监听器
     * @param type 消息id
     * @param callBack 回调函数
     * @param target 作用的对象
     */
    removeEventListener(type, callBack, target) {
        type = type.toString();
        let info = null;
        let array = this.m_eventDict[type];
        if (array != null) {
            let infoIndex = -1;
            array.every((value, index, array) => {
                if (value.target == target && value.callBack == callBack) {
                    infoIndex = index;
                    info = value;
                    return false;
                }
                return true;
            });
            if (infoIndex != -1) {
                array.splice(infoIndex, 1);
            }
        }
    }
    removeEventListenerAll() {
        this.m_eventData = new Array();
        this.m_eventDict = {};
    }
    /**
     * 是否存在这个监听消息
     * @param type 消息类型
     * @param callBack 回调类型
     * @param target 回调对象
     */
    hasEventListener(type, callBack, target) {
        let flag = false;
        let array = this.m_eventDict[type];
        if (array) {
            // @ts-ignore
            let index = array.findIndex((obj, index, any) => {
                return obj.target == target && obj.callBack == callBack;
            });
            flag = index != -1;
        }
        return flag;
    }
    /**
     * 派发消息
     * @param ed 派发的消息内容
     */
    dispatchEvent(ed) {
        this._dispatchEvent(ed);
    }
    /**
     * 派发消息
     * @param cmd 消息id
     * @param data 消息内容
     */
    dispatchEventByCmd(cmd, data = null) {
        let ed = this.createEventData(cmd, data);
        this._dispatchEvent(ed);
        if (ed != null) {
            this.returnEventData(ed);
        }
    }
    _dispatchEvent(ed) {
        let array = this.m_eventDict[ed.cmd];
        if (array != null) {
            for (let i = 0; i < array.length; i++) {
                let info = array[i];
                if (info.callBack != null) {
                    info.callBack.call(info.target, ed);
                }
                if (info.once) {
                    array.splice(i--, 1);
                }
                if (ed.isStop) {
                    break;
                }
            }
        }
    }
}
exports.EventNode = EventNode;
// ==================================================
// ==============  Local Event Section ==============
// ==================================================
EventNode.m_globalEventData = new Array();
EventNode.m_globalEventDict = {};
class EventContext {
}
exports.EventContext = EventContext;
EventContext.eventNodes = new Map();
},{"../../core/log":10,"../../core/singleton":12,"./event-data":15}],18:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JsonManager = void 0;
const res_manager_1 = require("../res/res-manager");
const singleton_1 = require("../../core/singleton");
const dictionary_1 = require("../../structure/dictionary");
const config_1 = require("../../setting/config");
const log_1 = require("../../core/log");
/**
* @author Sun
* @time 2019-08-12 14:40
* @project SFramework_LayaAir
* @description 配置表管理
*
*/
class JsonManager extends singleton_1.Singleton {
    constructor() {
        super();
        /**
         * 存放所有配置表模板
         */
        this.m_DicTemplate = null;
        /**
         * 存放所有解析过的配置表
         */
        this.m_DicData = null;
    }
    static get $() {
        if (!this.instance)
            this.instance = new JsonManager();
        return this.instance;
    }
    /**
     * 管理器统一设置方法
     */
    setup() {
        this.m_DicTemplate = new dictionary_1.Dictionary();
        this.m_DicData = new dictionary_1.Dictionary();
        this.load(config_1.ConfigData.$.jsonTemplateList);
    }
    update() {
    }
    /**
     * 管理器统一销毁方法
     */
    destroy() {
        this.unloadAll();
        if (this.m_DicTemplate) {
            this.m_DicTemplate.clear();
            this.m_DicTemplate = null;
        }
        if (this.m_DicData) {
            this.m_DicData.clear();
            this.m_DicData = null;
        }
    }
    /**
    * 加载所有的数据模板
    * @param list
    */
    load(list) {
        for (let i = 0; i < list.length; ++i) {
            log_1.Log.log("[load]加载配置表:" + list[i].url);
            this.m_DicTemplate.add(list[i].name, list[i]);
        }
    }
    /**
     * 获取一个单一结构的数据
     * @param name
     */
    getTable(name) {
        let data = this.m_DicData.value(name);
        if (data == null) {
            data = res_manager_1.ResManager.$.getRes(this.m_DicTemplate.value(name).url);
            this.m_DicData.add(name, data);
        }
        return data;
    }
    /**
     * 获取一行复合表的数据
     * @param name
     * @param key
     */
    getTableRow(name, key) {
        return this.getTable(name)[key];
    }
    /**
     * 卸载指定的模板
     * @param url
     */
    unload(name) {
        let template = this.m_DicTemplate.value(name);
        if (template) {
            this.m_DicData.remove(name);
        }
        res_manager_1.ResManager.$.releaseUrl(template.url);
        this.m_DicTemplate.remove(name);
    }
    /**
     * 卸载所有的模板
     * @constructor
     */
    unloadAll() {
        if (!this.m_DicTemplate)
            return;
        this.m_DicTemplate.foreach(function (key, value) {
            this.unload(key);
            return true;
        });
        this.m_DicData.clear();
        this.m_DicTemplate.clear();
    }
}
exports.JsonManager = JsonManager;
JsonManager.instance = null;
},{"../../core/log":10,"../../core/singleton":12,"../../setting/config":31,"../../structure/dictionary":33,"../res/res-manager":22}],19:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JsonTemplate = void 0;
/**
* @author Sun
* @time 2019-08-12 10:59
* @project SFramework_LayaAir
* @description 配置表模板项
*
*/
class JsonTemplate {
    constructor(url, name) {
        this.url = url;
        this.name = name;
    }
}
exports.JsonTemplate = JsonTemplate;
},{}],20:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResGroup = void 0;
const res_item_1 = require("./res-item");
/**
* @author Sun
* @time 2019-08-09 19:31
* @project SFramework_LayaAir
* @description 场景管理器所需的资源包定义
*
*/
class ResGroup {
    constructor() {
        /**加载进度 */
        this.progress = 0;
        /**加载资源 */
        this.needLoad = new Array();
    }
    /**
     * 向资源组添加目标
     * @param url 相对路径
     * @param type 类型
     * @param isKeepMemory 是否常驻内存
     */
    add(url, type, isKeepMemory = false) {
        let index = this.needLoad.findIndex((value, index, obj) => {
            return value.Url == url;
        });
        if (index == -1) {
            let info = new res_item_1.ResItem(url, type, isKeepMemory);
            this.needLoad.push(info);
        }
        return this;
    }
}
exports.ResGroup = ResGroup;
},{"./res-item":21}],21:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResItem = void 0;
/**
 * @author Sun
 * @time 2019-08-09 19:18
 * @project SFramework_LayaAir
 * @description 资源属性
 *
 */
class ResItem {
    constructor(url, type, iskeepMemory) {
        this.isKeepMemory = false;
        this.url = url;
        this.type = type;
        this.isKeepMemory = iskeepMemory;
    }
    get Url() {
        return this.url;
    }
    get Type() {
        return this.type;
    }
    get IsKeepMemory() {
        return this.isKeepMemory;
    }
}
exports.ResItem = ResItem;
},{}],22:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResManager = void 0;
var Handler = Laya.Handler;
const event_node_1 = require("../event/event-node");
const log_1 = require("../../core/log");
/**
 * @author Sun
 * @time 2019-08-12 13:33
 * @project SFramework_LayaAir
 * @description  资源管理  （所有资源均通过ResGroup的形式来加载）
 *
 */
class ResManager extends event_node_1.EventNode {
    constructor() {
        super();
        //存放所有已加载的资源
        this.m_dictResItem = new Map();
    }
    static get $() {
        if (this.instance == null)
            this.instance = new ResManager();
        return this.instance;
    }
    setup() {
    }
    update() {
    }
    destroy() {
    }
    /**
     * 通过URL获取资源
     * @param url
     */
    getRes(url) {
        return Laya.loader.getRes(url);
    }
    /**
     * 加载单个资源
     * @param resItem 资源信息
     * @param progressFuc 加载进度回调
     * @param completeFuc 加载完成回调
     */
    loadRes(resItem, progressFuc, completeFuc) {
        Laya.loader.load(resItem.Url, Handler.create(this, (success) => {
            if (success) {
                //完成回调
                if (completeFuc != null)
                    completeFuc.invoke();
                //标记资源
                if (!this.m_dictResItem.has(resItem.Url)) {
                    this.m_dictResItem.set(resItem.Url, resItem);
                }
            }
            else {
                log_1.Log.error("Load Resource Error：");
                log_1.Log.debug(resItem.Url);
            }
        }), Handler.create(this, (progress) => {
            //进度回调
            if (progressFuc != null)
                progressFuc.invoke(progress);
        }, null, false));
    }
    /**
     * 加载资源组
     * @param loads 资源信息
     * @param progressFuc 加载进度回调
     * @param completeFuc 加载完成回调
     */
    loadGroup(loads, progressFuc, completeFuc) {
        let urls = new Array();
        loads.needLoad.forEach(element => {
            urls.push({ url: element.Url, type: element.Type });
        });
        Laya.loader.load(urls, Handler.create(this, (success) => {
            if (success) {
                //完成回调
                if (completeFuc != null)
                    completeFuc.invoke();
                //标记资源
                for (let index = 0; index < loads.needLoad.length; index++) {
                    let info = loads.needLoad[index];
                    if (!this.m_dictResItem.has(info.Url)) {
                        this.m_dictResItem.set(info.Url, info);
                    }
                }
            }
            else {
                log_1.Log.error("Load Resource Error：");
                log_1.Log.debug(urls);
            }
        }), Handler.create(this, (progress) => {
            //进度回调
            if (progressFuc != null)
                progressFuc.invoke(progress);
        }, null, false));
    }
    /**
     * 加载预设物
     * @param filePath
     * @param complete
     */
    loadPrefab(filePath, complete) {
        Laya.loader.load(filePath, Laya.Handler.create(this, function (pre) {
            var playPre = new Laya.Prefab();
            playPre.json = pre;
            let cell = Laya.Pool.getItemByCreateFun("Cell", playPre.create, playPre);
            if (complete)
                complete.invoke(cell);
        }));
    }
    /**
     * 释放资源组
     * @param loads 资源组
     */
    releaseGroup(loads) {
        let urls = new Array();
        loads.needLoad.forEach(element => {
            urls.push(element.Url);
        });
        for (let i = 0; i < urls.length; i++) {
            Laya.loader.clearRes(urls[i]);
            this.m_dictResItem.forEach((v, key) => {
                if (key == urls[i]) {
                    this.m_dictResItem.delete(key);
                }
            });
        }
    }
    /**
     * 释放指定资源
     * @param url
     */
    releaseUrl(url) {
        let isActive = false;
        this.m_dictResItem.forEach((v, key) => {
            if (key == url) {
                isActive = true;
            }
        });
        if (isActive) {
            Laya.loader.clearRes(url);
        }
        else {
            log_1.Log.error("加载资源组内不存在该资源");
        }
    }
}
exports.ResManager = ResManager;
ResManager.instance = null;
},{"../../core/log":10,"../event/event-node":17}],23:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SoundManager = void 0;
const string_1 = require("../../util/string");
var SoundChannel = Laya.SoundChannel;
var Handler = Laya.Handler;
const event_node_1 = require("../event/event-node");
const log_1 = require("../../core/log");
const dictionary_1 = require("../../structure/dictionary");
const config_1 = require("../../setting/config");
/**
 * @author Sun
 * @time 2019-08-12 15:08
 * @project SFramework_LayaAir
 * @description 音效管理
 *
 */
class SoundManager extends event_node_1.EventNode {
    constructor() {
        /********************************************——**********************************************/
        ////////////////////////////////////////////分界线////////////////////////////////////////////
        /******************************************属性信息*******************************************/
        super(...arguments);
        /** Des:背景音乐 */
        this.m_CurBGSound = null;
        /**音效名字对应Url */
        this.dictSoundUrl = null;
        /********************************************——**********************************************/
        ////////////////////////////////////////////分界线////////////////////////////////////////////
    }
    static get $() {
        if (!this.instance)
            this.instance = new SoundManager();
        return this.instance;
    }
    setup() {
        this.m_CurBGSound = new SoundChannel();
        this.dictSoundUrl = new dictionary_1.Dictionary();
        config_1.ConfigSound.$.soundResList.forEach(item => {
            this.dictSoundUrl.add(item.name, item.url);
        });
        if (!string_1.UtilString.isEmpty(config_1.ConfigSound.$.bgSoundName)) {
            this.playBGSound(config_1.ConfigSound.$.bgSoundName, 0);
            this.setAllVolume(config_1.ConfigSound.$.volumeVoiceSound);
        }
    }
    update() {
    }
    destroy() {
    }
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /****************************************设置整体音量*****************************************/
    /**
     * 设置整体音量
     * @param number
     */
    setAllVolume(number) {
        config_1.ConfigSound.$.volumeVoiceSound = number;
        this.m_CurBGSound.volume = number;
    }
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /*****************************************控制背景音乐*****************************************/
    /**
     * 播放背景声音
     * @param    file_name    资源名字
     * @param    count        播放次数(0为循环)
     */
    playBGSound(file_name, count) {
        if (string_1.UtilString.isEmpty(file_name)) {
            log_1.Log.error("Sound file error!");
            return;
        }
        this.m_CurBGSound = Laya.SoundManager.playMusic(this.dictSoundUrl.value(file_name), count);
    }
    /**
     * 停止背景音播放
     */
    stopBGSound() {
        if (this.m_CurBGSound) {
            this.m_CurBGSound.stop();
        }
    }
    /**
     * 暂停背景音乐
     */
    pauseBGSound() {
        if (this.m_CurBGSound) {
            this.m_CurBGSound.pause();
        }
    }
    /**
     * 恢复背景音乐播放
     */
    resumeBGSound() {
        if (this.m_CurBGSound) {
            this.m_CurBGSound.resume();
        }
    }
    /**
     * 设置背景音量
     * @param volume
     */
    setBGSoundVolume(volume) {
        if (this.m_CurBGSound) {
            this.m_CurBGSound.volume = volume;
        }
    }
    /********************************************——**********************************************/
    ////////////////////////////////////////////分界线////////////////////////////////////////////
    /*****************************************控制音效播放*****************************************/
    /**
     * 播放效果声音
     * @param    file_name    资源
     * @param    count        播放次数
     */
    playSoundEffect(file_name, count) {
        if (string_1.UtilString.isEmpty(file_name)) {
            log_1.Log.error("声音文件错误");
            return null;
        }
        let sound = Laya.Pool.getItemByClass("Sound", SoundChannel);
        sound = Laya.SoundManager.playSound(this.dictSoundUrl.value(file_name), count, Handler.create(this, () => {
            Laya.Pool.recover("Sound", sound);
        }));
        sound.volume = config_1.ConfigSound.$.volumeVoiceSound;
        return sound;
    }
    /**
     * 停止播放
     * @param sound
     */
    stopSoundEffect(sound) {
        if (sound) {
            sound.stop();
        }
    }
}
exports.SoundManager = SoundManager;
/********************************************——**********************************************/
////////////////////////////////////////////分界线////////////////////////////////////////////
/******************************************生命周期*******************************************/
SoundManager.instance = null;
},{"../../core/log":10,"../../setting/config":31,"../../structure/dictionary":33,"../../util/string":41,"../event/event-node":17}],24:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimerEntity = void 0;
const time_1 = require("../../util/time");
const timer_interval_1 = require("./timer-interval");
/**
 * @author Sun
 * @time 2019-08-10 20:06
 * @project SFramework_LayaAir
 * @description  计时器基类
 *
 */
class TimerEntity {
    constructor() {
        this.mTime = new timer_interval_1.TimerInterval();
    }
    init() {
    }
    close() {
    }
    clear() {
        if (this.handle != null) {
            this.handle.recover();
            this.handle = null;
        }
    }
    set(id, rate, ticks, handle) {
        this.id = id;
        this.mRate = rate < 0 ? 0 : rate;
        this.mTicks = ticks < 0 ? 0 : ticks;
        this.handle = handle;
        this.mTicksElapsed = 0;
        this.isActive = true;
        this.mTime.init(this.mRate, false);
    }
    update(removeTimer) {
        if (this.isActive && this.mTime.update(time_1.UtilTime.deltaTime)) {
            if (this.handle != null)
                this.handle.run();
            this.mTicksElapsed++;
            if (this.mTicks > 0 && this.mTicks == this.mTicksElapsed) {
                this.isActive = false;
                removeTimer(this.id);
            }
        }
    }
}
exports.TimerEntity = TimerEntity;
},{"../../util/time":42,"./timer-interval":25}],25:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimerInterval = void 0;
/**
 * @author Sun
 * @time 2019-08-10 20:02
 * @project SFramework_LayaAir
 * @description  定时执行
 *
 */
class TimerInterval {
    constructor() {
        this.m_now_time = 0;
    }
    /**
     * 初始化定时器
     * @param    interval    触发间隔
     * @param    first_frame    是否第一帧开始执行
     */
    init(interval, first_frame) {
        this.m_interval_time = interval;
        if (first_frame)
            this.m_now_time = this.m_interval_time;
    }
    reset() {
        this.m_now_time = 0;
    }
    update(elapse_time) {
        this.m_now_time += elapse_time;
        if (this.m_now_time >= this.m_interval_time) {
            this.m_now_time -= this.m_interval_time;
            return true;
        }
        return false;
    }
}
exports.TimerInterval = TimerInterval;
},{}],26:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimerManager = void 0;
var Handler = Laya.Handler;
const array_1 = require("../../util/array");
const event_node_1 = require("../event/event-node");
const time_delay_1 = require("../../core/time-delay");
const object_pool_1 = require("../../core/object-pool");
const timer_entity_1 = require("./timer-entity");
/**
 * @author Sun
 * @time 2019-08-09 23:22
 * @project SFramework_LayaAir
 * @description  定时管理器
 *
 */
class TimerManager extends event_node_1.EventNode {
    constructor() {
        super(...arguments);
        this.m_idCounter = 0;
        this.m_RemovalPending = [];
        this.m_Timers = [];
    }
    static get $() {
        if (!this.instance)
            this.instance = new TimerManager();
        return this.instance;
    }
    setup() {
        this.m_idCounter = 0;
        time_delay_1.TimeDelay.$.add(0.1, 0, this.remove, this);
        time_delay_1.TimeDelay.$.addUpdate(this.tick, this);
    }
    update() {
    }
    destroy() {
        array_1.UtilArray.clear(this.m_RemovalPending);
        array_1.UtilArray.clear(this.m_Timers);
        time_delay_1.TimeDelay.$.remove(this.remove, this);
        time_delay_1.TimeDelay.$.remove(this.tick, this);
    }
    tick() {
        for (let i = 0; i < this.m_Timers.length; i++) {
            this.m_Timers[i].update(this.removeTimer);
        }
    }
    /**
     * 定时重复执行
     * @param    rate    间隔时间(单位毫秒)。
     * @param    ticks    执行次数
     * @param    caller    执行域(this)。
     * @param    method    定时器回调函数：注意，返回函数第一个参数为定时器id，后面参数依次时传入的参数。例OnTime(timer_id:number, args1:any, args2:any,...):void
     * @param    args    回调参数。
     */
    addLoop(rate, ticks, caller, method, args = null) {
        if (ticks <= 0)
            ticks = 0;
        let newTimer = object_pool_1.ObjectPool.get(timer_entity_1.TimerEntity);
        ++this.m_idCounter;
        if (args != null)
            array_1.UtilArray.insert(args, this.m_idCounter, 0);
        newTimer.set(this.m_idCounter, rate, ticks, Handler.create(caller, method, args, false));
        this.m_Timers.push(newTimer);
        return newTimer.id;
    }
    /**
     * 单次执行
     */
    addOnce(rate, caller, method, args = null) {
        let newTimer = object_pool_1.ObjectPool.get(timer_entity_1.TimerEntity);
        ++this.m_idCounter;
        if (args != null)
            array_1.UtilArray.insert(args, this.m_idCounter, 0);
        newTimer.set(this.m_idCounter, rate, 1, Handler.create(caller, method, args, false));
        this.m_Timers.push(newTimer);
        return newTimer.id;
    }
    /**
     * 移除定时器
     * @param    timerId    定时器id
     */
    removeTimer(timerId) {
        this.m_RemovalPending.push(timerId);
    }
    /**
     * 移除过期定时器
     */
    remove() {
        let timer;
        if (this.m_RemovalPending.length > 0) {
            for (let id of this.m_RemovalPending) {
                for (let i = 0; i < this.m_Timers.length; i++) {
                    timer = this.m_Timers[i];
                    if (timer.id == id) {
                        timer.clear();
                        object_pool_1.ObjectPool.recover(timer);
                        this.m_Timers.splice(i, 1);
                        break;
                    }
                }
            }
            array_1.UtilArray.clear(this.m_RemovalPending);
        }
    }
}
exports.TimerManager = TimerManager;
TimerManager.instance = null;
},{"../../core/object-pool":11,"../../core/time-delay":13,"../../util/array":34,"../event/event-node":17,"./timer-entity":24}],27:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PopupData = exports.CustomDialog = void 0;
var Tween = Laya.Tween;
var Ease = Laya.Ease;
var Handler = Laya.Handler;
const display_1 = require("../../util/display");
var CustomDialog;
(function (CustomDialog) {
    /**
     * @author Sun
     * @time 2019-08-09 17:41
     * @project SFramework_LayaAir
     * @description  UI组件的基类，继承自Laya.View
     *
     */
    class DialogBase extends Laya.Dialog {
        constructor() {
            super();
            /**遮罩层 */
            this.maskLayer = null;
            /**弹窗内物体 */
            this.contentPnl = null;
            /**弹窗数据 */
            this.popupData = new PopupData();
            this.bundleButtons();
            this.contentPnl = this.getChildAt(0);
        }
        createView(view) {
            super.createView(view);
        }
        /**
         * 添加遮罩层
         */
        crateMaskLayer() {
            this.maskLayer = display_1.UtilDisplay.createMaskLayer();
            this.maskLayer.mouseEnabled = true;
            let t = this.maskLayer;
            t.x = Math.round(((Laya.stage.width - t.width) >> 1) + t.pivotX);
            t.y = Math.round(((Laya.stage.height - t.height) >> 1) + t.pivotY);
            this.addChild(this.maskLayer);
            this.maskLayer.zOrder = -1;
        }
        /**
         * 在场景中居中组件
         */
        center(view) {
            if (view == null)
                view = this;
            view.x = Math.round(((Laya.stage.width - view.width) >> 1) + view.pivotX);
            view.y = Math.round(((Laya.stage.height - view.height) >> 1) + view.pivotY);
        }
        /**
         * 添加默认按钮事件
         */
        bundleButtons() {
            if (this["btnClose"] != null) {
                this["btnClose"].on(Laya.Event.CLICK, this, this.close);
            }
        }
        /**
         * 关闭空白处点击关闭事件
         */
        closeOutsieClick() {
            if (this.maskLayer != null) {
                this.maskLayer.off(Laya.Event.CLICK, this, this.close);
            }
        }
        /**
         * 对话框弹出方法
         * @param time 弹出时间
         * @param data 数据
         * @param isMask 是否生成遮罩
         * @param closeOutside 是否点击空白处关闭
         */
        popupDialog(popupData = null) {
            // this.popup(false,false);
            if (popupData == null) {
                popupData = this.popupData;
            }
            else {
                this.popupData = popupData;
            }
            Laya.stage.addChild(this);
            this.popupInit();
            if (popupData.isMask && this.maskLayer == null) {
                this.crateMaskLayer();
                if (!popupData.closeOutside)
                    this.maskLayer.on(Laya.Event.CLICK, this, this.close);
            }
            this.onShowAnimation(popupData.time, () => {
                if (popupData.callBack)
                    popupData.callBack.invoke();
            });
        }
        /** Des:弹出调用 */
        popupInit() {
        }
        onShowAnimation(time = 300, cb) {
            let target = this.contentPnl;
            this.center();
            // @ts-ignore
            target.scale(0, 0);
            Tween.to(target, {
                scaleX: 1,
                scaleY: 1
            }, time, Ease.backOut, Handler.create(this, cb, [this]), 0, false, false);
        }
        close() {
            this.removeSelf();
        }
    }
    CustomDialog.DialogBase = DialogBase;
})(CustomDialog = exports.CustomDialog || (exports.CustomDialog = {}));
/**
 * @author Sun
 * @time 2019-08-12 17:43
 * @project SFramework_LayaAir
 * @description  窗体弹出数据
 *time: number = 300, data: any = null, isMask: boolean = true, closeOutside: boolean = true,cb?
 */
class PopupData {
    constructor(time = 300, data = null, isMask = true, closeOutside = true, cb = null) {
        this.time = 300;
        this.data = null;
        this.isMask = true;
        this.closeOutside = true;
        this.callBack = null;
        if (time != null)
            this.time = time;
        if (data != null)
            this.data = data;
        if (isMask != null)
            this.isMask = isMask;
        if (closeOutside != null)
            this.closeOutside = closeOutside;
        if (cb != null)
            this.callBack = cb;
    }
}
exports.PopupData = PopupData;
},{"../../util/display":36}],28:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomScene = void 0;
const res_group_1 = require("../res/res-group");
const res_manager_1 = require("../res/res-manager");
const log_1 = require("../../core/log");
const timer_manager_1 = require("../timer/timer-manager");
var CustomScene;
(function (CustomScene) {
    /**
     * @author Sun
     * @time 2019-08-09 19:12
     * @project SFramework_LayaAir
     * @description  Scene的基类
     *
     */
    class LyScene extends Laya.Scene {
        constructor() {
            super();
            /**
             * 场景第一个加载的窗口
             */
            this.firstView = null;
            this.m_loaded = false;
            this.sceneTimers = new Array();
            this.needLoadRes = new res_group_1.ResGroup();
        }
        createChildren() {
            super.createChildren();
            this.createView(LyScene.uiView);
            this.width = Laya.stage.width;
            this.height = Laya.stage.height;
        }
        /**
         * 进入场景
         * @param param 参数
         * @param progressFuc 进度回调
         * @param completeFuc 完成回调
         */
        enter(param, progressFuc, completeFuc) {
            this.m_loaded = false;
            this.m_param = param;
            this.onInit(param);
            res_manager_1.ResManager.$.loadGroup(this.needLoadRes, progressFuc, completeFuc);
        }
        leave() {
            this.onLeave();
            this.destroy();
        }
        destroy() {
            this.onClean();
            this.sceneTimers.forEach((timer) => {
                timer_manager_1.TimerManager.$.removeTimer(timer);
            });
            super.destroy();
        }
        /**
         * 加载完成
         * @param error 加载错误
         */
        loaded(error) {
            if (error != null) {
                log_1.Log.error(error);
            }
            else {
                this.onLoaded();
                this.m_loaded = true;
                this.chechEnter();
            }
        }
        chechEnter() {
            if (this.m_loaded) {
                if (this.firstView != null) {
                    let cls = this.firstView;
                    let win = new cls();
                    this.addChild(win);
                }
                this.onEnter(this.m_param);
            }
        }
        /**
         * 加载完成
         */
        onLoaded() {
        }
        /**
         * 场景初始化
         * @param param 参数
         */
        onInit(param) {
        }
        /**
         * 进入场景
         */
        onEnter(param) {
        }
        /**
         * 逐帧循环
         */
        update() {
        }
        /**
         * 离开场景
         */
        onLeave() {
        }
        /**
         * 当场景被销毁的时候
         */
        onClean() {
        }
    }
    /**
     * 内嵌模式空的场景资源，必须实现这个createView，否则有问题
     */
    LyScene.uiView = { "type": "Scene", "props": { "width": 1334, "height": 750 }, "loadList": [], "loadList3D": [] };
    CustomScene.LyScene = LyScene;
})(CustomScene = exports.CustomScene || (exports.CustomScene = {}));
},{"../../core/log":10,"../res/res-group":20,"../res/res-manager":22,"../timer/timer-manager":26}],29:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomView = void 0;
const data_manager_1 = require("../data/data-manager");
var CustomView;
(function (CustomView) {
    /**
     * @author Sun
     * @time 2019-08-09 15:51
     * @project SFramework_LayaAir
     * @description  UI组件的基类，继承自Laya.View
     *
     */
    class ViewBase extends Laya.View {
        constructor() {
            super(...arguments);
            /*所有数据观察者*/
            this.dataWatchs = [];
            this.data = null;
        }
        //override
        createView(view) {
            super.createView(view);
            this.fullScreen();
            this.parseElement();
        }
        onDisable() {
            this.dataWatchs.forEach((cmd) => {
                data_manager_1.DataManager.$.removeEventListener(cmd, this.onData, this);
            });
        }
        /**
         * 背景图适应
         */
        parseElement() {
            if (this["imgBg"] != null) {
                let imgBg = this["imgBg"];
                this.fullScreen(imgBg);
            }
        }
        /**
         * 在场景中居中组件
         */
        center(view) {
            if (view == null)
                view = this;
            view.x = Math.round(((Laya.stage.width - view.width) >> 1) + view.pivotX);
            view.y = Math.round(((Laya.stage.height - view.height) >> 1) + view.pivotY);
        }
        /**
         * 设置大小为全屏
         * @param view Laya.Sprite
         */
        fullScreen(view) {
            if (view == null)
                view = this;
            view.width = Laya.stage.width;
            view.height = Laya.stage.height;
        }
        /**
         * 绑定数据监听
         * @param cmd 监听类型
         */
        addDataWatch(cmd) {
            this.dataWatchs.push(cmd);
            data_manager_1.DataManager.$.addEventListener(cmd, this.onData, this);
            data_manager_1.DataManager.$.get(cmd).notify();
        }
        /**
         * 当数据刷新是重绘
         */
        onData(data) {
            // if (data.cmd == DataDefine.CoinInfo){
            //
            // }
        }
        /**
         * 添加到画布
         * @param data 数据
         */
        add(data = null) {
            this.data = data;
            Laya.stage.addChild(this);
            this.show();
        }
        /**
         * 显示view
         */
        show() {
            this.visible = true;
        }
        /**
         * 隐藏view
         */
        hide() {
            this.visible = false;
        }
    }
    CustomView.ViewBase = ViewBase;
})(CustomView = exports.CustomView || (exports.CustomView = {}));
},{"../data/data-manager":14}],30:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Engine = void 0;
const config_1 = require("../setting/config");
const log_1 = require("../core/log");
const time_1 = require("../util/time");
const enum_1 = require("../setting/enum");
var Browser = Laya.Browser;
const res_manager_1 = require("../manager/res/res-manager");
const event_data_1 = require("../manager/event/event-data");
const data_manager_1 = require("../manager/data/data-manager");
const event_manager_1 = require("../manager/event/event-manager");
const json_manager_1 = require("../manager/json/json-manager");
const sound_manager_1 = require("../manager/sound/sound-manager");
const timer_manager_1 = require("../manager/timer/timer-manager");
const gameSetting_1 = require("../../client/setting/gameSetting");
/**
 * @author Sun
 * @time 2019-08-11 18:08
 * @project SFramework_LayaAir
 * @description 框架初始化和游戏入口
 *
 */
class Engine {
    constructor() {
        this.layout = config_1.ConfigLayout.$;
        this.game = config_1.ConfigGame.$;
        this.ui = config_1.ConfigUI.$;
        this.debug = config_1.ConfigDebug.$;
    }
    static get $() {
        if (this.instance == null)
            this.instance = new Engine();
        return this.instance;
    }
    /**
     * 引擎启动入口
     */
    run() {
        log_1.Log.info("::: Game Engine Run :::");
        gameSetting_1.GameSetting.$.init();
        if (config_1.ConfigUI.$.defaultLoadView != null && config_1.ConfigRes.$.defaultLoadRes != null) {
            this.engineSetup(() => {
                //游戏开始
                time_1.UtilTime.start();
                //初始化游戏管理器
                this.managerSetUp();
                //初始化游戏主循环
                Laya.timer.frameLoop(1, this, this.managerUpdate);
                //加载Loading页的默认资源并显示Loading页
                res_manager_1.ResManager.$.loadGroup(config_1.ConfigRes.$.defaultLoadRes, null, new event_data_1.EventFunc(this, () => {
                    let scrpt = config_1.ConfigUI.$.defaultLoadView;
                    if (scrpt != undefined) {
                        let loadingView = new scrpt();
                        Laya.stage.addChild(loadingView);
                        loadingView.onStart();
                    }
                }));
            });
        }
        else {
            log_1.Log.error("Error:Loading资源为空加载失败！");
        }
    }
    /**
     * 引擎的初始化设置
     */
    engineSetup(startCallback) {
        /**初始化Laya */
        if (this.game.dimension == enum_1.enumDimension.Dim3) {
            Laya3D.init(config_1.ConfigLayout.$.designWidth, config_1.ConfigLayout.$.designHeight);
        }
        else {
            Laya.init(config_1.ConfigLayout.$.designWidth, config_1.ConfigLayout.$.designHeight, Laya.WebGL);
        }
        /**背景颜色 */
        Laya.stage.bgColor = "none";
        /**缩放模式 */
        Laya.stage.scaleMode = enum_1.enumScaleType.ScaleShowAll.toString();
        /**设置屏幕大小 */
        Laya.stage.setScreenSize(Browser.clientWidth, Browser.clientHeight);
        /**设置横竖屏 */
        Laya.stage.screenMode = enum_1.enumScreenModel.ScreenNone;
        /**水平对齐方式 */
        Laya.stage.alignH = enum_1.enumAlige.AligeCenter;
        /**垂直对齐方式 */
        Laya.stage.alignV = enum_1.enumAlige.AligeMiddle;
        /**开启物理引擎 */
        if (config_1.ConfigGame.$.physics)
            Laya["Physics"] && Laya["Physics"].enable();
        /**打开调试面板（通过IDE设置调试模式，或者url地址增加debug=true参数，均可打开调试面板） */
        if (config_1.ConfigDebug.$.isEnableDebugPanel || Laya.Utils.getQueryString("debug") == "true")
            Laya.enableDebugPanel();
        /**物理辅助线 */
        if (config_1.ConfigDebug.$.isPhysicsDebug && Laya["PhysicsDebugDraw"])
            Laya["PhysicsDebugDraw"].enable();
        /**性能同级面板 */
        if (config_1.ConfigDebug.$.isStat)
            Laya.Stat.show(config_1.ConfigDebug.$.panelX, config_1.ConfigDebug.$.panelY);
        /**微信开放域子域设置*/
        if (Browser.onWeiXin || Browser.onMiniGame) {
            Laya.MiniAdpter.init();
            Laya.isWXOpenDataContext = false;
        }
        /**模式窗口点击边缘 */
        UIConfig.closeDialogOnSide = true;
        /**是否显示滚动条按钮 */
        UIConfig.showButtons = true;
        /**按钮的点击效果 */
        UIConfig.singleButtonStyle = "scale"; //"color","scale"
        /**弹出框背景透明度 */
        UIConfig.popupBgAlpha = 0.75;
        /**兼容Scene后缀场景 */
        Laya.URL.exportSceneToJson = true;
        /**是否开启版本管理 */
        if (config_1.ConfigVersion.$.isOpenVersion) {
            Laya.ResourceVersion.enable(config_1.ConfigVersion.$.versionFloder, Laya.Handler.create(this, startCallback), Laya.ResourceVersion.FILENAME_VERSION);
        }
        else {
            startCallback.call();
        }
    }
    /**
     * 管理器的初始化
     */
    managerSetUp() {
        data_manager_1.DataManager.$.setup();
        event_manager_1.EventManager.$.setup();
        res_manager_1.ResManager.$.setup();
        json_manager_1.JsonManager.$.setup();
        sound_manager_1.SoundManager.$.setup();
        timer_manager_1.TimerManager.$.setup();
    }
    /**
     * 管理器的Update
     */
    managerUpdate() {
    }
}
exports.Engine = Engine;
Engine.instance = null;
},{"../../client/setting/gameSetting":3,"../core/log":10,"../manager/data/data-manager":14,"../manager/event/event-data":15,"../manager/event/event-manager":16,"../manager/json/json-manager":18,"../manager/res/res-manager":22,"../manager/sound/sound-manager":23,"../manager/timer/timer-manager":26,"../setting/config":31,"../setting/enum":32,"../util/time":42}],31:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Config3D = exports.ConfigDebug = exports.ConfigLayout = exports.ConfigVersion = exports.ConfigGame = exports.ConfigData = exports.ConfigSound = exports.ConfigRes = exports.ConfigUI = void 0;
const enum_1 = require("./enum");
const singleton_1 = require("../core/singleton");
const main_scene_1 = require("../../client/scene/main-scene");
const res_group_1 = require("../manager/res/res-group");
const loading_view_1 = require("../../client/view/layer-view/loading-view");
/**
* @author Sun
* @time 2019-08-09 14:01
* @project SFramework_LayaAir
* @description 游戏配置信息
*/
/**
 * 界面配置
 */
class ConfigUI extends singleton_1.Singleton {
    constructor() {
        super(...arguments);
        /**默认字体 */
        this.defaultFontName = '黑体';
        /**默认字体大小 */
        this.defaultFontSize = 16;
        /**默认加载场景 */
        this.defaultMainScene = main_scene_1.MainScene;
        /**默认加载的Loading页面 */
        this.defaultLoadView = loading_view_1.LoadingView;
    }
    static get $() {
        if (!this.instance)
            this.instance = new ConfigUI();
        return this.instance;
    }
}
exports.ConfigUI = ConfigUI;
ConfigUI.instance = null;
/**
 * 资源配置
 */
class ConfigRes extends singleton_1.Singleton {
    constructor() {
        super();
        /**默认Loading页面的资源信息 */
        this.defaultLoadRes = new res_group_1.ResGroup();
        /**默认的基础页面资源信息 */
        this.defaultMainRes = new res_group_1.ResGroup();
        //加载Json配置文件
        ConfigData.$.jsonTemplateList.forEach(item => {
            this.defaultMainRes
                .add(item.url, Laya.Loader.JSON);
        });
        //加载音效资源
        ConfigSound.$.soundResList.forEach(item => {
            this.defaultMainRes
                .add(item.url, Laya.Loader.SOUND);
        });
    }
    static get $() {
        if (!this.instance)
            this.instance = new ConfigRes();
        return this.instance;
    }
}
exports.ConfigRes = ConfigRes;
ConfigRes.instance = null;
/**
 * 声音配置
 */
class ConfigSound extends singleton_1.Singleton {
    constructor() {
        super();
        /**背景音乐名字 */
        this.bgSoundName = "";
        /**背景音开关 */
        this.isCloseBGSound = false;
        /**效果音开关 */
        this.isCloseEffectSound = false;
        /**所有音效开关 */
        this.isCloseVoiceSound = false;
        /**总音量 */
        this.volumeVoiceSound = 1;
        /**音效资源 */
        this.soundResList = null;
        this.soundResList = new Array();
        // this.soundResList.push(new SoundTemplate("res/sound/bg.mp3",enumSoundName.bg));
    }
    static get $() {
        if (!this.instance)
            this.instance = new ConfigSound();
        return this.instance;
    }
}
exports.ConfigSound = ConfigSound;
ConfigSound.instance = null;
/**
 * 数据表配置
 */
class ConfigData extends singleton_1.Singleton {
    constructor() {
        super();
        /**json配置表信息 */
        this.jsonTemplateList = new Array();
    }
    static get $() {
        if (!this.instance)
            this.instance = new ConfigData();
        return this.instance;
    }
}
exports.ConfigData = ConfigData;
ConfigData.instance = null;
/**
 * 游戏配置
 */
class ConfigGame extends singleton_1.Singleton {
    constructor() {
        super(...arguments);
        /**默认模式信息 2D/3D */
        this.dimension = enum_1.enumDimension.Dim2;
        /**物理开关 */
        this.physics = false;
    }
    static get $() {
        if (!this.instance)
            this.instance = new ConfigGame();
        return this.instance;
    }
}
exports.ConfigGame = ConfigGame;
ConfigGame.instance = null;
/**
 * 版本配置
 */
class ConfigVersion extends singleton_1.Singleton {
    constructor() {
        super(...arguments);
        /**版本控制开关 */
        this.isOpenVersion = false;
        /**版本号 */
        this.versionNum = 0;
        /**版本控制文件名 */
        this.versionFloder = "Version" + this.versionNum;
    }
    static get $() {
        if (!this.instance)
            this.instance = new ConfigVersion();
        return this.instance;
    }
}
exports.ConfigVersion = ConfigVersion;
ConfigVersion.instance = null;
/**
 * 布局配置
 */
class ConfigLayout extends singleton_1.Singleton {
    constructor() {
        super(...arguments);
        /**设计分辨率X */
        this.designWidth = 750;
        /**设计分辨率Y */
        this.designHeight = 1334;
        /**缩放模式 */
        this.scaleMode = enum_1.enumScaleType.ScaleFixedAuto;
    }
    static get $() {
        if (!this.instance)
            this.instance = new ConfigLayout();
        return this.instance;
    }
}
exports.ConfigLayout = ConfigLayout;
ConfigLayout.instance = null;
/**
 * Debug配置
 */
class ConfigDebug extends singleton_1.Singleton {
    constructor() {
        super(...arguments);
        /**调试信息开关 */
        this.isDebug = true;
        /**物理辅助线开关 */
        this.isPhysicsDebug = false;
        /**调试面板 */
        this.isEnableDebugPanel = false;
        /**性能面板开关 */
        this.isStat = true;
        /**性能统计面板X */
        this.panelX = 0;
        /**性能统计面板Y */
        this.panelY = 100;
    }
    static get $() {
        if (!this.instance)
            this.instance = new ConfigDebug();
        return this.instance;
    }
}
exports.ConfigDebug = ConfigDebug;
ConfigDebug.instance = null;
/**
 * 3D配置
 */
class Config3D extends singleton_1.Singleton {
    constructor() {
        super(...arguments);
        /**场景资源路径 */
        this.scenePath = "res/u3d/LayaScene_Main/Conventional/Main.ls";
    }
    static get $() {
        if (!this.instance)
            this.instance = new Config3D();
        return this.instance;
    }
}
exports.Config3D = Config3D;
Config3D.instance = null;
// /**
//  * Network配置
//  */
// export class ConfigNet extends Singleton {
//     public httpUrl: string = "http://127.0.0.1:34568";
//     public wsUrl: string = "wss://wx.donopo.com/ws/ws";
//     public resUrl: string = "ws://127.0.0.1:16669";
//     public timeOut: number = 10;
//     public heartBeat: number = 10;
//     public serverHeartBeat: number = 3;
//     private static instance: ConfigNet = null;
//     public static get $():ConfigNet {
//         if (!this.instance) this.instance = new ConfigNet();
//         return this.instance;
//     }
// }
// /**
//  * 微信配置
//  */
// export class ConfWechat extends Singleton {
//     public appid: string = "";
//     public secret: string = "";
//     public adUnitId: string = "";
//     public code2sessionUrl = "https://api.weixin.qq.com/sns/jscode2session?appid={0}&secret={1}&js_code={2}&grant_type=authorization_code";
//     private static instance: ConfWechat = null;
//     public static get $():ConfWechat {
//         if (!this.instance) this.instance = new ConfWechat();
//         return this.instance;
//     }
// }
},{"../../client/scene/main-scene":2,"../../client/view/layer-view/loading-view":9,"../core/singleton":12,"../manager/res/res-group":20,"./enum":32}],32:[function(require,module,exports){
"use strict";
/**
 * 重要的枚举定义,框架级别
 *
 * @author Tim Wars
 * @date 2019-01-18 16:20
 * @project firebolt
 * @copyright (C) DONOPO
 *
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.enumSoundName = exports.enumJsonDefine = exports.enumElementPrefix = exports.enumGameStatus = exports.enumDimension = exports.enumClearStrategy = exports.enumAlige = exports.enumAligeType = exports.enumGamePlatform = exports.enumArraySortOrder = exports.enumScreenModel = exports.enumScaleType = void 0;
var Stage = Laya.Stage;
/**
 * 舞台的缩放格式
 */
var enumScaleType;
(function (enumScaleType) {
    // @ts-ignore
    enumScaleType[enumScaleType["ScaleNoScale"] = Stage.SCALE_FULL] = "ScaleNoScale";
    // @ts-ignore
    enumScaleType[enumScaleType["ScaleExactFit"] = Stage.SCALE_EXACTFIT] = "ScaleExactFit";
    // @ts-ignore
    enumScaleType[enumScaleType["ScaleShowAll"] = Stage.SCALE_SHOWALL] = "ScaleShowAll";
    // @ts-ignore
    enumScaleType[enumScaleType["ScaleNoBorder"] = Stage.SCALE_NOBORDER] = "ScaleNoBorder";
    // @ts-ignore
    enumScaleType[enumScaleType["ScaleFull"] = Stage.SCALE_FULL] = "ScaleFull";
    // @ts-ignore
    enumScaleType[enumScaleType["ScaleFixedWidth"] = Stage.SCALE_FIXED_WIDTH] = "ScaleFixedWidth";
    // @ts-ignore
    enumScaleType[enumScaleType["ScaleFixedHeight"] = Stage.SCALE_FIXED_HEIGHT] = "ScaleFixedHeight";
    // @ts-ignore
    enumScaleType[enumScaleType["ScaleFixedAuto"] = Stage.SCALE_FIXED_AUTO] = "ScaleFixedAuto";
    // @ts-ignore
    enumScaleType[enumScaleType["ScaleNoScale"] = Stage.SCALE_NOSCALE] = "ScaleNoScale";
})(enumScaleType = exports.enumScaleType || (exports.enumScaleType = {}));
/**
 * 屏幕的自适应方式
 */
var enumScreenModel;
(function (enumScreenModel) {
    enumScreenModel["ScreenNone"] = "none";
    enumScreenModel["ScreenHorizontal"] = "horizontal";
    enumScreenModel["ScreenVertical"] = "vertical";
})(enumScreenModel = exports.enumScreenModel || (exports.enumScreenModel = {}));
/**
 * 数组排序方式
 * */
var enumArraySortOrder;
(function (enumArraySortOrder) {
    enumArraySortOrder[enumArraySortOrder["Ascending"] = 0] = "Ascending";
    enumArraySortOrder[enumArraySortOrder["Descending"] = 1] = "Descending";
})(enumArraySortOrder = exports.enumArraySortOrder || (exports.enumArraySortOrder = {}));
/**
 * 游戏的运行容器
 */
var enumGamePlatform;
(function (enumGamePlatform) {
    enumGamePlatform[enumGamePlatform["Web"] = 0] = "Web";
    enumGamePlatform[enumGamePlatform["Phone"] = 1] = "Phone";
    enumGamePlatform[enumGamePlatform["Weixin"] = 2] = "Weixin";
})(enumGamePlatform = exports.enumGamePlatform || (exports.enumGamePlatform = {}));
/**
 * 对齐方式
 */
var enumAligeType;
(function (enumAligeType) {
    enumAligeType[enumAligeType["NONE"] = 0] = "NONE";
    enumAligeType[enumAligeType["RIGHT"] = 1] = "RIGHT";
    enumAligeType[enumAligeType["RIGHT_BOTTOM"] = 2] = "RIGHT_BOTTOM";
    enumAligeType[enumAligeType["BOTTOM"] = 3] = "BOTTOM";
    enumAligeType[enumAligeType["LEFT_BOTTOM"] = 4] = "LEFT_BOTTOM";
    enumAligeType[enumAligeType["LEFT"] = 5] = "LEFT";
    enumAligeType[enumAligeType["LEFT_TOP"] = 6] = "LEFT_TOP";
    enumAligeType[enumAligeType["TOP"] = 7] = "TOP";
    enumAligeType[enumAligeType["RIGHT_TOP"] = 8] = "RIGHT_TOP";
    enumAligeType[enumAligeType["MID"] = 9] = "MID";
})(enumAligeType = exports.enumAligeType || (exports.enumAligeType = {}));
/**
 * 对齐标注
 */
var enumAlige;
(function (enumAlige) {
    enumAlige["AligeLeft"] = "left";
    enumAlige["AligeCenter"] = "center";
    enumAlige["AligeRight"] = "right";
    enumAlige["AligeTop"] = "top";
    enumAlige["AligeMiddle"] = "middle";
    enumAlige["AligeBottom"] = "bottom";
})(enumAlige = exports.enumAlige || (exports.enumAlige = {}));
/**
 * 清理资源的次序策略
 */
var enumClearStrategy;
(function (enumClearStrategy) {
    enumClearStrategy[enumClearStrategy["FIFO"] = 0] = "FIFO";
    enumClearStrategy[enumClearStrategy["FILO"] = 1] = "FILO";
    enumClearStrategy[enumClearStrategy["LRU"] = 2] = "LRU";
    enumClearStrategy[enumClearStrategy["UN_USED"] = 3] = "UN_USED";
    enumClearStrategy[enumClearStrategy["ALL"] = 4] = "ALL";
})(enumClearStrategy = exports.enumClearStrategy || (exports.enumClearStrategy = {}));
/**
 * 游戏是否采用的2D或者3D
 */
var enumDimension;
(function (enumDimension) {
    enumDimension["Dim2"] = "2d";
    enumDimension["Dim3"] = "3d";
})(enumDimension = exports.enumDimension || (exports.enumDimension = {}));
/**
 * 游戏的状态
 */
var enumGameStatus;
(function (enumGameStatus) {
    enumGameStatus["Start"] = "GAME-STATUS-START";
    enumGameStatus["Stop"] = "GAME-STATUS-STOP";
    enumGameStatus["Restart"] = "GAME-STATUS-RESTART";
})(enumGameStatus = exports.enumGameStatus || (exports.enumGameStatus = {}));
/**
 lbl  --->Label(文本)
 txt  --->Text(文本)
 rtxt  --->RichText(富文本)
 ipt  --->Input(输入框)
 img  --->Image(图片)
 spt  --->Sprite(精灵)
 grh  --->Graph(图形)
 list --->List(列表)
 load --->Load(装载器)
 gup  --->Group(组)
 com  --->Component(组件)
 btn  --->Button(按钮)
 cob  --->ComboBow(下拉框)
 pbar --->ProgressBar(进度条)
 sld  --->Slider(滑动条)
 win  --->Window（窗口）
 ani  --->Movie(动画)
 eft  --->Transition(动效)
 ctl  --->Controller(控制器)
 */
/**
 * 控件前缀
 */
var enumElementPrefix;
(function (enumElementPrefix) {
    enumElementPrefix["Lable"] = "lbl_";
    enumElementPrefix["Input"] = "ipt_";
    enumElementPrefix["Text"] = "txt_";
    enumElementPrefix["RichText"] = "rtxt_";
    enumElementPrefix["Image"] = "img_";
    enumElementPrefix["Sprite"] = "spt_";
    enumElementPrefix["Graph"] = "grh_";
    enumElementPrefix["List"] = "list_";
    enumElementPrefix["Load"] = "load_";
    enumElementPrefix["Group"] = "gup_";
    enumElementPrefix["Component"] = "com_";
    enumElementPrefix["Button"] = "btn_";
    enumElementPrefix["ComboBow"] = "cob_";
    enumElementPrefix["ProgressBar"] = "pbar_";
    enumElementPrefix["Slider"] = "sld_";
    enumElementPrefix["Window"] = "win_";
    enumElementPrefix["Movie"] = "ani_";
    enumElementPrefix["Transition"] = "eft_";
    enumElementPrefix["Controller"] = "ctl_";
})(enumElementPrefix = exports.enumElementPrefix || (exports.enumElementPrefix = {}));
/**
 * 数据表配置
 */
var enumJsonDefine;
(function (enumJsonDefine) {
    enumJsonDefine["invite"] = "invite";
    enumJsonDefine["level"] = "level";
    enumJsonDefine["lottery"] = "lottery";
    enumJsonDefine["offline"] = "offline";
})(enumJsonDefine = exports.enumJsonDefine || (exports.enumJsonDefine = {}));
/**
 * 音效标记
 */
var enumSoundName;
(function (enumSoundName) {
    enumSoundName["bg"] = "bgSound";
    enumSoundName["botton"] = "btnSound";
})(enumSoundName = exports.enumSoundName || (exports.enumSoundName = {}));
},{}],33:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Dictionary = void 0;
const dict_1 = require("../util/dict");
/**
 * @author Sun
 * @time 2019-05-21 19:22
 * @project SFramework_LayaAir
 * @description  字典
 *
 */
class Dictionary {
    constructor() {
        this.m_dict = {};
    }
    add(key, value) {
        if (this.hasKey(key))
            return false;
        this.m_dict[key] = value;
        return true;
    }
    remove(key) {
        delete this.m_dict[key];
    }
    hasKey(key) {
        return (this.m_dict[key] != null);
    }
    value(key) {
        if (!this.hasKey(key))
            return null;
        return this.m_dict[key];
    }
    keys() {
        let list = [];
        for (let key in this.m_dict) {
            list.push(key);
        }
        return list;
    }
    values() {
        let list = [];
        for (let key in this.m_dict) {
            list.push(this.m_dict[key]);
        }
        return list;
    }
    clear() {
        for (let key in this.m_dict) {
            delete this.m_dict[key];
        }
    }
    foreach(compareFn) {
        for (let key in this.m_dict) {
            compareFn.call(null, key, this.m_dict[key]);
        }
    }
    foreachBreak(compareFn) {
        for (let key in this.m_dict) {
            if (!compareFn.call(null, key, this.m_dict[key]))
                break;
        }
    }
    get length() {
        return dict_1.UtilDict.size(this.m_dict);
    }
}
exports.Dictionary = Dictionary;
},{"../util/dict":35}],34:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UtilArray = void 0;
const enum_1 = require("../setting/enum");
/**
* @author Sun
* @time 2019-08-09 23:15
* @project SFramework_LayaAir
* @description 数组工具类
*/
class UtilArray {
    /** 插入元素
     * @param arr 需要操作的数组
     * @param value 需要插入的元素
     * @param index 插入位置
     */
    static insert(arr, value, index) {
        if (index > arr.length - 1) {
            arr.push(value);
        }
        else {
            arr.splice(index, 0, value);
        }
    }
    /**从数组移除元素*/
    static remove(arr, v) {
        let i = arr.indexOf(v);
        if (i != -1) {
            arr.splice(i, 1);
        }
    }
    /**移除所有值等于v的元素*/
    static removeAll(arr, v) {
        let i = arr.indexOf(v);
        while (i >= 0) {
            arr.splice(i, 1);
            i = arr.indexOf(v);
        }
    }
    /**包含元素*/
    static contain(arr, v) {
        return arr.length > 0 ? arr.indexOf(v) != -1 : false;
    }
    /**复制*/
    static copy(arr) {
        return arr.slice();
    }
    /**
     * 排序
     * @param arr 需要排序的数组
     * @param key 排序字段
     * @param order 排序方式
     */
    static sort(arr, key, order = enum_1.enumArraySortOrder.Descending) {
        if (arr == null)
            return;
        arr.sort(function (info1, info2) {
            switch (order) {
                case enum_1.enumArraySortOrder.Ascending: {
                    if (info1[key] < info2[key])
                        return -1;
                    if (info1[key] > info2[key])
                        return 1;
                    else
                        return 0;
                }
                case enum_1.enumArraySortOrder.Descending: {
                    if (info1[key] > info2[key])
                        return -1;
                    if (info1[key] < info2[key])
                        return 1;
                    else
                        return 0;
                }
            }
        });
    }
    /**清空数组*/
    static clear(arr) {
        let i = 0;
        let len = arr.length;
        for (; i < len; ++i) {
            arr[i] = null;
        }
        arr.splice(0);
    }
    /**数据是否为空*/
    static isEmpty(arr) {
        if (arr == null || arr.length == 0) {
            return true;
        }
        return false;
    }
}
exports.UtilArray = UtilArray;
},{"../setting/enum":32}],35:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UtilDict = void 0;
/**
* @author Sun
* @time 2019-08-10 20:22
* @project SFramework_LayaAir
* @description  字典工具类
*
*/
class UtilDict {
    /**
     * 键列表
     */
    static keys(d) {
        let a = [];
        for (let key in d) {
            a.push(key);
        }
        return a;
    }
    /**
     * 值列表
     */
    static values(d) {
        let a = [];
        for (let key in d) {
            a.push(d[key]);
        }
        return a;
    }
    /**
     * 清空字典
     */
    static clear(dic) {
        let v;
        for (let key in dic) {
            v = dic[key];
            if (v instanceof Object) {
                UtilDict.clear(v);
            }
            delete dic[key];
        }
    }
    /**
     * 全部应用
     */
    static foreach(dic, compareFn) {
        for (let key in dic) {
            if (!compareFn.call(null, key, dic[key]))
                break;
        }
    }
    static isEmpty(dic) {
        if (dic == null)
            return true;
        // @ts-ignore
        for (let key in dic) {
            return false;
        }
        return true;
    }
    static size(dic) {
        if (dic == null)
            return 0;
        let count = 0;
        for (let key in dic) {
            ++count;
        }
        return count;
    }
}
exports.UtilDict = UtilDict;
},{}],36:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UtilDisplay = void 0;
var Sprite = Laya.Sprite;
class UtilDisplay {
    /**
     * 移除全部子对象
     * @param container
     */
    static removeAllChild(container) {
        if (!container)
            return;
        if (container.numChildren <= 0)
            return;
        while (container.numChildren > 0) {
            container.removeChildAt(0);
        }
    }
    /**
     *
     * @param node 销毁UI节点
     */
    static destroyUINode(node) {
        if (node) {
            node.removeSelf();
            node.destroy();
            node = null;
        }
    }
    /**
     * 通过名字获得子节点
     * @param parent
     * @param name
     */
    static getChildByName(parent, name) {
        if (!parent)
            return null;
        if (parent.name === name)
            return parent;
        let child = null;
        let num = parent.numChildren;
        for (let i = 0; i < num; ++i) {
            child = UtilDisplay.getChildByName(parent.getChildAt(i), name);
            if (child)
                return child;
        }
        return null;
    }
    // /**
    //  * 设置对齐方式
    //  * @param alige 对齐方式
    //  */
    // public static setAlige(node: Sprite, alige: enumAligeType, w: number = 0, h: number = 0) {
    //     if (!node) return;
    //     let rect: Rectangle;
    //     if (w <= 0 || h <= 0) rect = node.getBounds();
    //     let width: number = w > 0 ? w : rect.width;
    //     let heigth: number = h > 0 ? h : rect.height;
    //     switch (alige) {
    //         case enumAligeType.LEFT_TOP:
    //             node.pivot(0, 0);
    //             break;
    //         case enumAligeType.LEFT:
    //             node.pivot(0, heigth * 0.5);
    //             break;
    //         case enumAligeType.LEFT_BOTTOM:
    //             node.pivot(0, heigth);
    //             break;
    //         case enumAligeType.TOP:
    //             node.pivot(width * 0.5, 0);
    //             break;
    //         case enumAligeType.MID:
    //             node.pivot(width * 0.5, heigth * 0.5);
    //             break;
    //         case enumAligeType.BOTTOM:
    //             node.pivot(width * 0.5, heigth);
    //             break;
    //         case enumAligeType.RIGHT_TOP:
    //             node.pivot(width, 0);
    //             break;
    //         case enumAligeType.RIGHT:
    //             node.pivot(width, heigth * 0.5);
    //             break;
    //         case enumAligeType.RIGHT_BOTTOM:
    //             node.pivot(width, heigth);
    //             break;
    //     }
    // }
    /**
     * 创建透明遮罩
     */
    static createMaskLayer() {
        let layer = new Sprite();
        layer.mouseEnabled = true;
        let width = layer.width = Laya.stage.width + 200;
        var height = layer.height = Laya.stage.height + 400;
        layer.graphics.clear(true);
        layer.graphics.drawRect(0, 0, width, height, UIConfig.popupBgColor);
        layer.alpha = UIConfig.popupBgAlpha;
        return layer;
    }
}
exports.UtilDisplay = UtilDisplay;
},{}],37:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UtilLoad3D = void 0;
const log_1 = require("../core/log");
/**
* @author Sun
* @time 2019-02-25 17:22
* @project SFramework_LayaAir
* @description  3D模型加载工具类
*
*/
class UtilLoad3D {
    /**
     * 加载U3D场景
     * @param area 作用域
     * @param path 场景文件路径
     * @param cb   加载完成回调
     */
    static loadScene(path, area, cb) {
        Laya.loader.create(path, Laya.Handler.create(this, () => {
            Laya.stage.addChild(Laya.loader.getRes(path));
            if (cb) {
                cb.call(area);
            }
        }));
    }
    /**
     * 获取场景内物体
     * @param scene3d 场景
     * @param childName 子物体名字
     */
    static getScene3DChild(scene3d, childName) {
        let ms = scene3d.getChildByName(childName);
        if (ms) {
            return ms;
        }
        log_1.Log.error("Error:获取场景物体失败");
        return null;
    }
    /**
     * 获取模型的子物体模型
     * @param fatSP 父方
     * @param childName 子方名字
     */
    static getModelChildByName(fatSP, childName) {
        let child = fatSP.getChildByName(childName);
        if (child) {
            return child;
        }
        log_1.Log.error("Error:获取模型子物体信息错误");
        return null;
    }
    /**
     * 替换模型
     * @param targetSP 被替换模型
     * @param mianSP   替换模型
     */
    static replaceModel(targetSP, mainSP) {
        if (!targetSP || !mainSP) {
            log_1.Log.error("Error:替换或被替换模型信息错误");
            return null;
        }
        if (targetSP.parent) {
            targetSP.parent.addChild(mainSP);
        }
        mainSP.transform.position = new Laya.Vector3(targetSP.transform.position.x, targetSP.transform.position.y, targetSP.transform.position.z);
        mainSP.transform.scale = new Laya.Vector3(targetSP.transform.scale.x, targetSP.transform.scale.y, targetSP.transform.scale.y);
    }
    /**
     * 替换Mesh模型材质
     * @param targetSP 目标模型
     * @param targetMat 目标材质
     */
    static replaceModelMeshMat(targetSP, targetMat) {
        if (!targetSP || !targetMat) {
            log_1.Log.error("Error:模型或材质信息错误");
            return null;
        }
        targetSP;
        targetSP.meshRenderer.material = targetMat;
    }
    /**
     * 替换SkinMesh模型材质
     * @param targetSP 目标模型
     * @param targetMat 目标材质
     */
    static replaceModelSkinMeshMat(targetSP, targetMat) {
        if (!targetSP || !targetMat) {
            log_1.Log.error("Error:模型或材质信息错误");
            return null;
        }
        targetSP;
        targetSP.skinnedMeshRenderer.material = targetMat;
    }
    /**
     * 获取容器上的材质并存入哈希表
     * @param targetObj 承载材质的容器
     */
    static getMaterials(scene3d) {
        /**Unity场景存贮一个空物体，附带MeshRender用来存储材质**/
        var container = UtilLoad3D.getScene3DChild(scene3d, "MatContainer");
        if (!container) {
            log_1.Log.error("Error:材质容器错误或不存在");
            return null;
        }
        var userInfo = {};
        var matArrary = container.meshRenderer.materials;
        for (var i = 0; i < matArrary.length; i++) {
            var name = matArrary[i].name.slice(0, matArrary[i].name.length - 10);
            userInfo[name] = matArrary[i];
        }
        return userInfo;
    }
    /**
     * 返回指定名字的材质
     * @param matArraty 存放材质的哈希表
     * @param matName   材质名字
     */
    static getMaterialByName(matArrary, matName) {
        if (!matArrary) {
            log_1.Log.error("Error:材质哈希表信息错误");
            return null;
        }
        if (!matArrary[matName]) {
            log_1.Log.error("Error:指定哈希表内不存在[" + matName + "]材质");
            return null;
        }
        return matArrary[matName];
    }
    /**
     * 播放模型动画
     * @param targetSp 播放物体
     * @param aniName  动画名字
     * @param isCross  是否过度
     * 通过this.animator.getCurrentAnimatorPlayState(0).normalizedTime>=1去判断当前动画是否播放完成
     */
    static playAnimatorByName(targetSp, aniName, isCross) {
        var animator = targetSp.getComponent(Laya.Animator);
        if (!animator) {
            log_1.Log.error("Error:动画机信息错误");
            return;
        }
        if (isCross != null && isCross == false) {
            animator.play(aniName);
            return;
        }
        animator.crossFade(aniName, 0.2);
        return animator;
    }
    /**
     * 控制动画速度
     * @param targetSp 目标物体
     * @param speed 播放速度
     */
    static controlAnimatorSpeed(targetSp, speed) {
        var animator = targetSp.getComponent(Laya.Animator);
        if (!animator) {
            log_1.Log.error("Error:动画机信息错误");
            return;
        }
        if (speed) {
            animator.speed = speed;
        }
        else {
            animator.speed = 1;
        }
    }
    /**
     * 判断动画是否完成
     * @param animator
     */
    static confirmAniComplete(animator) {
        var bool = false;
        let index = animator.getCurrentAnimatorPlayState(0).normalizedTime;
        if (index >= 1) {
            bool = true;
        }
        return bool;
    }
}
exports.UtilLoad3D = UtilLoad3D;
},{"../core/log":10}],38:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UtilMath = void 0;
const math3d_1 = require("./math3d");
/**
 * @author Sun
 * @time 2019-01-18 16:20
 * @project SFramework_LayaAir
 * @description 算法工具类
 *
 */
class UtilMath {
    static Sign(f) {
        return ((f < 0) ? -1 : 1);
    }
    /**
     * 限定数字在范围区间并返回
     * @param num
     * @param min
     * @param max
     * @constructor
     */
    static clamp(num, min, max) {
        if (num < min) {
            num = min;
        }
        else if (num > max) {
            num = max;
        }
        return num;
    }
    static clamp01(value) {
        if (value < 0)
            return 0;
        if (value > 1)
            return 1;
        return value;
    }
    static lerp(from, to, t) {
        return (from + ((to - from) * UtilMath.clamp01(t)));
    }
    static lerpAngle(a, b, t) {
        let num = UtilMath.repeat(b - a, 360);
        if (num > 180)
            num -= 360;
        return (a + (num * UtilMath.clamp01(t)));
    }
    static repeat(t, length) {
        return (t - (Math.floor(t / length) * length));
    }
    /**
     * 产生随机数
     * 结果：x>=param1 && x<param2
     */
    static randRange(param1, param2) {
        let loc = Math.random() * (param2 - param1) + param1;
        return loc;
    }
    /**
     * 产生随机数
     * 结果：x>=param1 && x<=param2
     */
    static randRangeInt(param1, param2) {
        let loc = Math.random() * (param2 - param1 + 1) + param1;
        return Math.floor(loc);
    }
    /**
     * 从数组中产生随机数[-1,1,2]
     * 结果：-1/1/2中的一个
     */
    static randRangeArray(arr) {
        if (arr.length == 0)
            return null;
        let loc = arr[UtilMath.randRangeInt(0, arr.length - 1)];
        return loc;
    }
    /**
     * 转换为360度角度
     */
    static clampDegrees(degrees) {
        while (degrees < 0)
            degrees = degrees + 360;
        while (degrees >= 360)
            degrees = degrees - 360;
        return degrees;
    }
    /**
     * 转换为360度弧度
     */
    static clampRadians(radians) {
        while (radians < 0)
            radians = radians + 2 * Math.PI;
        while (radians >= 2 * Math.PI)
            radians = radians - 2 * Math.PI;
        return radians;
    }
    /**
     * 两点间的距离
     */
    static getDistance(x1, y1, x2, y2) {
        return Math.sqrt(Math.pow(y2 - y1, 2) + Math.pow(x2 - x1, 2));
    }
    static getSquareDistance(x1, y1, x2, y2) {
        return Math.pow(y2 - y1, 2) + Math.pow(x2 - x1, 2);
    }
    /**
     * 两点间的弧度：x正方形为0，Y轴向下,顺时针为正
     */
    static getLineRadians(x1, y1, x2, y2) {
        return Math.atan2(y2 - y1, x2 - x1);
    }
    static getLineDegree(x1, y1, x2, y2) {
        let degree = UtilMath.toDegree(UtilMath.getLineRadians(x1, y1, x2, y2));
        return UtilMath.clampDegrees(degree);
    }
    static getPointRadians(x, y) {
        return Math.atan2(y, x);
    }
    static getPointDegree(x, y) {
        let degree = UtilMath.toDegree(UtilMath.getPointRadians(x, y));
        return UtilMath.clampDegrees(degree);
    }
    /**
     * 弧度转化为度
     */
    static toDegree(radian) {
        return radian * (180.0 / Math.PI);
    }
    /**
     * 度转化为弧度
     */
    static toRadian(degree) {
        return degree * (Math.PI / 180.0);
    }
    static moveTowards(current, target, maxDelta) {
        if (Math.abs(target - current) <= maxDelta) {
            return target;
        }
        return (current + (UtilMath.Sign(target - current) * maxDelta));
    }
    /**
     * 获取一定范围内的随机整数
     * @param min 最小值
     * @param max 最大值
     * @constructor
     */
    static random(min, max) {
        return Math.floor(Math.random() * (max - min) + min);
    }
    /**
     * 二维向量归一化
     * @param x
     * @param y
     */
    static normalize(x, y) {
        return Math.sqrt(x * x + y * y);
    }
    /**
     * 返回两向量夹角
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     */
    static vectorAngle(x1, y1, x2, y2) {
        if (x1 == x2 && y1 == y2) {
            return;
        }
        var cosAngle = (x1 * x2 + y1 * y2) / (UtilMath.normalize(x1, y1) * UtilMath.normalize(x2, y2));
        var aCosAngle = Math.acos(cosAngle);
        var angle = math3d_1.UtilMath3D.Rad2Deg(aCosAngle);
        if (x1 / y1 < x2 / y2)
            angle = -angle;
        return angle;
    }
}
exports.UtilMath = UtilMath;
/**字节转换M*/
UtilMath.BYTE_TO_M = 1 / (1024 * 1024);
/**字节转换K*/
UtilMath.BYTE_TO_K = 1 / (1024);
UtilMath.Deg2Rad = 0.01745329;
UtilMath.Rad2Deg = 57.29578;
},{"./math3d":39}],39:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getLineFromRadians = exports.Vec3ToString = exports.Vec3MoveTowards = exports.Vec3ClampMagnitude = exports.Vec3Set = exports.Vec3Normal = exports.Vec3Normalized = exports.Vec3SqrMagnitude = exports.Vec3Magnitude = exports.Vec3Max = exports.Vec3Min = exports.Vec3Project = exports.Vec3Cross = exports.Vec3Div = exports.Vec3Mul = exports.Vec3Multiply = exports.Vec3Sub = exports.Vec3Add = exports.Vec2ToString = exports.Vec2MoveTowards = exports.Vec2ClampMagnitude = exports.Vec2Set = exports.Vec2Normal = exports.Vec2Normalized = exports.Vec2SqrMagnitude = exports.Vec2Magnitude = exports.Vec2Max = exports.Vec2Min = exports.Vec2Project = exports.Vec2Dot = exports.Vec2Div = exports.Vec2Mul = exports.Vec2Multiply = exports.Vec2Sub = exports.Vec2Add = exports.UtilMath3D = void 0;
var Vector2 = Laya.Vector2;
var Vector3 = Laya.Vector3;
var Vector4 = Laya.Vector4;
const string_1 = require("./string");
/**
 * @author Sun
 * @time 2019-08-11 18:08
 * @project SFramework_LayaAir
 * @description 3d算法工具类
 *
 */
class UtilMath3D {
    static get TempVec2() {
        if (!UtilMath3D._Vec2Temp)
            UtilMath3D._Vec2Temp = new Vector2(0, 0);
        return UtilMath3D._Vec2Temp;
    }
    static get TempVec3() {
        if (!UtilMath3D._Vec3Temp)
            UtilMath3D._Vec3Temp = new Vector3(0, 0, 0);
        return UtilMath3D._Vec3Temp;
    }
    static get TempVec4() {
        if (!UtilMath3D._Vec4Temp)
            UtilMath3D._Vec4Temp = new Vector4(0, 0, 0, 0);
        return UtilMath3D._Vec4Temp;
    }
    /**转换为水平方向*/
    static ToHorizontal(vec) {
        vec.y = 0;
        return vec;
    }
    /**水平距离*/
    static HorizontalDistance(vec1, vec2) {
        vec1.y = 0;
        vec2.y = 0;
        return Vector3.scalarLength(Vec3Sub(vec1, vec2));
    }
    /**射线上的一点*/
    static GetRayPoint(ray, distance) {
        return Vec3Add(ray.origin, Vec3Mul(ray.direction, distance));
    }
    /** Des:三维求两点距离 */
    static Vec3Magnitude(vec1, vec2) {
        return Math.sqrt((vec1.x - vec2.x) * (vec1.x - vec2.x) + ((vec1.y - vec2.y) * (vec1.y - vec2.y)) + ((vec1.z - vec2.z) * (vec1.z - vec2.z)));
    }
    /** Des:二维求两点距离 */
    static Vec2Magnitude(vec1, vec2) {
        return Math.sqrt((vec1.x - vec2.x) * (vec1.x - vec2.x) + ((vec1.y - vec2.y) * (vec1.y - vec2.y)));
    }
    /** Des:角度转弧度 */
    static Deg2Rad(angle) {
        return Laya.Utils.toRadian(angle);
    }
    /** Des:弧度转角度 */
    static Rad2Deg(radian) {
        return Laya.Utils.toAngle(radian);
    }
    /** Des:正弦 */
    static sin(angle) {
        var radian = UtilMath3D.Deg2Rad(angle);
        return Math.sin(radian);
    }
    /** Des:余弦 */
    static cos(angle) {
        var radian = UtilMath3D.Deg2Rad(angle);
        return Math.cos(radian);
    }
    /** Des:正切 */
    static tan(angle) {
        var radian = UtilMath3D.Deg2Rad(angle);
        return Math.tan(radian);
    }
    /** Des:反正弦 */
    static asin(angle) {
        var radian = UtilMath3D.Deg2Rad(angle);
        return Math.asin(radian);
    }
    /** Des:反余弦 */
    static acos(angle) {
        var radian = UtilMath3D.Deg2Rad(angle);
        return Math.acos(radian);
    }
    /** Des:反正切 */
    static atan(angle) {
        var radian = UtilMath3D.Deg2Rad(angle);
        return Math.atan(radian);
    }
}
exports.UtilMath3D = UtilMath3D;
UtilMath3D._Vec2Temp = null;
UtilMath3D._Vec3Temp = null;
UtilMath3D._Vec4Temp = null;
//～～～～～～～～～～～～～～～～～～～～～～～vec2～～～～～～～～～～～～～～～～～～～～～～～//
function Vec2Add(a, b) {
    return new Vector2(a.x + b.x, a.y + b.y);
}
exports.Vec2Add = Vec2Add;
function Vec2Sub(a, b) {
    return new Vector2(a.x - b.x, a.y - b.y);
}
exports.Vec2Sub = Vec2Sub;
function Vec2Multiply(a, b) {
    return new Vector2(a.x * b.x, a.y * b.y);
}
exports.Vec2Multiply = Vec2Multiply;
function Vec2Mul(a, d) {
    return new Vector2(a.x * d, a.y * d);
}
exports.Vec2Mul = Vec2Mul;
function Vec2Div(a, d) {
    return new Vector2(a.x / d, a.y / d);
}
exports.Vec2Div = Vec2Div;
function Vec2Dot(lhs, rhs) {
    return ((lhs.x * rhs.x) + (lhs.y * rhs.y));
}
exports.Vec2Dot = Vec2Dot;
function Vec2Project(vector, onNormal) {
    let num = Vec2Dot(onNormal, onNormal);
    if (num < 1E-05) {
        return Vector2.ZERO;
    }
    return (Vec2Div(Vec2Mul(onNormal, Vec2Dot(vector, onNormal)), num));
}
exports.Vec2Project = Vec2Project;
function Vec2Min(lhs, rhs) {
    return new Vector2(Math.min(lhs.x, rhs.x), Math.min(lhs.y, rhs.y));
}
exports.Vec2Min = Vec2Min;
function Vec2Max(lhs, rhs) {
    return new Vector2(Math.max(lhs.x, rhs.x), Math.max(lhs.y, rhs.y));
}
exports.Vec2Max = Vec2Max;
function Vec2Magnitude(vec) {
    return Math.sqrt((vec.x * vec.x) + (vec.y * vec.y));
}
exports.Vec2Magnitude = Vec2Magnitude;
function Vec2SqrMagnitude(vec) {
    return (vec.x * vec.x) + (vec.y * vec.y);
}
exports.Vec2SqrMagnitude = Vec2SqrMagnitude;
function Vec2Normalized(vec) {
    let magnitude = Vec2Magnitude(vec);
    let v;
    if (magnitude > 1E-05)
        v = Vec2Div(vec, magnitude);
    else
        v = new Vector2(0, 0);
    return v;
}
exports.Vec2Normalized = Vec2Normalized;
function Vec2Normal(vec) {
    let magnitude = Vec2Magnitude(vec);
    if (magnitude > 1E-05) {
        let v = Vec2Div(vec, magnitude);
        Vec2Set(vec, v.x, v.y);
    }
    else
        Vec2Set(vec, 0, 0);
}
exports.Vec2Normal = Vec2Normal;
function Vec2Set(v, x, y) {
    v.x = x;
    v.y = y;
    ;
    return v;
}
exports.Vec2Set = Vec2Set;
// export function Vec2Angle(from: Vector2, to: Vector2): number {
//     return (Math.acos(UtilMath.clamp(Vec2Dot(Vec2Normalized(from), Vec2Normalized(to)), -1, 1)) * 57.29578);
// }
function Vec2ClampMagnitude(vector, maxLength) {
    if (Vec2SqrMagnitude(vector) > (maxLength * maxLength)) {
        return (Vec2Mul(Vec2Normalized(vector), maxLength));
    }
    return vector;
}
exports.Vec2ClampMagnitude = Vec2ClampMagnitude;
// export function Vec2Lerp(from: Vector2, to: Vector2, t: number): Vector2 {
//     t = UtilMath.clamp(t, 0, 1);
//     return new Vector2(from.x + ((to.x - from.x) * t), from.y + ((to.y - from.y) * t));
// }
function Vec2MoveTowards(current, target, maxDistanceDelta) {
    let vector = Vec2Sub(target, current);
    let magnitude = Vec2Magnitude(vector);
    if ((magnitude > maxDistanceDelta) && (magnitude != 0)) {
        return Vec2Add(current, (Vec2Mul(Vec2Div(vector, magnitude), maxDistanceDelta)));
    }
    return target;
}
exports.Vec2MoveTowards = Vec2MoveTowards;
function Vec2ToString(vec) {
    return string_1.UtilString.format("({0}, {1})", vec.x, vec.y);
}
exports.Vec2ToString = Vec2ToString;
//～～～～～～～～～～～～～～～～～～～～～～～vec3～～～～～～～～～～～～～～～～～～～～～～～//
function Vec3Add(a, b) {
    return new Vector3(a.x + b.x, a.y + b.y, a.z + b.z);
}
exports.Vec3Add = Vec3Add;
function Vec3Sub(a, b) {
    return new Vector3(a.x - b.x, a.y - b.y, a.z - b.z);
}
exports.Vec3Sub = Vec3Sub;
function Vec3Multiply(a, b) {
    return new Vector3(a.x * b.x, a.y * b.y, a.z * b.z);
}
exports.Vec3Multiply = Vec3Multiply;
function Vec3Mul(a, d) {
    return new Vector3(a.x * d, a.y * d, a.z * d);
}
exports.Vec3Mul = Vec3Mul;
function Vec3Div(a, d) {
    return new Vector3(a.x / d, a.y / d, a.z / d);
}
exports.Vec3Div = Vec3Div;
function Vec3Cross(lhs, rhs) {
    return new Vector3((lhs.y * rhs.z) - (lhs.z * rhs.y), (lhs.z * rhs.x) - (lhs.x * rhs.z), (lhs.x * rhs.y) - (lhs.y * rhs.x));
}
exports.Vec3Cross = Vec3Cross;
function Vec3Project(vector, onNormal) {
    let num = Vector3.dot(onNormal, onNormal);
    if (num < 1E-05) {
        return new Vector3();
    }
    return (Vec3Div(Vec3Mul(onNormal, Vector3.dot(vector, onNormal)), num));
}
exports.Vec3Project = Vec3Project;
function Vec3Min(lhs, rhs) {
    return new Vector3(Math.min(lhs.x, rhs.x), Math.min(lhs.y, rhs.y), Math.min(lhs.z, rhs.z));
}
exports.Vec3Min = Vec3Min;
function Vec3Max(lhs, rhs) {
    return new Vector3(Math.max(lhs.x, rhs.x), Math.max(lhs.y, rhs.y), Math.max(lhs.z, rhs.z));
}
exports.Vec3Max = Vec3Max;
function Vec3Magnitude(vec) {
    return Math.sqrt((vec.x * vec.x) + (vec.y * vec.y) + (vec.z * vec.z));
}
exports.Vec3Magnitude = Vec3Magnitude;
function Vec3SqrMagnitude(vec) {
    return (vec.x * vec.x) + (vec.y * vec.y) + (vec.z * vec.z);
}
exports.Vec3SqrMagnitude = Vec3SqrMagnitude;
function Vec3Normalized(vec) {
    let magnitude = Vector3.scalarLength(vec);
    let v;
    if (magnitude > 1E-05)
        v = Vec3Div(vec, magnitude);
    else
        v = new Vector3(0, 0, 0);
    return v;
}
exports.Vec3Normalized = Vec3Normalized;
function Vec3Normal(vec) {
    let magnitude = Vector3.scalarLength(vec);
    if (magnitude > 1E-05) {
        let v = Vec3Div(vec, magnitude);
        Vec3Set(vec, v.x, v.y, v.z);
    }
    else
        Vec3Set(vec, 0, 0, 0);
}
exports.Vec3Normal = Vec3Normal;
function Vec3Set(v, x, y, z) {
    v.x = x;
    v.y = y;
    v.z = z;
    return v;
}
exports.Vec3Set = Vec3Set;
// export function Vec3Angle(from: Vector3, to: Vector3): number {
//     return (Math.acos(UtilMath.clamp(Vector3.dot(Vec3Normalized(from), Vec3Normalized(to)), -1, 1)) * 57.29578);
// }
function Vec3ClampMagnitude(vector, maxLength) {
    if (Vector3.scalarLengthSquared(vector) > (maxLength * maxLength)) {
        return (Vec3Mul(Vec3Normalized(vector), maxLength));
    }
    return vector;
}
exports.Vec3ClampMagnitude = Vec3ClampMagnitude;
// export function Vec3Lerp(from: Vector3, to: Vector3, t: number): Vector3 {
//     t = UtilMath.clamp(t, 0, 1);
//     return new Vector3(from.x + ((to.x - from.x) * t), from.y + ((to.y - from.y) * t), from.z + ((to.z - from.z) * t));
// }
function Vec3MoveTowards(current, target, maxDistanceDelta) {
    let vector = Vec3Sub(target, current);
    let magnitude = Vector3.scalarLength(vector);
    if ((magnitude > maxDistanceDelta) && (magnitude != 0)) {
        return Vec3Add(current, (Vec3Mul(Vec3Div(vector, magnitude), maxDistanceDelta)));
    }
    return target;
}
exports.Vec3MoveTowards = Vec3MoveTowards;
function Vec3ToString(vec) {
    return string_1.UtilString.format("({0}, {1}, {2})", vec.x, vec.y, vec.z);
}
exports.Vec3ToString = Vec3ToString;
/**
 * 弧度转向量
 * @param    radians    弧度
 */
function getLineFromRadians(radians) {
    let x = Math.cos(radians);
    let y = Math.sin(radians);
    let dir = new Vector2(x, y);
    Vec2Normal(dir);
    return dir;
}
exports.getLineFromRadians = getLineFromRadians;
},{"./string":41}],40:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UtilNumber = void 0;
const string_1 = require("./string");
/**
 * @author Sun
 * @time 2019-08-11 18:54
 * @project SFramework_LayaAir
 * @description 数值工具类
 *
 */
class UtilNumber {
    /**
     * 保留小数点后几位
     */
    static toFixed(value, p) {
        return string_1.UtilString.toNumber(value.toFixed(p));
    }
    static toInt(value) {
        return Math.floor(value);
    }
    static isInt(value) {
        return Math.ceil(value) == value;
    }
    /**
     * 保留有效数值
     */
    static reserveNumber(num, size) {
        let str = String(num);
        let l = str.length;
        let p_index = str.indexOf(".");
        if (p_index < 0) {
            return num;
        }
        let ret = str.slice(0, p_index + 1);
        let lastNum = l - p_index;
        if (lastNum > size) {
            lastNum = size;
        }
        let lastStr = str.slice(p_index + 1, p_index + 1 + lastNum);
        return string_1.UtilString.toNumber(ret + lastStr);
    }
    /**
     * 保留有效数值，不够补0；注意返回的是字符串
     */
    static reserveNumberWithZero(num, size) {
        let str = String(num);
        let l = str.length;
        let p_index = str.indexOf(".");
        if (p_index < 0) { //是整数
            str += '.';
            for (let i = 0; i < size; ++i)
                str += '0';
            return str;
        }
        let ret = str.slice(0, p_index + 1);
        let lastNum = l - p_index - 1;
        if (lastNum > size) { //超过
            lastNum = size;
            let lastStr = str.slice(p_index + 1, p_index + 1 + lastNum);
            return ret + lastStr;
        }
        else if (lastNum < size) { //不足补0
            let diff = size - lastNum;
            for (let i = 0; i < diff; ++i)
                str += '0';
            return str;
        }
        else
            return str;
    }
    /**
     *
     */
    static formatThousandsNumber(num) {
        if (num < 1000000) {
            return num.toLocaleString();
        }
        else if (num < 1000000000) {
            let t = Math.floor(num / 1000);
            return t.toLocaleString() + "K";
        }
        else {
            let t = Math.floor(num / 1000000);
            return t.toLocaleString() + "M";
        }
    }
    /**
     *
     */
    static formatNumberShort(num, fixed = 0) {
        if (num < 1000) {
            return num;
        }
        else if (num < 1000000) {
            let t = Math.floor(num / 1000).toFixed(fixed);
            return t + "K";
        }
        else if (num < 1000000000) {
            let t = Math.floor(num / 1000000).toFixed(fixed);
            return t + "M";
        }
        else {
            let t = Math.floor(num / 1000000000).toFixed(fixed);
            return t.toLocaleString() + "G";
        }
    }
    /**
     * 科学计数法显示
     * @param num1
     */
    static bigNumberFormat(num, fixed = 2) {
        let exts = [
            '', 'K', "M", "G", "T", "P", "E", "Z", "Y", "AA",
            "BB", "CC", 'DD', 'EE', "FF", "GG", "HH", "II",
            "JJ", "KK", 'LL', 'MM', "NN", "OO", "PP", "QQ",
            "RR", "SS", 'TT', 'UU', "VV", "WW", "XX", "YY",
            "ZZ", "aa", 'bb', 'cc', "dd", "ee", "ff", "gg",
            "hh", "ii", 'gg', 'kk', "ll", "mm", "nn", "oo",
            "pp", "qq", 'rr', 'ss', "tt", "uu", "vv", "ww"
        ];
        let t1, t2;
        let n1 = num.indexOf("e+");
        if (n1 == -1)
            n1 = num.indexOf("E");
        if (n1 && n1 != -1) {
            t1 = parseFloat(num.substr(0, n1));
            t2 = parseInt(num.substr(n1 + 2));
            let ext = Math.floor(t2 / 3);
            let m = t2 % 3;
            return (t1 * Math.pow(10, m)).toFixed(fixed) + exts[ext];
        }
        return num;
    }
    /**
     * 数字相加
     */
    static bigNumberAdd(num1, num2) {
        let b = Number(num1) + Number(num2);
        return b.toExponential(4);
    }
    /**
     * 数字相减
     */
    static bigNumberSub(num1, num2) {
        let n1 = Number(num1);
        let n2 = Number(num2);
        if (n1 < n2) {
            return null;
        }
        return (n1 - n2).toExponential(4);
    }
    /**
     * 数字相乘法
     */
    static bigNumberMul(num1, num2) {
        return (Number(num1) * num2).toExponential(4);
    }
    /**
     * 数字相除
     */
    static bigNumberDiv(num1, num2) {
        return (Number(num1) / num2).toExponential(4);
    }
    /**
     * 两个科学计数相除
     */
    static bigNumberDivDounble(num1, num2) {
        return (Number(num1) / Number(num2));
    }
}
exports.UtilNumber = UtilNumber;
},{"./string":41}],41:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UtilString = void 0;
/**
 * @author Sun
 * @time 2019-08-11 18:55
 * @project SFramework_LayaAir
 * @description 字符串工具类
 *
 */
class UtilString {
    static get empty() {
        return "";
    }
    /**
     * 字符串是否有值
     */
    static isEmpty(s) {
        return (s != null && s.length > 0) ? false : true;
    }
    static toInt(str) {
        if (!str || str.length == 0)
            return 0;
        return parseInt(str);
    }
    static toNumber(str) {
        if (!str || str.length == 0)
            return 0;
        return parseFloat(str);
    }
    /**
     * 获取字符串真实长度,注：
     * 1.普通数组，字符占1字节；汉子占两个字节
     * 2.如果变成编码，可能计算接口不对
     */
    static getNumBytes(str) {
        let realLength = 0, len = str.length, charCode = -1;
        for (var i = 0; i < len; i++) {
            charCode = str.charCodeAt(i);
            if (charCode >= 0 && charCode <= 128)
                realLength += 1;
            else
                realLength += 2;
        }
        return realLength;
    }
    /**
     * 补零
     * @param str
     * @param len
     * @param dir 0-后；1-前
     * @return
     */
    static addZero(str, len, dir = 0) {
        let _str = "";
        let _len = str.length;
        let str_pre_zero = "";
        let str_end_zero = "";
        if (dir == 0)
            str_end_zero = "0";
        else
            str_pre_zero = "0";
        if (_len < len) {
            let i = 0;
            while (i < len - _len) {
                _str = str_pre_zero + _str + str_end_zero;
                ++i;
            }
            return _str + str;
        }
        return str;
    }
    /**
     * 去除左右空格
     * @param input
     * @return
     */
    static trim(input) {
        if (input == null) {
            return "";
        }
        return input.replace(/^\s+|\s+$""^\s+|\s+$/g, "");
    }
    /**
     * 去除左侧空格
     * @param input
     * @return
     */
    static trimLeft(input) {
        if (input == null) {
            return "";
        }
        return input.replace(/^\s+""^\s+/, "");
    }
    /**
     * 去除右侧空格
     * @param input
     * @return
     */
    static trimRight(input) {
        if (input == null) {
            return "";
        }
        return input.replace(/\s+$""\s+$/, "");
    }
    /**
     * 分钟与秒格式(如-> 40:15)
     * @param seconds 秒数
     * @return
     */
    static minuteFormat(seconds) {
        let min = Math.floor(seconds / 60);
        let sec = Math.floor(seconds % 60);
        let min_str = min < 10 ? ("0" + min.toString()) : (min.toString());
        let sec_str = sec < 10 ? ("0" + sec.toString()) : (sec.toString());
        return min_str + ":" + sec_str;
    }
    /**
     * 时分秒格式(如-> 05:32:20)
     * @param seconds(秒)
     * @return
     */
    static hourFormat(seconds) {
        let hour = Math.floor(seconds / 3600);
        let hour_str = hour < 10 ? ("0" + hour.toString()) : (hour.toString());
        return hour_str + ":" + UtilString.minuteFormat(seconds % 3600);
    }
    /**
     * 格式化字符串
     * @param str 需要格式化的字符串，【"杰卫，这里有{0}个苹果，和{1}个香蕉！", 5,10】
     * @param args 参数列表
     */
    static format(str, ...args) {
        for (let i = 0; i < args.length; i++) {
            str = str.replace(new RegExp("\\{" + i + "\\}", "gm"), args[i]);
        }
        return str;
    }
    /**
     * 以指定字符开始
     */
    static beginsWith(input, prefix) {
        return prefix == input.substring(0, prefix.length);
    }
    /**
     * 以指定字符结束
     */
    static endsWith(input, suffix) {
        return suffix == input.substring(input.length - suffix.length);
    }
    /**guid*/
    static getGUIDString() {
        let d = Date.now();
        if (window.performance && typeof window.performance.now === "function") {
            d += performance.now(); //use high-precision timer if available
        }
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
            let r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
    }
    /**
     * 首字母大学
     */
    static firstUpperCase(word) {
        return word.charAt(0).toUpperCase() + word.slice(1);
    }
    /**
     * 格式化下划线的单词
     */
    static formatDashWord(word, capFirst = false) {
        let first = true;
        let result = "";
        word.split('_').forEach((sec) => {
            if (first) {
                if (capFirst) {
                    result = UtilString.firstUpperCase(sec);
                }
                else {
                    result = sec;
                }
                first = false;
            }
            else {
                result = result + UtilString.firstUpperCase(sec);
            }
        });
        return result;
    }
    /**
     * 截取字符串
     * @param str 字符串
     * @param start 开始位置
     * @param end 结束位置
     */
    static substring(str, start, end) {
        return str.substring(start, end);
    }
    /**
     * 截取字符串
     * @param str 字符串
     * @param start 开始位置
     * @param long 截取长度
     */
    static substr(str, start, long) {
        return str.substr(start, long);
    }
    /**
     * 字符串转对象
     * @param str
     */
    static strToObject(str) {
        const strToObj = JSON.parse(str);
        return strToObj;
    }
    /**
     * 对象转字符串
     * @param str
     */
    static objToStr(obj) {
        const objToStr = JSON.stringify(obj);
        return objToStr;
    }
}
exports.UtilString = UtilString;
},{}],42:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UtilTime = void 0;
/**
 * @author Sun
 * @time 2019-08-09 19:18
 * @project SFramework_LayaAir
 * @description  时间工具
 *
 */
class UtilTime {
    static start() {
        this.m_StartTime = Laya.timer.currTimer;
    }
    /**两帧之间的时间间隔,单位秒*/
    static get deltaTime() {
        return Laya.timer.delta * 0.001;
    }
    /**固定两帧之间的时间间隔*/
    static get fixedDeltaTime() {
        return 0;
    }
    /**当前时间，相对xxxx年开始经过的毫秒数*/
    static get time() {
        return Laya.timer.currTimer;
    }
    /**游戏启动到现在的时间,单位毫秒*/
    static get timeSinceStartup() {
        return (Laya.timer.currTimer - this.m_StartTime);
    }
    /**游戏启动后，经过的帧数*/
    static get frameCount() {
        return Laya.timer.currFrame;
    }
    static get timeScale() {
        return Laya.timer.scale;
    }
    static set timeScale(scale) {
        Laya.timer.scale = scale;
    }
}
exports.UtilTime = UtilTime;
UtilTime.m_StartTime = 0;
},{}],43:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ui = void 0;
const view_base_1 = require("../framework/manager/ui/view-base");
const dialog_base_1 = require("../framework/manager/ui/dialog-base");
var DialogBase = dialog_base_1.CustomDialog.DialogBase;
var ViewBase = view_base_1.CustomView.ViewBase;
var REG = Laya.ClassUtils.regClass;
var ui;
(function (ui) {
    var view;
    (function (view) {
        var com;
        (function (com) {
            class day7sUI extends DialogBase {
                constructor() { super(); }
                createChildren() {
                    super.createChildren();
                    this.createView(day7sUI.uiView);
                }
            }
            day7sUI.uiView = { "type": "DialogBase", "props": { "width": 750, "height": 1334 }, "compId": 2, "loadList": [], "loadList3D": [] };
            com.day7sUI = day7sUI;
            REG("ui.view.com.day7sUI", day7sUI);
            class inviteUI extends DialogBase {
                constructor() { super(); }
                createChildren() {
                    super.createChildren();
                    this.createView(inviteUI.uiView);
                }
            }
            inviteUI.uiView = { "type": "DialogBase", "props": { "width": 750, "height": 1334 }, "compId": 2, "loadList": [], "loadList3D": [] };
            com.inviteUI = inviteUI;
            REG("ui.view.com.inviteUI", inviteUI);
            class lotteryUI extends DialogBase {
                constructor() { super(); }
                createChildren() {
                    super.createChildren();
                    this.createView(lotteryUI.uiView);
                }
            }
            lotteryUI.uiView = { "type": "DialogBase", "props": { "y": 0, "x": 0, "width": 750, "height": 1334 }, "compId": 2, "child": [{ "type": "Image", "props": { "y": 615, "x": 375, "skin": "res/com/img_lottery_border.png", "anchorY": 0.5, "anchorX": 0.5 }, "compId": 45, "child": [{ "type": "Image", "props": { "y": 314, "x": 314, "var": "imgContext", "skin": "res/com/img_lottery_content.png", "anchorY": 0.5, "anchorX": 0.5 }, "compId": 46 }, { "type": "Image", "props": { "y": -66, "x": 253, "skin": "res/com/img_zhen.png" }, "compId": 47 }, { "type": "Button", "props": { "y": 780, "x": 314, "width": 258, "var": "btnConfirm", "stateNum": 1, "skin": "res/main/effect/btn_common_2.png", "height": 130, "anchorY": 0.5, "anchorX": 0.5 }, "compId": 48, "child": [{ "type": "Label", "props": { "valign": "middle", "top": 0, "text": "抽奖", "right": 0, "left": 0, "fontSize": 60, "bottom": 0, "bold": true, "align": "center" }, "compId": 49 }] }, { "type": "Button", "props": { "y": -194, "x": 587, "var": "btnClose", "stateNum": 1, "skin": "res/main/effect/btn_close.png", "anchorY": 0.5, "anchorX": 0.5 }, "compId": 50 }] }], "animations": [{ "nodes": [{ "target": 34, "keyframes": { "x": [{ "value": 367, "tweenMethod": "linearNone", "tween": true, "target": 34, "key": "x", "index": 0 }, { "value": 367, "tweenMethod": "linearNone", "tween": true, "target": 34, "key": "x", "index": 10 }, { "value": 367, "tweenMethod": "linearNone", "tween": true, "target": 34, "key": "x", "index": 25 }], "visible": [{ "value": true, "tweenMethod": "linearNone", "tween": false, "target": 34, "key": "visible", "index": 0 }, { "value": false, "tweenMethod": "linearNone", "tween": false, "target": 34, "key": "visible", "index": 10 }, { "value": true, "tweenMethod": "linearNone", "tween": false, "target": 34, "key": "visible", "index": 15 }, { "value": false, "tweenMethod": "linearNone", "tween": false, "target": 34, "key": "visible", "index": 25 }, { "value": true, "tweenMethod": "linearNone", "tween": false, "target": 34, "key": "visible", "index": 30 }], "rotation": [{ "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 34, "key": "rotation", "index": 0 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 34, "key": "rotation", "index": 10 }, { "value": 7, "tweenMethod": "linearNone", "tween": true, "target": 34, "key": "rotation", "index": 15 }, { "value": 7, "tweenMethod": "linearNone", "tween": true, "target": 34, "key": "rotation", "index": 25 }, { "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 34, "key": "rotation", "index": 30 }] } }], "name": "idle", "id": 1, "frameRate": 24, "action": 0 }], "loadList": ["res/com/img_lottery_border.png", "res/com/img_lottery_content.png", "res/com/img_zhen.png", "res/main/effect/btn_common_2.png", "res/main/effect/btn_close.png"], "loadList3D": [] };
            com.lotteryUI = lotteryUI;
            REG("ui.view.com.lotteryUI", lotteryUI);
            class rankUI extends DialogBase {
                constructor() { super(); }
                createChildren() {
                    super.createChildren();
                    this.createView(rankUI.uiView);
                }
            }
            rankUI.uiView = { "type": "DialogBase", "props": { "width": 750, "height": 1334 }, "compId": 2, "child": [{ "type": "WXOpenDataViewer", "props": { "y": 381, "x": 116, "width": 524, "mouseThrough": true, "iconSign": "wx", "height": 858, "runtime": "Laya.WXOpenDataViewer" }, "compId": 3 }], "loadList": [], "loadList3D": [] };
            com.rankUI = rankUI;
            REG("ui.view.com.rankUI", rankUI);
            class shopUI extends DialogBase {
                constructor() { super(); }
                createChildren() {
                    super.createChildren();
                    this.createView(shopUI.uiView);
                }
            }
            shopUI.uiView = { "type": "DialogBase", "props": { "width": 750, "mouseThrough": true, "height": 1334 }, "compId": 2, "loadList": [], "loadList3D": [] };
            com.shopUI = shopUI;
            REG("ui.view.com.shopUI", shopUI);
        })(com = view.com || (view.com = {}));
    })(view = ui.view || (ui.view = {}));
})(ui = exports.ui || (exports.ui = {}));
(function (ui) {
    var view;
    (function (view) {
        var main;
        (function (main) {
            class bgUI extends ViewBase {
                constructor() { super(); }
                createChildren() {
                    super.createChildren();
                    this.createView(bgUI.uiView);
                }
            }
            bgUI.uiView = { "type": "ViewBase", "props": { "width": 750, "height": 1334 }, "compId": 2, "child": [{ "type": "Image", "props": { "var": "imgBg", "top": 0, "skin": "res/main/bg/bg.png", "right": 0, "left": 0, "bottom": 0 }, "compId": 5 }], "loadList": ["res/main/bg/bg.png"], "loadList3D": [] };
            main.bgUI = bgUI;
            REG("ui.view.main.bgUI", bgUI);
            class d3UI extends ViewBase {
                constructor() { super(); }
                createChildren() {
                    super.createChildren();
                    this.createView(d3UI.uiView);
                }
            }
            d3UI.uiView = { "type": "ViewBase", "props": { "width": 750, "height": 1334 }, "compId": 2, "loadList": [], "loadList3D": [] };
            main.d3UI = d3UI;
            REG("ui.view.main.d3UI", d3UI);
            class effectUI extends ViewBase {
                constructor() { super(); }
                createChildren() {
                    super.createChildren();
                    this.createView(effectUI.uiView);
                }
            }
            effectUI.uiView = { "type": "ViewBase", "props": { "width": 750, "height": 1334 }, "compId": 2, "child": [{ "type": "Image", "props": { "y": 64, "x": 72, "width": 213, "skin": "res/main/effect/image_status.png", "height": 46 }, "compId": 3 }, { "type": "Image", "props": { "y": 64, "x": 459, "width": 213, "skin": "res/main/effect/image_status.png", "height": 46 }, "compId": 4 }, { "type": "Image", "props": { "y": 48, "x": 403, "skin": "res/main/effect/img_diamond.png" }, "compId": 5 }, { "type": "Image", "props": { "y": 44, "x": 30, "skin": "res/main/effect/img_glod.png" }, "compId": 6 }, { "type": "Button", "props": { "y": 282, "x": 375, "width": 207, "var": "btnLucky", "stateNum": 1, "skin": "res/main/effect/btn_common_1.png", "height": 104, "anchorY": 0.5, "anchorX": 0.5 }, "compId": 7, "child": [{ "type": "Label", "props": { "valign": "middle", "top": 0, "text": "转盘", "right": 0, "left": 0, "fontSize": 40, "bottom": 0, "bold": true, "align": "center" }, "compId": 11 }] }, { "type": "Button", "props": { "y": 439, "x": 375, "width": 207, "var": "btnRank", "stateNum": 1, "skin": "res/main/effect/btn_common_2.png", "height": 104, "anchorY": 0.5, "anchorX": 0.5 }, "compId": 8, "child": [{ "type": "Label", "props": { "valign": "middle", "top": 0, "text": "排行", "right": 0, "left": 0, "fontSize": 40, "bottom": 0, "bold": true, "align": "center" }, "compId": 12 }] }, { "type": "Button", "props": { "y": 606, "x": 375, "width": 207, "var": "btnInvite", "stateNum": 1, "skin": "res/main/effect/btn_common_3.png", "height": 104, "anchorY": 0.5, "anchorX": 0.5 }, "compId": 9, "child": [{ "type": "Label", "props": { "valign": "middle", "top": 0, "text": "邀请", "right": 0, "left": 0, "fontSize": 40, "bottom": 0, "bold": true, "align": "center" }, "compId": 13 }] }, { "type": "Button", "props": { "y": 776, "x": 375, "width": 207, "var": "btnSetting", "stateNum": 1, "skin": "res/main/effect/btn_common_4.png", "height": 104, "anchorY": 0.5, "anchorX": 0.5 }, "compId": 10, "child": [{ "type": "Label", "props": { "valign": "middle", "top": 0, "text": "设置", "right": 0, "left": 0, "fontSize": 40, "bottom": 0, "bold": true, "align": "center" }, "compId": 14 }] }], "loadList": ["res/main/effect/image_status.png", "res/main/effect/img_diamond.png", "res/main/effect/img_glod.png", "res/main/effect/btn_common_1.png", "res/main/effect/btn_common_2.png", "res/main/effect/btn_common_3.png", "res/main/effect/btn_common_4.png"], "loadList3D": [] };
            main.effectUI = effectUI;
            REG("ui.view.main.effectUI", effectUI);
            class gameUI extends ViewBase {
                constructor() { super(); }
                createChildren() {
                    super.createChildren();
                    this.createView(gameUI.uiView);
                }
            }
            gameUI.uiView = { "type": "ViewBase", "props": { "width": 750, "height": 1334 }, "compId": 2, "animations": [{ "nodes": [{ "target": 413, "keyframes": { "visible": [{ "value": false, "tweenMethod": "linearNone", "tween": false, "target": 413, "key": "visible", "index": 0 }, { "value": true, "tweenMethod": "linearNone", "tween": false, "target": 413, "key": "visible", "index": 2 }, { "value": false, "tweenMethod": "linearNone", "tween": false, "target": 413, "key": "visible", "index": 4 }, { "value": true, "tweenMethod": "linearNone", "tween": false, "target": 413, "key": "visible", "index": 6 }, { "value": false, "tweenMethod": "linearNone", "tween": false, "target": 413, "key": "visible", "index": 8 }, { "value": true, "tweenMethod": "linearNone", "tween": false, "target": 413, "key": "visible", "index": 10 }, { "value": false, "tweenMethod": "linearNone", "tween": false, "target": 413, "key": "visible", "index": 12 }] } }, { "target": 324, "keyframes": { "visible": [{ "value": true, "tweenMethod": "linearNone", "tween": false, "target": 324, "key": "visible", "index": 0 }, { "value": false, "tweenMethod": "linearNone", "tween": false, "target": 324, "key": "visible", "index": 2 }, { "value": true, "tweenMethod": "linearNone", "tween": false, "target": 324, "key": "visible", "index": 4 }, { "value": false, "tweenMethod": "linearNone", "tween": false, "target": 324, "key": "visible", "index": 6 }, { "value": true, "tweenMethod": "linearNone", "tween": false, "target": 324, "key": "visible", "index": 8 }, { "value": false, "tweenMethod": "linearNone", "tween": false, "target": 324, "key": "visible", "index": 10 }, { "value": true, "tweenMethod": "linearNone", "tween": false, "target": 324, "key": "visible", "index": 12 }] } }], "name": "ani_grap", "id": 29, "frameRate": 24, "action": 0 }, { "nodes": [{ "target": 468, "keyframes": { "rotation": [{ "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 468, "key": "rotation", "index": 0 }, { "value": 360, "tweenMethod": "linearNone", "tween": true, "target": 468, "key": "rotation", "index": 200 }], "alpha": [{ "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 468, "key": "alpha", "index": 0 }, { "value": 0.5, "tweenMethod": "linearNone", "tween": true, "target": 468, "key": "alpha", "index": 50 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 468, "key": "alpha", "index": 100 }, { "value": 0.5, "tweenMethod": "linearNone", "tween": true, "target": 468, "key": "alpha", "index": 150 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 468, "key": "alpha", "index": 200 }] } }, { "target": 469, "keyframes": { "rotation": [{ "value": 0, "tweenMethod": "linearNone", "tween": true, "target": 469, "key": "rotation", "index": 0 }, { "value": -360, "tweenMethod": "linearNone", "tween": true, "target": 469, "key": "rotation", "index": 200 }], "alpha": [{ "value": 0.5, "tweenMethod": "linearNone", "tween": true, "target": 469, "key": "alpha", "index": 0 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 469, "key": "alpha", "index": 50 }, { "value": 0.5, "tweenMethod": "linearNone", "tween": true, "target": 469, "key": "alpha", "index": 100 }, { "value": 1, "tweenMethod": "linearNone", "tween": true, "target": 469, "key": "alpha", "index": 150 }] } }], "name": "ani_luckBL", "id": 30, "frameRate": 24, "action": 0 }], "loadList": [], "loadList3D": [] };
            main.gameUI = gameUI;
            REG("ui.view.main.gameUI", gameUI);
            class loadingUI extends ViewBase {
                constructor() { super(); }
                createChildren() {
                    super.createChildren();
                    this.createView(loadingUI.uiView);
                }
            }
            loadingUI.uiView = { "type": "ViewBase", "props": { "width": 750, "height": 1334 }, "compId": 2, "child": [{ "type": "Image", "props": { "y": 0, "x": 0, "var": "img_bg", "top": 0, "skin": "res/loading/img_loading_bg.png", "right": 0, "left": 0, "bottom": 0 }, "compId": 3 }, { "type": "Box", "props": { "y": 0, "x": 0, "width": 493, "var": "box_btm", "pivotY": 149, "pivotX": 249, "height": 149, "centerX": 0, "bottom": 0 }, "compId": 5, "child": [{ "type": "ProgressBar", "props": { "y": 20, "x": 247, "var": "pro_Loading", "skin": "res/loading/progress_loading.png", "pivotY": 12, "pivotX": 175 }, "compId": 6 }, { "type": "Label", "props": { "y": 20, "width": 238, "var": "lblLoading", "valign": "middle", "text": "100%", "strokeColor": "#ffffff", "stroke": 4, "pivotY": 16, "pivotX": 119, "height": 32, "fontSize": 26, "font": "Arial", "color": "#592222", "centerX": 0, "bold": true, "align": "center" }, "compId": 7 }, { "type": "Image", "props": { "y": 85, "x": 247, "width": 493, "skin": "res/loading/img_8r.png", "pivotY": 20, "pivotX": 247, "height": 39 }, "compId": 8 }, { "type": "Label", "props": { "y": 128, "x": 247, "width": 283, "var": "lbl_p", "valign": "middle", "text": "Powered by LayaAir Engine", "pivotY": 21, "pivotX": 142, "height": 42, "fontSize": 18, "color": "#ffffff", "bold": true, "align": "center" }, "compId": 9 }] }], "loadList": ["res/loading/img_loading_bg.png", "res/loading/progress_loading.png", "res/loading/img_8r.png"], "loadList3D": [] };
            main.loadingUI = loadingUI;
            REG("ui.view.main.loadingUI", loadingUI);
        })(main = view.main || (view.main = {}));
    })(view = ui.view || (ui.view = {}));
})(ui = exports.ui || (exports.ui = {}));
},{"../framework/manager/ui/dialog-base":27,"../framework/manager/ui/view-base":29}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL2xheWFpZGRlLzIuMTMuMS9yZXNvdXJjZXMvYXBwL25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvTWFpbi50cyIsInNyYy9jbGllbnQvc2NlbmUvbWFpbi1zY2VuZS50cyIsInNyYy9jbGllbnQvc2V0dGluZy9nYW1lU2V0dGluZy50cyIsInNyYy9jbGllbnQvdmlldy9jb21wb25lbnQtdmlldy9sb3R0ZXJ5LXZpZXcudHMiLCJzcmMvY2xpZW50L3ZpZXcvbGF5ZXItdmlldy9iZy12aWV3LnRzIiwic3JjL2NsaWVudC92aWV3L2xheWVyLXZpZXcvZDMtdmlldy50cyIsInNyYy9jbGllbnQvdmlldy9sYXllci12aWV3L2VmZmVjdC12aWV3LnRzIiwic3JjL2NsaWVudC92aWV3L2xheWVyLXZpZXcvZ2FtZS12aWV3LnRzIiwic3JjL2NsaWVudC92aWV3L2xheWVyLXZpZXcvbG9hZGluZy12aWV3LnRzIiwic3JjL2ZyYW1ld29yay9jb3JlL2xvZy50cyIsInNyYy9mcmFtZXdvcmsvY29yZS9vYmplY3QtcG9vbC50cyIsInNyYy9mcmFtZXdvcmsvY29yZS9zaW5nbGV0b24udHMiLCJzcmMvZnJhbWV3b3JrL2NvcmUvdGltZS1kZWxheS50cyIsInNyYy9mcmFtZXdvcmsvbWFuYWdlci9kYXRhL2RhdGEtbWFuYWdlci50cyIsInNyYy9mcmFtZXdvcmsvbWFuYWdlci9ldmVudC9ldmVudC1kYXRhLnRzIiwic3JjL2ZyYW1ld29yay9tYW5hZ2VyL2V2ZW50L2V2ZW50LW1hbmFnZXIudHMiLCJzcmMvZnJhbWV3b3JrL21hbmFnZXIvZXZlbnQvZXZlbnQtbm9kZS50cyIsInNyYy9mcmFtZXdvcmsvbWFuYWdlci9qc29uL2pzb24tbWFuYWdlci50cyIsInNyYy9mcmFtZXdvcmsvbWFuYWdlci9qc29uL2pzb24tdGVtcGxhdGUudHMiLCJzcmMvZnJhbWV3b3JrL21hbmFnZXIvcmVzL3Jlcy1ncm91cC50cyIsInNyYy9mcmFtZXdvcmsvbWFuYWdlci9yZXMvcmVzLWl0ZW0udHMiLCJzcmMvZnJhbWV3b3JrL21hbmFnZXIvcmVzL3Jlcy1tYW5hZ2VyLnRzIiwic3JjL2ZyYW1ld29yay9tYW5hZ2VyL3NvdW5kL3NvdW5kLW1hbmFnZXIudHMiLCJzcmMvZnJhbWV3b3JrL21hbmFnZXIvdGltZXIvdGltZXItZW50aXR5LnRzIiwic3JjL2ZyYW1ld29yay9tYW5hZ2VyL3RpbWVyL3RpbWVyLWludGVydmFsLnRzIiwic3JjL2ZyYW1ld29yay9tYW5hZ2VyL3RpbWVyL3RpbWVyLW1hbmFnZXIudHMiLCJzcmMvZnJhbWV3b3JrL21hbmFnZXIvdWkvZGlhbG9nLWJhc2UudHMiLCJzcmMvZnJhbWV3b3JrL21hbmFnZXIvdWkvc2NlbmUtYmFzZS50cyIsInNyYy9mcmFtZXdvcmsvbWFuYWdlci91aS92aWV3LWJhc2UudHMiLCJzcmMvZnJhbWV3b3JrL3J1bnRpbWUvZW5naW5lLnRzIiwic3JjL2ZyYW1ld29yay9zZXR0aW5nL2NvbmZpZy50cyIsInNyYy9mcmFtZXdvcmsvc2V0dGluZy9lbnVtLnRzIiwic3JjL2ZyYW1ld29yay9zdHJ1Y3R1cmUvZGljdGlvbmFyeS50cyIsInNyYy9mcmFtZXdvcmsvdXRpbC9hcnJheS50cyIsInNyYy9mcmFtZXdvcmsvdXRpbC9kaWN0LnRzIiwic3JjL2ZyYW1ld29yay91dGlsL2Rpc3BsYXkudHMiLCJzcmMvZnJhbWV3b3JrL3V0aWwvbG9hZDNkLnRzIiwic3JjL2ZyYW1ld29yay91dGlsL21hdGgudHMiLCJzcmMvZnJhbWV3b3JrL3V0aWwvbWF0aDNkLnRzIiwic3JjL2ZyYW1ld29yay91dGlsL251bWJlci50cyIsInNyYy9mcmFtZXdvcmsvdXRpbC9zdHJpbmcudHMiLCJzcmMvZnJhbWV3b3JrL3V0aWwvdGltZS50cyIsInNyYy91aS9sYXlhTWF4VUkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FDVEEsdURBQW9EO0FBR3BEOzs7Ozs7R0FNRztBQUNILE1BQU0sSUFBSTtJQUNUO1FBRUMsZUFBTSxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztJQUNoQixDQUFDO0NBQ0Q7QUFDRCxPQUFPO0FBQ1AsSUFBSSxJQUFJLEVBQUUsQ0FBQzs7Ozs7QUNsQlgsc0VBQWtFO0FBQ2xFLElBQU8sT0FBTyxHQUFHLHdCQUFXLENBQUMsT0FBTyxDQUFDO0FBSXBDOzs7Ozs7RUFNRTtBQUNILE1BQWEsU0FBVSxTQUFRLE9BQU87SUFDbEM7UUFDSSxLQUFLLEVBQUUsQ0FBQztRQUVSLElBQUksQ0FBQyxXQUFXO2FBQ1gsR0FBRyxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbEQsQ0FBQztDQUNKO0FBUEQsOEJBT0M7Ozs7O0FDbkJELDhEQUEyRDtBQUMzRCwyREFBcUU7QUFDckUsOEVBQXdFO0FBQ3hFLHVEQUE0RDtBQUc1RDs7Ozs7R0FLRztBQUNILE1BQWEsV0FBWSxTQUFRLHFCQUFTO0lBUXRDO1FBRUksS0FBSyxFQUFFLENBQUM7SUFDWixDQUFDO0lBUk0sTUFBTSxLQUFLLENBQUM7UUFDZixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVE7WUFBRSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksV0FBVyxFQUFFLENBQUM7UUFDdEQsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ3pCLENBQUM7SUFPRCxJQUFJO1FBQ0Msa0NBQWtDO1FBQ25DLG1CQUFVLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixHQUFHO1lBQzVCLElBQUksNEJBQVksQ0FBQywwQkFBMEIsRUFBRSxxQkFBYyxDQUFDLE1BQU0sQ0FBQztZQUNuRSxJQUFJLDRCQUFZLENBQUMseUJBQXlCLEVBQUUscUJBQWMsQ0FBQyxLQUFLLENBQUM7WUFDakUsSUFBSSw0QkFBWSxDQUFDLDJCQUEyQixFQUFFLHFCQUFjLENBQUMsT0FBTyxDQUFDO1lBQ3JFLElBQUksNEJBQVksQ0FBQyw2QkFBNkIsRUFBRSxxQkFBYyxDQUFDLE9BQU8sQ0FBQztTQUMxRSxDQUFDO1FBQ0YsZUFBZTtRQUNmLGtCQUFTLENBQUMsQ0FBQyxDQUFDLGNBQWM7YUFDckIsR0FBRyxDQUFDLGdDQUFnQyxFQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO2FBQ3ZELEdBQUcsQ0FBQyxrQ0FBa0MsRUFBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQzthQUN6RCxHQUFHLENBQUMsd0JBQXdCLEVBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNyRCxVQUFVO1FBQ1Ysa0JBQVMsQ0FBQyxDQUFDLENBQUMsY0FBYzthQUNyQixHQUFHLENBQUMsaUNBQWlDLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7YUFDekQsR0FBRyxDQUFDLHlCQUF5QixFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO2FBQ2pELEdBQUcsQ0FBQyxnQ0FBZ0MsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQzthQUN4RCxHQUFHLENBQUMsaUNBQWlDLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7YUFDekQsR0FBRyxDQUFDLG9CQUFvQixFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUE7SUFDckQsQ0FBQzs7QUFqQ0wsa0NBbUNDO0FBakNrQixvQkFBUSxHQUFnQixJQUFJLENBQUM7Ozs7O0FDZGhELHFEQUEyQztBQUMzQyxJQUFPLFNBQVMsR0FBSSxjQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUM7QUFFMUMsK0VBQTJFO0FBQzNFLDBEQUFpRTtBQUNqRSx1REFBd0Q7QUFDeEQscURBQWtEO0FBR2xEOzs7Ozs7R0FNRztBQUNILE1BQWEsV0FBWSxTQUFRLFNBQVM7SUFzQnhDO1FBQ0ksS0FBSyxFQUFFLENBQUM7UUFyQloseUZBQXlGO1FBRXpGLGFBQWE7UUFDTCxjQUFTLEdBQVUsQ0FBQyxDQUFDO1FBQzdCLGVBQWU7UUFDUCxnQkFBVyxHQUFPLElBQUksQ0FBQztJQWlCL0IsQ0FBQztJQVBNLE1BQU0sS0FBSyxDQUFDO1FBQ2YsSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFFLElBQUk7WUFBRSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksV0FBVyxFQUFFLENBQUM7UUFDM0QsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ3pCLENBQUM7SUFRRCxPQUFPO1FBQ0gsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNoQixDQUFDO0lBRUQsS0FBSztRQUNELEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNsQixDQUFDO0lBRUQsOEZBQThGO0lBQzlGLDJGQUEyRjtJQUMzRix5RkFBeUY7SUFHakYsSUFBSTtRQUVSLElBQUksQ0FBQyxXQUFXLEdBQUcsMEJBQVcsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLHFCQUFjLENBQUMsT0FBTyxDQUFDLENBQUE7UUFDakUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUMsSUFBSSxFQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBR0QsOEZBQThGO0lBQzlGLDJGQUEyRjtJQUMzRix5RkFBeUY7SUFFekYsVUFBVTtRQUVOLElBQUksTUFBTSxHQUFHLGVBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRXBDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDekIsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsSUFBRSxNQUFNLElBQUUsTUFBTSxJQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFDO2dCQUMzRSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO2dCQUM1QyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNsQixNQUFNO2FBQ1Q7U0FDSDtJQUNMLENBQUM7SUFHRCw4RkFBOEY7SUFDOUYsMkZBQTJGO0lBQzNGLDBGQUEwRjtJQUVsRixTQUFTLENBQUMsU0FBaUIsQ0FBQztRQUVoQyxVQUFVO1FBQ1YsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQzlCLFFBQVE7UUFDUixJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDckMsTUFBTTtRQUNOLElBQUksTUFBTSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLE1BQU0sQ0FBQztRQUVsRCxJQUFJLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDcEIsSUFBSSxNQUFNLEdBQUcsR0FBRyxHQUFHLE1BQU0sQ0FBQztRQUMxQixJQUFJLE1BQU0sR0FBRyxDQUFDLEdBQUcsR0FBRyxNQUFNLEdBQUcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxlQUFRLENBQUMsWUFBWSxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFFNUYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDO1FBQzdCLElBQUksV0FBVyxHQUFHLElBQUksR0FBRyxNQUFNLENBQUM7UUFDaEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUMzQixRQUFRLEVBQUUsV0FBVztTQUN4QixFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsR0FBRSxFQUFFO1lBRTVELElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUNwQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDN0IsU0FBRyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEdBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRTlCLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDekIsQ0FBQztDQU1GO0FBckdELGtDQXFHQzs7Ozs7QUNySEQscURBQTJDO0FBQzNDLElBQU8sSUFBSSxHQUFJLGNBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztBQUlqQzs7Ozs7O0dBTUc7QUFDSCxNQUFhLE1BQU8sU0FBUSxJQUFJO0lBa0I1QjtRQUNJLEtBQUssRUFBRSxDQUFDO0lBQ1osQ0FBQztJQVJNLE1BQU0sS0FBSyxDQUFDO1FBQ2YsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRO1lBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLE1BQU0sRUFBRSxDQUFDO1FBQ2pELE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN6QixDQUFDO0lBT0QsT0FBTztRQUNILEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUNoQixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDWixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFFcEIsQ0FBQztJQUdEOztPQUVHO0lBQ0ksSUFBSTtRQUVQLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUVoQixTQUFTO1FBQ1QsMENBQTBDO1FBRTFDLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUU7WUFDdkIsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1NBQ25CO0lBQ0wsQ0FBQztJQUVEOztPQUVHO0lBQ0gsUUFBUTtRQUVKLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7UUFDOUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztJQUNwQyxDQUFDO0lBR0QsOEZBQThGO0lBQzlGLDJGQUEyRjtJQUMzRiwwRkFBMEY7SUFFMUYsbUJBQW1CO0lBQ1gsUUFBUTtJQUdoQixDQUFDO0lBR0QsOEZBQThGO0lBQzlGLDJGQUEyRjtJQUMzRix3RkFBd0Y7SUFFeEYsbUJBQW1CO0lBQ1gsUUFBUTtJQUdoQixDQUFDO0lBRUQsOEZBQThGO0lBQzlGLDJGQUEyRjtJQUMzRiwwRkFBMEY7SUFJMUYsOEZBQThGO0lBQzlGLDJGQUEyRjtJQUMzRix5RkFBeUY7SUFFekY7O09BRUc7SUFDTyxNQUFNLENBQUMsSUFBYztJQUUvQixDQUFDO0NBSUo7QUEvRkQsd0JBK0ZDOzs7OztBQ3hHRCxxREFBMkM7QUFDM0MsSUFBTyxJQUFJLEdBQUksY0FBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO0FBRWpDLDJEQUE0RDtBQUM1RCw4REFBNkQ7QUFJN0Q7Ozs7OztHQU1HO0FBQ0gsTUFBYSxNQUFPLFNBQVEsSUFBSTtJQXFCNUI7UUFDSSxLQUFLLEVBQUUsQ0FBQztJQUNaLENBQUM7SUFWTSxNQUFNLEtBQUssQ0FBQztRQUNmLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUTtZQUFFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxNQUFNLEVBQUUsQ0FBQztRQUNqRCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekIsQ0FBQztJQVNELE9BQU87UUFDSCxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ1osSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBRXBCLENBQUM7SUFHRDs7T0FFRztJQUNJLElBQUk7UUFFUCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFaEIsU0FBUztRQUNULDBDQUEwQztJQUM5QyxDQUFDO0lBRUQ7O09BRUc7SUFDSCxTQUFTO1FBQ0wsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ25CLENBQUM7SUFHRDs7T0FFRztJQUNILFFBQVE7UUFFSixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQzlCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7SUFDcEMsQ0FBQztJQUdELDhGQUE4RjtJQUM5RiwyRkFBMkY7SUFDM0YsMEZBQTBGO0lBRTFGLG1CQUFtQjtJQUNYLFFBQVE7SUFHaEIsQ0FBQztJQUVELGtCQUFrQjtJQUNWLE9BQU87SUFHZixDQUFDO0lBSUQsOEZBQThGO0lBQzlGLDJGQUEyRjtJQUMzRix3RkFBd0Y7SUFFeEYsbUJBQW1CO0lBQ1gsUUFBUTtJQUdoQixDQUFDO0lBRUQsOEZBQThGO0lBQzlGLDJGQUEyRjtJQUMzRiwwRkFBMEY7SUFJMUYsOEZBQThGO0lBQzlGLDJGQUEyRjtJQUMzRiw0RkFBNEY7SUFFNUY7O09BRUc7SUFDSSxXQUFXLENBQUMsSUFBSSxFQUFDLFFBQVE7UUFFNUIsbUJBQVUsQ0FBQyxTQUFTLENBQUMsaUJBQVEsQ0FBQyxDQUFDLENBQUMsU0FBUyxFQUFDLElBQUksRUFBQyxRQUFRLENBQUMsQ0FBQztJQUM3RCxDQUFDO0lBRUQsOEZBQThGO0lBQzlGLDJGQUEyRjtJQUMzRix5RkFBeUY7SUFFekY7O09BRUc7SUFDTyxNQUFNLENBQUMsSUFBYztJQUUvQixDQUFDO0NBSUo7QUF6SEQsd0JBeUhDOzs7OztBQzNJRCxxREFBeUM7QUFDekMsSUFBTyxRQUFRLEdBQUksY0FBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO0FBRXpDLElBQU8sT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7QUFHOUIsaUVBQTZEO0FBRzdELE1BQWEsVUFBVyxTQUFRLFFBQVE7SUFrQnBDO1FBQ0ksS0FBSyxFQUFFLENBQUM7SUFDWixDQUFDO0lBVk0sTUFBTSxLQUFLLENBQUM7UUFDZixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVE7WUFBRSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksVUFBVSxFQUFFLENBQUM7UUFDckQsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ3pCLENBQUM7SUFTRCxPQUFPO1FBQ0gsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNaLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUVwQixDQUFDO0lBR0Q7O09BRUc7SUFDSSxJQUFJO1FBRVAsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBRWhCLFNBQVM7UUFDVCwwQ0FBMEM7UUFFMUMsSUFBSSxPQUFPLENBQUMsUUFBUSxFQUFFO1lBQ2xCLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztTQUNuQjtJQUVMLENBQUM7SUFFRDs7T0FFRztJQUNILFNBQVM7UUFDTCxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDbkIsQ0FBQztJQUdEOztPQUVHO0lBQ0gsUUFBUTtRQUVKLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7UUFDOUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztJQUNwQyxDQUFDO0lBR0QsOEZBQThGO0lBQzlGLDJGQUEyRjtJQUMzRiwwRkFBMEY7SUFFMUYsbUJBQW1CO0lBQ1gsUUFBUTtRQUVaLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFDLElBQUksRUFBQyxHQUFFLEVBQUU7WUFDeEMsSUFBSSxJQUFJLEdBQUcsMEJBQVcsQ0FBQyxDQUFDLENBQUM7WUFDekIsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3RCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGtCQUFrQjtJQUNWLE9BQU87SUFHZixDQUFDO0lBSUQsOEZBQThGO0lBQzlGLDJGQUEyRjtJQUMzRix3RkFBd0Y7SUFFeEYsbUJBQW1CO0lBQ1gsUUFBUTtJQUdoQixDQUFDO0lBRUQsOEZBQThGO0lBQzlGLDJGQUEyRjtJQUMzRiwwRkFBMEY7SUFLMUYsOEZBQThGO0lBQzlGLDJGQUEyRjtJQUMzRix5RkFBeUY7SUFFekY7O09BRUc7SUFDTyxNQUFNLENBQUMsSUFBYztJQUUvQixDQUFDO0NBSUo7QUFuSEQsZ0NBbUhDOzs7OztBQzVIRCxxREFBeUM7QUFDekMsSUFBTyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztBQUM5QixJQUFPLE1BQU0sR0FBRyxjQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7QUFHcEM7Ozs7OztHQU1HO0FBQ0gsTUFBYSxRQUFTLFNBQVEsTUFBTTtJQWtCaEM7UUFDSSxLQUFLLEVBQUUsQ0FBQztJQUNaLENBQUM7SUFWTSxNQUFNLEtBQUssQ0FBQztRQUNmLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUTtZQUFFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxRQUFRLEVBQUUsQ0FBQztRQUNuRCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekIsQ0FBQztJQVNELE9BQU87UUFDSCxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ1osSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ3BCLENBQUM7SUFFRDs7T0FFRztJQUNJLElBQUk7UUFFUCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFaEIsU0FBUztRQUNULDBDQUEwQztRQUUxQyxJQUFJLE9BQU8sQ0FBQyxRQUFRLEVBQUU7WUFDbEIsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1NBQ25CO0lBQ0wsQ0FBQztJQUVEOztPQUVHO0lBQ0gsU0FBUztRQUNMLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNuQixDQUFDO0lBR0Q7O09BRUc7SUFDSCxRQUFRO0lBR1IsQ0FBQztJQUdELDhGQUE4RjtJQUM5RiwyRkFBMkY7SUFDM0YsMEZBQTBGO0lBRTFGLG1CQUFtQjtJQUNYLFFBQVE7SUFFaEIsQ0FBQztJQUVELGtCQUFrQjtJQUNWLE9BQU87SUFHZixDQUFDO0lBSUQsOEZBQThGO0lBQzlGLDJGQUEyRjtJQUMzRix3RkFBd0Y7SUFFeEYsbUJBQW1CO0lBQ1gsUUFBUTtJQUdoQixDQUFDO0lBRUQsOEZBQThGO0lBQzlGLDJGQUEyRjtJQUMzRiwwRkFBMEY7SUFJMUYsOEZBQThGO0lBQzlGLDJGQUEyRjtJQUMzRix5RkFBeUY7SUFFekY7O09BRUc7SUFDTyxNQUFNLENBQUMsSUFBYztJQUUvQixDQUFDO0NBTUo7QUE1R0QsNEJBNEdDO0FBRUQsTUFBTSxPQUFPO0lBRVQsWUFBWSxPQUFlO1FBQ3ZCLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDO0lBQzVCLENBQUM7SUFDRCxLQUFLO1FBQ0QsT0FBTyxTQUFTLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUNyQyxDQUFDO0NBQ0o7Ozs7O0FDbElELHFEQUF5QztBQUN6QyxJQUFPLFNBQVMsR0FBRyxjQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7QUFFMUMsdUNBQW1DO0FBQ25DLHVDQUFtQztBQUVuQyw4REFBb0Y7QUFFcEYsMkRBQTREO0FBQzVELDBEQUFnRTtBQUNoRSwyQ0FBdUM7QUFDdkMsK0NBQTJDO0FBQzNDLDRFQUF3RTtBQUV4RSw0RUFBd0U7QUFLeEUsTUFBYSxXQUFZLFNBQVEsU0FBUztJQUV0QywwRkFBMEY7SUFHMUYsOEZBQThGO0lBQzlGLDJGQUEyRjtJQUMzRiwwRkFBMEY7SUFFMUY7UUFDSSxLQUFLLEVBQUUsQ0FBQztJQUNaLENBQUM7SUFFRCxPQUFPO1FBQ0gsbUJBQW1CO1FBQ25CLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNaLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNwQixDQUFDO0lBRUM7O0tBRUM7SUFDSCxPQUFPO1FBRUgsZUFBZTtRQUNmLHdCQUFVLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FDbEIsa0JBQVMsQ0FBQyxDQUFDLENBQUMsY0FBYyxFQUMxQixJQUFJLHNCQUFTLENBQUMsSUFBSSxFQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFDbkMsSUFBSSxzQkFBUyxDQUFDLElBQUksRUFBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQ3ZDLENBQUM7UUFDRixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksR0FBRyxVQUFVLENBQUM7SUFDdEMsQ0FBQztJQUVEOzs7T0FHRztJQUNILFdBQVcsQ0FBQyxPQUFnQjtRQUV4QixNQUFNO1FBQ04sSUFBSSxNQUFNLEdBQUcsZ0JBQU0sQ0FBQyxDQUFDLENBQUM7UUFDdEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFNUIsSUFBRyxtQkFBVSxDQUFDLENBQUMsQ0FBQyxTQUFTLElBQUUsb0JBQWEsQ0FBQyxJQUFJLEVBQzdDO1lBQ0ksTUFBTTtZQUNOLElBQUksTUFBTSxHQUFHLGdCQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ3RCLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzVCLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUMxQzthQUFJO1lBQ0QsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1NBQ25CO0lBQ0wsQ0FBQztJQUNPLFFBQVE7UUFFWixJQUFJO1FBQ0osSUFBSSxRQUFRLEdBQUcsb0JBQVEsQ0FBQyxDQUFDLENBQUM7UUFDMUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDOUIsS0FBSztRQUNMLElBQUksVUFBVSxHQUFHLHdCQUFVLENBQUMsQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ2hDLFNBQVM7UUFDVCxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDbkIsQ0FBQztJQUVEOzs7T0FHRztJQUNILFVBQVUsQ0FBQyxRQUFnQjtRQUV2QixJQUFJLEtBQUssR0FBRyxtQkFBVSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEdBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ2hELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLEtBQUssR0FBRyxHQUFHLENBQUM7UUFDbkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEdBQUcsS0FBSyxHQUFDLEdBQUcsQ0FBQztJQUN2QyxDQUFDO0lBS0Q7O09BRUc7SUFDSSxJQUFJO1FBQ1AsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ3BCLENBQUM7SUFFRDs7T0FFRztJQUNILFNBQVM7UUFDTCxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDbkIsQ0FBQztJQUdEOztPQUVHO0lBQ0gsUUFBUTtRQUVKLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7UUFDOUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztRQUNoQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQy9CLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDakMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2xCLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN0QixDQUFDO0lBR0QsOEZBQThGO0lBQzlGLDJGQUEyRjtJQUMzRiwwRkFBMEY7SUFFMUYsbUJBQW1CO0lBQ1gsUUFBUTtJQUdoQixDQUFDO0lBRUQsa0JBQWtCO0lBQ1YsT0FBTztJQUdmLENBQUM7SUFNRCw4RkFBOEY7SUFDOUYsMkZBQTJGO0lBQzNGLHdGQUF3RjtJQUV4RixtQkFBbUI7SUFDWCxRQUFRO0lBR2hCLENBQUM7SUFFRCw4RkFBOEY7SUFDOUYsMkZBQTJGO0lBQzNGLDBGQUEwRjtJQUcxRiw4RkFBOEY7SUFDOUYsMkZBQTJGO0lBQzNGLHlGQUF5RjtJQUV6Rjs7T0FFRztJQUNPLE1BQU0sQ0FBQyxJQUFjO0lBRS9CLENBQUM7SUFFRCw4RkFBOEY7SUFDOUYsMkZBQTJGO0lBQzNGLDBGQUEwRjtJQUUxRixPQUFPO1FBRUgscUJBQXFCO1FBQ3JCLHlEQUF5RDtJQUM3RCxDQUFDO0NBSUo7QUF0S0Qsa0NBc0tDOzs7OztBQ3pMRCw4Q0FBZ0Q7QUFFL0M7Ozs7O0VBS0U7QUFDSCxNQUFhLEdBQUc7SUFFTCxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBVztRQUM5QixJQUFJLG9CQUFXLENBQUMsQ0FBQyxDQUFDLE9BQU87WUFBRSxPQUFPLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztJQUN6RSxDQUFDO0lBRU0sTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQVc7UUFDN0IsSUFBSSxvQkFBVyxDQUFDLENBQUMsQ0FBQyxPQUFPO1lBQUUsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7SUFDdkUsQ0FBQztJQUVNLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFXO1FBQzdCLElBQUksb0JBQVcsQ0FBQyxDQUFDLENBQUMsT0FBTztZQUFFLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7SUFFTSxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBVztRQUM5QixJQUFJLG9CQUFXLENBQUMsQ0FBQyxDQUFDLE9BQU87WUFBRSxPQUFPLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztJQUN6RSxDQUFDO0lBRU0sTUFBTSxDQUFDLFNBQVMsQ0FBQyxHQUFHLElBQVc7UUFDbEMsSUFBSSxvQkFBVyxDQUFDLENBQUMsQ0FBQyxPQUFPO1lBQUUsT0FBTyxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7SUFDNUUsQ0FBQztJQUVNLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFXO1FBQzVCLElBQUksb0JBQVcsQ0FBQyxDQUFDLENBQUMsT0FBTztZQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO0lBQ3JFLENBQUM7SUFHRCxXQUFXO0lBQ0osTUFBTSxDQUFDLGVBQWU7UUFDekIsSUFBSSxvQkFBVyxDQUFDLENBQUMsQ0FBQyxPQUFPLElBQUksU0FBUyxFQUFFO1lBQ3BDLElBQUksUUFBUSxHQUFHLFNBQVMsQ0FBQyxTQUFTLENBQUM7WUFFbkMsSUFBSSxLQUFLLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNsQyxJQUFJLEdBQUcsR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRWhDLElBQUksS0FBSyxHQUFHLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxJQUFJLEdBQUcsR0FBRyxLQUFLLEVBQUU7Z0JBQ3JDLE9BQU87YUFDVjtZQUVELElBQUksT0FBTyxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxHQUFHLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUVqRCxJQUFJLE1BQWMsRUFBRSxNQUFjLEVBQUUsT0FBZSxDQUFDO1lBQ3BELElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDL0IsSUFBSSxLQUFLLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtnQkFDbkIsaUNBQWlDO2dCQUNqQyxNQUFNLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNsQixZQUFZO2dCQUNaLElBQUksV0FBVyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ3RDLElBQUksV0FBVyxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7b0JBQ3pCLE1BQU0sR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3hCLE9BQU8sR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQzVCO2FBQ0o7aUJBQU0sSUFBSSxLQUFLLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtnQkFDMUIsTUFBTSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbEIsTUFBTSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbEIsT0FBTyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUN0QjtpQkFBTTtnQkFDSCxNQUFNLEdBQUcsU0FBUyxDQUFDLFFBQVEsQ0FBQztnQkFDNUIsTUFBTSxHQUFHLFNBQVMsQ0FBQyxRQUFRLENBQUM7Z0JBQzVCLE9BQU8sR0FBRyxPQUFPLENBQUM7YUFDckI7WUFDRCxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7U0FDckM7SUFDTCxDQUFDO0NBRUo7QUFqRUQsa0JBaUVDOzs7OztBQ3pFRCwrQkFBNEI7QUFFNUI7Ozs7OztHQU1HO0FBQ0gsTUFBYSxVQUFVO0lBRW5COzs7T0FHRztJQUNJLE1BQU0sQ0FBQyxHQUFHLENBQUMsUUFBYTtRQUMzQixJQUFJLElBQUksR0FBVyxLQUFLLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztRQUN6QyxJQUFJLEdBQUcsR0FBUSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN2QyxJQUFJLENBQUMsR0FBRyxFQUFFO1lBQ04sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUNwQyxTQUFHLENBQUMsS0FBSyxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsQ0FBQztnQkFDbEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDO2FBQzVDO1lBQ0QsR0FBRyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzNDO1FBQ0QsSUFBSSxHQUFHLElBQUksR0FBRyxDQUFDLE1BQU0sQ0FBQztZQUFFLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNuQyxPQUFPLEdBQUcsQ0FBQztJQUNmLENBQUM7SUFFRDs7O09BR0c7SUFDSSxNQUFNLENBQUMsT0FBTyxDQUFDLEdBQVE7UUFDMUIsSUFBSSxDQUFDLEdBQUc7WUFBRSxPQUFPO1FBRWpCLElBQUksS0FBSyxHQUFRLE1BQU0sQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDNUMsSUFBSSxLQUFLLEdBQVEsS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3RDLElBQUksSUFBSSxHQUFXLEtBQUssR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDO1FBQ3RDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNaLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNqQyxDQUFDO0NBQ0o7QUFqQ0QsZ0NBaUNDOzs7OztBQzFDRCwrQkFBNEI7QUFFM0I7Ozs7O0VBS0U7QUFDSCxNQUFhLFNBQVM7SUFLbEI7UUFDSSxJQUFJLEtBQUssR0FBUSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDckMsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNSLE9BQU8sQ0FBQyxJQUFJLENBQUMsMEJBQTBCLENBQUMsQ0FBQztZQUN6QyxTQUFHLENBQUMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLENBQUM7WUFDckMsT0FBTztTQUNWO1FBQ0QsVUFBVTtRQUNWLElBQUksU0FBUyxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3hDLE1BQU0sSUFBSSxLQUFLLENBQUMsSUFBSSxHQUFHLHFCQUFxQixDQUFDLENBQUM7YUFDN0M7WUFDRCxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNoQyxTQUFTLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNwQztJQUNMLENBQUM7O0FBbkJMLDhCQXFCQztBQW5Ca0IsbUJBQVMsR0FBZSxFQUFFLENBQUM7QUFDM0IscUJBQVcsR0FBVSxFQUFFLENBQUM7Ozs7O0FDVDNDOzs7Ozs7R0FNRztBQUNILE1BQWEsYUFBYTtJQWlCZixHQUFHLENBQUMsUUFBZ0IsRUFBRSxNQUFjLEVBQUUsUUFBdUIsRUFBRSxPQUFZLEVBQUUsS0FBVTtRQUMxRixJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztRQUN6QixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztRQUN6QixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNuQixJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztJQUMzQixDQUFDO0NBQ0o7QUF4QkQsc0NBd0JDO0FBSUE7Ozs7OztFQU1FO0FBQ0gsMkNBQXNDO0FBRXRDLE1BQWEsU0FBVSxTQUFRLHFCQUFTO0lBRXBDO1FBQ0ksS0FBSyxFQUFFLENBQUM7UUFJWixlQUFlO1FBQ1IsV0FBTSxHQUFXLENBQUMsQ0FBQTtRQUNqQixVQUFLLEdBQXlCLElBQUksS0FBSyxFQUFpQixDQUFDO1FBQ3pELFVBQUssR0FBeUIsSUFBSSxLQUFLLEVBQWlCLENBQUM7UUFDekQsYUFBUSxHQUF5QixJQUFJLEtBQUssRUFBaUIsQ0FBQztRQUM1RCxTQUFJLEdBQXlCLElBQUksS0FBSyxFQUFpQixDQUFDO1FBbUd4RCxhQUFRLEdBQVcsQ0FBQyxDQUFDO1FBQ3JCLGNBQVMsR0FBVyxDQUFDLENBQUM7UUE1RzFCLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFXTSxNQUFNLEtBQUssQ0FBQztRQUNmLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLEVBQUU7WUFDeEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLFNBQVMsRUFBRSxDQUFDO1NBQ3BDO1FBQ0QsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFBO0lBQ3pCLENBQUM7SUFFRDs7T0FFRztJQUNLLFdBQVc7UUFDZixJQUFJLENBQWdCLENBQUM7UUFDckIsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDdEIsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUE7U0FDdEI7O1lBQ0csQ0FBQyxHQUFHLElBQUksYUFBYSxFQUFFLENBQUM7UUFDNUIsT0FBTyxDQUFDLENBQUM7SUFDYixDQUFDO0lBRUQ7OztPQUdHO0lBQ0ssWUFBWSxDQUFDLENBQWdCO1FBQ2pDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFBO1FBQzdCLENBQUMsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFBO1FBQ2IsQ0FBQyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUE7UUFDakIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUE7SUFDckIsQ0FBQztJQUVNLE1BQU0sQ0FBQyxRQUF1QixFQUFFLE9BQVk7UUFDL0MsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFvQixFQUFFLEtBQWEsRUFBRSxHQUF5QixFQUFFLEVBQUU7WUFDdkYsT0FBTyxLQUFLLENBQUMsUUFBUSxJQUFJLFFBQVEsSUFBSSxLQUFLLENBQUMsT0FBTyxJQUFJLE9BQU8sQ0FBQTtRQUNqRSxDQUFDLENBQUMsQ0FBQTtRQUVGLElBQUksQ0FBQyxJQUFJLElBQUksRUFBRTtZQUNYLE9BQU8sSUFBSSxDQUFBO1NBQ2Q7UUFDRCxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFvQixFQUFFLEtBQWEsRUFBRSxHQUF5QixFQUFFLEVBQUU7WUFDbkYsT0FBTyxLQUFLLENBQUMsUUFBUSxJQUFJLFFBQVEsSUFBSSxLQUFLLENBQUMsT0FBTyxJQUFJLE9BQU8sQ0FBQTtRQUNqRSxDQUFDLENBQUMsQ0FBQTtRQUNGLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUU7WUFDekIsT0FBTyxJQUFJLENBQUE7U0FDZDtRQUNELE9BQU8sS0FBSyxDQUFBO0lBQ2hCLENBQUM7SUFFTSxHQUFHLENBQUMsUUFBZ0IsRUFBRSxNQUFjLEVBQUUsUUFBdUIsRUFBRSxPQUFZLEVBQUUsZ0JBQXFCLElBQUk7UUFDekcsSUFBSSxDQUFnQixDQUFDO1FBQ3JCLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQW9CLEVBQUUsS0FBYSxFQUFFLEdBQXlCLEVBQUUsRUFBRTtZQUNuRixPQUFPLEtBQUssQ0FBQyxRQUFRLElBQUksUUFBUSxJQUFJLEtBQUssQ0FBQyxPQUFPLElBQUksT0FBTyxDQUFBO1FBQ2pFLENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLElBQUksSUFBSSxFQUFFO1lBQ1gsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBb0IsRUFBRSxLQUFhLEVBQUUsR0FBeUIsRUFBRSxFQUFFO2dCQUNuRixPQUFPLEtBQUssQ0FBQyxRQUFRLElBQUksUUFBUSxJQUFJLEtBQUssQ0FBQyxPQUFPLElBQUksT0FBTyxDQUFBO1lBQ2pFLENBQUMsQ0FBQyxDQUFBO1NBQ0w7UUFFRCxJQUFJLENBQUMsSUFBSSxJQUFJLEVBQUU7WUFDWCxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ3RCO1FBRUQsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUUsYUFBYSxDQUFDLENBQUM7UUFDMUQsQ0FBQyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDbEIsQ0FBQyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7SUFDbEIsQ0FBQztJQUVNLFNBQVMsQ0FBQyxRQUF1QixFQUFFLE9BQVksRUFBRSxnQkFBcUIsSUFBSTtRQUM3RSxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDLEVBQUUsUUFBUSxFQUFFLE9BQU8sRUFBRSxhQUFhLENBQUMsQ0FBQztJQUN6RCxDQUFDO0lBRU0sTUFBTSxDQUFDLFFBQXVCLEVBQUUsT0FBWTtRQUMvQyxJQUFJLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQTtRQUNsQixJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQW9CLEVBQUUsS0FBYSxFQUFFLEdBQXlCLEVBQUUsRUFBRTtZQUN2RixJQUFJLEtBQUssQ0FBQyxRQUFRLElBQUksUUFBUSxJQUFJLEtBQUssQ0FBQyxPQUFPLElBQUksT0FBTyxFQUFFO2dCQUN4RCxTQUFTLEdBQUcsS0FBSyxDQUFDO2dCQUNsQixPQUFPLElBQUksQ0FBQzthQUNmO2lCQUFNO2dCQUNILE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsSUFBSSxJQUFJLEVBQUU7WUFDWCxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDaEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUN4QjtRQUVELENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQW9CLEVBQUUsS0FBYSxFQUFFLEdBQXlCLEVBQUUsRUFBRTtZQUNuRixPQUFPLEtBQUssQ0FBQyxRQUFRLElBQUksUUFBUSxJQUFJLEtBQUssQ0FBQyxPQUFPLElBQUksT0FBTyxDQUFDO1FBQ2xFLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLElBQUksSUFBSTtZQUNULENBQUMsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0lBQ3pCLENBQUM7SUFLRCxLQUFLO1FBQ0QsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQztJQUN6QyxDQUFDO0lBRUQsTUFBTTtRQUNGLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxDQUFDO1FBQy9ELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUM7UUFFckMsS0FBSyxJQUFJLEtBQUssR0FBRyxDQUFDLEVBQUUsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFFO1lBQ3BELElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsSUFBSSxDQUFDLENBQUMsT0FBTyxFQUFFO2dCQUNYLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN0QixTQUFTO2FBQ1o7WUFFRCxDQUFDLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDNUIsSUFBSSxDQUFDLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxRQUFRLEVBQUU7Z0JBQ3hCLFNBQVM7YUFDWjtZQUVELENBQUMsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1lBRWQsSUFBSSxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDZCxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7Z0JBQ1gsSUFBSSxDQUFDLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtvQkFDZixDQUFDLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztvQkFDakIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ3pCO2FBQ0o7WUFDRCxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUM7WUFDdkIsSUFBSSxDQUFDLENBQUMsUUFBUSxJQUFJLElBQUksRUFBRTtnQkFDcEIsSUFBSTtvQkFDQSxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDdkM7Z0JBQUMsT0FBTyxLQUFLLEVBQUU7b0JBQ1osQ0FBQyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7aUJBQ3BCO2FBQ0o7U0FDSjtRQUNELElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDO1FBQy9CLE9BQU8sR0FBRyxFQUFFO1lBQ1IsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUM1QixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsQyxJQUFJLENBQUMsQ0FBQyxPQUFPLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQyxFQUFFO2dCQUMxQixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQzVCLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDeEI7WUFDRCxHQUFHLEVBQUUsQ0FBQztTQUNUO1FBQ0QsR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO1FBQ3hCLE9BQU8sR0FBRyxFQUFFO1lBQ1IsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUN6QixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuQixHQUFHLEVBQUUsQ0FBQztTQUNUO0lBQ0wsQ0FBQzs7QUF4S0wsOEJBeUtDO0FBMUprQixtQkFBUyxHQUFjLElBQUksQ0FBQzs7Ozs7QUM3RC9DLG9EQUFnRDtBQU1oRDs7Ozs7R0FLRztBQUNILE1BQWEsV0FBWSxTQUFRLHNCQUFTO0lBSXRDO1FBQ0ksS0FBSyxFQUFFLENBQUM7UUFVRixVQUFLLEdBQTBCLElBQUksR0FBRyxFQUFvQixDQUFDO0lBVHJFLENBQUM7SUFJTSxNQUFNLEtBQUssQ0FBQztRQUNmLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUTtZQUFFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxXQUFXLEVBQUUsQ0FBQztRQUN0RCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekIsQ0FBQztJQUlELEtBQUs7SUFDTCxDQUFDO0lBRUQsTUFBTTtJQUNOLENBQUM7SUFFRCxPQUFPO1FBQ0gsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBR00sUUFBUSxDQUFDLElBQWM7UUFDMUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMvQixPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRU0sR0FBRyxDQUFDLEdBQVc7UUFDbEIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUMvQixDQUFDOztBQW5DTCxrQ0FvQ0M7QUE1QmtCLG9CQUFRLEdBQWdCLElBQUksQ0FBQzs7Ozs7QUNsQi9DOzs7OztFQUtFO0FBQ0gsTUFBYSxTQUFTO0lBRWxCLFlBQVksR0FBVyxFQUFFLE1BQVcsSUFBSSxFQUFFLFNBQWtCLEtBQUs7UUFRMUQsV0FBTSxHQUFHLEtBQUssQ0FBQztRQVBsQixJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztRQUNmLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDO1FBQ2hCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQ3hCLENBQUM7SUFNRDs7Ozs7T0FLRztJQUNJLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBVyxFQUFFLE9BQVksSUFBSSxFQUFFLFNBQWtCLEtBQUs7UUFDdkUsT0FBTyxJQUFJLFNBQVMsQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFFTSxJQUFJO1FBQ1AsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUE7SUFDdEIsQ0FBQztDQUNKO0FBekJELDhCQXlCQztBQUdBOzs7OztFQUtFO0FBQ0gsTUFBYSxTQUFTO0lBS2xCLFlBQW1CLE9BQVksRUFBRSxRQUFrQjtRQUMvQyxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQztRQUN0QixJQUFJLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQztJQUN6QixDQUFDO0lBRU0sTUFBTSxDQUFDLEdBQUcsSUFBVztRQUN4QixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsSUFBSSxDQUFDLENBQUM7SUFDekMsQ0FBQztDQUNKO0FBYkQsOEJBYUM7Ozs7O0FDdkRELDZDQUE4RTtBQUs3RTs7Ozs7RUFLRTtBQUNILE1BQWEsWUFBYSxTQUFRLHNCQUFTO0lBU3ZDO1FBQ0ksS0FBSyxFQUFFLENBQUM7SUFDWixDQUFDO0lBUE0sTUFBTSxLQUFLLENBQUM7UUFDZixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVE7WUFBRSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDdkQsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ3pCLENBQUM7SUFRRCxLQUFLO1FBQ0QseUJBQVksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDcEMsQ0FBQztJQUVELE1BQU07SUFDTixDQUFDO0lBRUQsT0FBTztRQUNILHlCQUFZLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ3BDLENBQUM7SUFFRDs7O09BR0c7SUFDSCxNQUFNLENBQUMsSUFBZTtRQUNsQixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUM5Qix5QkFBWSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUVEOzs7T0FHRztJQUNILHFCQUFxQixDQUFDLEVBQWE7UUFDL0IseUJBQVksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBYSxFQUFFLEVBQUU7WUFDOUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUN6QixDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsMEJBQTBCLENBQUMsR0FBb0IsRUFBRSxPQUFZLElBQUk7UUFDN0QseUJBQVksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBYSxFQUFFLEVBQUU7WUFDOUMsRUFBRSxDQUFDLGtCQUFrQixDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNyQyxDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUM7SUFHRDs7Ozs7OztPQU9HO0lBQ0ksV0FBVyxDQUFDLElBQXFCLEVBQUUsUUFBK0IsRUFBRSxNQUFXLEVBQUUsV0FBbUIsQ0FBQyxFQUFFLE9BQWdCLEtBQUs7UUFDL0gsc0JBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUMsUUFBUSxFQUFDLE1BQU0sRUFBQyxRQUFRLEVBQUMsSUFBSSxDQUFDLENBQUM7SUFDcEUsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0ksY0FBYyxDQUFDLElBQXFCLEVBQUUsUUFBK0IsRUFBRSxNQUFXO1FBQ3JGLHNCQUFTLENBQUMsb0JBQW9CLENBQUMsSUFBSSxFQUFDLFFBQVEsRUFBQyxNQUFNLENBQUMsQ0FBQztJQUN6RCxDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSSxXQUFXLENBQUMsSUFBcUIsRUFBRSxRQUErQixFQUFFLE1BQVc7UUFDbEYsc0JBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUMsUUFBUSxFQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFFRDs7OztPQUlHO0lBQ0ksa0JBQWtCLENBQUMsR0FBb0IsRUFBRSxPQUFZLElBQUk7UUFDNUQsc0JBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLEVBQUMsSUFBSSxDQUFDLENBQUM7SUFDNUMsQ0FBQzs7QUFoR0wsb0NBa0dDO0FBL0ZrQixxQkFBUSxHQUFpQixJQUFJLENBQUM7Ozs7O0FDZGpELDZDQUF5QztBQUN6Qyx3Q0FBcUM7QUFDckMsb0RBQWlEO0FBR2hEOzs7OztFQUtFO0FBQ0gsTUFBYSxTQUFVLFNBQVEscUJBQVM7SUFFcEM7UUFDSSxLQUFLLEVBQUUsQ0FBQztRQStKWixxREFBcUQ7UUFDckQscURBQXFEO1FBQ3JELHFEQUFxRDtRQUU3QyxnQkFBVyxHQUFxQixJQUFJLEtBQUssRUFBYSxDQUFDO1FBQ3ZELGdCQUFXLEdBQTJCLEVBQUUsQ0FBQztRQW5LN0MsWUFBWSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFTTyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxFQUFFLElBQUk7UUFDckMsSUFBSSxFQUFhLENBQUM7UUFDbEIsSUFBSSxTQUFTLENBQUMsaUJBQWlCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUN4QyxFQUFFLEdBQUcsU0FBUyxDQUFDLGlCQUFpQixDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQ3ZDLEVBQUUsQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO1lBQ2IsRUFBRSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7WUFDZixFQUFFLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztTQUNyQjthQUFNO1lBQ0gsRUFBRSxHQUFHLElBQUksc0JBQVMsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDakM7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7SUFFTyxNQUFNLENBQUMscUJBQXFCLENBQUMsRUFBYTtRQUM5QyxFQUFFLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUNmLEVBQUUsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDO1FBQ2QsRUFBRSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDbEIsU0FBUyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQTtJQUN4QyxDQUFDO0lBRUQ7Ozs7Ozs7T0FPRztJQUNJLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxJQUFxQixFQUFFLFFBQStCLEVBQUUsTUFBVyxFQUFFLFdBQW1CLENBQUMsRUFBRSxPQUFnQixLQUFLO1FBQzVJLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDdkIsSUFBSSxJQUFJLEdBQTBCO1lBQzlCLElBQUksRUFBRSxJQUFJO1lBQ1YsUUFBUSxFQUFFLFFBQVE7WUFDbEIsTUFBTSxFQUFFLE1BQU07WUFDZCxRQUFRLEVBQUUsUUFBUTtZQUNsQixJQUFJLEVBQUUsSUFBSTtTQUNiLENBQUM7UUFFRixJQUFJLEtBQUssR0FBRyxTQUFTLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDOUMsSUFBSSxHQUFHLEdBQUcsS0FBSyxDQUFDO1FBQ2hCLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQztRQUNaLElBQUksS0FBSyxJQUFJLElBQUksRUFBRTtZQUNmLEtBQUssQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ3BCLElBQUksT0FBTyxDQUFDLE1BQU0sSUFBSSxNQUFNLElBQUksT0FBTyxDQUFDLFFBQVEsSUFBSSxRQUFRLEVBQUU7b0JBQzFELEdBQUcsR0FBRyxJQUFJLENBQUM7aUJBQ2Q7Z0JBQ0QsSUFBSSxPQUFPLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLEVBQUU7b0JBQ2xDLEdBQUcsRUFBRSxDQUFDO2lCQUNUO1lBQ0wsQ0FBQyxDQUFDLENBQUM7U0FDTjthQUFNO1lBQ0gsS0FBSyxHQUFHLElBQUksS0FBSyxFQUF5QixDQUFDO1lBQzNDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsR0FBRyxLQUFLLENBQUM7U0FDN0M7UUFDRCxJQUFJLEdBQUcsRUFBRTtZQUNMLHdDQUF3QztZQUN4QyxTQUFHLENBQUMsS0FBSyxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsQ0FBQTtTQUNuQzthQUFNO1lBQ0gsS0FBSyxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQzlCO0lBQ0wsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0ksTUFBTSxDQUFDLG9CQUFvQixDQUFDLElBQXFCLEVBQUUsUUFBK0IsRUFBRSxNQUFXO1FBQ2xHLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDdkIsSUFBSSxJQUFJLEdBQTBCLElBQUksQ0FBQztRQUN2QyxJQUFJLEtBQUssR0FBRyxTQUFTLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDOUMsSUFBSSxLQUFLLElBQUksSUFBSSxFQUFFO1lBQ2YsSUFBSSxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDbkIsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQTRCLEVBQUUsS0FBYSxFQUFFLEtBQThCLEVBQUUsRUFBRTtnQkFDeEYsSUFBSSxLQUFLLENBQUMsTUFBTSxJQUFJLE1BQU0sSUFBSSxLQUFLLENBQUMsUUFBUSxJQUFJLFFBQVEsRUFBRTtvQkFDdEQsU0FBUyxHQUFHLEtBQUssQ0FBQztvQkFDbEIsSUFBSSxHQUFHLEtBQUssQ0FBQztvQkFDYixPQUFPLEtBQUssQ0FBQztpQkFDaEI7Z0JBQ0QsT0FBTyxJQUFJLENBQUM7WUFDaEIsQ0FBQyxDQUFDLENBQUM7WUFFSCxJQUFJLFNBQVMsSUFBSSxDQUFDLENBQUMsRUFBRTtnQkFDakIsS0FBSyxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUM7YUFDOUI7U0FDSjtJQUNMLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNJLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxJQUFxQixFQUFFLFFBQStCLEVBQUUsTUFBVztRQUMvRixJQUFJLElBQUksR0FBRyxLQUFLLENBQUM7UUFDakIsSUFBSSxLQUFLLEdBQUcsU0FBUyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlDLElBQUksS0FBSyxFQUFFO1lBQ1AsYUFBYTtZQUNiLElBQUksS0FBSyxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxFQUFFO2dCQUM1QyxPQUFPLEdBQUcsQ0FBQyxNQUFNLElBQUksTUFBTSxJQUFJLEdBQUcsQ0FBQyxRQUFRLElBQUksUUFBUSxDQUFDO1lBQzVELENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxHQUFHLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQztTQUN0QjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRDs7O09BR0c7SUFDSSxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQWE7UUFDdEMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxHQUFvQixFQUFFLE9BQVksSUFBSTtRQUNwRSxJQUFJLEVBQUUsR0FBRyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQy9DLFNBQVMsQ0FBQyxlQUFlLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDOUIsSUFBSSxFQUFFLElBQUksSUFBSSxFQUFFO1lBQ1osU0FBUyxDQUFDLHFCQUFxQixDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQ3ZDO0lBQ0wsQ0FBQztJQUVPLE1BQU0sQ0FBQyxlQUFlLENBQUMsRUFBYTtRQUN4QyxJQUFJLEtBQUssR0FBRyxTQUFTLENBQUMsaUJBQWlCLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2hELElBQUksS0FBSyxJQUFJLElBQUksRUFBRTtZQUVmLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNuQyxJQUFJLElBQUksR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BCLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLEVBQUU7b0JBQ3ZCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLENBQUM7aUJBQ3ZDO2dCQUNELElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtvQkFDWCxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO2lCQUN4QjtnQkFDRCxJQUFJLEVBQUUsQ0FBQyxNQUFNLEVBQUU7b0JBQ1gsTUFBTTtpQkFDVDthQUNKO1NBQ0o7SUFDTCxDQUFDO0lBU08sZUFBZSxDQUFDLEdBQUcsRUFBRSxJQUFJO1FBQzdCLElBQUksRUFBYSxDQUFDO1FBQ2xCLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQzdCLEVBQUUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQzVCLEVBQUUsQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO1lBQ2IsRUFBRSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7WUFDZixFQUFFLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztTQUNyQjthQUFNO1lBQ0gsRUFBRSxHQUFHLElBQUksc0JBQVMsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDakM7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7SUFFTyxlQUFlLENBQUMsRUFBYTtRQUNqQyxFQUFFLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUNmLEVBQUUsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDO1FBQ2QsRUFBRSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDbEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUE7SUFDN0IsQ0FBQztJQUVEOzs7Ozs7O09BT0c7SUFDSSxnQkFBZ0IsQ0FBQyxJQUFxQixFQUFFLFFBQStCLEVBQUUsTUFBVyxFQUFFLFdBQW1CLENBQUMsRUFBRSxPQUFnQixLQUFLO1FBQ3BJLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDdkIsSUFBSSxJQUFJLEdBQTBCO1lBQzlCLElBQUksRUFBRSxJQUFJO1lBQ1YsUUFBUSxFQUFFLFFBQVE7WUFDbEIsTUFBTSxFQUFFLE1BQU07WUFDZCxRQUFRLEVBQUUsUUFBUTtZQUNsQixJQUFJLEVBQUUsSUFBSTtTQUNiLENBQUM7UUFFRixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ25DLElBQUksR0FBRyxHQUFHLEtBQUssQ0FBQztRQUNoQixJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUM7UUFDWixJQUFJLEtBQUssSUFBSSxJQUFJLEVBQUU7WUFDZixLQUFLLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNwQixJQUFJLE9BQU8sQ0FBQyxNQUFNLElBQUksTUFBTSxJQUFJLE9BQU8sQ0FBQyxRQUFRLElBQUksUUFBUSxFQUFFO29CQUMxRCxHQUFHLEdBQUcsSUFBSSxDQUFDO2lCQUNkO2dCQUNELElBQUksT0FBTyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFO29CQUNsQyxHQUFHLEVBQUUsQ0FBQztpQkFDVDtZQUNMLENBQUMsQ0FBQyxDQUFDO1NBQ047YUFBTTtZQUNILEtBQUssR0FBRyxJQUFJLEtBQUssRUFBeUIsQ0FBQztZQUMzQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLEtBQUssQ0FBQztTQUNsQztRQUNELElBQUksR0FBRyxFQUFFO1lBQ0wsd0NBQXdDO1lBQ3hDLFNBQUcsQ0FBQyxLQUFLLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxDQUFBO1lBQ2hDLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7YUFBTTtZQUNILEtBQUssQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUMzQixPQUFPLElBQUksQ0FBQztTQUNmO0lBQ0wsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0ksbUJBQW1CLENBQUMsSUFBcUIsRUFBRSxRQUErQixFQUFFLE1BQVc7UUFDMUYsSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUN2QixJQUFJLElBQUksR0FBMEIsSUFBSSxDQUFDO1FBQ3ZDLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbkMsSUFBSSxLQUFLLElBQUksSUFBSSxFQUFFO1lBQ2YsSUFBSSxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDbkIsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQTRCLEVBQUUsS0FBYSxFQUFFLEtBQThCLEVBQUUsRUFBRTtnQkFDeEYsSUFBSSxLQUFLLENBQUMsTUFBTSxJQUFJLE1BQU0sSUFBSSxLQUFLLENBQUMsUUFBUSxJQUFJLFFBQVEsRUFBRTtvQkFDdEQsU0FBUyxHQUFHLEtBQUssQ0FBQztvQkFDbEIsSUFBSSxHQUFHLEtBQUssQ0FBQztvQkFDYixPQUFPLEtBQUssQ0FBQztpQkFDaEI7Z0JBQ0QsT0FBTyxJQUFJLENBQUM7WUFDaEIsQ0FBQyxDQUFDLENBQUM7WUFFSCxJQUFJLFNBQVMsSUFBSSxDQUFDLENBQUMsRUFBRTtnQkFDakIsS0FBSyxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUM7YUFDOUI7U0FDSjtJQUNMLENBQUM7SUFFTSxzQkFBc0I7UUFDekIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLEtBQUssRUFBYSxDQUFDO1FBQzFDLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNJLGdCQUFnQixDQUFDLElBQXFCLEVBQUUsUUFBK0IsRUFBRSxNQUFXO1FBQ3ZGLElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQztRQUNqQixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ25DLElBQUksS0FBSyxFQUFFO1lBQ1AsYUFBYTtZQUNiLElBQUksS0FBSyxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxFQUFFO2dCQUM1QyxPQUFPLEdBQUcsQ0FBQyxNQUFNLElBQUksTUFBTSxJQUFJLEdBQUcsQ0FBQyxRQUFRLElBQUksUUFBUSxDQUFDO1lBQzVELENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxHQUFHLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQztTQUN0QjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRDs7O09BR0c7SUFDSSxhQUFhLENBQUMsRUFBYTtRQUM5QixJQUFJLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQzVCLENBQUM7SUFFRDs7OztPQUlHO0lBQ0ksa0JBQWtCLENBQUMsR0FBb0IsRUFBRSxPQUFZLElBQUk7UUFDNUQsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDekMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUN4QixJQUFJLEVBQUUsSUFBSSxJQUFJLEVBQUU7WUFDWixJQUFJLENBQUMsZUFBZSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQzVCO0lBQ0wsQ0FBQztJQUVPLGNBQWMsQ0FBQyxFQUFhO1FBQ2hDLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3JDLElBQUksS0FBSyxJQUFJLElBQUksRUFBRTtZQUVmLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNuQyxJQUFJLElBQUksR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BCLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLEVBQUU7b0JBQ3ZCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLENBQUM7aUJBQ3ZDO2dCQUNELElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtvQkFDWCxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO2lCQUN4QjtnQkFDRCxJQUFJLEVBQUUsQ0FBQyxNQUFNLEVBQUU7b0JBQ1gsTUFBTTtpQkFDVDthQUNKO1NBQ0o7SUFDTCxDQUFDOztBQWxVTCw4QkFxVUM7QUE5VEcscURBQXFEO0FBQ3JELHFEQUFxRDtBQUNyRCxxREFBcUQ7QUFFdEMsMkJBQWlCLEdBQXFCLElBQUksS0FBSyxFQUFhLENBQUM7QUFDN0QsMkJBQWlCLEdBQTJCLEVBQUUsQ0FBQztBQTJVbEUsTUFBYSxZQUFZOztBQUF6QixvQ0FJQztBQUZpQix1QkFBVSxHQUE4QixJQUFJLEdBQUcsRUFBd0IsQ0FBQzs7Ozs7QUNsVzFGLG9EQUE4QztBQUM5QyxvREFBaUQ7QUFFakQsMkRBQXdEO0FBRXhELGlEQUFrRDtBQUNsRCx3Q0FBcUM7QUFFbkM7Ozs7OztFQU1DO0FBQ0gsTUFBYSxXQUFZLFNBQVEscUJBQVM7SUFXdEM7UUFDSSxLQUFLLEVBQUUsQ0FBQztRQVZaOztXQUVHO1FBQ0ssa0JBQWEsR0FBNkIsSUFBSSxDQUFDO1FBQ3ZEOztXQUVHO1FBQ0ssY0FBUyxHQUFvQixJQUFJLENBQUM7SUFJMUMsQ0FBQztJQUVNLE1BQU0sS0FBSyxDQUFDO1FBQ2YsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRO1lBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLFdBQVcsRUFBRSxDQUFDO1FBQ3RELE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN6QixDQUFDO0lBRUQ7O09BRUc7SUFDSSxLQUFLO1FBQ1IsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLHVCQUFVLEVBQWdCLENBQUM7UUFDcEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLHVCQUFVLEVBQU8sQ0FBQztRQUN2QyxJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFVLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDN0MsQ0FBQztJQUVELE1BQU07SUFDTixDQUFDO0lBRUQ7O09BRUc7SUFDSSxPQUFPO1FBQ1YsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2pCLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUNwQixJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQzNCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1NBQzdCO1FBQ0QsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDdkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7U0FDekI7SUFDTCxDQUFDO0lBRUE7OztNQUdFO0lBQ0ssSUFBSSxDQUFDLElBQW9CO1FBQzdCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxFQUFFO1lBQ2xDLFNBQUcsQ0FBQyxHQUFHLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN0QyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ2pEO0lBQ0wsQ0FBQztJQUdEOzs7T0FHRztJQUNJLFFBQVEsQ0FBQyxJQUFZO1FBRXhCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RDLElBQUcsSUFBSSxJQUFFLElBQUksRUFBQztZQUNWLElBQUksR0FBRyx3QkFBVSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDL0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFDLElBQUksQ0FBQyxDQUFDO1NBQ2pDO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVEOzs7O09BSUc7SUFDSSxXQUFXLENBQUMsSUFBWSxFQUFFLEdBQWtCO1FBQy9DLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBSUQ7OztPQUdHO0lBQ0ksTUFBTSxDQUFDLElBQVk7UUFDdEIsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDOUMsSUFBSSxRQUFRLEVBQUU7WUFDVixJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUMvQjtRQUNELHdCQUFVLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUVEOzs7T0FHRztJQUNJLFNBQVM7UUFDWixJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWE7WUFBRSxPQUFPO1FBRWhDLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFVBQVUsR0FBRyxFQUFFLEtBQUs7WUFDM0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNqQixPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUMvQixDQUFDOztBQTlHTCxrQ0ErR0M7QUFqR2tCLG9CQUFRLEdBQWdCLElBQUksQ0FBQzs7Ozs7QUM3Qi9DOzs7Ozs7RUFNRTtBQUNILE1BQWEsWUFBWTtJQUtyQixZQUFZLEdBQVcsRUFBRSxJQUFZO1FBQ2pDLElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO1FBQ2YsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7SUFDckIsQ0FBQztDQUNKO0FBVEQsb0NBU0M7Ozs7O0FDakJELHlDQUFxQztBQUVwQzs7Ozs7O0VBTUU7QUFDSCxNQUFhLFFBQVE7SUFBckI7UUFFSSxVQUFVO1FBQ0gsYUFBUSxHQUFXLENBQUMsQ0FBQztRQUM1QixVQUFVO1FBQ0gsYUFBUSxHQUFtQixJQUFJLEtBQUssRUFBVyxDQUFDO0lBdUIzRCxDQUFDO0lBakJHOzs7OztPQUtHO0lBQ0ksR0FBRyxDQUFDLEdBQVcsRUFBRSxJQUFZLEVBQUUsWUFBWSxHQUFHLEtBQUs7UUFFdEQsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxLQUFjLEVBQUUsS0FBYSxFQUFFLEdBQW1CLEVBQUUsRUFBRTtZQUN2RixPQUFPLEtBQUssQ0FBQyxHQUFHLElBQUksR0FBRyxDQUFBO1FBQzNCLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxLQUFLLElBQUksQ0FBQyxDQUFDLEVBQUU7WUFDYixJQUFJLElBQUksR0FBRyxJQUFJLGtCQUFPLENBQUMsR0FBRyxFQUFDLElBQUksRUFBQyxZQUFZLENBQUMsQ0FBQztZQUM5QyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUM1QjtRQUNELE9BQU8sSUFBSSxDQUFBO0lBQ2YsQ0FBQztDQUNKO0FBNUJELDRCQTRCQzs7Ozs7QUNyQ0Q7Ozs7OztHQU1HO0FBQ0gsTUFBYSxPQUFPO0lBS2hCLFlBQVksR0FBRyxFQUFDLElBQUksRUFBQyxZQUFZO1FBRnpCLGlCQUFZLEdBQUcsS0FBSyxDQUFDO1FBSXpCLElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO1FBQ2YsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDakIsSUFBSSxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUM7SUFDckMsQ0FBQztJQUVELElBQVcsR0FBRztRQUNWLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQTtJQUNuQixDQUFDO0lBRUQsSUFBVyxJQUFJO1FBRVgsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFBO0lBQ3BCLENBQUM7SUFFRCxJQUFXLFlBQVk7UUFFbkIsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFBO0lBQzVCLENBQUM7Q0FDSjtBQXpCRCwwQkF5QkM7Ozs7O0FDakNELElBQU8sT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7QUFDOUIsb0RBQWdEO0FBR2hELHdDQUFxQztBQVVyQzs7Ozs7O0dBTUc7QUFDSCxNQUFhLFVBQVcsU0FBUSxzQkFBUztJQVNyQztRQUNJLEtBQUssRUFBRSxDQUFDO1FBR1osWUFBWTtRQUNKLGtCQUFhLEdBQXlCLElBQUksR0FBRyxFQUFtQixDQUFDO0lBSHpFLENBQUM7SUFQTSxNQUFNLEtBQUssQ0FBQztRQUNmLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJO1lBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLFVBQVUsRUFBRSxDQUFDO1FBQzVELE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN6QixDQUFDO0lBV00sS0FBSztJQUNaLENBQUM7SUFFRCxNQUFNO0lBQ04sQ0FBQztJQUVNLE9BQU87SUFDZCxDQUFDO0lBR0Q7OztPQUdHO0lBQ0ksTUFBTSxDQUFDLEdBQVc7UUFDckIsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSSxPQUFPLENBQUMsT0FBZSxFQUFDLFdBQXFCLEVBQUMsV0FBcUI7UUFHdEUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDLE9BQWdCLEVBQUUsRUFBRTtZQUVwRSxJQUFJLE9BQU8sRUFBRTtnQkFDVCxNQUFNO2dCQUNOLElBQUcsV0FBVyxJQUFFLElBQUk7b0JBQUUsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDO2dCQUMzQyxNQUFNO2dCQUNOLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUU7b0JBQ3RDLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsT0FBTyxDQUFDLENBQUM7aUJBQ2hEO2FBQ0o7aUJBQU07Z0JBQ0gsU0FBRyxDQUFDLEtBQUssQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO2dCQUNsQyxTQUFHLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUMxQjtRQUVMLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLENBQUMsUUFBZ0IsRUFBRSxFQUFFO1lBQzFDLE1BQU07WUFDTixJQUFHLFdBQVcsSUFBRSxJQUFJO2dCQUFFLFdBQVcsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7UUFFdkQsQ0FBQyxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBQ3JCLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNJLFNBQVMsQ0FBQyxLQUFlLEVBQUMsV0FBcUIsRUFBQyxXQUFxQjtRQUN4RSxJQUFJLElBQUksR0FBZSxJQUFJLEtBQUssRUFBTyxDQUFDO1FBQ3hDLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBQyxHQUFHLEVBQUUsT0FBTyxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsT0FBTyxDQUFDLElBQUksRUFBQyxDQUFDLENBQUE7UUFDckQsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQyxPQUFnQixFQUFFLEVBQUU7WUFFN0QsSUFBSSxPQUFPLEVBQUU7Z0JBQ1QsTUFBTTtnQkFDTixJQUFHLFdBQVcsSUFBRSxJQUFJO29CQUFFLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDM0MsTUFBTTtnQkFDTixLQUFLLElBQUksS0FBSyxHQUFHLENBQUMsRUFBRSxLQUFLLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLEVBQUU7b0JBQ3hELElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ2pDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUU7d0JBQ25DLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7cUJBQzFDO2lCQUNKO2FBQ0o7aUJBQU07Z0JBQ0gsU0FBRyxDQUFDLEtBQUssQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO2dCQUNsQyxTQUFHLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ25CO1FBRUwsQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQyxRQUFnQixFQUFFLEVBQUU7WUFDMUMsTUFBTTtZQUNOLElBQUcsV0FBVyxJQUFFLElBQUk7Z0JBQUUsV0FBVyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUV2RCxDQUFDLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFFckIsQ0FBQztJQUVEOzs7O09BSUc7SUFDSSxVQUFVLENBQUMsUUFBZSxFQUFDLFFBQWtCO1FBRWhELElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUMsVUFBVSxHQUFnQjtZQUN6RSxJQUFJLE9BQU8sR0FBZSxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUM1QyxPQUFPLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQztZQUNuQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ3pFLElBQUksUUFBUTtnQkFBRSxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3hDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDUixDQUFDO0lBR0Q7OztPQUdHO0lBQ0ksWUFBWSxDQUFDLEtBQWM7UUFFOUIsSUFBSSxJQUFJLEdBQUcsSUFBSSxLQUFLLEVBQVUsQ0FBQztRQUMvQixLQUFLLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQTtRQUMxQixDQUFDLENBQUMsQ0FBQztRQUVILEtBQUksSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFDLENBQUMsR0FBQyxJQUFJLENBQUMsTUFBTSxFQUFDLENBQUMsRUFBRSxFQUFDO1lBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzlCLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBVSxFQUFFLEdBQVcsRUFBRSxFQUFFO2dCQUNwRCxJQUFHLEdBQUcsSUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUM7b0JBQ1gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQ25DO1lBQ0osQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7SUFFRDs7O09BR0c7SUFDSSxVQUFVLENBQUMsR0FBVTtRQUV2QixJQUFJLFFBQVEsR0FBVyxLQUFLLENBQUM7UUFDN0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFVLEVBQUUsR0FBVyxFQUFFLEVBQUU7WUFDcEQsSUFBRyxHQUFHLElBQUUsR0FBRyxFQUFDO2dCQUNSLFFBQVEsR0FBRyxJQUFJLENBQUM7YUFDbkI7UUFDSixDQUFDLENBQUMsQ0FBQztRQUVILElBQUcsUUFBUSxFQUFDO1lBQ1QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDNUI7YUFBSTtZQUNGLFNBQUcsQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLENBQUM7U0FDNUI7SUFDTixDQUFDOztBQTlKTCxnQ0ErSkM7QUE1SmtCLG1CQUFRLEdBQWUsSUFBSSxDQUFDOzs7OztBQ3hCL0MsOENBQTZDO0FBQzdDLElBQU8sWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7QUFDeEMsSUFBTyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztBQUU5QixvREFBZ0Q7QUFFaEQsd0NBQXFDO0FBQ3JDLDJEQUF3RDtBQUN4RCxpREFBbUQ7QUFHbkQ7Ozs7OztHQU1HO0FBQ0gsTUFBYSxZQUFhLFNBQVEsc0JBQVM7SUFBM0M7UUFHSSw4RkFBOEY7UUFDOUYsMkZBQTJGO1FBQzNGLDJGQUEyRjs7UUFFM0YsZUFBZTtRQUNQLGlCQUFZLEdBQWlCLElBQUksQ0FBQztRQUMxQyxlQUFlO1FBQ1AsaUJBQVksR0FBc0IsSUFBSSxDQUFDO1FBcUkvQyw4RkFBOEY7UUFDOUYsMkZBQTJGO0lBRS9GLENBQUM7SUEvSFUsTUFBTSxLQUFLLENBQUM7UUFDZixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVE7WUFBRSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDdkQsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ3pCLENBQUM7SUFFRCxLQUFLO1FBQ0QsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSx1QkFBVSxFQUFVLENBQUM7UUFDN0Msb0JBQVcsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUEsRUFBRTtZQUNyQyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM5QyxDQUFDLENBQUMsQ0FBQztRQUNILElBQUcsQ0FBQyxtQkFBVSxDQUFDLE9BQU8sQ0FBQyxvQkFBVyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsRUFDakQ7WUFDSSxJQUFJLENBQUMsV0FBVyxDQUFDLG9CQUFXLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBQyxDQUFDLENBQUMsQ0FBQztZQUM5QyxJQUFJLENBQUMsWUFBWSxDQUFDLG9CQUFXLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUM7U0FDckQ7SUFDTCxDQUFDO0lBQ0QsTUFBTTtJQUNOLENBQUM7SUFDRCxPQUFPO0lBQ1AsQ0FBQztJQUdELDhGQUE4RjtJQUM5RiwyRkFBMkY7SUFDM0YseUZBQXlGO0lBRXpGOzs7T0FHRztJQUNJLFlBQVksQ0FBQyxNQUFNO1FBRXRCLG9CQUFXLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixHQUFHLE1BQU0sQ0FBQztRQUN4QyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7SUFDdEMsQ0FBQztJQUVELDhGQUE4RjtJQUM5RiwyRkFBMkY7SUFDM0YsMEZBQTBGO0lBRTFGOzs7O09BSUc7SUFDSSxXQUFXLENBQUMsU0FBaUIsRUFBRSxLQUFhO1FBQy9DLElBQUksbUJBQVUsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLEVBQUU7WUFDL0IsU0FBRyxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1lBQy9CLE9BQU87U0FDVjtRQUNELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLEVBQUMsS0FBSyxDQUFDLENBQUM7SUFDOUYsQ0FBQztJQUVEOztPQUVHO0lBQ0ksV0FBVztRQUNkLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNuQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFBO1NBQzNCO0lBQ0wsQ0FBQztJQUVEOztPQUVHO0lBQ0ksWUFBWTtRQUNmLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNuQixJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQzdCO0lBQ0wsQ0FBQztJQUVEOztPQUVHO0lBQ0ksYUFBYTtRQUNoQixJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDbkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsQ0FBQztTQUM5QjtJQUNMLENBQUM7SUFFRDs7O09BR0c7SUFDSSxnQkFBZ0IsQ0FBQyxNQUFjO1FBQ2xDLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNuQixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7U0FDckM7SUFDTCxDQUFDO0lBRUQsOEZBQThGO0lBQzlGLDJGQUEyRjtJQUMzRiwwRkFBMEY7SUFFMUY7Ozs7T0FJRztJQUNJLGVBQWUsQ0FBQyxTQUFpQixFQUFFLEtBQWE7UUFDbkQsSUFBSSxtQkFBVSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsRUFBRTtZQUMvQixTQUFHLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3BCLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7UUFDRCxJQUFJLEtBQUssR0FBaUIsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxFQUFDLFlBQVksQ0FBQyxDQUFDO1FBRXpFLEtBQUssR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsRUFBQyxLQUFLLEVBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUMsR0FBRSxFQUFFO1lBQ2pHLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBQyxLQUFLLENBQUMsQ0FBQztRQUNyQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ0osS0FBSyxDQUFDLE1BQU0sR0FBRyxvQkFBVyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQztRQUM5QyxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksZUFBZSxDQUFDLEtBQW1CO1FBQ3RDLElBQUksS0FBSyxFQUFFO1lBQ1AsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDO1NBQ2hCO0lBQ0wsQ0FBQzs7QUE3SUwsb0NBa0pDO0FBcklHLDhGQUE4RjtBQUM5RiwyRkFBMkY7QUFDM0YsMkZBQTJGO0FBRTVFLHFCQUFRLEdBQWlCLElBQUksQ0FBQzs7Ozs7QUNuQ2pELDBDQUF5QztBQUd6QyxxREFBaUQ7QUFFakQ7Ozs7OztHQU1HO0FBQ0gsTUFBYSxXQUFXO0lBV3BCO1FBQ0ksSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLDhCQUFhLEVBQUUsQ0FBQztJQUNyQyxDQUFDO0lBRU0sSUFBSTtJQUNYLENBQUM7SUFFTSxLQUFLO0lBQ1osQ0FBQztJQUdNLEtBQUs7UUFDUixJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxFQUFFO1lBQ3JCLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDdEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7U0FDdEI7SUFDTCxDQUFDO0lBRU0sR0FBRyxDQUFDLEVBQVUsRUFBRSxJQUFZLEVBQUUsS0FBYSxFQUFFLE1BQWU7UUFDL0QsSUFBSSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUM7UUFDYixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ2pDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDcEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUM7UUFDdkIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDckIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBRU0sTUFBTSxDQUFDLFdBQWdCO1FBQzFCLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxlQUFRLENBQUMsU0FBUyxDQUFDLEVBQUU7WUFDeEQsSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUk7Z0JBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUUzQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7WUFDckIsSUFBSSxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7Z0JBQ3RELElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO2dCQUN0QixXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQ3hCO1NBQ0o7SUFDTCxDQUFDO0NBQ0o7QUFsREQsa0NBa0RDOzs7OztBQzlERDs7Ozs7O0dBTUc7QUFDSCxNQUFhLGFBQWE7SUFLdEI7UUFDSSxJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQztJQUN4QixDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLElBQUksQ0FBQyxRQUFnQixFQUFFLFdBQW9CO1FBQzlDLElBQUksQ0FBQyxlQUFlLEdBQUcsUUFBUSxDQUFDO1FBQ2hDLElBQUksV0FBVztZQUFFLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQztJQUM1RCxDQUFDO0lBRU0sS0FBSztRQUNSLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO0lBQ3hCLENBQUM7SUFFTSxNQUFNLENBQUMsV0FBbUI7UUFDN0IsSUFBSSxDQUFDLFVBQVUsSUFBSSxXQUFXLENBQUM7UUFDL0IsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDekMsSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDO1lBQ3hDLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0NBQ0o7QUEvQkQsc0NBK0JDOzs7OztBQ3RDRCxJQUFPLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO0FBQzlCLDRDQUEyQztBQUMzQyxvREFBZ0Q7QUFFaEQsc0RBQWtEO0FBQ2xELHdEQUFvRDtBQUNwRCxpREFBNkM7QUFFN0M7Ozs7OztHQU1HO0FBQ0gsTUFBYSxZQUFhLFNBQVEsc0JBQVM7SUFBM0M7O1FBRVksZ0JBQVcsR0FBVyxDQUFDLENBQUM7UUFDeEIscUJBQWdCLEdBQWtCLEVBQUUsQ0FBQztRQUNyQyxhQUFRLEdBQXVCLEVBQUUsQ0FBQztJQTBGOUMsQ0FBQztJQXRGVSxNQUFNLEtBQUssQ0FBQztRQUNmLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUTtZQUFFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN2RCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekIsQ0FBQztJQUVNLEtBQUs7UUFDUixJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztRQUNyQixzQkFBUyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzNDLHNCQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFRCxNQUFNO0lBQ04sQ0FBQztJQUVNLE9BQU87UUFDVixpQkFBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUN2QyxpQkFBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDL0Isc0JBQVMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDdEMsc0JBQVMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDeEMsQ0FBQztJQUVPLElBQUk7UUFDUixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDM0MsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1NBQzdDO0lBQ0wsQ0FBQztJQUVEOzs7Ozs7O09BT0c7SUFDSSxPQUFPLENBQUMsSUFBWSxFQUFFLEtBQWEsRUFBRSxNQUFXLEVBQUUsTUFBZ0IsRUFBRSxPQUFtQixJQUFJO1FBQzlGLElBQUksS0FBSyxJQUFJLENBQUM7WUFBRSxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQzFCLElBQUksUUFBUSxHQUFnQix3QkFBVSxDQUFDLEdBQUcsQ0FBQywwQkFBVyxDQUFDLENBQUM7UUFDeEQsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQ25CLElBQUksSUFBSSxJQUFJLElBQUk7WUFBRSxpQkFBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUM5RCxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDekYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDN0IsT0FBTyxRQUFRLENBQUMsRUFBRSxDQUFDO0lBQ3ZCLENBQUM7SUFFRDs7T0FFRztJQUNJLE9BQU8sQ0FBQyxJQUFZLEVBQUUsTUFBVyxFQUFFLE1BQWdCLEVBQUUsT0FBbUIsSUFBSTtRQUMvRSxJQUFJLFFBQVEsR0FBZ0Isd0JBQVUsQ0FBQyxHQUFHLENBQUMsMEJBQVcsQ0FBQyxDQUFDO1FBQ3hELEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUNuQixJQUFJLElBQUksSUFBSSxJQUFJO1lBQUUsaUJBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDOUQsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQ3JGLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzdCLE9BQU8sUUFBUSxDQUFDLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksV0FBVyxDQUFDLE9BQWU7UUFDOUIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN4QyxDQUFDO0lBRUQ7O09BRUc7SUFDSyxNQUFNO1FBQ1YsSUFBSSxLQUFrQixDQUFDO1FBQ3ZCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDbEMsS0FBSyxJQUFJLEVBQUUsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQ2xDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDM0MsS0FBSyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3pCLElBQUksS0FBSyxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUU7d0JBQ2hCLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQzt3QkFDZCx3QkFBVSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDMUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO3dCQUMzQixNQUFNO3FCQUNUO2lCQUNKO2FBQ0o7WUFFRCxpQkFBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztTQUMxQztJQUNMLENBQUM7O0FBN0ZMLG9DQThGQztBQXhGa0IscUJBQVEsR0FBaUIsSUFBSSxDQUFDOzs7OztBQ2xCakQsSUFBTyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztBQUMxQixJQUFPLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO0FBQ3hCLElBQU8sT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7QUFDOUIsZ0RBQWlEO0FBR2pELElBQWMsWUFBWSxDQTBIekI7QUExSEQsV0FBYyxZQUFZO0lBRXRCOzs7Ozs7T0FNRztJQUNILE1BQWEsVUFBVyxTQUFRLElBQUksQ0FBQyxNQUFNO1FBY3ZDO1lBQ0ksS0FBSyxFQUFFLENBQUM7WUFiWixTQUFTO1lBQ0QsY0FBUyxHQUFXLElBQUksQ0FBQztZQUNqQyxXQUFXO1lBQ0gsZUFBVSxHQUFjLElBQUksQ0FBQztZQUNyQyxVQUFVO1lBQ0gsY0FBUyxHQUFHLElBQUksU0FBUyxFQUFFLENBQUM7WUFTL0IsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBRXJCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN6QyxDQUFDO1FBVkQsVUFBVSxDQUFDLElBQVM7WUFDaEIsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQixDQUFDO1FBVUQ7O1dBRUc7UUFDSCxjQUFjO1lBQ1YsSUFBSSxDQUFDLFNBQVMsR0FBRyxxQkFBVyxDQUFDLGVBQWUsRUFBRSxDQUFDO1lBQy9DLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUVuQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ3ZCLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNqRSxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFbkUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDOUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFFL0IsQ0FBQztRQUVEOztXQUVHO1FBQ08sTUFBTSxDQUFDLElBQWtCO1lBQy9CLElBQUksSUFBSSxJQUFJLElBQUk7Z0JBQUUsSUFBSSxHQUFHLElBQUksQ0FBQztZQUM5QixJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDMUUsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2hGLENBQUM7UUFHRDs7V0FFRztRQUNILGFBQWE7WUFDVCxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxJQUFJLEVBQUU7Z0JBQzFCLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUMzRDtRQUNMLENBQUM7UUFFRDs7V0FFRztRQUNILGdCQUFnQjtZQUNaLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLEVBQUU7Z0JBQ3hCLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDMUQ7UUFDTCxDQUFDO1FBRUQ7Ozs7OztXQU1HO1FBQ0gsV0FBVyxDQUFDLFlBQXNCLElBQUk7WUFDbEMsMkJBQTJCO1lBQzNCLElBQUcsU0FBUyxJQUFFLElBQUksRUFBRTtnQkFDaEIsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7YUFDOUI7aUJBQUk7Z0JBQ0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7YUFDOUI7WUFDRCxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMxQixJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7WUFDakIsSUFBSSxTQUFTLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxFQUFFO2dCQUM1QyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7Z0JBQ3RCLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWTtvQkFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3RGO1lBQ0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFDLEdBQUUsRUFBRTtnQkFDcEMsSUFBRyxTQUFTLENBQUMsUUFBUTtvQkFBRSxTQUFTLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ3ZELENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVELGVBQWU7UUFDZixTQUFTO1FBQ1QsQ0FBQztRQUdELGVBQWUsQ0FBQyxPQUFlLEdBQUcsRUFBQyxFQUFFO1lBQ2pDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7WUFDN0IsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBRWQsYUFBYTtZQUNiLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ25CLEtBQUssQ0FBQyxFQUFFLENBQUMsTUFBTSxFQUFFO2dCQUNiLE1BQU0sRUFBRSxDQUFDO2dCQUNULE1BQU0sRUFBRSxDQUFDO2FBQ1osRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDOUUsQ0FBQztRQUVELEtBQUs7WUFDRCxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDdEIsQ0FBQztLQUdKO0lBaEhZLHVCQUFVLGFBZ0h0QixDQUFBO0FBQ0wsQ0FBQyxFQTFIYSxZQUFZLEdBQVosb0JBQVksS0FBWixvQkFBWSxRQTBIekI7QUFHRzs7Ozs7O0dBTUc7QUFDSCxNQUFhLFNBQVM7SUFPbEIsWUFBWSxPQUFlLEdBQUcsRUFBRSxPQUFZLElBQUksRUFBRSxTQUFrQixJQUFJLEVBQUUsZUFBd0IsSUFBSSxFQUFDLEtBQWMsSUFBSTtRQU5sSCxTQUFJLEdBQVUsR0FBRyxDQUFDO1FBQ2xCLFNBQUksR0FBTyxJQUFJLENBQUM7UUFDaEIsV0FBTSxHQUFXLElBQUksQ0FBQztRQUN0QixpQkFBWSxHQUFXLElBQUksQ0FBQztRQUM1QixhQUFRLEdBQWEsSUFBSSxDQUFDO1FBSTdCLElBQUcsSUFBSSxJQUFFLElBQUk7WUFBRSxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQyxJQUFHLElBQUksSUFBRSxJQUFJO1lBQUUsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEMsSUFBRyxNQUFNLElBQUUsSUFBSTtZQUFFLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3RDLElBQUcsWUFBWSxJQUFFLElBQUk7WUFBRSxJQUFJLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQztRQUN4RCxJQUFHLEVBQUUsSUFBRSxJQUFJO1lBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDcEMsQ0FBQztDQUNKO0FBZkQsOEJBZUM7Ozs7O0FDNUpMLGdEQUE0QztBQUM1QyxvREFBZ0Q7QUFDaEQsd0NBQXFDO0FBQ3JDLDBEQUFzRDtBQUd0RCxJQUFjLFdBQVcsQ0FtSnhCO0FBbkpELFdBQWMsV0FBVztJQUVyQjs7Ozs7O09BTUc7SUFDSCxNQUFhLE9BQVEsU0FBUSxJQUFJLENBQUMsS0FBSztRQXNCbkM7WUFDSSxLQUFLLEVBQUUsQ0FBQztZQWZaOztlQUVHO1lBQ08sY0FBUyxHQUFRLElBQUksQ0FBQztZQU94QixhQUFRLEdBQUcsS0FBSyxDQUFDO1lBRWxCLGdCQUFXLEdBQWtCLElBQUksS0FBSyxFQUFVLENBQUM7WUFJcEQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLG9CQUFRLEVBQUUsQ0FBQztRQUN0QyxDQUFDO1FBRUQsY0FBYztZQUNWLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN2QixJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNoQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1lBQzlCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7UUFDcEMsQ0FBQztRQUVEOzs7OztXQUtHO1FBQ0ksS0FBSyxDQUFDLEtBQVUsRUFBQyxXQUFxQixFQUFDLFdBQXFCO1lBRS9ELElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1lBQ3RCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQ3JCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFbkIsd0JBQVUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUMsV0FBVyxFQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3JFLENBQUM7UUFHTSxLQUFLO1lBQ1IsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ2YsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ25CLENBQUM7UUFFTSxPQUFPO1lBQ1YsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ2YsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFhLEVBQUUsRUFBRTtnQkFDdkMsNEJBQVksQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3RDLENBQUMsQ0FBQyxDQUFBO1lBQ0YsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ3BCLENBQUM7UUFHRDs7O1dBR0c7UUFDTyxNQUFNLENBQUMsS0FBSztZQUVsQixJQUFJLEtBQUssSUFBSSxJQUFJLEVBQUU7Z0JBQ2YsU0FBRyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQTthQUNuQjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ2hCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO2dCQUNyQixJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7YUFDckI7UUFFTCxDQUFDO1FBR08sVUFBVTtZQUNkLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtnQkFDZixJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxFQUFFO29CQUN4QixJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO29CQUN6QixJQUFJLEdBQUcsR0FBRyxJQUFJLEdBQUcsRUFBRSxDQUFDO29CQUNwQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lCQUN0QjtnQkFDRCxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUM5QjtRQUNMLENBQUM7UUFHRDs7V0FFRztRQUNPLFFBQVE7UUFFbEIsQ0FBQztRQUVEOzs7V0FHRztRQUNPLE1BQU0sQ0FBQyxLQUFVO1FBRTNCLENBQUM7UUFFRDs7V0FFRztRQUNPLE9BQU8sQ0FBQyxLQUFVO1FBRTVCLENBQUM7UUFHRDs7V0FFRztRQUNJLE1BQU07UUFFYixDQUFDO1FBRUQ7O1dBRUc7UUFDTyxPQUFPO1FBRWpCLENBQUM7UUFFRDs7V0FFRztRQUNPLE9BQU87UUFFakIsQ0FBQzs7SUFySUQ7O09BRUc7SUFDWSxjQUFNLEdBQU0sRUFBQyxNQUFNLEVBQUMsT0FBTyxFQUFDLE9BQU8sRUFBQyxFQUFDLE9BQU8sRUFBQyxJQUFJLEVBQUMsUUFBUSxFQUFDLEdBQUcsRUFBQyxFQUFDLFVBQVUsRUFBQyxFQUFFLEVBQUMsWUFBWSxFQUFDLEVBQUUsRUFBQyxDQUFDO0lBTHJHLG1CQUFPLFVBeUluQixDQUFBO0FBQ0wsQ0FBQyxFQW5KYSxXQUFXLEdBQVgsbUJBQVcsS0FBWCxtQkFBVyxRQW1KeEI7Ozs7O0FDekpELHVEQUFtRDtBQUduRCxJQUFjLFVBQVUsQ0F3R3ZCO0FBeEdELFdBQWMsVUFBVTtJQUVwQjs7Ozs7O09BTUc7SUFDSCxNQUFhLFFBQVMsU0FBUSxJQUFJLENBQUMsSUFBSTtRQUF2Qzs7WUFFSSxXQUFXO1lBQ0QsZUFBVSxHQUFrQixFQUFFLENBQUM7WUFFbEMsU0FBSSxHQUFRLElBQUksQ0FBQztRQXlGNUIsQ0FBQztRQXZGRyxVQUFVO1FBQ1YsVUFBVSxDQUFDLElBQVM7WUFDaEIsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN2QixJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDbEIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3hCLENBQUM7UUFFRCxTQUFTO1lBRUwsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFXLEVBQUUsRUFBRTtnQkFDcEMsMEJBQVcsQ0FBQyxDQUFDLENBQUMsbUJBQW1CLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDOUQsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBRUQ7O1dBRUc7UUFDTyxZQUFZO1lBQ2xCLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLElBQUksRUFBRTtnQkFDdkIsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBZ0IsQ0FBQTtnQkFDeEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUMxQjtRQUNMLENBQUM7UUFFRDs7V0FFRztRQUNPLE1BQU0sQ0FBQyxJQUFrQjtZQUMvQixJQUFJLElBQUksSUFBSSxJQUFJO2dCQUFFLElBQUksR0FBRyxJQUFJLENBQUM7WUFDOUIsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzFFLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNoRixDQUFDO1FBRUQ7OztXQUdHO1FBQ08sVUFBVSxDQUFDLElBQWtCO1lBQ25DLElBQUksSUFBSSxJQUFJLElBQUk7Z0JBQUUsSUFBSSxHQUFHLElBQUksQ0FBQztZQUM5QixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1lBQzlCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7UUFDcEMsQ0FBQztRQUVEOzs7V0FHRztRQUNPLFlBQVksQ0FBQyxHQUFXO1lBQzlCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzFCLDBCQUFXLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ3ZELDBCQUFXLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNwQyxDQUFDO1FBRUQ7O1dBRUc7UUFDTyxNQUFNLENBQUMsSUFBYztZQUMzQix3Q0FBd0M7WUFDeEMsRUFBRTtZQUNGLElBQUk7UUFDUixDQUFDO1FBRUQ7OztXQUdHO1FBQ0gsR0FBRyxDQUFDLE9BQVksSUFBSTtZQUVoQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztZQUNqQixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMxQixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDaEIsQ0FBQztRQUVEOztXQUVHO1FBQ0gsSUFBSTtZQUNBLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLENBQUM7UUFFRDs7V0FFRztRQUNILElBQUk7WUFDQSxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUN6QixDQUFDO0tBRUo7SUE5RlksbUJBQVEsV0E4RnBCLENBQUE7QUFDTCxDQUFDLEVBeEdhLFVBQVUsR0FBVixrQkFBVSxLQUFWLGtCQUFVLFFBd0d2Qjs7Ozs7QUMxR0QsOENBQThHO0FBQzlHLHFDQUFrQztBQUNsQyx1Q0FBd0M7QUFDeEMsMENBQTJGO0FBQzNGLElBQU8sT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7QUFDOUIsNERBQXdEO0FBQ3hELDREQUF3RDtBQUV4RCwrREFBMkQ7QUFDM0Qsa0VBQThEO0FBQzlELCtEQUEyRDtBQUMzRCxrRUFBOEQ7QUFDOUQsa0VBQThEO0FBQzlELGtFQUE2RDtBQUM3RDs7Ozs7O0dBTUc7QUFDSCxNQUFhLE1BQU07SUFTZjtRQU5PLFdBQU0sR0FBaUIscUJBQVksQ0FBQyxDQUFDLENBQUM7UUFDdEMsU0FBSSxHQUFlLG1CQUFVLENBQUMsQ0FBQyxDQUFDO1FBQ2hDLE9BQUUsR0FBYSxpQkFBUSxDQUFDLENBQUMsQ0FBQztRQUMxQixVQUFLLEdBQWdCLG9CQUFXLENBQUMsQ0FBQyxDQUFDO0lBSTFDLENBQUM7SUFHTSxNQUFNLEtBQUssQ0FBQztRQUNmLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBRSxJQUFJO1lBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLE1BQU0sRUFBRSxDQUFDO1FBQ3RELE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN6QixDQUFDO0lBRUQ7O09BRUc7SUFDSSxHQUFHO1FBQ04sU0FBRyxDQUFDLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1FBQ3BDLHlCQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3JCLElBQUksaUJBQVEsQ0FBQyxDQUFDLENBQUMsZUFBZSxJQUFJLElBQUksSUFBSSxrQkFBUyxDQUFDLENBQUMsQ0FBQyxjQUFjLElBQUksSUFBSSxFQUFFO1lBQzFFLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRSxFQUFFO2dCQUNqQixNQUFNO2dCQUNOLGVBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDakIsVUFBVTtnQkFDVixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7Z0JBQ3BCLFVBQVU7Z0JBQ1YsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQ2xELDRCQUE0QjtnQkFDNUIsd0JBQVUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLGtCQUFTLENBQUMsQ0FBQyxDQUFDLGNBQWMsRUFBQyxJQUFJLEVBQUMsSUFBSSxzQkFBUyxDQUFDLElBQUksRUFBQyxHQUFFLEVBQUU7b0JBQzFFLElBQUksS0FBSyxHQUFHLGlCQUFRLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQztvQkFDdkMsSUFBSSxLQUFLLElBQUksU0FBUyxFQUFFO3dCQUNwQixJQUFJLFdBQVcsR0FBRyxJQUFJLEtBQUssRUFBRSxDQUFDO3dCQUM5QixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQzt3QkFDakMsV0FBVyxDQUFDLE9BQU8sRUFBRSxDQUFDO3FCQUN6QjtnQkFDTCxDQUFDLENBQUMsQ0FBQyxDQUFBO1lBQ1AsQ0FBQyxDQUFDLENBQUM7U0FFTjthQUFNO1lBQ0osU0FBRyxDQUFDLEtBQUssQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1NBQ3RDO0lBRUwsQ0FBQztJQUVEOztPQUVHO0lBQ0ssV0FBVyxDQUFDLGFBQWE7UUFFN0IsYUFBYTtRQUNiLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLElBQUksb0JBQWEsQ0FBQyxJQUFJLEVBQUU7WUFDM0MsTUFBTSxDQUFDLElBQUksQ0FBQyxxQkFBWSxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUscUJBQVksQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDeEU7YUFBTTtZQUNILElBQUksQ0FBQyxJQUFJLENBQUMscUJBQVksQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLHFCQUFZLENBQUMsQ0FBQyxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDbEY7UUFDRCxVQUFVO1FBQ1YsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1FBQzVCLFVBQVU7UUFDVixJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxvQkFBYSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUM3RCxZQUFZO1FBQ1osSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDcEUsV0FBVztRQUNYLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLHNCQUFlLENBQUMsVUFBVSxDQUFDO1FBQ25ELFlBQVk7UUFDWixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxnQkFBUyxDQUFDLFdBQVcsQ0FBQztRQUMxQyxZQUFZO1FBQ1osSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsZ0JBQVMsQ0FBQyxXQUFXLENBQUM7UUFDMUMsWUFBWTtRQUNaLElBQUcsbUJBQVUsQ0FBQyxDQUFDLENBQUMsT0FBTztZQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDM0Usd0RBQXdEO1FBQ2xELElBQUksb0JBQVcsQ0FBQyxDQUFDLENBQUMsa0JBQWtCLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLElBQUksTUFBTTtZQUFFLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzlHLFdBQVc7UUFDWCxJQUFJLG9CQUFXLENBQUMsQ0FBQyxDQUFDLGNBQWMsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUM7WUFBRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNoRyxZQUFZO1FBQ1osSUFBSSxvQkFBVyxDQUFDLENBQUMsQ0FBQyxNQUFNO1lBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQVcsQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFDLG9CQUFXLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3BGLGNBQWM7UUFDZCxJQUFJLE9BQU8sQ0FBQyxRQUFRLElBQUksT0FBTyxDQUFDLFVBQVUsRUFBRTtZQUN4QyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFLLENBQUM7U0FDcEM7UUFDRCxjQUFjO1FBQ2QsUUFBUSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztRQUNsQyxlQUFlO1FBQ2YsUUFBUSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFDNUIsYUFBYTtRQUNiLFFBQVEsQ0FBQyxpQkFBaUIsR0FBRyxPQUFPLENBQUMsQ0FBQyxpQkFBaUI7UUFDdkQsY0FBYztRQUNkLFFBQVEsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1FBQzdCLGlCQUFpQjtRQUNqQixJQUFJLENBQUMsR0FBRyxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztRQUNsQyxjQUFjO1FBQ2QsSUFBRyxzQkFBYSxDQUFDLENBQUMsQ0FBQyxhQUFhLEVBQUM7WUFDN0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsc0JBQWEsQ0FBQyxDQUFDLENBQUMsYUFBYSxFQUN6RCxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsYUFBYSxDQUFDLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1NBQ3BGO2FBQUk7WUFDRCxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDeEI7SUFHTCxDQUFDO0lBRUQ7O09BRUc7SUFDTSxZQUFZO1FBQ2pCLDBCQUFXLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3RCLDRCQUFZLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3ZCLHdCQUFVLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3JCLDBCQUFXLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3RCLDRCQUFZLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3ZCLDRCQUFZLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQzNCLENBQUM7SUFFRDs7T0FFRztJQUNLLGFBQWE7SUFFckIsQ0FBQzs7QUEzSEwsd0JBNkhDO0FBakhrQixlQUFRLEdBQVcsSUFBSSxDQUFDOzs7OztBQ2pDM0MsaUNBQXFGO0FBQ3JGLGlEQUE4QztBQUM5Qyw4REFBMEQ7QUFDMUQsd0RBQW9EO0FBQ3BELDRFQUF3RTtBQUd2RTs7Ozs7RUFLRTtBQUdIOztHQUVHO0FBQ0gsTUFBYSxRQUFTLFNBQVEscUJBQVM7SUFBdkM7O1FBRUksVUFBVTtRQUNILG9CQUFlLEdBQVcsSUFBSSxDQUFDO1FBQ3RDLFlBQVk7UUFDTCxvQkFBZSxHQUFXLEVBQUUsQ0FBQztRQUNwQyxZQUFZO1FBQ0wscUJBQWdCLEdBQVEsc0JBQVMsQ0FBQztRQUN6QyxvQkFBb0I7UUFDYixvQkFBZSxHQUFRLDBCQUFXLENBQUM7SUFTOUMsQ0FBQztJQUxVLE1BQU0sS0FBSyxDQUFDO1FBQ2YsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRO1lBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLFFBQVEsRUFBRSxDQUFDO1FBQ25ELE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN6QixDQUFDOztBQWhCTCw0QkFrQkM7QUFOa0IsaUJBQVEsR0FBYSxJQUFJLENBQUM7QUFRN0M7O0dBRUc7QUFDSCxNQUFhLFNBQVUsU0FBUSxxQkFBUztJQWFwQztRQUNJLEtBQUssRUFBRSxDQUFDO1FBWlosc0JBQXNCO1FBQ2YsbUJBQWMsR0FBYSxJQUFJLG9CQUFRLEVBQUUsQ0FBQztRQUNqRCxpQkFBaUI7UUFDVixtQkFBYyxHQUFZLElBQUksb0JBQVEsRUFBRSxDQUFDO1FBVTVDLFlBQVk7UUFDWixVQUFVLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUEsRUFBRTtZQUN4QyxJQUFJLENBQUMsY0FBYztpQkFDbEIsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNyQyxDQUFDLENBQUMsQ0FBQztRQUNILFFBQVE7UUFDUixXQUFXLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFBLEVBQUU7WUFDckMsSUFBSSxDQUFDLGNBQWM7aUJBQ2xCLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdEMsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBakJNLE1BQU0sS0FBSyxDQUFDO1FBQ2YsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRO1lBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLFNBQVMsRUFBRSxDQUFDO1FBQ3BELE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN6QixDQUFDOztBQVhMLDhCQTBCQztBQW5Ca0Isa0JBQVEsR0FBYyxJQUFJLENBQUM7QUFxQjlDOztHQUVHO0FBQ0gsTUFBYSxXQUFZLFNBQVEscUJBQVM7SUFxQnRDO1FBRUksS0FBSyxFQUFFLENBQUM7UUFyQlosWUFBWTtRQUNMLGdCQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ3hCLFdBQVc7UUFDSixtQkFBYyxHQUFHLEtBQUssQ0FBQztRQUM5QixXQUFXO1FBQ0osdUJBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQ2xDLFlBQVk7UUFDTCxzQkFBaUIsR0FBRyxLQUFLLENBQUM7UUFDakMsU0FBUztRQUNGLHFCQUFnQixHQUFHLENBQUMsQ0FBQztRQUM1QixVQUFVO1FBQ0gsaUJBQVksR0FBd0IsSUFBSSxDQUFDO1FBVzVDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxLQUFLLEVBQWlCLENBQUM7UUFDL0Msa0ZBQWtGO0lBQ3RGLENBQUM7SUFWTSxNQUFNLEtBQUssQ0FBQztRQUNmLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUTtZQUFFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxXQUFXLEVBQUUsQ0FBQztRQUN0RCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekIsQ0FBQzs7QUFuQkwsa0NBMkJDO0FBWmtCLG9CQUFRLEdBQWdCLElBQUksQ0FBQztBQWNoRDs7R0FFRztBQUNILE1BQWEsVUFBVyxTQUFRLHFCQUFTO0lBSXJDO1FBRUksS0FBSyxFQUFFLENBQUM7UUFKWixlQUFlO1FBQ1IscUJBQWdCLEdBQXVCLElBQUksS0FBSyxFQUFnQixDQUFDO0lBSXhFLENBQUM7SUFFTSxNQUFNLEtBQUssQ0FBQztRQUNmLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUTtZQUFFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxVQUFVLEVBQUUsQ0FBQztRQUNyRCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekIsQ0FBQzs7QUFaTCxnQ0FhQztBQUxrQixtQkFBUSxHQUFlLElBQUksQ0FBQztBQU8vQzs7R0FFRztBQUNILE1BQWEsVUFBVyxTQUFRLHFCQUFTO0lBQXpDOztRQUVJLGtCQUFrQjtRQUNYLGNBQVMsR0FBa0Isb0JBQWEsQ0FBQyxJQUFJLENBQUM7UUFDckQsVUFBVTtRQUNILFlBQU8sR0FBVyxLQUFLLENBQUM7SUFRbkMsQ0FBQztJQUpVLE1BQU0sS0FBSyxDQUFDO1FBQ2YsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRO1lBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLFVBQVUsRUFBRSxDQUFDO1FBQ3JELE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN6QixDQUFDOztBQVpMLGdDQWFDO0FBTGtCLG1CQUFRLEdBQWUsSUFBSSxDQUFDO0FBTy9DOztHQUVHO0FBQ0gsTUFBYSxhQUFjLFNBQVEscUJBQVM7SUFBNUM7O1FBRUksWUFBWTtRQUNMLGtCQUFhLEdBQVcsS0FBSyxDQUFDO1FBQ3JDLFNBQVM7UUFDRixlQUFVLEdBQVUsQ0FBQyxDQUFDO1FBQzdCLGFBQWE7UUFDTixrQkFBYSxHQUFVLFNBQVMsR0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBTzVELENBQUM7SUFKVSxNQUFNLEtBQUssQ0FBQztRQUNmLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUTtZQUFFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxhQUFhLEVBQUUsQ0FBQztRQUN4RCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekIsQ0FBQzs7QUFiTCxzQ0FjQztBQUxrQixzQkFBUSxHQUFrQixJQUFJLENBQUM7QUFRbEQ7O0dBRUc7QUFDSCxNQUFhLFlBQWEsU0FBUSxxQkFBUztJQUEzQzs7UUFFSSxZQUFZO1FBQ0wsZ0JBQVcsR0FBVyxHQUFHLENBQUM7UUFDakMsWUFBWTtRQUNMLGlCQUFZLEdBQVcsSUFBSSxDQUFDO1FBQ25DLFVBQVU7UUFDSCxjQUFTLEdBQWtCLG9CQUFhLENBQUMsY0FBYyxDQUFDO0lBUW5FLENBQUM7SUFMVSxNQUFNLEtBQUssQ0FBQztRQUNmLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUTtZQUFFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN2RCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekIsQ0FBQzs7QUFiTCxvQ0FlQztBQU5rQixxQkFBUSxHQUFpQixJQUFJLENBQUM7QUFTakQ7O0dBRUc7QUFDSCxNQUFhLFdBQVksU0FBUSxxQkFBUztJQUExQzs7UUFFSSxZQUFZO1FBQ0wsWUFBTyxHQUFZLElBQUksQ0FBQztRQUMvQixhQUFhO1FBQ04sbUJBQWMsR0FBWSxLQUFLLENBQUM7UUFDdkMsVUFBVTtRQUNILHVCQUFrQixHQUFXLEtBQUssQ0FBQztRQUMxQyxZQUFZO1FBQ0wsV0FBTSxHQUFZLElBQUksQ0FBQztRQUM5QixhQUFhO1FBQ04sV0FBTSxHQUFVLENBQUMsQ0FBQztRQUN6QixhQUFhO1FBQ04sV0FBTSxHQUFVLEdBQUcsQ0FBQztJQU8vQixDQUFDO0lBSlUsTUFBTSxLQUFLLENBQUM7UUFDZixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVE7WUFBRSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksV0FBVyxFQUFFLENBQUM7UUFDdEQsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ3pCLENBQUM7O0FBbkJMLGtDQW9CQztBQUxrQixvQkFBUSxHQUFnQixJQUFJLENBQUM7QUFPaEQ7O0dBRUc7QUFDSCxNQUFhLFFBQVMsU0FBUSxxQkFBUztJQUF2Qzs7UUFFSSxZQUFZO1FBQ0wsY0FBUyxHQUFVLDZDQUE2QyxDQUFDO0lBTzVFLENBQUM7SUFKVSxNQUFNLEtBQUssQ0FBQztRQUNmLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUTtZQUFFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxRQUFRLEVBQUUsQ0FBQztRQUNuRCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekIsQ0FBQzs7QUFUTCw0QkFVQztBQUxrQixpQkFBUSxHQUFhLElBQUksQ0FBQztBQVM3QyxNQUFNO0FBQ04sZUFBZTtBQUNmLE1BQU07QUFDTiw2Q0FBNkM7QUFFN0MseURBQXlEO0FBQ3pELDBEQUEwRDtBQUMxRCxzREFBc0Q7QUFDdEQsbUNBQW1DO0FBQ25DLHFDQUFxQztBQUNyQywwQ0FBMEM7QUFFMUMsaURBQWlEO0FBRWpELHdDQUF3QztBQUN4QywrREFBK0Q7QUFDL0QsZ0NBQWdDO0FBQ2hDLFFBQVE7QUFFUixJQUFJO0FBRUosTUFBTTtBQUNOLFVBQVU7QUFDVixNQUFNO0FBQ04sOENBQThDO0FBRTlDLGlDQUFpQztBQUNqQyxrQ0FBa0M7QUFDbEMsb0NBQW9DO0FBQ3BDLDhJQUE4STtBQUc5SSxrREFBa0Q7QUFFbEQseUNBQXlDO0FBQ3pDLGdFQUFnRTtBQUNoRSxnQ0FBZ0M7QUFDaEMsUUFBUTtBQUNSLElBQUk7OztBQ25RSjs7Ozs7Ozs7R0FRRzs7O0FBRUgsSUFBTyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztBQUUxQjs7R0FFRztBQUNILElBQVksYUFtQlg7QUFuQkQsV0FBWSxhQUFhO0lBQ3JCLGFBQWE7SUFDYiw4Q0FBZSxLQUFLLENBQUMsVUFBVSxrQkFBQSxDQUFBO0lBQy9CLGFBQWE7SUFDYiwrQ0FBZ0IsS0FBSyxDQUFDLGNBQWMsbUJBQUEsQ0FBQTtJQUNwQyxhQUFhO0lBQ2IsOENBQWUsS0FBSyxDQUFDLGFBQWEsa0JBQUEsQ0FBQTtJQUNsQyxhQUFhO0lBQ2IsK0NBQWdCLEtBQUssQ0FBQyxjQUFjLG1CQUFBLENBQUE7SUFDcEMsYUFBYTtJQUNiLDJDQUFZLEtBQUssQ0FBQyxVQUFVLGVBQUEsQ0FBQTtJQUM1QixhQUFhO0lBQ2IsaURBQWtCLEtBQUssQ0FBQyxpQkFBaUIscUJBQUEsQ0FBQTtJQUN6QyxhQUFhO0lBQ2Isa0RBQW1CLEtBQUssQ0FBQyxrQkFBa0Isc0JBQUEsQ0FBQTtJQUMzQyxhQUFhO0lBQ2IsZ0RBQWlCLEtBQUssQ0FBQyxnQkFBZ0Isb0JBQUEsQ0FBQTtJQUN2QyxhQUFhO0lBQ2IsOENBQWUsS0FBSyxDQUFDLGFBQWEsa0JBQUEsQ0FBQTtBQUN0QyxDQUFDLEVBbkJXLGFBQWEsR0FBYixxQkFBYSxLQUFiLHFCQUFhLFFBbUJ4QjtBQUVEOztHQUVHO0FBQ0gsSUFBWSxlQUlYO0FBSkQsV0FBWSxlQUFlO0lBQ3ZCLHNDQUFtQixDQUFBO0lBQ25CLGtEQUErQixDQUFBO0lBQy9CLDhDQUEyQixDQUFBO0FBQy9CLENBQUMsRUFKVyxlQUFlLEdBQWYsdUJBQWUsS0FBZix1QkFBZSxRQUkxQjtBQUVEOztLQUVLO0FBQ0wsSUFBWSxrQkFHWDtBQUhELFdBQVksa0JBQWtCO0lBQzFCLHFFQUFTLENBQUE7SUFDVCx1RUFBVSxDQUFBO0FBQ2QsQ0FBQyxFQUhXLGtCQUFrQixHQUFsQiwwQkFBa0IsS0FBbEIsMEJBQWtCLFFBRzdCO0FBRUQ7O0dBRUc7QUFDSCxJQUFZLGdCQUlYO0FBSkQsV0FBWSxnQkFBZ0I7SUFDeEIscURBQUcsQ0FBQTtJQUNILHlEQUFLLENBQUE7SUFDTCwyREFBTSxDQUFBO0FBQ1YsQ0FBQyxFQUpXLGdCQUFnQixHQUFoQix3QkFBZ0IsS0FBaEIsd0JBQWdCLFFBSTNCO0FBRUQ7O0dBRUc7QUFDSCxJQUFZLGFBV1g7QUFYRCxXQUFZLGFBQWE7SUFDckIsaURBQVEsQ0FBQTtJQUNSLG1EQUFLLENBQUE7SUFDTCxpRUFBWSxDQUFBO0lBQ1oscURBQU0sQ0FBQTtJQUNOLCtEQUFXLENBQUE7SUFDWCxpREFBSSxDQUFBO0lBQ0oseURBQVEsQ0FBQTtJQUNSLCtDQUFHLENBQUE7SUFDSCwyREFBUyxDQUFBO0lBQ1QsK0NBQUcsQ0FBQTtBQUNQLENBQUMsRUFYVyxhQUFhLEdBQWIscUJBQWEsS0FBYixxQkFBYSxRQVd4QjtBQUVEOztHQUVHO0FBQ0gsSUFBWSxTQU9YO0FBUEQsV0FBWSxTQUFTO0lBQ2pCLCtCQUFrQixDQUFBO0lBQ2xCLG1DQUFzQixDQUFBO0lBQ3RCLGlDQUFvQixDQUFBO0lBQ3BCLDZCQUFnQixDQUFBO0lBQ2hCLG1DQUFzQixDQUFBO0lBQ3RCLG1DQUFzQixDQUFBO0FBQzFCLENBQUMsRUFQVyxTQUFTLEdBQVQsaUJBQVMsS0FBVCxpQkFBUyxRQU9wQjtBQUVEOztHQUVHO0FBQ0gsSUFBWSxpQkFNWDtBQU5ELFdBQVksaUJBQWlCO0lBQ3pCLHlEQUFRLENBQUE7SUFDUix5REFBSSxDQUFBO0lBQ0osdURBQUcsQ0FBQTtJQUNILCtEQUFPLENBQUE7SUFDUCx1REFBRyxDQUFBO0FBQ1AsQ0FBQyxFQU5XLGlCQUFpQixHQUFqQix5QkFBaUIsS0FBakIseUJBQWlCLFFBTTVCO0FBRUQ7O0dBRUc7QUFDSCxJQUFZLGFBR1g7QUFIRCxXQUFZLGFBQWE7SUFDckIsNEJBQVcsQ0FBQTtJQUNYLDRCQUFXLENBQUE7QUFDZixDQUFDLEVBSFcsYUFBYSxHQUFiLHFCQUFhLEtBQWIscUJBQWEsUUFHeEI7QUFFRDs7R0FFRztBQUNILElBQVksY0FJWDtBQUpELFdBQVksY0FBYztJQUN0Qiw2Q0FBMkIsQ0FBQTtJQUMzQiwyQ0FBeUIsQ0FBQTtJQUN6QixpREFBK0IsQ0FBQTtBQUNuQyxDQUFDLEVBSlcsY0FBYyxHQUFkLHNCQUFjLEtBQWQsc0JBQWMsUUFJekI7QUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0FvQkc7QUFFSDs7R0FFRztBQUNILElBQVksaUJBb0JYO0FBcEJELFdBQVksaUJBQWlCO0lBQ3pCLG1DQUFjLENBQUE7SUFDZCxtQ0FBYyxDQUFBO0lBQ2Qsa0NBQWEsQ0FBQTtJQUNiLHVDQUFrQixDQUFBO0lBQ2xCLG1DQUFjLENBQUE7SUFDZCxvQ0FBZSxDQUFBO0lBQ2YsbUNBQWMsQ0FBQTtJQUNkLG1DQUFjLENBQUE7SUFDZCxtQ0FBYyxDQUFBO0lBQ2QsbUNBQWMsQ0FBQTtJQUNkLHVDQUFrQixDQUFBO0lBQ2xCLG9DQUFlLENBQUE7SUFDZixzQ0FBaUIsQ0FBQTtJQUNqQiwwQ0FBcUIsQ0FBQTtJQUNyQixvQ0FBZSxDQUFBO0lBQ2Ysb0NBQWUsQ0FBQTtJQUNmLG1DQUFjLENBQUE7SUFDZCx3Q0FBbUIsQ0FBQTtJQUNuQix3Q0FBbUIsQ0FBQTtBQUN2QixDQUFDLEVBcEJXLGlCQUFpQixHQUFqQix5QkFBaUIsS0FBakIseUJBQWlCLFFBb0I1QjtBQUVEOztHQUVHO0FBQ0gsSUFBWSxjQUtYO0FBTEQsV0FBWSxjQUFjO0lBQ3RCLG1DQUFpQixDQUFBO0lBQ2pCLGlDQUFlLENBQUE7SUFDZixxQ0FBbUIsQ0FBQTtJQUNuQixxQ0FBbUIsQ0FBQTtBQUN2QixDQUFDLEVBTFcsY0FBYyxHQUFkLHNCQUFjLEtBQWQsc0JBQWMsUUFLekI7QUFFRDs7R0FFRztBQUNILElBQVksYUFHWDtBQUhELFdBQVksYUFBYTtJQUNyQiwrQkFBYyxDQUFBO0lBQ2Qsb0NBQW1CLENBQUE7QUFDdkIsQ0FBQyxFQUhXLGFBQWEsR0FBYixxQkFBYSxLQUFiLHFCQUFhLFFBR3hCOzs7OztBQ3JMRCx1Q0FBd0M7QUFFeEM7Ozs7OztHQU1HO0FBQ0gsTUFBYSxVQUFVO0lBQXZCO1FBRVksV0FBTSxHQUFXLEVBQUUsQ0FBQztJQTJEaEMsQ0FBQztJQXpEVSxHQUFHLENBQUMsR0FBUSxFQUFFLEtBQVE7UUFDekIsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQztZQUFFLE9BQU8sS0FBSyxDQUFDO1FBQ25DLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFTSxNQUFNLENBQUMsR0FBUTtRQUNsQixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDNUIsQ0FBQztJQUVNLE1BQU0sQ0FBQyxHQUFRO1FBQ2xCLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDO0lBQ3RDLENBQUM7SUFFTSxLQUFLLENBQUMsR0FBUTtRQUNqQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUM7WUFBRSxPQUFPLElBQUksQ0FBQztRQUNuQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDNUIsQ0FBQztJQUVNLElBQUk7UUFDUCxJQUFJLElBQUksR0FBMkIsRUFBRSxDQUFDO1FBQ3RDLEtBQUssSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUN6QixJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ2xCO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVNLE1BQU07UUFDVCxJQUFJLElBQUksR0FBYSxFQUFFLENBQUM7UUFDeEIsS0FBSyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ3pCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1NBQy9CO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVNLEtBQUs7UUFDUixLQUFLLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDekIsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQzNCO0lBQ0wsQ0FBQztJQUVNLE9BQU8sQ0FBQyxTQUFxQztRQUNoRCxLQUFLLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDekIsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztTQUMvQztJQUNMLENBQUM7SUFFTSxZQUFZLENBQUMsU0FBeUM7UUFDekQsS0FBSyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ3pCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDNUMsTUFBTTtTQUNiO0lBQ0wsQ0FBQztJQUVELElBQVcsTUFBTTtRQUNiLE9BQU8sZUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDdEMsQ0FBQztDQUNKO0FBN0RELGdDQTZEQzs7Ozs7QUN0RUQsMENBQXFEO0FBRXBEOzs7OztFQUtFO0FBQ0gsTUFBYSxTQUFTO0lBRWxCOzs7O09BSUc7SUFDSSxNQUFNLENBQUMsTUFBTSxDQUFDLEdBQVUsRUFBRSxLQUFVLEVBQUUsS0FBYTtRQUN0RCxJQUFJLEtBQUssR0FBRyxHQUFHLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUN4QixHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ25CO2FBQU07WUFDSCxHQUFHLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDL0I7SUFFTCxDQUFDO0lBRUQsWUFBWTtJQUNMLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBVSxFQUFFLENBQU07UUFDbkMsSUFBSSxDQUFDLEdBQVcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMvQixJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRTtZQUNULEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ3BCO0lBQ0wsQ0FBQztJQUVELGdCQUFnQjtJQUNULE1BQU0sQ0FBQyxTQUFTLENBQUMsR0FBVSxFQUFFLENBQU07UUFDdEMsSUFBSSxDQUFDLEdBQVcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMvQixPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDWCxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNqQixDQUFDLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUN0QjtJQUNMLENBQUM7SUFFRCxTQUFTO0lBQ0YsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFVLEVBQUUsQ0FBTTtRQUNwQyxPQUFPLEdBQUcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDekQsQ0FBQztJQUVELE9BQU87SUFDQSxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQVU7UUFDekIsT0FBTyxHQUFHLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDdkIsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFVLEVBQUUsR0FBVyxFQUFFLFFBQTRCLHlCQUFrQixDQUFDLFVBQVU7UUFDakcsSUFBSSxHQUFHLElBQUksSUFBSTtZQUFFLE9BQU87UUFDeEIsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLEtBQUssRUFBRSxLQUFLO1lBQzNCLFFBQVEsS0FBSyxFQUFFO2dCQUNYLEtBQUsseUJBQWtCLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBQy9CLElBQUksS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEtBQUssQ0FBQyxHQUFHLENBQUM7d0JBQ3ZCLE9BQU8sQ0FBQyxDQUFDLENBQUM7b0JBQ2QsSUFBSSxLQUFLLENBQUMsR0FBRyxDQUFDLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQzt3QkFDdkIsT0FBTyxDQUFDLENBQUM7O3dCQUVULE9BQU8sQ0FBQyxDQUFDO2lCQUNoQjtnQkFDRCxLQUFLLHlCQUFrQixDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUNoQyxJQUFJLEtBQUssQ0FBQyxHQUFHLENBQUMsR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDO3dCQUN2QixPQUFPLENBQUMsQ0FBQyxDQUFDO29CQUNkLElBQUksS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEtBQUssQ0FBQyxHQUFHLENBQUM7d0JBQ3ZCLE9BQU8sQ0FBQyxDQUFDOzt3QkFFVCxPQUFPLENBQUMsQ0FBQztpQkFDaEI7YUFDSjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELFNBQVM7SUFDRixNQUFNLENBQUMsS0FBSyxDQUFDLEdBQVU7UUFDMUIsSUFBSSxDQUFDLEdBQVcsQ0FBQyxDQUFDO1FBQ2xCLElBQUksR0FBRyxHQUFXLEdBQUcsQ0FBQyxNQUFNLENBQUM7UUFDN0IsT0FBTyxDQUFDLEdBQUcsR0FBRyxFQUFFLEVBQUUsQ0FBQyxFQUFFO1lBQ2pCLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUM7U0FDakI7UUFDRCxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ2xCLENBQUM7SUFFRCxXQUFXO0lBQ0osTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFVO1FBQzVCLElBQUksR0FBRyxJQUFJLElBQUksSUFBSSxHQUFHLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtZQUNoQyxPQUFPLElBQUksQ0FBQztTQUNmO1FBQ0QsT0FBTyxLQUFLLENBQUE7SUFDaEIsQ0FBQztDQUNKO0FBMUZELDhCQTBGQzs7Ozs7QUNqR0E7Ozs7OztFQU1FO0FBQ0gsTUFBYSxRQUFRO0lBQ2pCOztPQUVHO0lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFTO1FBQ3hCLElBQUksQ0FBQyxHQUFVLEVBQUUsQ0FBQztRQUNsQixLQUFLLElBQUksR0FBRyxJQUFJLENBQUMsRUFBRTtZQUNmLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDZjtRQUVELE9BQU8sQ0FBQyxDQUFDO0lBQ2IsQ0FBQztJQUVEOztPQUVHO0lBQ0ksTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFTO1FBQzFCLElBQUksQ0FBQyxHQUFVLEVBQUUsQ0FBQztRQUVsQixLQUFLLElBQUksR0FBRyxJQUFJLENBQUMsRUFBRTtZQUNmLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7U0FDbEI7UUFFRCxPQUFPLENBQUMsQ0FBQztJQUNiLENBQUM7SUFFRDs7T0FFRztJQUNJLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBVztRQUMzQixJQUFJLENBQU0sQ0FBQztRQUNYLEtBQUssSUFBSSxHQUFHLElBQUksR0FBRyxFQUFFO1lBQ2pCLENBQUMsR0FBRyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDYixJQUFJLENBQUMsWUFBWSxNQUFNLEVBQUU7Z0JBQ3JCLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDckI7WUFDRCxPQUFPLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUNuQjtJQUNMLENBQUM7SUFFRDs7T0FFRztJQUNJLE1BQU0sQ0FBQyxPQUFPLENBQUMsR0FBVyxFQUFFLFNBQTRDO1FBQzNFLEtBQUssSUFBSSxHQUFHLElBQUksR0FBRyxFQUFFO1lBQ2pCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNwQyxNQUFNO1NBQ2I7SUFDTCxDQUFDO0lBRU0sTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFXO1FBQzdCLElBQUksR0FBRyxJQUFJLElBQUk7WUFBRSxPQUFPLElBQUksQ0FBQztRQUM3QixhQUFhO1FBQ2IsS0FBSyxJQUFJLEdBQUcsSUFBSSxHQUFHLEVBQUU7WUFDakIsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRU0sTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFXO1FBQzFCLElBQUksR0FBRyxJQUFJLElBQUk7WUFBRSxPQUFPLENBQUMsQ0FBQztRQUUxQixJQUFJLEtBQUssR0FBVyxDQUFDLENBQUM7UUFFdEIsS0FBSyxJQUFJLEdBQUcsSUFBSSxHQUFHLEVBQUU7WUFDakIsRUFBRSxLQUFLLENBQUM7U0FDWDtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Q0FDSjtBQXJFRCw0QkFxRUM7Ozs7O0FDNUVELElBQU8sTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7QUFJNUIsTUFBYSxXQUFXO0lBRXBCOzs7T0FHRztJQUNJLE1BQU0sQ0FBQyxjQUFjLENBQUMsU0FBc0I7UUFDL0MsSUFBSSxDQUFDLFNBQVM7WUFBRSxPQUFPO1FBQ3ZCLElBQUksU0FBUyxDQUFDLFdBQVcsSUFBSSxDQUFDO1lBQUUsT0FBTztRQUV2QyxPQUFPLFNBQVMsQ0FBQyxXQUFXLEdBQUcsQ0FBQyxFQUFFO1lBQzlCLFNBQVMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUE7U0FDN0I7SUFDTCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFVO1FBQ2xDLElBQUksSUFBSSxFQUFFO1lBQ04sSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ2xCLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUNmLElBQUksR0FBRyxJQUFJLENBQUM7U0FDZjtJQUNMLENBQUM7SUFFRDs7OztPQUlHO0lBQ0ksTUFBTSxDQUFDLGNBQWMsQ0FBQyxNQUFZLEVBQUUsSUFBWTtRQUNuRCxJQUFJLENBQUMsTUFBTTtZQUFFLE9BQU8sSUFBSSxDQUFDO1FBQ3pCLElBQUksTUFBTSxDQUFDLElBQUksS0FBSyxJQUFJO1lBQUUsT0FBTyxNQUFNLENBQUM7UUFDeEMsSUFBSSxLQUFLLEdBQVMsSUFBSSxDQUFDO1FBQ3ZCLElBQUksR0FBRyxHQUFXLE1BQU0sQ0FBQyxXQUFXLENBQUM7UUFDckMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEdBQUcsRUFBRSxFQUFFLENBQUMsRUFBRTtZQUMxQixLQUFLLEdBQUcsV0FBVyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQy9ELElBQUksS0FBSztnQkFBRSxPQUFPLEtBQUssQ0FBQztTQUMzQjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRCxNQUFNO0lBQ04sWUFBWTtJQUNaLHVCQUF1QjtJQUN2QixNQUFNO0lBQ04sNkZBQTZGO0lBQzdGLHlCQUF5QjtJQUN6QiwyQkFBMkI7SUFDM0IscURBQXFEO0lBQ3JELGtEQUFrRDtJQUNsRCxvREFBb0Q7SUFDcEQsdUJBQXVCO0lBQ3ZCLHVDQUF1QztJQUN2QyxnQ0FBZ0M7SUFDaEMscUJBQXFCO0lBQ3JCLG1DQUFtQztJQUNuQywyQ0FBMkM7SUFDM0MscUJBQXFCO0lBQ3JCLDBDQUEwQztJQUMxQyxxQ0FBcUM7SUFDckMscUJBQXFCO0lBQ3JCLGtDQUFrQztJQUNsQywwQ0FBMEM7SUFDMUMscUJBQXFCO0lBQ3JCLGtDQUFrQztJQUNsQyxxREFBcUQ7SUFDckQscUJBQXFCO0lBQ3JCLHFDQUFxQztJQUNyQywrQ0FBK0M7SUFDL0MscUJBQXFCO0lBQ3JCLHdDQUF3QztJQUN4QyxvQ0FBb0M7SUFDcEMscUJBQXFCO0lBQ3JCLG9DQUFvQztJQUNwQywrQ0FBK0M7SUFDL0MscUJBQXFCO0lBQ3JCLDJDQUEyQztJQUMzQyx5Q0FBeUM7SUFDekMscUJBQXFCO0lBQ3JCLFFBQVE7SUFDUixJQUFJO0lBRUo7O09BRUc7SUFDSSxNQUFNLENBQUMsZUFBZTtRQUN6QixJQUFJLEtBQUssR0FBRyxJQUFJLE1BQU0sRUFBRSxDQUFDO1FBQ3pCLEtBQUssQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1FBRTFCLElBQUksS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO1FBQ2pELElBQUksTUFBTSxHQUFHLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDO1FBQ3BELEtBQUssQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNCLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDcEUsS0FBSyxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsWUFBWSxDQUFDO1FBRXBDLE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Q0FDSjtBQXBHRCxrQ0FvR0M7Ozs7O0FDekdELHFDQUFrQztBQUdqQzs7Ozs7O0VBTUU7QUFDSCxNQUFhLFVBQVU7SUFFbkI7Ozs7O09BS0c7SUFDSSxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksRUFBQyxJQUFJLEVBQUMsRUFBRTtRQUVoQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFDLEdBQUUsRUFBRTtZQUNqRCxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQzlDLElBQUksRUFBRSxFQUFFO2dCQUNKLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDakI7UUFDTCxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ1IsQ0FBQztJQUVEOzs7O09BSUc7SUFDSSxNQUFNLENBQUMsZUFBZSxDQUFJLE9BQU8sRUFBQyxTQUFTO1FBRTlDLElBQUksRUFBRSxHQUFHLE9BQU8sQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFNLENBQUM7UUFDaEQsSUFBSSxFQUFFLEVBQUU7WUFDSixPQUFPLEVBQUUsQ0FBQztTQUNiO1FBQ0QsU0FBRyxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQzVCLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRDs7OztPQUlHO0lBQ0ksTUFBTSxDQUFDLG1CQUFtQixDQUFJLEtBQUssRUFBQyxTQUFTO1FBRWhELElBQUksS0FBSyxHQUFHLEtBQUssQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFNLENBQUM7UUFDakQsSUFBSSxLQUFLLEVBQUU7WUFDUCxPQUFPLEtBQUssQ0FBQztTQUNoQjtRQUNELFNBQUcsQ0FBQyxLQUFLLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUMvQixPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLE1BQU0sQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFDLE1BQU07UUFFdEMsSUFBSSxDQUFDLFFBQVEsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUN0QixTQUFHLENBQUMsS0FBSyxDQUFDLG9CQUFvQixDQUFDLENBQUM7WUFDaEMsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUNELElBQUksUUFBUSxDQUFDLE1BQU0sRUFBRTtZQUNqQixRQUFRLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUNwQztRQUNELE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hJLE1BQU0sQ0FBQyxTQUFTLENBQUMsS0FBSyxHQUFHLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ2hJLENBQUM7SUFFRDs7OztPQUlHO0lBQ0ksTUFBTSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBQyxTQUFTO1FBRWhELElBQUksQ0FBQyxRQUFRLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDekIsU0FBRyxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1lBQzdCLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7UUFDRCxRQUE2QixDQUFDO1FBQzlCLFFBQVEsQ0FBQyxZQUFZLENBQUMsUUFBUSxHQUFHLFNBQVMsQ0FBQztJQUMvQyxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLE1BQU0sQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLEVBQUMsU0FBUztRQUVwRCxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ3pCLFNBQUcsQ0FBQyxLQUFLLENBQUMsaUJBQWlCLENBQUMsQ0FBQztZQUM3QixPQUFPLElBQUksQ0FBQztTQUNmO1FBQ0QsUUFBb0MsQ0FBQztRQUNyQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsUUFBUSxHQUFHLFNBQVMsQ0FBQztJQUN0RCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPO1FBRTlCLHVDQUF1QztRQUN2QyxJQUFJLFNBQVMsR0FBRyxVQUFVLENBQUMsZUFBZSxDQUFvQixPQUFPLEVBQUMsY0FBYyxDQUFDLENBQUM7UUFDdEYsSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNaLFNBQUcsQ0FBQyxLQUFLLENBQUMsa0JBQWtCLENBQUMsQ0FBQztZQUM5QixPQUFPLElBQUksQ0FBQztTQUNmO1FBQ0QsSUFBSSxRQUFRLEdBQThDLEVBQUUsQ0FBQTtRQUM1RCxJQUFJLFNBQVMsR0FBRyxTQUFTLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQztRQUNqRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUFDLEdBQUMsU0FBUyxDQUFDLE1BQU0sRUFBQyxDQUFDLEVBQUUsRUFDckM7WUFDSSxJQUFJLElBQUksR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUMsRUFBRSxDQUFDLENBQUM7WUFDbEUsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQTRCLENBQUM7U0FDNUQ7UUFDRCxPQUFPLFFBQVEsQ0FBQztJQUNwQixDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLEVBQUMsT0FBTztRQUU3QyxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ1osU0FBRyxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1lBQzdCLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7UUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxFQUN2QjtZQUNJLFNBQUcsQ0FBQyxLQUFLLENBQUMsa0JBQWtCLEdBQUMsT0FBTyxHQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzVDLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7UUFDRCxPQUFPLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUM5QixDQUFDO0lBRUQ7Ozs7OztPQU1HO0lBQ0ksTUFBTSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsRUFBQyxPQUFPLEVBQUMsT0FBUTtRQUV0RCxJQUFJLFFBQVEsR0FBaUIsUUFBUSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDbEUsSUFBSSxDQUFDLFFBQVEsRUFDYjtZQUNJLFNBQUcsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDM0IsT0FBTztTQUNWO1FBQ0QsSUFBSSxPQUFPLElBQUksSUFBSSxJQUFJLE9BQU8sSUFBSSxLQUFLLEVBQUU7WUFDckMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN2QixPQUFPO1NBQ1Y7UUFDRCxRQUFRLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBQyxHQUFHLENBQUMsQ0FBQztRQUNoQyxPQUFPLFFBQVEsQ0FBQztJQUNwQixDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLEVBQUMsS0FBSztRQUU3QyxJQUFJLFFBQVEsR0FBaUIsUUFBUSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDbEUsSUFBSSxDQUFDLFFBQVEsRUFDYjtZQUNJLFNBQUcsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDM0IsT0FBTztTQUNWO1FBQ0QsSUFBSSxLQUFLLEVBQUU7WUFDUCxRQUFRLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztTQUMxQjthQUFLO1lBQ0YsUUFBUSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7U0FDdEI7SUFDTCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksTUFBTSxDQUFDLGtCQUFrQixDQUFDLFFBQXNCO1FBRW5ELElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQztRQUNqQixJQUFJLEtBQUssR0FBRyxRQUFRLENBQUMsMkJBQTJCLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDO1FBQ25FLElBQUksS0FBSyxJQUFJLENBQUMsRUFBRTtZQUNaLElBQUksR0FBRyxJQUFJLENBQUM7U0FDZjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Q0FFSjtBQWxNRCxnQ0FrTUM7Ozs7O0FDNU1ELHFDQUFvQztBQUVwQzs7Ozs7O0dBTUc7QUFDSCxNQUFhLFFBQVE7SUFXVixNQUFNLENBQUMsSUFBSSxDQUFDLENBQVM7UUFDeEIsT0FBTyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDOUIsQ0FBQztJQUdEOzs7Ozs7T0FNRztJQUNJLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBVyxFQUFFLEdBQVcsRUFBRSxHQUFXO1FBQ3JELElBQUksR0FBRyxHQUFHLEdBQUcsRUFBRTtZQUNYLEdBQUcsR0FBRyxHQUFHLENBQUM7U0FDYjthQUFNLElBQUksR0FBRyxHQUFHLEdBQUcsRUFBRTtZQUNsQixHQUFHLEdBQUcsR0FBRyxDQUFDO1NBQ2I7UUFDRCxPQUFPLEdBQUcsQ0FBQztJQUNmLENBQUM7SUFFTSxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQWE7UUFDL0IsSUFBSSxLQUFLLEdBQUcsQ0FBQztZQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ3hCLElBQUksS0FBSyxHQUFHLENBQUM7WUFBRSxPQUFPLENBQUMsQ0FBQztRQUN4QixPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRU0sTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFZLEVBQUUsRUFBVSxFQUFFLENBQVM7UUFDbEQsT0FBTyxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFFTSxNQUFNLENBQUMsU0FBUyxDQUFDLENBQVMsRUFBRSxDQUFTLEVBQUUsQ0FBUztRQUNuRCxJQUFJLEdBQUcsR0FBVyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDOUMsSUFBSSxHQUFHLEdBQUcsR0FBRztZQUFFLEdBQUcsSUFBSSxHQUFHLENBQUM7UUFDMUIsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBRU0sTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFTLEVBQUUsTUFBYztRQUMxQyxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksTUFBTSxDQUFDLFNBQVMsQ0FBQyxNQUFjLEVBQUUsTUFBYztRQUNsRCxJQUFJLEdBQUcsR0FBVyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLEdBQUcsTUFBTSxDQUFDO1FBQzdELE9BQU8sR0FBRyxDQUFDO0lBQ2YsQ0FBQztJQUVEOzs7T0FHRztJQUNJLE1BQU0sQ0FBQyxZQUFZLENBQUMsTUFBYyxFQUFFLE1BQWM7UUFDckQsSUFBSSxHQUFHLEdBQVcsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsTUFBTSxHQUFHLE1BQU0sR0FBRyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUM7UUFDakUsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFFRDs7O09BR0c7SUFDSSxNQUFNLENBQUMsY0FBYyxDQUFJLEdBQWE7UUFDekMsSUFBSSxHQUFHLENBQUMsTUFBTSxJQUFJLENBQUM7WUFDZixPQUFPLElBQUksQ0FBQztRQUNoQixJQUFJLEdBQUcsR0FBTSxHQUFHLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzNELE9BQU8sR0FBRyxDQUFDO0lBQ2YsQ0FBQztJQUVEOztPQUVHO0lBQ0ksTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFlO1FBQ3RDLE9BQU8sT0FBTyxHQUFHLENBQUM7WUFBRSxPQUFPLEdBQUcsT0FBTyxHQUFHLEdBQUcsQ0FBQztRQUM1QyxPQUFPLE9BQU8sSUFBSSxHQUFHO1lBQUUsT0FBTyxHQUFHLE9BQU8sR0FBRyxHQUFHLENBQUM7UUFDL0MsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQUVEOztPQUVHO0lBQ0ksTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFlO1FBQ3RDLE9BQU8sT0FBTyxHQUFHLENBQUM7WUFBRSxPQUFPLEdBQUcsT0FBTyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDO1FBQ3BELE9BQU8sT0FBTyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRTtZQUFFLE9BQU8sR0FBRyxPQUFPLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUM7UUFDL0QsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQUVEOztPQUVHO0lBQ0ksTUFBTSxDQUFDLFdBQVcsQ0FBQyxFQUFVLEVBQUUsRUFBVSxFQUFFLEVBQVUsRUFBRSxFQUFVO1FBQ3BFLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRyxFQUFFLEVBQUUsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDbEUsQ0FBQztJQUVNLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxFQUFVLEVBQUUsRUFBVSxFQUFFLEVBQVUsRUFBRSxFQUFVO1FBQzFFLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsRUFBRSxFQUFFLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUN2RCxDQUFDO0lBRUQ7O09BRUc7SUFDSSxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQVUsRUFBRSxFQUFVLEVBQUUsRUFBVSxFQUFFLEVBQVU7UUFDdkUsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsR0FBRyxFQUFFLEVBQUUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO0lBQ3hDLENBQUM7SUFFTSxNQUFNLENBQUMsYUFBYSxDQUFDLEVBQVUsRUFBRSxFQUFVLEVBQUUsRUFBVSxFQUFFLEVBQVU7UUFDdEUsSUFBSSxNQUFNLEdBQVcsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDaEYsT0FBTyxRQUFRLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFFTSxNQUFNLENBQUMsZUFBZSxDQUFDLENBQVMsRUFBRSxDQUFTO1FBQzlDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDNUIsQ0FBQztJQUVNLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBUyxFQUFFLENBQVM7UUFDN0MsSUFBSSxNQUFNLEdBQVcsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3ZFLE9BQU8sUUFBUSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRUQ7O09BRUc7SUFDSSxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQWM7UUFDakMsT0FBTyxNQUFNLEdBQUcsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3RDLENBQUM7SUFFRDs7T0FFRztJQUNJLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBYztRQUNqQyxPQUFPLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsS0FBSyxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUVNLE1BQU0sQ0FBQyxXQUFXLENBQUMsT0FBZSxFQUFFLE1BQWMsRUFBRSxRQUFnQjtRQUN2RSxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxJQUFJLFFBQVEsRUFBRTtZQUN4QyxPQUFPLE1BQU0sQ0FBQztTQUNqQjtRQUNELE9BQU8sQ0FBQyxPQUFPLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBQ3BFLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNJLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBVyxFQUFFLEdBQVc7UUFDekMsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQztJQUN6RCxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBUSxFQUFDLENBQVE7UUFDckMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBQyxDQUFDLEdBQUMsQ0FBQyxHQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzlCLENBQUM7SUFFRDs7Ozs7O09BTUc7SUFDSSxNQUFNLENBQUMsV0FBVyxDQUFDLEVBQVMsRUFBQyxFQUFTLEVBQUMsRUFBUyxFQUFDLEVBQVM7UUFFN0QsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLEVBQUU7WUFDdEIsT0FBTztTQUNWO1FBQ0QsSUFBSSxRQUFRLEdBQUcsQ0FBQyxFQUFFLEdBQUMsRUFBRSxHQUFDLEVBQUUsR0FBQyxFQUFFLENBQUMsR0FBQyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsRUFBRSxFQUFDLEVBQUUsQ0FBQyxHQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsRUFBRSxFQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDbkYsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNwQyxJQUFJLEtBQUssR0FBRyxtQkFBVSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMxQyxJQUFJLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUU7WUFBRSxLQUFLLEdBQUcsQ0FBRSxLQUFLLENBQUM7UUFDdkMsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7QUE1TEwsNEJBOExDO0FBNUxHLFVBQVU7QUFDSSxrQkFBUyxHQUFXLENBQUMsR0FBRyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsQ0FBQztBQUNwRCxVQUFVO0FBQ0ksa0JBQVMsR0FBVyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUUvQixnQkFBTyxHQUFXLFVBQVUsQ0FBQztBQUU3QixnQkFBTyxHQUFXLFFBQVEsQ0FBQzs7Ozs7QUNqQjdDLElBQU8sT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7QUFDOUIsSUFBTyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztBQUM5QixJQUFPLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO0FBQzlCLHFDQUFvQztBQUVwQzs7Ozs7O0dBTUc7QUFDSCxNQUFhLFVBQVU7SUFNWixNQUFNLEtBQUssUUFBUTtRQUN0QixJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVM7WUFBRSxVQUFVLENBQUMsU0FBUyxHQUFHLElBQUksT0FBTyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNwRSxPQUFPLFVBQVUsQ0FBQyxTQUFTLENBQUM7SUFDaEMsQ0FBQztJQUVNLE1BQU0sS0FBSyxRQUFRO1FBQ3RCLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUztZQUFFLFVBQVUsQ0FBQyxTQUFTLEdBQUcsSUFBSSxPQUFPLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUN2RSxPQUFPLFVBQVUsQ0FBQyxTQUFTLENBQUM7SUFDaEMsQ0FBQztJQUVNLE1BQU0sS0FBSyxRQUFRO1FBQ3RCLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUztZQUFFLFVBQVUsQ0FBQyxTQUFTLEdBQUcsSUFBSSxPQUFPLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDMUUsT0FBTyxVQUFVLENBQUMsU0FBUyxDQUFDO0lBQ2hDLENBQUM7SUFFRCxZQUFZO0lBQ0wsTUFBTSxDQUFDLFlBQVksQ0FBQyxHQUFZO1FBQ25DLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ1YsT0FBTyxHQUFHLENBQUM7SUFDZixDQUFDO0lBRUQsU0FBUztJQUNGLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxJQUFhLEVBQUUsSUFBYTtRQUN6RCxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNYLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ1gsT0FBTyxPQUFPLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRUQsV0FBVztJQUNKLE1BQU0sQ0FBQyxXQUFXLENBQUMsR0FBUSxFQUFFLFFBQWdCO1FBQ2hELE9BQU8sT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQztJQUNqRSxDQUFDO0lBRUQsa0JBQWtCO0lBQ1gsTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFhLEVBQUMsSUFBWTtRQUNsRCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3BJLENBQUM7SUFFRCxrQkFBa0I7SUFDWCxNQUFNLENBQUMsYUFBYSxDQUFDLElBQWEsRUFBQyxJQUFZO1FBQ2xELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzlGLENBQUM7SUFFRCxnQkFBZ0I7SUFDVCxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQVk7UUFDOUIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBRUQsZ0JBQWdCO0lBQ1QsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFhO1FBQy9CLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUVELGFBQWE7SUFDTixNQUFNLENBQUMsR0FBRyxDQUFDLEtBQVk7UUFDMUIsSUFBSSxNQUFNLEdBQUcsVUFBVSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN2QyxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDNUIsQ0FBQztJQUNELGFBQWE7SUFDTixNQUFNLENBQUMsR0FBRyxDQUFDLEtBQVk7UUFDMUIsSUFBSSxNQUFNLEdBQUcsVUFBVSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN2QyxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDNUIsQ0FBQztJQUNELGFBQWE7SUFDTixNQUFNLENBQUMsR0FBRyxDQUFDLEtBQVk7UUFDMUIsSUFBSSxNQUFNLEdBQUcsVUFBVSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN2QyxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDNUIsQ0FBQztJQUNELGNBQWM7SUFDUCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQVk7UUFDM0IsSUFBSSxNQUFNLEdBQUcsVUFBVSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN2QyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDN0IsQ0FBQztJQUNELGNBQWM7SUFDUCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQVk7UUFDM0IsSUFBSSxNQUFNLEdBQUcsVUFBVSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN2QyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDN0IsQ0FBQztJQUNELGNBQWM7SUFDUCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQVk7UUFDM0IsSUFBSSxNQUFNLEdBQUcsVUFBVSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN2QyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDN0IsQ0FBQzs7QUF4RkwsZ0NBNkZDO0FBM0ZrQixvQkFBUyxHQUFZLElBQUksQ0FBQztBQUMxQixvQkFBUyxHQUFZLElBQUksQ0FBQztBQUMxQixvQkFBUyxHQUFZLElBQUksQ0FBQztBQTJGN0Msc0RBQXNEO0FBQ3RELFNBQWdCLE9BQU8sQ0FBQyxDQUFVLEVBQUUsQ0FBVTtJQUMxQyxPQUFPLElBQUksT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztBQUM3QyxDQUFDO0FBRkQsMEJBRUM7QUFFRCxTQUFnQixPQUFPLENBQUMsQ0FBVSxFQUFFLENBQVU7SUFDMUMsT0FBTyxJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDN0MsQ0FBQztBQUZELDBCQUVDO0FBRUQsU0FBZ0IsWUFBWSxDQUFDLENBQVUsRUFBRSxDQUFVO0lBQy9DLE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQzdDLENBQUM7QUFGRCxvQ0FFQztBQUVELFNBQWdCLE9BQU8sQ0FBQyxDQUFVLEVBQUUsQ0FBUztJQUN6QyxPQUFPLElBQUksT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7QUFDekMsQ0FBQztBQUZELDBCQUVDO0FBRUQsU0FBZ0IsT0FBTyxDQUFDLENBQVUsRUFBRSxDQUFTO0lBQ3pDLE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztBQUN6QyxDQUFDO0FBRkQsMEJBRUM7QUFFRCxTQUFnQixPQUFPLENBQUMsR0FBWSxFQUFFLEdBQVk7SUFDOUMsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQy9DLENBQUM7QUFGRCwwQkFFQztBQUVELFNBQWdCLFdBQVcsQ0FBQyxNQUFlLEVBQUUsUUFBaUI7SUFDMUQsSUFBSSxHQUFHLEdBQVcsT0FBTyxDQUFDLFFBQVEsRUFBRSxRQUFRLENBQUMsQ0FBQztJQUM5QyxJQUFJLEdBQUcsR0FBRyxLQUFLLEVBQUU7UUFDYixPQUFPLE9BQU8sQ0FBQyxJQUFJLENBQUM7S0FDdkI7SUFDRCxPQUFPLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsT0FBTyxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUM7QUFDeEUsQ0FBQztBQU5ELGtDQU1DO0FBRUQsU0FBZ0IsT0FBTyxDQUFDLEdBQVksRUFBRSxHQUFZO0lBQzlDLE9BQU8sSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDdkUsQ0FBQztBQUZELDBCQUVDO0FBRUQsU0FBZ0IsT0FBTyxDQUFDLEdBQVksRUFBRSxHQUFZO0lBQzlDLE9BQU8sSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDdkUsQ0FBQztBQUZELDBCQUVDO0FBRUQsU0FBZ0IsYUFBYSxDQUFDLEdBQVk7SUFDdEMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQ3hELENBQUM7QUFGRCxzQ0FFQztBQUVELFNBQWdCLGdCQUFnQixDQUFDLEdBQVk7SUFDekMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDN0MsQ0FBQztBQUZELDRDQUVDO0FBRUQsU0FBZ0IsY0FBYyxDQUFDLEdBQVk7SUFDdkMsSUFBSSxTQUFTLEdBQVcsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzNDLElBQUksQ0FBVSxDQUFDO0lBQ2YsSUFBSSxTQUFTLEdBQUcsS0FBSztRQUNqQixDQUFDLEdBQUcsT0FBTyxDQUFDLEdBQUcsRUFBRSxTQUFTLENBQUMsQ0FBQzs7UUFFNUIsQ0FBQyxHQUFHLElBQUksT0FBTyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUMxQixPQUFPLENBQUMsQ0FBQztBQUNiLENBQUM7QUFSRCx3Q0FRQztBQUVELFNBQWdCLFVBQVUsQ0FBQyxHQUFZO0lBQ25DLElBQUksU0FBUyxHQUFXLGFBQWEsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUMzQyxJQUFJLFNBQVMsR0FBRyxLQUFLLEVBQUU7UUFDbkIsSUFBSSxDQUFDLEdBQVksT0FBTyxDQUFDLEdBQUcsRUFBRSxTQUFTLENBQUMsQ0FBQztRQUN6QyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0tBQzFCOztRQUNHLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0FBQzNCLENBQUM7QUFQRCxnQ0FPQztBQUVELFNBQWdCLE9BQU8sQ0FBQyxDQUFVLEVBQUUsQ0FBUyxFQUFFLENBQVM7SUFDcEQsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDUixDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNSLENBQUM7SUFDRCxPQUFPLENBQUMsQ0FBQztBQUNiLENBQUM7QUFMRCwwQkFLQztBQUVELGtFQUFrRTtBQUNsRSwrR0FBK0c7QUFDL0csSUFBSTtBQUVKLFNBQWdCLGtCQUFrQixDQUFDLE1BQWUsRUFBRSxTQUFTO0lBQ3pELElBQUksZ0JBQWdCLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDLEVBQUU7UUFDcEQsT0FBTyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLEVBQUUsU0FBUyxDQUFDLENBQUMsQ0FBQztLQUN2RDtJQUNELE9BQU8sTUFBTSxDQUFDO0FBQ2xCLENBQUM7QUFMRCxnREFLQztBQUVELDZFQUE2RTtBQUM3RSxtQ0FBbUM7QUFDbkMsMEZBQTBGO0FBQzFGLElBQUk7QUFFSixTQUFnQixlQUFlLENBQUMsT0FBZ0IsRUFBRSxNQUFlLEVBQUUsZ0JBQXdCO0lBQ3ZGLElBQUksTUFBTSxHQUFZLE9BQU8sQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDL0MsSUFBSSxTQUFTLEdBQVcsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzlDLElBQUksQ0FBQyxTQUFTLEdBQUcsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLENBQUMsRUFBRTtRQUNwRCxPQUFPLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxTQUFTLENBQUMsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQztLQUNwRjtJQUNELE9BQU8sTUFBTSxDQUFDO0FBQ2xCLENBQUM7QUFQRCwwQ0FPQztBQUVELFNBQWdCLFlBQVksQ0FBQyxHQUFZO0lBQ3JDLE9BQU8sbUJBQVUsQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFLEdBQUcsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQ3pELENBQUM7QUFGRCxvQ0FFQztBQUVELHNEQUFzRDtBQUN0RCxTQUFnQixPQUFPLENBQUMsQ0FBVSxFQUFFLENBQVU7SUFDMUMsT0FBTyxJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQ3hELENBQUM7QUFGRCwwQkFFQztBQUVELFNBQWdCLE9BQU8sQ0FBQyxDQUFVLEVBQUUsQ0FBVTtJQUMxQyxPQUFPLElBQUksT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDeEQsQ0FBQztBQUZELDBCQUVDO0FBRUQsU0FBZ0IsWUFBWSxDQUFDLENBQVUsRUFBRSxDQUFVO0lBQy9DLE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztBQUN4RCxDQUFDO0FBRkQsb0NBRUM7QUFFRCxTQUFnQixPQUFPLENBQUMsQ0FBVSxFQUFFLENBQVM7SUFDekMsT0FBTyxJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0FBQ2xELENBQUM7QUFGRCwwQkFFQztBQUVELFNBQWdCLE9BQU8sQ0FBQyxDQUFVLEVBQUUsQ0FBUztJQUN6QyxPQUFPLElBQUksT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7QUFDbEQsQ0FBQztBQUZELDBCQUVDO0FBRUQsU0FBZ0IsU0FBUyxDQUFDLEdBQVksRUFBRSxHQUFZO0lBQ2hELE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztBQUNoSSxDQUFDO0FBRkQsOEJBRUM7QUFFRCxTQUFnQixXQUFXLENBQUMsTUFBZSxFQUFFLFFBQWlCO0lBQzFELElBQUksR0FBRyxHQUFXLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQ2xELElBQUksR0FBRyxHQUFHLEtBQUssRUFBRTtRQUNiLE9BQU8sSUFBSSxPQUFPLEVBQUUsQ0FBQztLQUN4QjtJQUNELE9BQU8sQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUM7QUFDNUUsQ0FBQztBQU5ELGtDQU1DO0FBRUQsU0FBZ0IsT0FBTyxDQUFDLEdBQVksRUFBRSxHQUFZO0lBQzlDLE9BQU8sSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztBQUMvRixDQUFDO0FBRkQsMEJBRUM7QUFFRCxTQUFnQixPQUFPLENBQUMsR0FBWSxFQUFFLEdBQVk7SUFDOUMsT0FBTyxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQy9GLENBQUM7QUFGRCwwQkFFQztBQUVELFNBQWdCLGFBQWEsQ0FBQyxHQUFZO0lBQ3RDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQzFFLENBQUM7QUFGRCxzQ0FFQztBQUVELFNBQWdCLGdCQUFnQixDQUFDLEdBQVk7SUFDekMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztBQUMvRCxDQUFDO0FBRkQsNENBRUM7QUFFRCxTQUFnQixjQUFjLENBQUMsR0FBWTtJQUN2QyxJQUFJLFNBQVMsR0FBVyxPQUFPLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ2xELElBQUksQ0FBVSxDQUFDO0lBQ2YsSUFBSSxTQUFTLEdBQUcsS0FBSztRQUNqQixDQUFDLEdBQUcsT0FBTyxDQUFDLEdBQUcsRUFBRSxTQUFTLENBQUMsQ0FBQzs7UUFFNUIsQ0FBQyxHQUFHLElBQUksT0FBTyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDN0IsT0FBTyxDQUFDLENBQUM7QUFDYixDQUFDO0FBUkQsd0NBUUM7QUFFRCxTQUFnQixVQUFVLENBQUMsR0FBWTtJQUNuQyxJQUFJLFNBQVMsR0FBVyxPQUFPLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ2xELElBQUksU0FBUyxHQUFHLEtBQUssRUFBRTtRQUNuQixJQUFJLENBQUMsR0FBWSxPQUFPLENBQUMsR0FBRyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQ3pDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztLQUMvQjs7UUFDRyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7QUFDOUIsQ0FBQztBQVBELGdDQU9DO0FBRUQsU0FBZ0IsT0FBTyxDQUFDLENBQVUsRUFBRSxDQUFTLEVBQUUsQ0FBUyxFQUFFLENBQVM7SUFDL0QsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDUixDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNSLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ1IsT0FBTyxDQUFDLENBQUM7QUFDYixDQUFDO0FBTEQsMEJBS0M7QUFFRCxrRUFBa0U7QUFDbEUsbUhBQW1IO0FBQ25ILElBQUk7QUFFSixTQUFnQixrQkFBa0IsQ0FBQyxNQUFlLEVBQUUsU0FBUztJQUN6RCxJQUFJLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUMsRUFBRTtRQUMvRCxPQUFPLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsRUFBRSxTQUFTLENBQUMsQ0FBQyxDQUFDO0tBQ3ZEO0lBQ0QsT0FBTyxNQUFNLENBQUM7QUFDbEIsQ0FBQztBQUxELGdEQUtDO0FBRUQsNkVBQTZFO0FBQzdFLG1DQUFtQztBQUNuQywwSEFBMEg7QUFDMUgsSUFBSTtBQUVKLFNBQWdCLGVBQWUsQ0FBQyxPQUFnQixFQUFFLE1BQWUsRUFBRSxnQkFBd0I7SUFDdkYsSUFBSSxNQUFNLEdBQVksT0FBTyxDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQztJQUMvQyxJQUFJLFNBQVMsR0FBVyxPQUFPLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3JELElBQUksQ0FBQyxTQUFTLEdBQUcsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLENBQUMsRUFBRTtRQUNwRCxPQUFPLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxTQUFTLENBQUMsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQztLQUNwRjtJQUNELE9BQU8sTUFBTSxDQUFDO0FBQ2xCLENBQUM7QUFQRCwwQ0FPQztBQUVELFNBQWdCLFlBQVksQ0FBQyxHQUFZO0lBQ3JDLE9BQU8sbUJBQVUsQ0FBQyxNQUFNLENBQUMsaUJBQWlCLEVBQUUsR0FBRyxDQUFDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztBQUNyRSxDQUFDO0FBRkQsb0NBRUM7QUFFRDs7O0dBR0c7QUFDSCxTQUFnQixrQkFBa0IsQ0FBQyxPQUFlO0lBQzlDLElBQUksQ0FBQyxHQUFXLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDbEMsSUFBSSxDQUFDLEdBQVcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNsQyxJQUFJLEdBQUcsR0FBWSxJQUFJLE9BQU8sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDckMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ2hCLE9BQU8sR0FBRyxDQUFDO0FBQ2YsQ0FBQztBQU5ELGdEQU1DOzs7OztBQ3RVRCxxQ0FBc0M7QUFFdEM7Ozs7OztHQU1HO0FBQ0gsTUFBYSxVQUFVO0lBQ25COztPQUVHO0lBQ0ksTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFhLEVBQUUsQ0FBUztRQUMxQyxPQUFPLG1CQUFVLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBRU0sTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFhO1FBQzdCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM3QixDQUFDO0lBRU0sTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFhO1FBQzdCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUM7SUFDckMsQ0FBQztJQUVEOztPQUVHO0lBQ0ksTUFBTSxDQUFDLGFBQWEsQ0FBQyxHQUFXLEVBQUUsSUFBWTtRQUNqRCxJQUFJLEdBQUcsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdEIsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQztRQUNuQixJQUFJLE9BQU8sR0FBVyxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZDLElBQUksT0FBTyxHQUFHLENBQUMsRUFBRTtZQUNiLE9BQU8sR0FBRyxDQUFDO1NBQ2Q7UUFDRCxJQUFJLEdBQUcsR0FBVyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxPQUFPLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFFNUMsSUFBSSxPQUFPLEdBQUcsQ0FBQyxHQUFHLE9BQU8sQ0FBQztRQUMxQixJQUFJLE9BQU8sR0FBRyxJQUFJLEVBQUU7WUFDaEIsT0FBTyxHQUFHLElBQUksQ0FBQztTQUNsQjtRQUNELElBQUksT0FBTyxHQUFXLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLENBQUMsRUFBRSxPQUFPLEdBQUcsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxDQUFDO1FBQ3BFLE9BQU8sbUJBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxHQUFHLE9BQU8sQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFFRDs7T0FFRztJQUNJLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxHQUFXLEVBQUUsSUFBWTtRQUN6RCxJQUFJLEdBQUcsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdEIsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQztRQUNuQixJQUFJLE9BQU8sR0FBVyxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZDLElBQUksT0FBTyxHQUFHLENBQUMsRUFBRSxFQUFDLEtBQUs7WUFDbkIsR0FBRyxJQUFJLEdBQUcsQ0FBQztZQUNYLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLEVBQUUsRUFBRSxDQUFDO2dCQUFFLEdBQUcsSUFBSSxHQUFHLENBQUM7WUFDMUMsT0FBTyxHQUFHLENBQUM7U0FDZDtRQUNELElBQUksR0FBRyxHQUFXLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLE9BQU8sR0FBRyxDQUFDLENBQUMsQ0FBQztRQUU1QyxJQUFJLE9BQU8sR0FBRyxDQUFDLEdBQUcsT0FBTyxHQUFHLENBQUMsQ0FBQztRQUM5QixJQUFJLE9BQU8sR0FBRyxJQUFJLEVBQUUsRUFBQyxJQUFJO1lBQ3JCLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDZixJQUFJLE9BQU8sR0FBVyxHQUFHLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxDQUFDLEVBQUUsT0FBTyxHQUFHLENBQUMsR0FBRyxPQUFPLENBQUMsQ0FBQztZQUNwRSxPQUFPLEdBQUcsR0FBRyxPQUFPLENBQUM7U0FDeEI7YUFBTSxJQUFJLE9BQU8sR0FBRyxJQUFJLEVBQUUsRUFBQyxNQUFNO1lBQzlCLElBQUksSUFBSSxHQUFXLElBQUksR0FBRyxPQUFPLENBQUM7WUFDbEMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksRUFBRSxFQUFFLENBQUM7Z0JBQUUsR0FBRyxJQUFJLEdBQUcsQ0FBQztZQUMxQyxPQUFPLEdBQUcsQ0FBQztTQUNkOztZQUNHLE9BQU8sR0FBRyxDQUFDO0lBQ25CLENBQUM7SUFHRDs7T0FFRztJQUNJLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxHQUFXO1FBQzNDLElBQUksR0FBRyxHQUFHLE9BQU8sRUFBRTtZQUNmLE9BQU8sR0FBRyxDQUFDLGNBQWMsRUFBRSxDQUFDO1NBQy9CO2FBQU0sSUFBSSxHQUFHLEdBQUcsVUFBVSxFQUFFO1lBQ3pCLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxDQUFBO1lBQzlCLE9BQU8sQ0FBQyxDQUFDLGNBQWMsRUFBRSxHQUFHLEdBQUcsQ0FBQztTQUNuQzthQUFNO1lBQ0gsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsT0FBTyxDQUFDLENBQUE7WUFDakMsT0FBTyxDQUFDLENBQUMsY0FBYyxFQUFFLEdBQUcsR0FBRyxDQUFDO1NBQ25DO0lBQ0wsQ0FBQztJQUVEOztPQUVHO0lBQ0ksTUFBTSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsRUFBRSxRQUFnQixDQUFDO1FBRWxELElBQUksR0FBRyxHQUFHLElBQUksRUFBRTtZQUNaLE9BQU8sR0FBRyxDQUFDO1NBQ2Q7YUFBTSxJQUFJLEdBQUcsR0FBRyxPQUFPLEVBQUU7WUFDdEIsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzlDLE9BQU8sQ0FBQyxHQUFHLEdBQUcsQ0FBQztTQUNsQjthQUFNLElBQUksR0FBRyxHQUFHLFVBQVUsRUFBRTtZQUN6QixJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxPQUFPLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDakQsT0FBTyxDQUFDLEdBQUcsR0FBRyxDQUFDO1NBQ2xCO2FBQU07WUFDSCxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxVQUFVLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDcEQsT0FBTyxDQUFDLENBQUMsY0FBYyxFQUFFLEdBQUcsR0FBRyxDQUFDO1NBQ25DO0lBRUwsQ0FBQztJQUdEOzs7T0FHRztJQUNJLE1BQU0sQ0FBQyxlQUFlLENBQUMsR0FBVyxFQUFDLFFBQWUsQ0FBQztRQUN0RCxJQUFJLElBQUksR0FBRztZQUNQLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLElBQUk7WUFDaEQsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUk7WUFDOUMsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUk7WUFDOUMsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUk7WUFDOUMsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUk7WUFDOUMsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUk7WUFDOUMsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUk7U0FDakQsQ0FBQztRQUVGLElBQUksRUFBRSxFQUFFLEVBQUUsQ0FBQztRQUNYLElBQUksRUFBRSxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQUUsRUFBRSxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDcEMsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxFQUFFO1lBQ2hCLEVBQUUsR0FBRyxVQUFVLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNuQyxFQUFFLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFbEMsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDN0IsSUFBSSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUVmLE9BQU8sQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEVBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQzNEO1FBR0QsT0FBTyxHQUFHLENBQUM7SUFDZixDQUFDO0lBRUQ7O09BRUc7SUFDSSxNQUFNLENBQUMsWUFBWSxDQUFDLElBQVksRUFBRSxJQUFZO1FBQ2pELElBQUksQ0FBQyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEMsT0FBTyxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzlCLENBQUM7SUFFRDs7T0FFRztJQUNJLE1BQU0sQ0FBQyxZQUFZLENBQUMsSUFBWSxFQUFFLElBQVk7UUFDakQsSUFBSSxFQUFFLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RCLElBQUksRUFBRSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QixJQUFJLEVBQUUsR0FBRyxFQUFFLEVBQUU7WUFDVCxPQUFPLElBQUksQ0FBQztTQUNmO1FBRUQsT0FBTyxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUVEOztPQUVHO0lBQ0ksTUFBTSxDQUFDLFlBQVksQ0FBQyxJQUFZLEVBQUUsSUFBWTtRQUNqRCxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNsRCxDQUFDO0lBRUQ7O09BRUc7SUFDSSxNQUFNLENBQUMsWUFBWSxDQUFDLElBQVksRUFBRSxJQUFZO1FBQ2pELE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFRDs7T0FFRztJQUNJLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxJQUFZLEVBQUUsSUFBWTtRQUN4RCxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQ3pDLENBQUM7Q0FFSjtBQTlLRCxnQ0E4S0M7Ozs7O0FDdkxEOzs7Ozs7R0FNRztBQUNILE1BQWEsVUFBVTtJQUVaLE1BQU0sS0FBSyxLQUFLO1FBQ25CLE9BQU8sRUFBRSxDQUFDO0lBQ2QsQ0FBQztJQUVEOztPQUVHO0lBQ0ksTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFTO1FBQzNCLE9BQU8sQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO0lBQ3RELENBQUM7SUFFTSxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQVc7UUFDM0IsSUFBSSxDQUFDLEdBQUcsSUFBSSxHQUFHLENBQUMsTUFBTSxJQUFJLENBQUM7WUFBRSxPQUFPLENBQUMsQ0FBQztRQUN0QyxPQUFPLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN6QixDQUFDO0lBRU0sTUFBTSxDQUFDLFFBQVEsQ0FBQyxHQUFXO1FBQzlCLElBQUksQ0FBQyxHQUFHLElBQUksR0FBRyxDQUFDLE1BQU0sSUFBSSxDQUFDO1lBQUUsT0FBTyxDQUFDLENBQUM7UUFDdEMsT0FBTyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDM0IsQ0FBQztJQUVEOzs7O09BSUc7SUFDSSxNQUFNLENBQUMsV0FBVyxDQUFDLEdBQVc7UUFDakMsSUFBSSxVQUFVLEdBQUcsQ0FBQyxFQUFFLEdBQUcsR0FBRyxHQUFHLENBQUMsTUFBTSxFQUFFLFFBQVEsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUNwRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzFCLFFBQVEsR0FBRyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzdCLElBQUksUUFBUSxJQUFJLENBQUMsSUFBSSxRQUFRLElBQUksR0FBRztnQkFBRSxVQUFVLElBQUksQ0FBQyxDQUFDOztnQkFDakQsVUFBVSxJQUFJLENBQUMsQ0FBQztTQUN4QjtRQUNELE9BQU8sVUFBVSxDQUFDO0lBQ3RCLENBQUM7SUFFRDs7Ozs7O09BTUc7SUFDSSxNQUFNLENBQUMsT0FBTyxDQUFDLEdBQVcsRUFBRSxHQUFXLEVBQUUsTUFBYyxDQUFDO1FBQzNELElBQUksSUFBSSxHQUFXLEVBQUUsQ0FBQztRQUN0QixJQUFJLElBQUksR0FBVyxHQUFHLENBQUMsTUFBTSxDQUFDO1FBQzlCLElBQUksWUFBWSxHQUFXLEVBQUUsQ0FBQztRQUM5QixJQUFJLFlBQVksR0FBVyxFQUFFLENBQUM7UUFDOUIsSUFBSSxHQUFHLElBQUksQ0FBQztZQUNSLFlBQVksR0FBRyxHQUFHLENBQUM7O1lBRW5CLFlBQVksR0FBRyxHQUFHLENBQUM7UUFFdkIsSUFBSSxJQUFJLEdBQUcsR0FBRyxFQUFFO1lBQ1osSUFBSSxDQUFDLEdBQVcsQ0FBQyxDQUFDO1lBQ2xCLE9BQU8sQ0FBQyxHQUFHLEdBQUcsR0FBRyxJQUFJLEVBQUU7Z0JBQ25CLElBQUksR0FBRyxZQUFZLEdBQUcsSUFBSSxHQUFHLFlBQVksQ0FBQztnQkFDMUMsRUFBRSxDQUFDLENBQUM7YUFDUDtZQUVELE9BQU8sSUFBSSxHQUFHLEdBQUcsQ0FBQztTQUNyQjtRQUVELE9BQU8sR0FBRyxDQUFDO0lBQ2YsQ0FBQztJQUVEOzs7O09BSUc7SUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQWE7UUFDNUIsSUFBSSxLQUFLLElBQUksSUFBSSxFQUFFO1lBQ2YsT0FBTyxFQUFFLENBQUM7U0FDYjtRQUNELE9BQU8sS0FBSyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUN0RCxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLE1BQU0sQ0FBQyxRQUFRLENBQUMsS0FBYTtRQUNoQyxJQUFJLEtBQUssSUFBSSxJQUFJLEVBQUU7WUFDZixPQUFPLEVBQUUsQ0FBQztTQUNiO1FBQ0QsT0FBTyxLQUFLLENBQUMsT0FBTyxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLE1BQU0sQ0FBQyxTQUFTLENBQUMsS0FBYTtRQUNqQyxJQUFJLEtBQUssSUFBSSxJQUFJLEVBQUU7WUFDZixPQUFPLEVBQUUsQ0FBQztTQUNiO1FBQ0QsT0FBTyxLQUFLLENBQUMsT0FBTyxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBZTtRQUN0QyxJQUFJLEdBQUcsR0FBVyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUMsQ0FBQztRQUMzQyxJQUFJLEdBQUcsR0FBVyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUMsQ0FBQztRQUUzQyxJQUFJLE9BQU8sR0FBVyxHQUFHLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUMzRSxJQUFJLE9BQU8sR0FBVyxHQUFHLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUUzRSxPQUFPLE9BQU8sR0FBRyxHQUFHLEdBQUcsT0FBTyxDQUFDO0lBQ25DLENBQUM7SUFFRDs7OztPQUlHO0lBQ0ksTUFBTSxDQUFDLFVBQVUsQ0FBQyxPQUFlO1FBQ3BDLElBQUksSUFBSSxHQUFXLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxDQUFDO1FBQzlDLElBQUksUUFBUSxHQUFXLElBQUksR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1FBQy9FLE9BQU8sUUFBUSxHQUFHLEdBQUcsR0FBRyxVQUFVLENBQUMsWUFBWSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsQ0FBQztJQUNwRSxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNJLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBVyxFQUFFLEdBQUcsSUFBSTtRQUNyQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNsQyxHQUFHLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxLQUFLLEdBQUcsQ0FBQyxHQUFHLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNuRTtRQUNELE9BQU8sR0FBRyxDQUFDO0lBQ2YsQ0FBQztJQUVEOztPQUVHO0lBQ0ksTUFBTSxDQUFDLFVBQVUsQ0FBQyxLQUFhLEVBQUUsTUFBYztRQUNsRCxPQUFPLE1BQU0sSUFBSSxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUVEOztPQUVHO0lBQ0ksTUFBTSxDQUFDLFFBQVEsQ0FBQyxLQUFhLEVBQUUsTUFBYztRQUNoRCxPQUFPLE1BQU0sSUFBSSxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ25FLENBQUM7SUFFRCxTQUFTO0lBQ0YsTUFBTSxDQUFDLGFBQWE7UUFDdkIsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQ25CLElBQUksTUFBTSxDQUFDLFdBQVcsSUFBSSxPQUFPLE1BQU0sQ0FBQyxXQUFXLENBQUMsR0FBRyxLQUFLLFVBQVUsRUFBRTtZQUNwRSxDQUFDLElBQUksV0FBVyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsdUNBQXVDO1NBQ2xFO1FBQ0QsT0FBTyxzQ0FBc0MsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUU7WUFDakUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDMUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDO1lBQ3ZCLE9BQU8sQ0FBQyxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUN6RCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRDs7T0FFRztJQUNJLE1BQU0sQ0FBQyxjQUFjLENBQUMsSUFBWTtRQUNyQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUN4RCxDQUFDO0lBRUQ7O09BRUc7SUFDSSxNQUFNLENBQUMsY0FBYyxDQUFDLElBQVksRUFBRSxXQUFvQixLQUFLO1FBQ2hFLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQztRQUNqQixJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFXLEVBQUUsRUFBRTtZQUNwQyxJQUFJLEtBQUssRUFBRTtnQkFDUCxJQUFJLFFBQVEsRUFBRTtvQkFDVixNQUFNLEdBQUcsVUFBVSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDM0M7cUJBQU07b0JBQ0gsTUFBTSxHQUFHLEdBQUcsQ0FBQztpQkFDaEI7Z0JBQ0QsS0FBSyxHQUFHLEtBQUssQ0FBQzthQUNqQjtpQkFBTTtnQkFDSCxNQUFNLEdBQUcsTUFBTSxHQUFHLFVBQVUsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDcEQ7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sTUFBTSxDQUFDO0lBQ2xCLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNJLE1BQU0sQ0FBQyxTQUFTLENBQUMsR0FBVSxFQUFDLEtBQVksRUFBQyxHQUFVO1FBRXRELE9BQU8sR0FBRyxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUMsR0FBRyxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0ksTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFVLEVBQUMsS0FBWSxFQUFDLElBQVc7UUFFcEQsT0FBTyxHQUFHLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBQyxJQUFJLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksTUFBTSxDQUFDLFdBQVcsQ0FBQyxHQUFVO1FBRWhDLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDakMsT0FBTyxRQUFRLENBQUM7SUFDcEIsQ0FBQztJQUdEOzs7T0FHRztJQUNJLE1BQU0sQ0FBQyxRQUFRLENBQUMsR0FBVTtRQUU3QixNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFBO1FBQ3BDLE9BQU8sUUFBUSxDQUFDO0lBQ3BCLENBQUM7Q0FDSjtBQS9PRCxnQ0ErT0M7Ozs7O0FDdFBEOzs7Ozs7R0FNRztBQUNILE1BQWEsUUFBUTtJQUlWLE1BQU0sQ0FBQyxLQUFLO1FBQ2YsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQztJQUM1QyxDQUFDO0lBRUQsa0JBQWtCO0lBQ1gsTUFBTSxLQUFLLFNBQVM7UUFDdkIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDcEMsQ0FBQztJQUVELGdCQUFnQjtJQUNULE1BQU0sS0FBSyxjQUFjO1FBQzVCLE9BQU8sQ0FBQyxDQUFDO0lBQ2IsQ0FBQztJQUVELHlCQUF5QjtJQUNsQixNQUFNLEtBQUssSUFBSTtRQUNsQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDO0lBQ2hDLENBQUM7SUFFRCxvQkFBb0I7SUFDYixNQUFNLEtBQUssZ0JBQWdCO1FBQzlCLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDckQsQ0FBQztJQUVELGdCQUFnQjtJQUNULE1BQU0sS0FBSyxVQUFVO1FBQ3hCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUM7SUFDaEMsQ0FBQztJQUVNLE1BQU0sS0FBSyxTQUFTO1FBQ3ZCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7SUFDNUIsQ0FBQztJQUVNLE1BQU0sS0FBSyxTQUFTLENBQUMsS0FBYTtRQUNyQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDN0IsQ0FBQzs7QUF2Q0wsNEJBd0NDO0FBdENrQixvQkFBVyxHQUFXLENBQUMsQ0FBQzs7Ozs7QUNXM0MsaUVBQTZEO0FBQzdELHFFQUFpRTtBQUNqRSxJQUFPLFVBQVUsR0FBRywwQkFBWSxDQUFDLFVBQVUsQ0FBQztBQUM1QyxJQUFPLFFBQVEsR0FBRyxzQkFBVSxDQUFDLFFBQVEsQ0FBQztBQUN0QyxJQUFJLEdBQUcsR0FBYSxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQztBQUM3QyxJQUFjLEVBQUUsQ0FrRGY7QUFsREQsV0FBYyxFQUFFO0lBQUMsSUFBQSxJQUFJLENBa0RwQjtJQWxEZ0IsV0FBQSxJQUFJO1FBQUMsSUFBQSxHQUFHLENBa0R4QjtRQWxEcUIsV0FBQSxHQUFHO1lBQ3JCLE1BQWEsT0FBUSxTQUFRLFVBQVU7Z0JBRW5DLGdCQUFlLEtBQUssRUFBRSxDQUFBLENBQUEsQ0FBQztnQkFDdkIsY0FBYztvQkFDVixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7b0JBQ3ZCLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNwQyxDQUFDOztZQUxjLGNBQU0sR0FBTSxFQUFDLE1BQU0sRUFBQyxZQUFZLEVBQUMsT0FBTyxFQUFDLEVBQUMsT0FBTyxFQUFDLEdBQUcsRUFBQyxRQUFRLEVBQUMsSUFBSSxFQUFDLEVBQUMsUUFBUSxFQUFDLENBQUMsRUFBQyxVQUFVLEVBQUMsRUFBRSxFQUFDLFlBQVksRUFBQyxFQUFFLEVBQUMsQ0FBQztZQURySCxXQUFPLFVBT25CLENBQUE7WUFDRCxHQUFHLENBQUMscUJBQXFCLEVBQUMsT0FBTyxDQUFDLENBQUM7WUFDbkMsTUFBYSxRQUFTLFNBQVEsVUFBVTtnQkFFcEMsZ0JBQWUsS0FBSyxFQUFFLENBQUEsQ0FBQSxDQUFDO2dCQUN2QixjQUFjO29CQUNWLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztvQkFDdkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3JDLENBQUM7O1lBTGMsZUFBTSxHQUFNLEVBQUMsTUFBTSxFQUFDLFlBQVksRUFBQyxPQUFPLEVBQUMsRUFBQyxPQUFPLEVBQUMsR0FBRyxFQUFDLFFBQVEsRUFBQyxJQUFJLEVBQUMsRUFBQyxRQUFRLEVBQUMsQ0FBQyxFQUFDLFVBQVUsRUFBQyxFQUFFLEVBQUMsWUFBWSxFQUFDLEVBQUUsRUFBQyxDQUFDO1lBRHJILFlBQVEsV0FPcEIsQ0FBQTtZQUNELEdBQUcsQ0FBQyxzQkFBc0IsRUFBQyxRQUFRLENBQUMsQ0FBQztZQUNyQyxNQUFhLFNBQVUsU0FBUSxVQUFVO2dCQU1yQyxnQkFBZSxLQUFLLEVBQUUsQ0FBQSxDQUFBLENBQUM7Z0JBQ3ZCLGNBQWM7b0JBQ1YsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO29CQUN2QixJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDdEMsQ0FBQzs7WUFMYyxnQkFBTSxHQUFNLEVBQUMsTUFBTSxFQUFDLFlBQVksRUFBQyxPQUFPLEVBQUMsRUFBQyxHQUFHLEVBQUMsQ0FBQyxFQUFDLEdBQUcsRUFBQyxDQUFDLEVBQUMsT0FBTyxFQUFDLEdBQUcsRUFBQyxRQUFRLEVBQUMsSUFBSSxFQUFDLEVBQUMsUUFBUSxFQUFDLENBQUMsRUFBQyxPQUFPLEVBQUMsQ0FBQyxFQUFDLE1BQU0sRUFBQyxPQUFPLEVBQUMsT0FBTyxFQUFDLEVBQUMsR0FBRyxFQUFDLEdBQUcsRUFBQyxHQUFHLEVBQUMsR0FBRyxFQUFDLE1BQU0sRUFBQyxnQ0FBZ0MsRUFBQyxTQUFTLEVBQUMsR0FBRyxFQUFDLFNBQVMsRUFBQyxHQUFHLEVBQUMsRUFBQyxRQUFRLEVBQUMsRUFBRSxFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsTUFBTSxFQUFDLE9BQU8sRUFBQyxPQUFPLEVBQUMsRUFBQyxHQUFHLEVBQUMsR0FBRyxFQUFDLEdBQUcsRUFBQyxHQUFHLEVBQUMsS0FBSyxFQUFDLFlBQVksRUFBQyxNQUFNLEVBQUMsaUNBQWlDLEVBQUMsU0FBUyxFQUFDLEdBQUcsRUFBQyxTQUFTLEVBQUMsR0FBRyxFQUFDLEVBQUMsUUFBUSxFQUFDLEVBQUUsRUFBQyxFQUFDLEVBQUMsTUFBTSxFQUFDLE9BQU8sRUFBQyxPQUFPLEVBQUMsRUFBQyxHQUFHLEVBQUMsQ0FBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEdBQUcsRUFBQyxNQUFNLEVBQUMsc0JBQXNCLEVBQUMsRUFBQyxRQUFRLEVBQUMsRUFBRSxFQUFDLEVBQUMsRUFBQyxNQUFNLEVBQUMsUUFBUSxFQUFDLE9BQU8sRUFBQyxFQUFDLEdBQUcsRUFBQyxHQUFHLEVBQUMsR0FBRyxFQUFDLEdBQUcsRUFBQyxPQUFPLEVBQUMsR0FBRyxFQUFDLEtBQUssRUFBQyxZQUFZLEVBQUMsVUFBVSxFQUFDLENBQUMsRUFBQyxNQUFNLEVBQUMsa0NBQWtDLEVBQUMsUUFBUSxFQUFDLEdBQUcsRUFBQyxTQUFTLEVBQUMsR0FBRyxFQUFDLFNBQVMsRUFBQyxHQUFHLEVBQUMsRUFBQyxRQUFRLEVBQUMsRUFBRSxFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsTUFBTSxFQUFDLE9BQU8sRUFBQyxPQUFPLEVBQUMsRUFBQyxRQUFRLEVBQUMsUUFBUSxFQUFDLEtBQUssRUFBQyxDQUFDLEVBQUMsTUFBTSxFQUFDLElBQUksRUFBQyxPQUFPLEVBQUMsQ0FBQyxFQUFDLE1BQU0sRUFBQyxDQUFDLEVBQUMsVUFBVSxFQUFDLEVBQUUsRUFBQyxRQUFRLEVBQUMsQ0FBQyxFQUFDLE1BQU0sRUFBQyxJQUFJLEVBQUMsT0FBTyxFQUFDLFFBQVEsRUFBQyxFQUFDLFFBQVEsRUFBQyxFQUFFLEVBQUMsQ0FBQyxFQUFDLEVBQUMsRUFBQyxNQUFNLEVBQUMsUUFBUSxFQUFDLE9BQU8sRUFBQyxFQUFDLEdBQUcsRUFBQyxDQUFDLEdBQUcsRUFBQyxHQUFHLEVBQUMsR0FBRyxFQUFDLEtBQUssRUFBQyxVQUFVLEVBQUMsVUFBVSxFQUFDLENBQUMsRUFBQyxNQUFNLEVBQUMsK0JBQStCLEVBQUMsU0FBUyxFQUFDLEdBQUcsRUFBQyxTQUFTLEVBQUMsR0FBRyxFQUFDLEVBQUMsUUFBUSxFQUFDLEVBQUUsRUFBQyxDQUFDLEVBQUMsQ0FBQyxFQUFDLFlBQVksRUFBQyxDQUFDLEVBQUMsT0FBTyxFQUFDLENBQUMsRUFBQyxRQUFRLEVBQUMsRUFBRSxFQUFDLFdBQVcsRUFBQyxFQUFDLEdBQUcsRUFBQyxDQUFDLEVBQUMsT0FBTyxFQUFDLEdBQUcsRUFBQyxhQUFhLEVBQUMsWUFBWSxFQUFDLE9BQU8sRUFBQyxJQUFJLEVBQUMsUUFBUSxFQUFDLEVBQUUsRUFBQyxLQUFLLEVBQUMsR0FBRyxFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsRUFBQyxFQUFDLE9BQU8sRUFBQyxHQUFHLEVBQUMsYUFBYSxFQUFDLFlBQVksRUFBQyxPQUFPLEVBQUMsSUFBSSxFQUFDLFFBQVEsRUFBQyxFQUFFLEVBQUMsS0FBSyxFQUFDLEdBQUcsRUFBQyxPQUFPLEVBQUMsRUFBRSxFQUFDLEVBQUMsRUFBQyxPQUFPLEVBQUMsR0FBRyxFQUFDLGFBQWEsRUFBQyxZQUFZLEVBQUMsT0FBTyxFQUFDLElBQUksRUFBQyxRQUFRLEVBQUMsRUFBRSxFQUFDLEtBQUssRUFBQyxHQUFHLEVBQUMsT0FBTyxFQUFDLEVBQUUsRUFBQyxDQUFDLEVBQUMsU0FBUyxFQUFDLENBQUMsRUFBQyxPQUFPLEVBQUMsSUFBSSxFQUFDLGFBQWEsRUFBQyxZQUFZLEVBQUMsT0FBTyxFQUFDLEtBQUssRUFBQyxRQUFRLEVBQUMsRUFBRSxFQUFDLEtBQUssRUFBQyxTQUFTLEVBQUMsT0FBTyxFQUFDLENBQUMsRUFBQyxFQUFDLEVBQUMsT0FBTyxFQUFDLEtBQUssRUFBQyxhQUFhLEVBQUMsWUFBWSxFQUFDLE9BQU8sRUFBQyxLQUFLLEVBQUMsUUFBUSxFQUFDLEVBQUUsRUFBQyxLQUFLLEVBQUMsU0FBUyxFQUFDLE9BQU8sRUFBQyxFQUFFLEVBQUMsRUFBQyxFQUFDLE9BQU8sRUFBQyxJQUFJLEVBQUMsYUFBYSxFQUFDLFlBQVksRUFBQyxPQUFPLEVBQUMsS0FBSyxFQUFDLFFBQVEsRUFBQyxFQUFFLEVBQUMsS0FBSyxFQUFDLFNBQVMsRUFBQyxPQUFPLEVBQUMsRUFBRSxFQUFDLEVBQUMsRUFBQyxPQUFPLEVBQUMsS0FBSyxFQUFDLGFBQWEsRUFBQyxZQUFZLEVBQUMsT0FBTyxFQUFDLEtBQUssRUFBQyxRQUFRLEVBQUMsRUFBRSxFQUFDLEtBQUssRUFBQyxTQUFTLEVBQUMsT0FBTyxFQUFDLEVBQUUsRUFBQyxFQUFDLEVBQUMsT0FBTyxFQUFDLElBQUksRUFBQyxhQUFhLEVBQUMsWUFBWSxFQUFDLE9BQU8sRUFBQyxLQUFLLEVBQUMsUUFBUSxFQUFDLEVBQUUsRUFBQyxLQUFLLEVBQUMsU0FBUyxFQUFDLE9BQU8sRUFBQyxFQUFFLEVBQUMsQ0FBQyxFQUFDLFVBQVUsRUFBQyxDQUFDLEVBQUMsT0FBTyxFQUFDLENBQUMsRUFBQyxhQUFhLEVBQUMsWUFBWSxFQUFDLE9BQU8sRUFBQyxJQUFJLEVBQUMsUUFBUSxFQUFDLEVBQUUsRUFBQyxLQUFLLEVBQUMsVUFBVSxFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsRUFBQyxFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsYUFBYSxFQUFDLFlBQVksRUFBQyxPQUFPLEVBQUMsSUFBSSxFQUFDLFFBQVEsRUFBQyxFQUFFLEVBQUMsS0FBSyxFQUFDLFVBQVUsRUFBQyxPQUFPLEVBQUMsRUFBRSxFQUFDLEVBQUMsRUFBQyxPQUFPLEVBQUMsQ0FBQyxFQUFDLGFBQWEsRUFBQyxZQUFZLEVBQUMsT0FBTyxFQUFDLElBQUksRUFBQyxRQUFRLEVBQUMsRUFBRSxFQUFDLEtBQUssRUFBQyxVQUFVLEVBQUMsT0FBTyxFQUFDLEVBQUUsRUFBQyxFQUFDLEVBQUMsT0FBTyxFQUFDLENBQUMsRUFBQyxhQUFhLEVBQUMsWUFBWSxFQUFDLE9BQU8sRUFBQyxJQUFJLEVBQUMsUUFBUSxFQUFDLEVBQUUsRUFBQyxLQUFLLEVBQUMsVUFBVSxFQUFDLE9BQU8sRUFBQyxFQUFFLEVBQUMsRUFBQyxFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsYUFBYSxFQUFDLFlBQVksRUFBQyxPQUFPLEVBQUMsSUFBSSxFQUFDLFFBQVEsRUFBQyxFQUFFLEVBQUMsS0FBSyxFQUFDLFVBQVUsRUFBQyxPQUFPLEVBQUMsRUFBRSxFQUFDLENBQUMsRUFBQyxFQUFDLENBQUMsRUFBQyxNQUFNLEVBQUMsTUFBTSxFQUFDLElBQUksRUFBQyxDQUFDLEVBQUMsV0FBVyxFQUFDLEVBQUUsRUFBQyxRQUFRLEVBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxVQUFVLEVBQUMsQ0FBQyxnQ0FBZ0MsRUFBQyxpQ0FBaUMsRUFBQyxzQkFBc0IsRUFBQyxrQ0FBa0MsRUFBQywrQkFBK0IsQ0FBQyxFQUFDLFlBQVksRUFBQyxFQUFFLEVBQUMsQ0FBQztZQUxqN0UsYUFBUyxZQVdyQixDQUFBO1lBQ0QsR0FBRyxDQUFDLHVCQUF1QixFQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3ZDLE1BQWEsTUFBTyxTQUFRLFVBQVU7Z0JBRWxDLGdCQUFlLEtBQUssRUFBRSxDQUFBLENBQUEsQ0FBQztnQkFDdkIsY0FBYztvQkFDVixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7b0JBQ3ZCLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNuQyxDQUFDOztZQUxjLGFBQU0sR0FBTSxFQUFDLE1BQU0sRUFBQyxZQUFZLEVBQUMsT0FBTyxFQUFDLEVBQUMsT0FBTyxFQUFDLEdBQUcsRUFBQyxRQUFRLEVBQUMsSUFBSSxFQUFDLEVBQUMsUUFBUSxFQUFDLENBQUMsRUFBQyxPQUFPLEVBQUMsQ0FBQyxFQUFDLE1BQU0sRUFBQyxrQkFBa0IsRUFBQyxPQUFPLEVBQUMsRUFBQyxHQUFHLEVBQUMsR0FBRyxFQUFDLEdBQUcsRUFBQyxHQUFHLEVBQUMsT0FBTyxFQUFDLEdBQUcsRUFBQyxjQUFjLEVBQUMsSUFBSSxFQUFDLFVBQVUsRUFBQyxJQUFJLEVBQUMsUUFBUSxFQUFDLEdBQUcsRUFBQyxTQUFTLEVBQUMsdUJBQXVCLEVBQUMsRUFBQyxRQUFRLEVBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxVQUFVLEVBQUMsRUFBRSxFQUFDLFlBQVksRUFBQyxFQUFFLEVBQUMsQ0FBQztZQUQvUixVQUFNLFNBT2xCLENBQUE7WUFDRCxHQUFHLENBQUMsb0JBQW9CLEVBQUMsTUFBTSxDQUFDLENBQUM7WUFDakMsTUFBYSxNQUFPLFNBQVEsVUFBVTtnQkFFbEMsZ0JBQWUsS0FBSyxFQUFFLENBQUEsQ0FBQSxDQUFDO2dCQUN2QixjQUFjO29CQUNWLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztvQkFDdkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ25DLENBQUM7O1lBTGMsYUFBTSxHQUFNLEVBQUMsTUFBTSxFQUFDLFlBQVksRUFBQyxPQUFPLEVBQUMsRUFBQyxPQUFPLEVBQUMsR0FBRyxFQUFDLGNBQWMsRUFBQyxJQUFJLEVBQUMsUUFBUSxFQUFDLElBQUksRUFBQyxFQUFDLFFBQVEsRUFBQyxDQUFDLEVBQUMsVUFBVSxFQUFDLEVBQUUsRUFBQyxZQUFZLEVBQUMsRUFBRSxFQUFDLENBQUM7WUFEekksVUFBTSxTQU9sQixDQUFBO1lBQ0QsR0FBRyxDQUFDLG9CQUFvQixFQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3JDLENBQUMsRUFsRHFCLEdBQUcsR0FBSCxRQUFHLEtBQUgsUUFBRyxRQWtEeEI7SUFBRCxDQUFDLEVBbERnQixJQUFJLEdBQUosT0FBSSxLQUFKLE9BQUksUUFrRHBCO0FBQUQsQ0FBQyxFQWxEYSxFQUFFLEdBQUYsVUFBRSxLQUFGLFVBQUUsUUFrRGY7QUFDRCxXQUFjLEVBQUU7SUFBQyxJQUFBLElBQUksQ0EwRHBCO0lBMURnQixXQUFBLElBQUk7UUFBQyxJQUFBLElBQUksQ0EwRHpCO1FBMURxQixXQUFBLElBQUk7WUFDdEIsTUFBYSxJQUFLLFNBQVEsUUFBUTtnQkFHOUIsZ0JBQWUsS0FBSyxFQUFFLENBQUEsQ0FBQSxDQUFDO2dCQUN2QixjQUFjO29CQUNWLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztvQkFDdkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ2pDLENBQUM7O1lBTGMsV0FBTSxHQUFNLEVBQUMsTUFBTSxFQUFDLFVBQVUsRUFBQyxPQUFPLEVBQUMsRUFBQyxPQUFPLEVBQUMsR0FBRyxFQUFDLFFBQVEsRUFBQyxJQUFJLEVBQUMsRUFBQyxRQUFRLEVBQUMsQ0FBQyxFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsTUFBTSxFQUFDLE9BQU8sRUFBQyxPQUFPLEVBQUMsRUFBQyxLQUFLLEVBQUMsT0FBTyxFQUFDLEtBQUssRUFBQyxDQUFDLEVBQUMsTUFBTSxFQUFDLG9CQUFvQixFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsTUFBTSxFQUFDLENBQUMsRUFBQyxRQUFRLEVBQUMsQ0FBQyxFQUFDLEVBQUMsUUFBUSxFQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsVUFBVSxFQUFDLENBQUMsb0JBQW9CLENBQUMsRUFBQyxZQUFZLEVBQUMsRUFBRSxFQUFDLENBQUM7WUFGdlEsU0FBSSxPQVFoQixDQUFBO1lBQ0QsR0FBRyxDQUFDLG1CQUFtQixFQUFDLElBQUksQ0FBQyxDQUFDO1lBQzlCLE1BQWEsSUFBSyxTQUFRLFFBQVE7Z0JBRTlCLGdCQUFlLEtBQUssRUFBRSxDQUFBLENBQUEsQ0FBQztnQkFDdkIsY0FBYztvQkFDVixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7b0JBQ3ZCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNqQyxDQUFDOztZQUxjLFdBQU0sR0FBTSxFQUFDLE1BQU0sRUFBQyxVQUFVLEVBQUMsT0FBTyxFQUFDLEVBQUMsT0FBTyxFQUFDLEdBQUcsRUFBQyxRQUFRLEVBQUMsSUFBSSxFQUFDLEVBQUMsUUFBUSxFQUFDLENBQUMsRUFBQyxVQUFVLEVBQUMsRUFBRSxFQUFDLFlBQVksRUFBQyxFQUFFLEVBQUMsQ0FBQztZQURuSCxTQUFJLE9BT2hCLENBQUE7WUFDRCxHQUFHLENBQUMsbUJBQW1CLEVBQUMsSUFBSSxDQUFDLENBQUM7WUFDOUIsTUFBYSxRQUFTLFNBQVEsUUFBUTtnQkFNbEMsZ0JBQWUsS0FBSyxFQUFFLENBQUEsQ0FBQSxDQUFDO2dCQUN2QixjQUFjO29CQUNWLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztvQkFDdkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3JDLENBQUM7O1lBTGMsZUFBTSxHQUFNLEVBQUMsTUFBTSxFQUFDLFVBQVUsRUFBQyxPQUFPLEVBQUMsRUFBQyxPQUFPLEVBQUMsR0FBRyxFQUFDLFFBQVEsRUFBQyxJQUFJLEVBQUMsRUFBQyxRQUFRLEVBQUMsQ0FBQyxFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsTUFBTSxFQUFDLE9BQU8sRUFBQyxPQUFPLEVBQUMsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsT0FBTyxFQUFDLEdBQUcsRUFBQyxNQUFNLEVBQUMsa0NBQWtDLEVBQUMsUUFBUSxFQUFDLEVBQUUsRUFBQyxFQUFDLFFBQVEsRUFBQyxDQUFDLEVBQUMsRUFBQyxFQUFDLE1BQU0sRUFBQyxPQUFPLEVBQUMsT0FBTyxFQUFDLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxHQUFHLEVBQUMsR0FBRyxFQUFDLE9BQU8sRUFBQyxHQUFHLEVBQUMsTUFBTSxFQUFDLGtDQUFrQyxFQUFDLFFBQVEsRUFBQyxFQUFFLEVBQUMsRUFBQyxRQUFRLEVBQUMsQ0FBQyxFQUFDLEVBQUMsRUFBQyxNQUFNLEVBQUMsT0FBTyxFQUFDLE9BQU8sRUFBQyxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEdBQUcsRUFBQyxNQUFNLEVBQUMsaUNBQWlDLEVBQUMsRUFBQyxRQUFRLEVBQUMsQ0FBQyxFQUFDLEVBQUMsRUFBQyxNQUFNLEVBQUMsT0FBTyxFQUFDLE9BQU8sRUFBQyxFQUFDLEdBQUcsRUFBQyxFQUFFLEVBQUMsR0FBRyxFQUFDLEVBQUUsRUFBQyxNQUFNLEVBQUMsOEJBQThCLEVBQUMsRUFBQyxRQUFRLEVBQUMsQ0FBQyxFQUFDLEVBQUMsRUFBQyxNQUFNLEVBQUMsUUFBUSxFQUFDLE9BQU8sRUFBQyxFQUFDLEdBQUcsRUFBQyxHQUFHLEVBQUMsR0FBRyxFQUFDLEdBQUcsRUFBQyxPQUFPLEVBQUMsR0FBRyxFQUFDLEtBQUssRUFBQyxVQUFVLEVBQUMsVUFBVSxFQUFDLENBQUMsRUFBQyxNQUFNLEVBQUMsa0NBQWtDLEVBQUMsUUFBUSxFQUFDLEdBQUcsRUFBQyxTQUFTLEVBQUMsR0FBRyxFQUFDLFNBQVMsRUFBQyxHQUFHLEVBQUMsRUFBQyxRQUFRLEVBQUMsQ0FBQyxFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsTUFBTSxFQUFDLE9BQU8sRUFBQyxPQUFPLEVBQUMsRUFBQyxRQUFRLEVBQUMsUUFBUSxFQUFDLEtBQUssRUFBQyxDQUFDLEVBQUMsTUFBTSxFQUFDLElBQUksRUFBQyxPQUFPLEVBQUMsQ0FBQyxFQUFDLE1BQU0sRUFBQyxDQUFDLEVBQUMsVUFBVSxFQUFDLEVBQUUsRUFBQyxRQUFRLEVBQUMsQ0FBQyxFQUFDLE1BQU0sRUFBQyxJQUFJLEVBQUMsT0FBTyxFQUFDLFFBQVEsRUFBQyxFQUFDLFFBQVEsRUFBQyxFQUFFLEVBQUMsQ0FBQyxFQUFDLEVBQUMsRUFBQyxNQUFNLEVBQUMsUUFBUSxFQUFDLE9BQU8sRUFBQyxFQUFDLEdBQUcsRUFBQyxHQUFHLEVBQUMsR0FBRyxFQUFDLEdBQUcsRUFBQyxPQUFPLEVBQUMsR0FBRyxFQUFDLEtBQUssRUFBQyxTQUFTLEVBQUMsVUFBVSxFQUFDLENBQUMsRUFBQyxNQUFNLEVBQUMsa0NBQWtDLEVBQUMsUUFBUSxFQUFDLEdBQUcsRUFBQyxTQUFTLEVBQUMsR0FBRyxFQUFDLFNBQVMsRUFBQyxHQUFHLEVBQUMsRUFBQyxRQUFRLEVBQUMsQ0FBQyxFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsTUFBTSxFQUFDLE9BQU8sRUFBQyxPQUFPLEVBQUMsRUFBQyxRQUFRLEVBQUMsUUFBUSxFQUFDLEtBQUssRUFBQyxDQUFDLEVBQUMsTUFBTSxFQUFDLElBQUksRUFBQyxPQUFPLEVBQUMsQ0FBQyxFQUFDLE1BQU0sRUFBQyxDQUFDLEVBQUMsVUFBVSxFQUFDLEVBQUUsRUFBQyxRQUFRLEVBQUMsQ0FBQyxFQUFDLE1BQU0sRUFBQyxJQUFJLEVBQUMsT0FBTyxFQUFDLFFBQVEsRUFBQyxFQUFDLFFBQVEsRUFBQyxFQUFFLEVBQUMsQ0FBQyxFQUFDLEVBQUMsRUFBQyxNQUFNLEVBQUMsUUFBUSxFQUFDLE9BQU8sRUFBQyxFQUFDLEdBQUcsRUFBQyxHQUFHLEVBQUMsR0FBRyxFQUFDLEdBQUcsRUFBQyxPQUFPLEVBQUMsR0FBRyxFQUFDLEtBQUssRUFBQyxXQUFXLEVBQUMsVUFBVSxFQUFDLENBQUMsRUFBQyxNQUFNLEVBQUMsa0NBQWtDLEVBQUMsUUFBUSxFQUFDLEdBQUcsRUFBQyxTQUFTLEVBQUMsR0FBRyxFQUFDLFNBQVMsRUFBQyxHQUFHLEVBQUMsRUFBQyxRQUFRLEVBQUMsQ0FBQyxFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsTUFBTSxFQUFDLE9BQU8sRUFBQyxPQUFPLEVBQUMsRUFBQyxRQUFRLEVBQUMsUUFBUSxFQUFDLEtBQUssRUFBQyxDQUFDLEVBQUMsTUFBTSxFQUFDLElBQUksRUFBQyxPQUFPLEVBQUMsQ0FBQyxFQUFDLE1BQU0sRUFBQyxDQUFDLEVBQUMsVUFBVSxFQUFDLEVBQUUsRUFBQyxRQUFRLEVBQUMsQ0FBQyxFQUFDLE1BQU0sRUFBQyxJQUFJLEVBQUMsT0FBTyxFQUFDLFFBQVEsRUFBQyxFQUFDLFFBQVEsRUFBQyxFQUFFLEVBQUMsQ0FBQyxFQUFDLEVBQUMsRUFBQyxNQUFNLEVBQUMsUUFBUSxFQUFDLE9BQU8sRUFBQyxFQUFDLEdBQUcsRUFBQyxHQUFHLEVBQUMsR0FBRyxFQUFDLEdBQUcsRUFBQyxPQUFPLEVBQUMsR0FBRyxFQUFDLEtBQUssRUFBQyxZQUFZLEVBQUMsVUFBVSxFQUFDLENBQUMsRUFBQyxNQUFNLEVBQUMsa0NBQWtDLEVBQUMsUUFBUSxFQUFDLEdBQUcsRUFBQyxTQUFTLEVBQUMsR0FBRyxFQUFDLFNBQVMsRUFBQyxHQUFHLEVBQUMsRUFBQyxRQUFRLEVBQUMsRUFBRSxFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsTUFBTSxFQUFDLE9BQU8sRUFBQyxPQUFPLEVBQUMsRUFBQyxRQUFRLEVBQUMsUUFBUSxFQUFDLEtBQUssRUFBQyxDQUFDLEVBQUMsTUFBTSxFQUFDLElBQUksRUFBQyxPQUFPLEVBQUMsQ0FBQyxFQUFDLE1BQU0sRUFBQyxDQUFDLEVBQUMsVUFBVSxFQUFDLEVBQUUsRUFBQyxRQUFRLEVBQUMsQ0FBQyxFQUFDLE1BQU0sRUFBQyxJQUFJLEVBQUMsT0FBTyxFQUFDLFFBQVEsRUFBQyxFQUFDLFFBQVEsRUFBQyxFQUFFLEVBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxVQUFVLEVBQUMsQ0FBQyxrQ0FBa0MsRUFBQyxpQ0FBaUMsRUFBQyw4QkFBOEIsRUFBQyxrQ0FBa0MsRUFBQyxrQ0FBa0MsRUFBQyxrQ0FBa0MsRUFBQyxrQ0FBa0MsQ0FBQyxFQUFDLFlBQVksRUFBQyxFQUFFLEVBQUMsQ0FBQztZQUxwbUUsYUFBUSxXQVdwQixDQUFBO1lBQ0QsR0FBRyxDQUFDLHVCQUF1QixFQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3RDLE1BQWEsTUFBTyxTQUFRLFFBQVE7Z0JBSWhDLGdCQUFlLEtBQUssRUFBRSxDQUFBLENBQUEsQ0FBQztnQkFDdkIsY0FBYztvQkFDVixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7b0JBQ3ZCLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNuQyxDQUFDOztZQUxjLGFBQU0sR0FBTSxFQUFDLE1BQU0sRUFBQyxVQUFVLEVBQUMsT0FBTyxFQUFDLEVBQUMsT0FBTyxFQUFDLEdBQUcsRUFBQyxRQUFRLEVBQUMsSUFBSSxFQUFDLEVBQUMsUUFBUSxFQUFDLENBQUMsRUFBQyxZQUFZLEVBQUMsQ0FBQyxFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsUUFBUSxFQUFDLEdBQUcsRUFBQyxXQUFXLEVBQUMsRUFBQyxTQUFTLEVBQUMsQ0FBQyxFQUFDLE9BQU8sRUFBQyxLQUFLLEVBQUMsYUFBYSxFQUFDLFlBQVksRUFBQyxPQUFPLEVBQUMsS0FBSyxFQUFDLFFBQVEsRUFBQyxHQUFHLEVBQUMsS0FBSyxFQUFDLFNBQVMsRUFBQyxPQUFPLEVBQUMsQ0FBQyxFQUFDLEVBQUMsRUFBQyxPQUFPLEVBQUMsSUFBSSxFQUFDLGFBQWEsRUFBQyxZQUFZLEVBQUMsT0FBTyxFQUFDLEtBQUssRUFBQyxRQUFRLEVBQUMsR0FBRyxFQUFDLEtBQUssRUFBQyxTQUFTLEVBQUMsT0FBTyxFQUFDLENBQUMsRUFBQyxFQUFDLEVBQUMsT0FBTyxFQUFDLEtBQUssRUFBQyxhQUFhLEVBQUMsWUFBWSxFQUFDLE9BQU8sRUFBQyxLQUFLLEVBQUMsUUFBUSxFQUFDLEdBQUcsRUFBQyxLQUFLLEVBQUMsU0FBUyxFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsRUFBQyxFQUFDLE9BQU8sRUFBQyxJQUFJLEVBQUMsYUFBYSxFQUFDLFlBQVksRUFBQyxPQUFPLEVBQUMsS0FBSyxFQUFDLFFBQVEsRUFBQyxHQUFHLEVBQUMsS0FBSyxFQUFDLFNBQVMsRUFBQyxPQUFPLEVBQUMsQ0FBQyxFQUFDLEVBQUMsRUFBQyxPQUFPLEVBQUMsS0FBSyxFQUFDLGFBQWEsRUFBQyxZQUFZLEVBQUMsT0FBTyxFQUFDLEtBQUssRUFBQyxRQUFRLEVBQUMsR0FBRyxFQUFDLEtBQUssRUFBQyxTQUFTLEVBQUMsT0FBTyxFQUFDLENBQUMsRUFBQyxFQUFDLEVBQUMsT0FBTyxFQUFDLElBQUksRUFBQyxhQUFhLEVBQUMsWUFBWSxFQUFDLE9BQU8sRUFBQyxLQUFLLEVBQUMsUUFBUSxFQUFDLEdBQUcsRUFBQyxLQUFLLEVBQUMsU0FBUyxFQUFDLE9BQU8sRUFBQyxFQUFFLEVBQUMsRUFBQyxFQUFDLE9BQU8sRUFBQyxLQUFLLEVBQUMsYUFBYSxFQUFDLFlBQVksRUFBQyxPQUFPLEVBQUMsS0FBSyxFQUFDLFFBQVEsRUFBQyxHQUFHLEVBQUMsS0FBSyxFQUFDLFNBQVMsRUFBQyxPQUFPLEVBQUMsRUFBRSxFQUFDLENBQUMsRUFBQyxFQUFDLEVBQUMsRUFBQyxRQUFRLEVBQUMsR0FBRyxFQUFDLFdBQVcsRUFBQyxFQUFDLFNBQVMsRUFBQyxDQUFDLEVBQUMsT0FBTyxFQUFDLElBQUksRUFBQyxhQUFhLEVBQUMsWUFBWSxFQUFDLE9BQU8sRUFBQyxLQUFLLEVBQUMsUUFBUSxFQUFDLEdBQUcsRUFBQyxLQUFLLEVBQUMsU0FBUyxFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsRUFBQyxFQUFDLE9BQU8sRUFBQyxLQUFLLEVBQUMsYUFBYSxFQUFDLFlBQVksRUFBQyxPQUFPLEVBQUMsS0FBSyxFQUFDLFFBQVEsRUFBQyxHQUFHLEVBQUMsS0FBSyxFQUFDLFNBQVMsRUFBQyxPQUFPLEVBQUMsQ0FBQyxFQUFDLEVBQUMsRUFBQyxPQUFPLEVBQUMsSUFBSSxFQUFDLGFBQWEsRUFBQyxZQUFZLEVBQUMsT0FBTyxFQUFDLEtBQUssRUFBQyxRQUFRLEVBQUMsR0FBRyxFQUFDLEtBQUssRUFBQyxTQUFTLEVBQUMsT0FBTyxFQUFDLENBQUMsRUFBQyxFQUFDLEVBQUMsT0FBTyxFQUFDLEtBQUssRUFBQyxhQUFhLEVBQUMsWUFBWSxFQUFDLE9BQU8sRUFBQyxLQUFLLEVBQUMsUUFBUSxFQUFDLEdBQUcsRUFBQyxLQUFLLEVBQUMsU0FBUyxFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsRUFBQyxFQUFDLE9BQU8sRUFBQyxJQUFJLEVBQUMsYUFBYSxFQUFDLFlBQVksRUFBQyxPQUFPLEVBQUMsS0FBSyxFQUFDLFFBQVEsRUFBQyxHQUFHLEVBQUMsS0FBSyxFQUFDLFNBQVMsRUFBQyxPQUFPLEVBQUMsQ0FBQyxFQUFDLEVBQUMsRUFBQyxPQUFPLEVBQUMsS0FBSyxFQUFDLGFBQWEsRUFBQyxZQUFZLEVBQUMsT0FBTyxFQUFDLEtBQUssRUFBQyxRQUFRLEVBQUMsR0FBRyxFQUFDLEtBQUssRUFBQyxTQUFTLEVBQUMsT0FBTyxFQUFDLEVBQUUsRUFBQyxFQUFDLEVBQUMsT0FBTyxFQUFDLElBQUksRUFBQyxhQUFhLEVBQUMsWUFBWSxFQUFDLE9BQU8sRUFBQyxLQUFLLEVBQUMsUUFBUSxFQUFDLEdBQUcsRUFBQyxLQUFLLEVBQUMsU0FBUyxFQUFDLE9BQU8sRUFBQyxFQUFFLEVBQUMsQ0FBQyxFQUFDLEVBQUMsQ0FBQyxFQUFDLE1BQU0sRUFBQyxVQUFVLEVBQUMsSUFBSSxFQUFDLEVBQUUsRUFBQyxXQUFXLEVBQUMsRUFBRSxFQUFDLFFBQVEsRUFBQyxDQUFDLEVBQUMsRUFBQyxFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsUUFBUSxFQUFDLEdBQUcsRUFBQyxXQUFXLEVBQUMsRUFBQyxVQUFVLEVBQUMsQ0FBQyxFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsYUFBYSxFQUFDLFlBQVksRUFBQyxPQUFPLEVBQUMsSUFBSSxFQUFDLFFBQVEsRUFBQyxHQUFHLEVBQUMsS0FBSyxFQUFDLFVBQVUsRUFBQyxPQUFPLEVBQUMsQ0FBQyxFQUFDLEVBQUMsRUFBQyxPQUFPLEVBQUMsR0FBRyxFQUFDLGFBQWEsRUFBQyxZQUFZLEVBQUMsT0FBTyxFQUFDLElBQUksRUFBQyxRQUFRLEVBQUMsR0FBRyxFQUFDLEtBQUssRUFBQyxVQUFVLEVBQUMsT0FBTyxFQUFDLEdBQUcsRUFBQyxDQUFDLEVBQUMsT0FBTyxFQUFDLENBQUMsRUFBQyxPQUFPLEVBQUMsQ0FBQyxFQUFDLGFBQWEsRUFBQyxZQUFZLEVBQUMsT0FBTyxFQUFDLElBQUksRUFBQyxRQUFRLEVBQUMsR0FBRyxFQUFDLEtBQUssRUFBQyxPQUFPLEVBQUMsT0FBTyxFQUFDLENBQUMsRUFBQyxFQUFDLEVBQUMsT0FBTyxFQUFDLEdBQUcsRUFBQyxhQUFhLEVBQUMsWUFBWSxFQUFDLE9BQU8sRUFBQyxJQUFJLEVBQUMsUUFBUSxFQUFDLEdBQUcsRUFBQyxLQUFLLEVBQUMsT0FBTyxFQUFDLE9BQU8sRUFBQyxFQUFFLEVBQUMsRUFBQyxFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsYUFBYSxFQUFDLFlBQVksRUFBQyxPQUFPLEVBQUMsSUFBSSxFQUFDLFFBQVEsRUFBQyxHQUFHLEVBQUMsS0FBSyxFQUFDLE9BQU8sRUFBQyxPQUFPLEVBQUMsR0FBRyxFQUFDLEVBQUMsRUFBQyxPQUFPLEVBQUMsR0FBRyxFQUFDLGFBQWEsRUFBQyxZQUFZLEVBQUMsT0FBTyxFQUFDLElBQUksRUFBQyxRQUFRLEVBQUMsR0FBRyxFQUFDLEtBQUssRUFBQyxPQUFPLEVBQUMsT0FBTyxFQUFDLEdBQUcsRUFBQyxFQUFDLEVBQUMsT0FBTyxFQUFDLENBQUMsRUFBQyxhQUFhLEVBQUMsWUFBWSxFQUFDLE9BQU8sRUFBQyxJQUFJLEVBQUMsUUFBUSxFQUFDLEdBQUcsRUFBQyxLQUFLLEVBQUMsT0FBTyxFQUFDLE9BQU8sRUFBQyxHQUFHLEVBQUMsQ0FBQyxFQUFDLEVBQUMsRUFBQyxFQUFDLFFBQVEsRUFBQyxHQUFHLEVBQUMsV0FBVyxFQUFDLEVBQUMsVUFBVSxFQUFDLENBQUMsRUFBQyxPQUFPLEVBQUMsQ0FBQyxFQUFDLGFBQWEsRUFBQyxZQUFZLEVBQUMsT0FBTyxFQUFDLElBQUksRUFBQyxRQUFRLEVBQUMsR0FBRyxFQUFDLEtBQUssRUFBQyxVQUFVLEVBQUMsT0FBTyxFQUFDLENBQUMsRUFBQyxFQUFDLEVBQUMsT0FBTyxFQUFDLENBQUMsR0FBRyxFQUFDLGFBQWEsRUFBQyxZQUFZLEVBQUMsT0FBTyxFQUFDLElBQUksRUFBQyxRQUFRLEVBQUMsR0FBRyxFQUFDLEtBQUssRUFBQyxVQUFVLEVBQUMsT0FBTyxFQUFDLEdBQUcsRUFBQyxDQUFDLEVBQUMsT0FBTyxFQUFDLENBQUMsRUFBQyxPQUFPLEVBQUMsR0FBRyxFQUFDLGFBQWEsRUFBQyxZQUFZLEVBQUMsT0FBTyxFQUFDLElBQUksRUFBQyxRQUFRLEVBQUMsR0FBRyxFQUFDLEtBQUssRUFBQyxPQUFPLEVBQUMsT0FBTyxFQUFDLENBQUMsRUFBQyxFQUFDLEVBQUMsT0FBTyxFQUFDLENBQUMsRUFBQyxhQUFhLEVBQUMsWUFBWSxFQUFDLE9BQU8sRUFBQyxJQUFJLEVBQUMsUUFBUSxFQUFDLEdBQUcsRUFBQyxLQUFLLEVBQUMsT0FBTyxFQUFDLE9BQU8sRUFBQyxFQUFFLEVBQUMsRUFBQyxFQUFDLE9BQU8sRUFBQyxHQUFHLEVBQUMsYUFBYSxFQUFDLFlBQVksRUFBQyxPQUFPLEVBQUMsSUFBSSxFQUFDLFFBQVEsRUFBQyxHQUFHLEVBQUMsS0FBSyxFQUFDLE9BQU8sRUFBQyxPQUFPLEVBQUMsR0FBRyxFQUFDLEVBQUMsRUFBQyxPQUFPLEVBQUMsQ0FBQyxFQUFDLGFBQWEsRUFBQyxZQUFZLEVBQUMsT0FBTyxFQUFDLElBQUksRUFBQyxRQUFRLEVBQUMsR0FBRyxFQUFDLEtBQUssRUFBQyxPQUFPLEVBQUMsT0FBTyxFQUFDLEdBQUcsRUFBQyxDQUFDLEVBQUMsRUFBQyxDQUFDLEVBQUMsTUFBTSxFQUFDLFlBQVksRUFBQyxJQUFJLEVBQUMsRUFBRSxFQUFDLFdBQVcsRUFBQyxFQUFFLEVBQUMsUUFBUSxFQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsVUFBVSxFQUFDLEVBQUUsRUFBQyxZQUFZLEVBQUMsRUFBRSxFQUFDLENBQUM7WUFIejZGLFdBQU0sU0FTbEIsQ0FBQTtZQUNELEdBQUcsQ0FBQyxxQkFBcUIsRUFBQyxNQUFNLENBQUMsQ0FBQztZQUNsQyxNQUFhLFNBQVUsU0FBUSxRQUFRO2dCQU9uQyxnQkFBZSxLQUFLLEVBQUUsQ0FBQSxDQUFBLENBQUM7Z0JBQ3ZCLGNBQWM7b0JBQ1YsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO29CQUN2QixJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDdEMsQ0FBQzs7WUFMYyxnQkFBTSxHQUFNLEVBQUMsTUFBTSxFQUFDLFVBQVUsRUFBQyxPQUFPLEVBQUMsRUFBQyxPQUFPLEVBQUMsR0FBRyxFQUFDLFFBQVEsRUFBQyxJQUFJLEVBQUMsRUFBQyxRQUFRLEVBQUMsQ0FBQyxFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsTUFBTSxFQUFDLE9BQU8sRUFBQyxPQUFPLEVBQUMsRUFBQyxHQUFHLEVBQUMsQ0FBQyxFQUFDLEdBQUcsRUFBQyxDQUFDLEVBQUMsS0FBSyxFQUFDLFFBQVEsRUFBQyxLQUFLLEVBQUMsQ0FBQyxFQUFDLE1BQU0sRUFBQyxnQ0FBZ0MsRUFBQyxPQUFPLEVBQUMsQ0FBQyxFQUFDLE1BQU0sRUFBQyxDQUFDLEVBQUMsUUFBUSxFQUFDLENBQUMsRUFBQyxFQUFDLFFBQVEsRUFBQyxDQUFDLEVBQUMsRUFBQyxFQUFDLE1BQU0sRUFBQyxLQUFLLEVBQUMsT0FBTyxFQUFDLEVBQUMsR0FBRyxFQUFDLENBQUMsRUFBQyxHQUFHLEVBQUMsQ0FBQyxFQUFDLE9BQU8sRUFBQyxHQUFHLEVBQUMsS0FBSyxFQUFDLFNBQVMsRUFBQyxRQUFRLEVBQUMsR0FBRyxFQUFDLFFBQVEsRUFBQyxHQUFHLEVBQUMsUUFBUSxFQUFDLEdBQUcsRUFBQyxTQUFTLEVBQUMsQ0FBQyxFQUFDLFFBQVEsRUFBQyxDQUFDLEVBQUMsRUFBQyxRQUFRLEVBQUMsQ0FBQyxFQUFDLE9BQU8sRUFBQyxDQUFDLEVBQUMsTUFBTSxFQUFDLGFBQWEsRUFBQyxPQUFPLEVBQUMsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxHQUFHLEVBQUMsS0FBSyxFQUFDLGFBQWEsRUFBQyxNQUFNLEVBQUMsa0NBQWtDLEVBQUMsUUFBUSxFQUFDLEVBQUUsRUFBQyxRQUFRLEVBQUMsR0FBRyxFQUFDLEVBQUMsUUFBUSxFQUFDLENBQUMsRUFBQyxFQUFDLEVBQUMsTUFBTSxFQUFDLE9BQU8sRUFBQyxPQUFPLEVBQUMsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLE9BQU8sRUFBQyxHQUFHLEVBQUMsS0FBSyxFQUFDLFlBQVksRUFBQyxRQUFRLEVBQUMsUUFBUSxFQUFDLE1BQU0sRUFBQyxNQUFNLEVBQUMsYUFBYSxFQUFDLFNBQVMsRUFBQyxRQUFRLEVBQUMsQ0FBQyxFQUFDLFFBQVEsRUFBQyxFQUFFLEVBQUMsUUFBUSxFQUFDLEdBQUcsRUFBQyxRQUFRLEVBQUMsRUFBRSxFQUFDLFVBQVUsRUFBQyxFQUFFLEVBQUMsTUFBTSxFQUFDLE9BQU8sRUFBQyxPQUFPLEVBQUMsU0FBUyxFQUFDLFNBQVMsRUFBQyxDQUFDLEVBQUMsTUFBTSxFQUFDLElBQUksRUFBQyxPQUFPLEVBQUMsUUFBUSxFQUFDLEVBQUMsUUFBUSxFQUFDLENBQUMsRUFBQyxFQUFDLEVBQUMsTUFBTSxFQUFDLE9BQU8sRUFBQyxPQUFPLEVBQUMsRUFBQyxHQUFHLEVBQUMsRUFBRSxFQUFDLEdBQUcsRUFBQyxHQUFHLEVBQUMsT0FBTyxFQUFDLEdBQUcsRUFBQyxNQUFNLEVBQUMsd0JBQXdCLEVBQUMsUUFBUSxFQUFDLEVBQUUsRUFBQyxRQUFRLEVBQUMsR0FBRyxFQUFDLFFBQVEsRUFBQyxFQUFFLEVBQUMsRUFBQyxRQUFRLEVBQUMsQ0FBQyxFQUFDLEVBQUMsRUFBQyxNQUFNLEVBQUMsT0FBTyxFQUFDLE9BQU8sRUFBQyxFQUFDLEdBQUcsRUFBQyxHQUFHLEVBQUMsR0FBRyxFQUFDLEdBQUcsRUFBQyxPQUFPLEVBQUMsR0FBRyxFQUFDLEtBQUssRUFBQyxPQUFPLEVBQUMsUUFBUSxFQUFDLFFBQVEsRUFBQyxNQUFNLEVBQUMsMkJBQTJCLEVBQUMsUUFBUSxFQUFDLEVBQUUsRUFBQyxRQUFRLEVBQUMsR0FBRyxFQUFDLFFBQVEsRUFBQyxFQUFFLEVBQUMsVUFBVSxFQUFDLEVBQUUsRUFBQyxPQUFPLEVBQUMsU0FBUyxFQUFDLE1BQU0sRUFBQyxJQUFJLEVBQUMsT0FBTyxFQUFDLFFBQVEsRUFBQyxFQUFDLFFBQVEsRUFBQyxDQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxVQUFVLEVBQUMsQ0FBQyxnQ0FBZ0MsRUFBQyxrQ0FBa0MsRUFBQyx3QkFBd0IsQ0FBQyxFQUFDLFlBQVksRUFBQyxFQUFFLEVBQUMsQ0FBQztZQU52d0MsY0FBUyxZQVlyQixDQUFBO1lBQ0QsR0FBRyxDQUFDLHdCQUF3QixFQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzVDLENBQUMsRUExRHFCLElBQUksR0FBSixTQUFJLEtBQUosU0FBSSxRQTBEekI7SUFBRCxDQUFDLEVBMURnQixJQUFJLEdBQUosT0FBSSxLQUFKLE9BQUksUUEwRHBCO0FBQUQsQ0FBQyxFQTFEYSxFQUFFLEdBQUYsVUFBRSxLQUFGLFVBQUUsUUEwRGYiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbInZhciBfX2V4dGVuZHMgPSAodGhpcyAmJiB0aGlzLl9fZXh0ZW5kcykgfHwgKGZ1bmN0aW9uICgpIHtcclxuICAgIHZhciBleHRlbmRTdGF0aWNzID0gT2JqZWN0LnNldFByb3RvdHlwZU9mIHx8XHJcbiAgICAgICAgKHsgX19wcm90b19fOiBbXSB9IGluc3RhbmNlb2YgQXJyYXkgJiYgZnVuY3Rpb24gKGQsIGIpIHsgZC5fX3Byb3RvX18gPSBiOyB9KSB8fFxyXG4gICAgICAgIGZ1bmN0aW9uIChkLCBiKSB7IGZvciAodmFyIHAgaW4gYikgaWYgKGIuaGFzT3duUHJvcGVydHkocCkpIGRbcF0gPSBiW3BdOyB9O1xyXG4gICAgcmV0dXJuIGZ1bmN0aW9uIChkLCBiKSB7XHJcbiAgICAgICAgZXh0ZW5kU3RhdGljcyhkLCBiKTtcclxuICAgICAgICBmdW5jdGlvbiBfXygpIHsgdGhpcy5jb25zdHJ1Y3RvciA9IGQ7IH1cclxuICAgICAgICBkLnByb3RvdHlwZSA9IGIgPT09IG51bGwgPyBPYmplY3QuY3JlYXRlKGIpIDogKF9fLnByb3RvdHlwZSA9IGIucHJvdG90eXBlLCBuZXcgX18oKSk7XHJcbiAgICB9O1xyXG59KSgpO1xyXG4oZnVuY3Rpb24oKXtmdW5jdGlvbiByKGUsbix0KXtmdW5jdGlvbiBvKGksZil7aWYoIW5baV0pe2lmKCFlW2ldKXt2YXIgYz1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlO2lmKCFmJiZjKXJldHVybiBjKGksITApO2lmKHUpcmV0dXJuIHUoaSwhMCk7dmFyIGE9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitpK1wiJ1wiKTt0aHJvdyBhLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsYX12YXIgcD1uW2ldPXtleHBvcnRzOnt9fTtlW2ldWzBdLmNhbGwocC5leHBvcnRzLGZ1bmN0aW9uKHIpe3ZhciBuPWVbaV1bMV1bcl07cmV0dXJuIG8obnx8cil9LHAscC5leHBvcnRzLHIsZSxuLHQpfXJldHVybiBuW2ldLmV4cG9ydHN9Zm9yKHZhciB1PVwiZnVuY3Rpb25cIj09dHlwZW9mIHJlcXVpcmUmJnJlcXVpcmUsaT0wO2k8dC5sZW5ndGg7aSsrKW8odFtpXSk7cmV0dXJuIG99cmV0dXJuIHJ9KSgpIiwiaW1wb3J0IEdhbWVDb25maWcgZnJvbSBcIi4vR2FtZUNvbmZpZ1wiO1xuaW1wb3J0IHsgRW5naW5lIH0gZnJvbSAnLi9mcmFtZXdvcmsvcnVudGltZS9lbmdpbmUnO1xuXG5cbi8qKlxuICogQGF1dGhvciBTdW5cbiAqIEB0aW1lIDIwMTktMDgtMTEgMTk6MDVcbiAqIEBwcm9qZWN0IFNGcmFtZXdvcmtfTGF5YUFpclxuICogQGRlc2NyaXB0aW9uIOa4uOaIj+WQr+WKqOWFpeWPo1xuICpcbiAqL1xuY2xhc3MgTWFpbiB7XG5cdGNvbnN0cnVjdG9yKClcblx0e1xuXHRcdEVuZ2luZS4kLnJ1bigpO1xuXHR9XG59XG4vL+a/gOa0u+WQr+WKqOexu1xubmV3IE1haW4oKTtcbiIsImltcG9ydCB7Q3VzdG9tU2NlbmV9IGZyb20gXCIuLi8uLi9mcmFtZXdvcmsvbWFuYWdlci91aS9zY2VuZS1iYXNlXCI7XG5pbXBvcnQgTHlTY2VuZSA9IEN1c3RvbVNjZW5lLkx5U2NlbmU7XG5pbXBvcnQgeyBCZ1ZpZXcgfSBmcm9tICcuLi92aWV3L2xheWVyLXZpZXcvYmctdmlldyc7XG5pbXBvcnQgeyBMb2cgfSBmcm9tICcuLi8uLi9mcmFtZXdvcmsvY29yZS9sb2cnO1xuXG4gLyoqXG4gKiBAYXV0aG9yIFN1blxuICogQHRpbWUgMjAxOS0wOC0xMSAxMToyMFxuICogQHByb2plY3QgU0ZyYW1ld29ya19MYXlhQWlyXG4gKiBAZGVzY3JpcHRpb24g5Li75Zy65pmvXG4gKlxuICovXG5leHBvcnQgY2xhc3MgTWFpblNjZW5lIGV4dGVuZHMgTHlTY2VuZSB7XG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKCk7XG4gXG4gICAgICAgIHRoaXMubmVlZExvYWRSZXNcbiAgICAgICAgICAgIC5hZGQoXCJyZXMvYmcvMTIzLnBuZ1wiLCBMYXlhLkxvYWRlci5JTUFHRSk7XG4gICAgfVxufSIsImltcG9ydCB7IFNpbmdsZXRvbiB9IGZyb20gJy4uLy4uL2ZyYW1ld29yay9jb3JlL3NpbmdsZXRvbic7XG5pbXBvcnQge0NvbmZpZ0RhdGEsIENvbmZpZ1Jlc30gZnJvbSBcIi4uLy4uL2ZyYW1ld29yay9zZXR0aW5nL2NvbmZpZ1wiO1xuaW1wb3J0IHtKc29uVGVtcGxhdGV9IGZyb20gXCIuLi8uLi9mcmFtZXdvcmsvbWFuYWdlci9qc29uL2pzb24tdGVtcGxhdGVcIjtcbmltcG9ydCB7ZW51bUpzb25EZWZpbmV9IGZyb20gXCIuLi8uLi9mcmFtZXdvcmsvc2V0dGluZy9lbnVtXCI7XG5pbXBvcnQgeyBMb2cgfSBmcm9tICcuLi8uLi9mcmFtZXdvcmsvY29yZS9sb2cnO1xuXG4vKipcbiAqIEBhdXRob3IgU3VuXG4gKiBAdGltZSAyMDE5LTEwLTE2IDIxOjI4XG4gKiBAcHJvamVjdCBTRnJhbWV3b3JrX0xheWFBaXJcbiAqIEBkZXNjcmlwdGlvbiDmiYvliqjkv67mlLnnmoTmuLjmiI/phY3nva4g77yI5LiN55u05o6l5L+u5pS5ZnJhbWV3b3JrIOS/neaMgeahhuaetueahOaVtOa0ge+8iVxuICovXG5leHBvcnQgY2xhc3MgR2FtZVNldHRpbmcgZXh0ZW5kcyBTaW5nbGV0b257XG5cbiAgICBwcml2YXRlIHN0YXRpYyBpbnN0YW5jZTogR2FtZVNldHRpbmcgPSBudWxsO1xuICAgIHB1YmxpYyBzdGF0aWMgZ2V0ICQoKTogR2FtZVNldHRpbmcge1xuICAgICAgICBpZiAoIXRoaXMuaW5zdGFuY2UpIHRoaXMuaW5zdGFuY2UgPSBuZXcgR2FtZVNldHRpbmcoKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuaW5zdGFuY2U7XG4gICAgfVxuXG4gICAgY29uc3RydWN0b3IoKVxuICAgIHtcbiAgICAgICAgc3VwZXIoKTtcbiAgICB9XG4gICAgXG4gICAgaW5pdCgpe1xuICAgICAgICAgLy/miYvliqjphY3nva5Kc29u5paH5Lu2IGpzb24g5b+F6aG75omn6KGM5ZyoQ29uZmlnUmVz5LmL5YmNXG4gICAgICAgIENvbmZpZ0RhdGEuJC5qc29uVGVtcGxhdGVMaXN0ID0gW1xuICAgICAgICAgICAgbmV3IEpzb25UZW1wbGF0ZShcInJlcy9kYXRhL0ludml0ZURhdGEuanNvblwiLCBlbnVtSnNvbkRlZmluZS5pbnZpdGUpLFxuICAgICAgICAgICAgbmV3IEpzb25UZW1wbGF0ZShcInJlcy9kYXRhL0xldmVsRGF0YS5qc29uXCIsIGVudW1Kc29uRGVmaW5lLmxldmVsKSxcbiAgICAgICAgICAgIG5ldyBKc29uVGVtcGxhdGUoXCJyZXMvZGF0YS9PZmZsaW5lRGF0YS5qc29uXCIsIGVudW1Kc29uRGVmaW5lLm9mZmxpbmUpLFxuICAgICAgICAgICAgbmV3IEpzb25UZW1wbGF0ZShcInJlcy9kYXRhL1R1cm50YWJsZURhdGEuanNvblwiLCBlbnVtSnNvbkRlZmluZS5sb3R0ZXJ5KSxcbiAgICAgICAgXTtcbiAgICAgICAgLy/miYvliqjphY3nva5sb2FkaW5n6LWE5rqQXG4gICAgICAgIENvbmZpZ1Jlcy4kLmRlZmF1bHRMb2FkUmVzXG4gICAgICAgICAgICAuYWRkKFwicmVzL2xvYWRpbmcvaW1nX2xvYWRpbmdfYmcucG5nXCIsTGF5YS5Mb2FkZXIuSU1BR0UpXG4gICAgICAgICAgICAuYWRkKFwicmVzL2xvYWRpbmcvcHJvZ3Jlc3NfbG9hZGluZy5wbmdcIixMYXlhLkxvYWRlci5JTUFHRSlcbiAgICAgICAgICAgIC5hZGQoXCJyZXMvbG9hZGluZy9pbWdfOHIucG5nXCIsTGF5YS5Mb2FkZXIuSU1BR0UpO1xuICAgICAgICAvL+aJi+WKqOmFjee9ruS4u+mhtei1hOa6kFxuICAgICAgICBDb25maWdSZXMuJC5kZWZhdWx0TWFpblJlc1xuICAgICAgICAgICAgLmFkZChcInJlcy9hdGxhcy9yZXMvbWFpbi9lZmZlY3QuYXRsYXNcIiwgTGF5YS5Mb2FkZXIuQVRMQVMpXG4gICAgICAgICAgICAuYWRkKFwicmVzL2F0bGFzL3Jlcy9jb20uYXRsYXNcIiwgTGF5YS5Mb2FkZXIuQVRMQVMpXG4gICAgICAgICAgICAuYWRkKFwicmVzL2NvbS9pbWdfbG90dGVyeV9ib3JkZXIucG5nXCIsIExheWEuTG9hZGVyLklNQUdFKVxuICAgICAgICAgICAgLmFkZChcInJlcy9jb20vaW1nX2xvdHRlcnlfY29udGVudC5wbmdcIiwgTGF5YS5Mb2FkZXIuSU1BR0UpXG4gICAgICAgICAgICAuYWRkKFwicmVzL21haW4vYmcvYmcucG5nXCIsIExheWEuTG9hZGVyLklNQUdFKVxuICAgIH1cblxufSIsImltcG9ydCB7IHVpIH0gZnJvbSAnLi4vLi4vLi4vdWkvbGF5YU1heFVJJztcbmltcG9ydCBsb3R0ZXJ5VUkgPSAgdWkudmlldy5jb20ubG90dGVyeVVJO1xuaW1wb3J0IHsgUmVzTWFuYWdlciB9IGZyb20gJy4uLy4uLy4uL2ZyYW1ld29yay9tYW5hZ2VyL3Jlcy9yZXMtbWFuYWdlcic7XG5pbXBvcnQgeyBKc29uTWFuYWdlciB9IGZyb20gJy4uLy4uLy4uL2ZyYW1ld29yay9tYW5hZ2VyL2pzb24vanNvbi1tYW5hZ2VyJztcbmltcG9ydCB7ZW51bUpzb25EZWZpbmUgfSBmcm9tICAnLi4vLi4vLi4vZnJhbWV3b3JrL3NldHRpbmcvZW51bSc7XG5pbXBvcnQgeyBVdGlsTWF0aCB9IGZyb20gJy4uLy4uLy4uL2ZyYW1ld29yay91dGlsL21hdGgnO1xuaW1wb3J0IHsgTG9nIH0gZnJvbSAnLi4vLi4vLi4vZnJhbWV3b3JrL2NvcmUvbG9nJztcbmltcG9ydCB7IFV0aWxTdHJpbmcgfSBmcm9tICcuLi8uLi8uLi9mcmFtZXdvcmsvdXRpbC9zdHJpbmcnO1xuXG4vKipcbiAqIEBhdXRob3IgU3VuXG4gKiBAdGltZSAyMDE5LTA4LTEyIDE3OjMxXG4gKiBAcHJvamVjdCBTRnJhbWV3b3JrX0xheWFBaXJcbiAqIEBkZXNjcmlwdGlvbiDovaznm5jmqKHmnb9cbiAqXG4gKi9cbmV4cG9ydCBjbGFzcyBMb3R0ZXJ5VmlldyBleHRlbmRzIGxvdHRlcnlVSSB7XG5cbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirkuLvpobXpnaLlsZ7mgKforr7nva4qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG4gIC8qKiBEZXM65YCN546HICovXG4gIHByaXZhdGUgcmV3YXJkTXVsOm51bWJlciA9IDI7XG4gIC8qKiBEZXM66L2s55uY5pWw5o2uICovXG4gIHByaXZhdGUgbG90dGVyeURhdGE6YW55ID0gbnVsbDtcblxuXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKuKAlOKAlCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8v5YiG55WM57q/Ly8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirkuLvpobXpnaLnlJ/lkb3lkajmnJ8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG5cbiAgcHJpdmF0ZSBzdGF0aWMgaW5zdGFuY2U6IExvdHRlcnlWaWV3XG5cbiAgcHVibGljIHN0YXRpYyBnZXQgJCgpOiBMb3R0ZXJ5VmlldyB7XG4gICAgICBpZiAodGhpcy5pbnN0YW5jZT09bnVsbCkgdGhpcy5pbnN0YW5jZSA9IG5ldyBMb3R0ZXJ5VmlldygpO1xuICAgICAgcmV0dXJuIHRoaXMuaW5zdGFuY2U7XG4gIH1cblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgIHN1cGVyKCk7XG4gIH1cblxuXG5cbiAgb25Bd2FrZSgpOiB2b2lkIHtcbiAgICAgIHN1cGVyLm9uQXdha2UoKTtcbiAgICAgIHRoaXMuaW5pdCgpO1xuICB9XG5cbiAgY2xvc2UoKTogdm9pZCB7XG4gICAgICBzdXBlci5jbG9zZSgpO1xuICB9XG5cbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq4oCU4oCUKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy/liIbnlYznur8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKuS4u+mhtemdouWIneWni+aVsOaNrioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cblxuICBwcml2YXRlIGluaXQoKVxuICB7XG4gICAgICB0aGlzLmxvdHRlcnlEYXRhID0gSnNvbk1hbmFnZXIuJC5nZXRUYWJsZShlbnVtSnNvbkRlZmluZS5sb3R0ZXJ5KVxuICAgICAgdGhpcy5idG5Db25maXJtLm9uKExheWEuRXZlbnQuQ0xJQ0ssdGhpcyx0aGlzLm9uQnRuU3RhcnQpO1xuICB9XG5cblxuICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirigJTigJQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL+WIhueVjOe6vy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq5Li76aG16Z2i54K55Ye75LqL5Lu2KioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxuICBvbkJ0blN0YXJ0KCkge1xuXG4gICAgICBsZXQgcmFuZG9tID0gVXRpbE1hdGgucmFuZG9tKDEsMTAwKTtcblxuICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCA2OyBpKyspIHtcbiAgICAgICAgIGlmICh0aGlzLmxvdHRlcnlEYXRhW2ldLnJhbmdlTWluPD1yYW5kb20mJnJhbmRvbTw9dGhpcy5sb3R0ZXJ5RGF0YVtpXS5yYW5nZU1heCl7XG4gICAgICAgICAgICAgdGhpcy5yZXdhcmRNdWwgPSB0aGlzLmxvdHRlcnlEYXRhW2ldLnJld2FyZDtcbiAgICAgICAgICAgICB0aGlzLm9uVHVybmluZyhpKTtcbiAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgIH1cbiAgICAgIH1cbiAgfVxuXG5cbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq4oCU4oCUKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy/liIbnlYznur8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirovaznm5jliqjnlLvmmL7npLoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxuICBwcml2YXRlIG9uVHVybmluZyhyZXdhcmQ6IG51bWJlciA9IDApIHtcblxuICAgICAgLy/lhbPpl63lhbPpl63mjInpkq7mmL7npLpcbiAgICAgIHRoaXMuYnRuQ2xvc2UudmlzaWJsZSA9IGZhbHNlO1xuICAgICAgLy/npoHnlKjovaznm5jmjInpkq5cbiAgICAgIHRoaXMuYnRuQ29uZmlybS5tb3VzZUVuYWJsZWQgPSBmYWxzZTtcbiAgICAgIC8v6L2s55uY5Yqo55S7XG4gICAgICBsZXQgYUNvdW50ID0gT2JqZWN0LmtleXModGhpcy5sb3R0ZXJ5RGF0YSkubGVuZ3RoO1xuXG4gICAgICBsZXQgY0luZGV4ID0gcmV3YXJkO1xuICAgICAgbGV0IHBlckRlZyA9IDM2MCAvIGFDb3VudDtcbiAgICAgIGxldCBjdXJEZWcgPSAoMzYwIC0gcGVyRGVnICogKGNJbmRleCAtIDEpKSArIFV0aWxNYXRoLnJhbmRSYW5nZUludCgtcGVyRGVnIC8gMiwgcGVyRGVnIC8gMik7XG5cbiAgICAgIHRoaXMuaW1nQ29udGV4dC5yb3RhdGlvbiA9IDA7XG4gICAgICBsZXQgZHN0Um90YXRpb24gPSAzNjAwICsgY3VyRGVnO1xuICAgICAgTGF5YS5Ud2Vlbi50byh0aGlzLmltZ0NvbnRleHQsIHtcbiAgICAgICAgICByb3RhdGlvbjogZHN0Um90YXRpb24sXG4gICAgICB9LCA2MDAwLCBMYXlhLkVhc2Uuc3Ryb25nT3V0LCBMYXlhLkhhbmRsZXIuY3JlYXRlKHRoaXMsICgpPT57XG5cbiAgICAgIHRoaXMuYnRuQ29uZmlybS5tb3VzZUVuYWJsZWQgPSB0cnVlO1xuICAgICAgdGhpcy5idG5DbG9zZS52aXNpYmxlID0gdHJ1ZTtcbiAgICAgIExvZy5sb2coXCLlgI3njofvvJpcIit0aGlzLnJld2FyZE11bCk7XG5cbiAgICAgIH0pLCAwLCBmYWxzZSwgZmFsc2UpO1xuICB9XG5cblxuXG5cblxufVxuIiwiaW1wb3J0IHsgdWkgfSBmcm9tICcuLi8uLi8uLi91aS9sYXlhTWF4VUknO1xuaW1wb3J0IGJnVUkgPSAgdWkudmlldy5tYWluLmJnVUk7XG5pbXBvcnQgeyBEYXRhQmFzZSB9IGZyb20gJy4uLy4uLy4uL2ZyYW1ld29yay9tYW5hZ2VyL2RhdGEvZGF0YS1iYXNlJztcblxuXG4vKipcbiAqIEBhdXRob3IgU3VuXG4gKiBAdGltZSAyMDE5LTA4LTExIDExOjIzXG4gKiBAcHJvamVjdCBTRnJhbWV3b3JrX0xheWFBaXJcbiAqIEBkZXNjcmlwdGlvbiBcbiAqXG4gKi9cbmV4cG9ydCBjbGFzcyBCZ1ZpZXcgZXh0ZW5kcyBiZ1VJe1xuXG5cblxuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKumhtemdouWxnuaAp+euoeeQhioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG5cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirigJTigJQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8v5YiG55WM57q/Ly8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirpobXpnaLnlJ/lkb3lkajmnJ8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICBwcml2YXRlIHN0YXRpYyBpbnN0YW5jZTogQmdWaWV3XG5cbiAgICBwdWJsaWMgc3RhdGljIGdldCAkKCk6IEJnVmlldyB7XG4gICAgICAgIGlmICghdGhpcy5pbnN0YW5jZSkgdGhpcy5pbnN0YW5jZSA9IG5ldyBCZ1ZpZXcoKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuaW5zdGFuY2U7XG4gICAgfVxuXG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgc3VwZXIoKTtcbiAgICB9XG5cbiAgICBvbkF3YWtlKCk6IHZvaWQge1xuICAgICAgICBzdXBlci5vbkF3YWtlKCk7XG4gICAgICAgIHRoaXMuSW5pdCgpO1xuICAgICAgICB0aGlzLnN1aXRJbml0KCk7XG5cbiAgICB9XG5cblxuICAgIC8qKlxuICAgICAqIOWIneWni+WMluS4gOasoVxuICAgICAqL1xuICAgIHB1YmxpYyBJbml0KCkge1xuXG4gICAgICAgIHRoaXMuaW5pdE9uY2UoKTtcblxuICAgICAgICAvLyAvL+aVsOaNruebkeWQrFxuICAgICAgICAvLyB0aGlzLmFkZERhdGFXYXRjaChEYXRhRGVmaW5lLlVzZXJJbmZvKTtcblxuICAgICAgICBpZiAoTGF5YS5Ccm93c2VyLm9uV2VpWGluKSB7XG4gICAgICAgICAgICB0aGlzLmluaXRMaW5rKCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDpgILphY1cbiAgICAgKi9cbiAgICBzdWl0SW5pdCgpXG4gICAge1xuICAgICAgICB0aGlzLndpZHRoID0gTGF5YS5zdGFnZS53aWR0aDtcbiAgICAgICAgdGhpcy5oZWlnaHQgPSBMYXlhLnN0YWdlLmhlaWdodDtcbiAgICB9XG5cblxuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKuKAlOKAlCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy/liIbnlYznur8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKumhtemdouWIneWni+aVsOaNrioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG4gICAgLyoqIERlczrmnoTpgKDmmK/liJ3lp4vljJbkuIDmrKEgKi9cbiAgICBwcml2YXRlIGluaXRPbmNlKClcbiAgICB7XG5cbiAgICB9XG5cblxuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKuKAlOKAlCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy/liIbnlYznur8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirlpJbpg6jov57mjqXov5vlhaXliKTmlq0qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbiAgICAvKiogRGVzOuWIpOaWrei/m+WFpei/nuaOpeS/oeaBryAqL1xuICAgIHByaXZhdGUgaW5pdExpbmsoKSB7XG5cblxuICAgIH1cblxuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKuKAlOKAlCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy/liIbnlYznur8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKumhtemdouS6i+S7tuebuOWFsyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG5cblxuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKuKAlOKAlCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy/liIbnlYznur8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq5pWw5o2u5pS55Y+Y55qE55uR5ZCsKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxuICAgIC8qKlxuICAgICAqIOWIt+aWsOaVsOaNrlxuICAgICAqL1xuICAgIHByb3RlY3RlZCBvbkRhdGEoZGF0YTogRGF0YUJhc2UpIHtcbiAgICAgICBcbiAgICB9XG5cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirigJTigJQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8t5YiG55WM57q/LS8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbn0iLCJpbXBvcnQgQnJvd3NlciA9IExheWEuQnJvd3NlcjtcbmltcG9ydCB7R2FtZVZpZXd9IGZyb20gXCIuL2dhbWUtdmlld1wiO1xuaW1wb3J0IHtFZmZlY3RWaWV3fSBmcm9tIFwiLi9lZmZlY3Qtdmlld1wiO1xuaW1wb3J0IHsgdWkgfSBmcm9tICcuLi8uLi8uLi91aS9sYXlhTWF4VUknO1xuaW1wb3J0IGQzVUkgPSAgdWkudmlldy5tYWluLmQzVUk7XG5pbXBvcnQgeyBEYXRhQmFzZSB9IGZyb20gJy4uLy4uLy4uL2ZyYW1ld29yay9tYW5hZ2VyL2RhdGEvZGF0YS1iYXNlJztcbmltcG9ydCB7IFV0aWxMb2FkM0QgfSBmcm9tICcuLi8uLi8uLi9mcmFtZXdvcmsvdXRpbC9sb2FkM2QnO1xuaW1wb3J0IHsgQ29uZmlnM0QgfSBmcm9tICcuLi8uLi8uLi9mcmFtZXdvcmsvc2V0dGluZy9jb25maWcnO1xuaW1wb3J0IHsgRXZlbnRGdW5jIH0gZnJvbSAnLi4vLi4vLi4vZnJhbWV3b3JrL21hbmFnZXIvZXZlbnQvZXZlbnQtZGF0YSc7XG5cblxuLyoqXG4gKiBAYXV0aG9yIFN1blxuICogQHRpbWUgMjAxOS0wOC0xMSAxMjowM1xuICogQHByb2plY3QgU0ZyYW1ld29ya19MYXlhQWlyXG4gKiBAZGVzY3JpcHRpb24gM0TlnLrmma/lsYJcbiAqXG4gKi9cbmV4cG9ydCBjbGFzcyBEM1ZpZXcgZXh0ZW5kcyBkM1VJe1xuXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq6aG16Z2i5bGe5oCn566h55CGKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbiAgICAvKiogRGVzOjNE5Zy65pmvICovXG4gICAgcHVibGljIHNjZW5lM0Q6TGF5YS5TY2VuZTNEO1xuXG5cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirigJTigJQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8v5YiG55WM57q/Ly8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirpobXpnaLnlJ/lkb3lkajmnJ8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICBwcml2YXRlIHN0YXRpYyBpbnN0YW5jZTogRDNWaWV3XG5cbiAgICBwdWJsaWMgc3RhdGljIGdldCAkKCk6IEQzVmlldyB7XG4gICAgICAgIGlmICghdGhpcy5pbnN0YW5jZSkgdGhpcy5pbnN0YW5jZSA9IG5ldyBEM1ZpZXcoKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuaW5zdGFuY2U7XG4gICAgfVxuXG5cblxuXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKCk7XG4gICAgfVxuXG4gICAgb25Bd2FrZSgpOiB2b2lkIHtcbiAgICAgICAgc3VwZXIub25Bd2FrZSgpO1xuICAgICAgICB0aGlzLkluaXQoKTtcbiAgICAgICAgdGhpcy5zdWl0SW5pdCgpO1xuXG4gICAgfVxuXG5cbiAgICAvKipcbiAgICAgKiDliJ3lp4vljJbkuIDmrKFcbiAgICAgKi9cbiAgICBwdWJsaWMgSW5pdCgpIHtcblxuICAgICAgICB0aGlzLmluaXRPbmNlKCk7XG5cbiAgICAgICAgLy8gLy/mlbDmja7nm5HlkKxcbiAgICAgICAgLy8gdGhpcy5hZGREYXRhV2F0Y2goRGF0YURlZmluZS5Vc2VySW5mbyk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5q+P5qyh5by55Ye65Yid5aeL5YyW5LiA5qyhXG4gICAgICovXG4gICAgcG9wdXBJbml0KCkge1xuICAgICAgICB0aGlzLmluaXRBbGwoKTtcbiAgICB9XG5cblxuICAgIC8qKlxuICAgICAqIOmAgumFjVxuICAgICAqL1xuICAgIHN1aXRJbml0KClcbiAgICB7XG4gICAgICAgIHRoaXMud2lkdGggPSBMYXlhLnN0YWdlLndpZHRoO1xuICAgICAgICB0aGlzLmhlaWdodCA9IExheWEuc3RhZ2UuaGVpZ2h0O1xuICAgIH1cblxuXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq4oCU4oCUKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL+WIhueVjOe6vy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq6aG16Z2i5Yid5aeL5pWw5o2uKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbiAgICAvKiogRGVzOuaehOmAoOaYr+WIneWni+WMluS4gOasoSAqL1xuICAgIHByaXZhdGUgaW5pdE9uY2UoKVxuICAgIHtcbiAgICAgICAgXG4gICAgfVxuXG4gICAgLyoqIERlczrmr4/mrKHlvLnlh7rliJ3lp4vljJYgKi9cbiAgICBwcml2YXRlIGluaXRBbGwoKVxuICAgIHtcblxuICAgIH1cblxuXG5cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirigJTigJQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8v5YiG55WM57q/Ly8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq5aSW6YOo6L+e5o6l6L+b5YWl5Yik5patKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG4gICAgLyoqIERlczrliKTmlq3ov5vlhaXov57mjqXkv6Hmga8gKi9cbiAgICBwcml2YXRlIGluaXRMaW5rKCkge1xuXG5cbiAgICB9XG5cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirigJTigJQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8v5YiG55WM57q/Ly8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirpobXpnaLkuovku7bnm7jlhbMqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxuXG5cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirigJTigJQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8v5YiG55WM57q/Ly8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKjNE5Zy65pmv5Yqg6L295a6M5oiQ5Zue6LCDKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxuICAgIC8qKlxuICAgICAqIOWKoOi9vTNE5Zy65pmvXG4gICAgICovXG4gICAgcHVibGljIGxvYWQzRFNjZW5lKGFyZWEsY2FsbEJhY2spXG4gICAge1xuICAgICAgICBVdGlsTG9hZDNELmxvYWRTY2VuZShDb25maWczRC4kLnNjZW5lUGF0aCxhcmVhLGNhbGxCYWNrKTtcbiAgICB9XG5cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirigJTigJQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8v5YiG55WM57q/Ly8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKuaVsOaNruaUueWPmOeahOebkeWQrCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbiAgICAvKipcbiAgICAgKiDliLfmlrDmlbDmja5cbiAgICAgKi9cbiAgICBwcm90ZWN0ZWQgb25EYXRhKGRhdGE6IERhdGFCYXNlKSB7XG4gICAgICBcbiAgICB9XG5cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirigJTigJQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8t5YiG55WM57q/LS8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbn0iLCJpbXBvcnQge3VpfSBmcm9tIFwiLi4vLi4vLi4vdWkvbGF5YU1heFVJXCI7XG5pbXBvcnQgZWZmZWN0VUkgPSAgdWkudmlldy5tYWluLmVmZmVjdFVJO1xuXG5pbXBvcnQgQnJvd3NlciA9IExheWEuQnJvd3NlcjtcbmltcG9ydCB7IERhdGFCYXNlIH0gZnJvbSAnLi4vLi4vLi4vZnJhbWV3b3JrL21hbmFnZXIvZGF0YS9kYXRhLWJhc2UnO1xuaW1wb3J0IHsgTG9nIH0gZnJvbSAnLi4vLi4vLi4vZnJhbWV3b3JrL2NvcmUvbG9nJztcbmltcG9ydCB7IExvdHRlcnlWaWV3IH0gZnJvbSAnLi4vY29tcG9uZW50LXZpZXcvbG90dGVyeS12aWV3JztcbmltcG9ydCB7IFBvcHVwRGF0YSB9IGZyb20gJy4uLy4uLy4uL2ZyYW1ld29yay9tYW5hZ2VyL3VpL2RpYWxvZy1iYXNlJztcblxuZXhwb3J0IGNsYXNzIEVmZmVjdFZpZXcgZXh0ZW5kcyBlZmZlY3RVSXtcblxuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKumhtemdouWxnuaAp+euoeeQhioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG5cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirigJTigJQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8v5YiG55WM57q/Ly8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirpobXpnaLnlJ/lkb3lkajmnJ8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICBwcml2YXRlIHN0YXRpYyBpbnN0YW5jZTogRWZmZWN0Vmlld1xuXG4gICAgcHVibGljIHN0YXRpYyBnZXQgJCgpOiBFZmZlY3RWaWV3IHtcbiAgICAgICAgaWYgKCF0aGlzLmluc3RhbmNlKSB0aGlzLmluc3RhbmNlID0gbmV3IEVmZmVjdFZpZXcoKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuaW5zdGFuY2U7XG4gICAgfVxuXG5cblxuXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKCk7XG4gICAgfVxuXG4gICAgb25Bd2FrZSgpOiB2b2lkIHtcbiAgICAgICAgc3VwZXIub25Bd2FrZSgpO1xuICAgICAgICB0aGlzLkluaXQoKTtcbiAgICAgICAgdGhpcy5zdWl0SW5pdCgpO1xuXG4gICAgfVxuXG5cbiAgICAvKipcbiAgICAgKiDliJ3lp4vljJbkuIDmrKFcbiAgICAgKi9cbiAgICBwdWJsaWMgSW5pdCgpIHtcblxuICAgICAgICB0aGlzLmluaXRPbmNlKCk7XG5cbiAgICAgICAgLy8gLy/mlbDmja7nm5HlkKxcbiAgICAgICAgLy8gdGhpcy5hZGREYXRhV2F0Y2goRGF0YURlZmluZS5Vc2VySW5mbyk7XG5cbiAgICAgICAgaWYgKEJyb3dzZXIub25XZWlYaW4pIHtcbiAgICAgICAgICAgIHRoaXMuaW5pdExpbmsoKTtcbiAgICAgICAgfVxuXG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5q+P5qyh5by55Ye65Yid5aeL5YyW5LiA5qyhXG4gICAgICovXG4gICAgcG9wdXBJbml0KCkge1xuICAgICAgICB0aGlzLmluaXRBbGwoKTtcbiAgICB9XG5cblxuICAgIC8qKlxuICAgICAqIOmAgumFjVxuICAgICAqL1xuICAgIHN1aXRJbml0KClcbiAgICB7XG4gICAgICAgIHRoaXMud2lkdGggPSBMYXlhLnN0YWdlLndpZHRoO1xuICAgICAgICB0aGlzLmhlaWdodCA9IExheWEuc3RhZ2UuaGVpZ2h0O1xuICAgIH1cblxuXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq4oCU4oCUKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL+WIhueVjOe6vy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq6aG16Z2i5Yid5aeL5pWw5o2uKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbiAgICAvKiogRGVzOuaehOmAoOaYr+WIneWni+WMluS4gOasoSAqL1xuICAgIHByaXZhdGUgaW5pdE9uY2UoKVxuICAgIHtcbiAgICAgICAgdGhpcy5idG5MdWNreS5vbihMYXlhLkV2ZW50LkNMSUNLLHRoaXMsKCk9PntcbiAgICAgICAgICAgbGV0IHZpZXcgPSBMb3R0ZXJ5Vmlldy4kO1xuICAgICAgICAgICB2aWV3LnBvcHVwRGlhbG9nKCk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIC8qKiBEZXM65q+P5qyh5by55Ye65Yid5aeL5YyWICovXG4gICAgcHJpdmF0ZSBpbml0QWxsKClcbiAgICB7XG5cbiAgICB9XG5cblxuXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq4oCU4oCUKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL+WIhueVjOe6vy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKuWklumDqOi/nuaOpei/m+WFpeWIpOaWrSoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxuICAgIC8qKiBEZXM65Yik5pat6L+b5YWl6L+e5o6l5L+h5oGvICovXG4gICAgcHJpdmF0ZSBpbml0TGluaygpIHtcblxuXG4gICAgfVxuXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq4oCU4oCUKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL+WIhueVjOe6vy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq6aG16Z2i54K55Ye75LqL5Lu2KioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cblxuXG5cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirigJTigJQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8v5YiG55WM57q/Ly8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKuaVsOaNruaUueWPmOeahOebkeWQrCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbiAgICAvKipcbiAgICAgKiDliLfmlrDmlbDmja5cbiAgICAgKi9cbiAgICBwcm90ZWN0ZWQgb25EYXRhKGRhdGE6IERhdGFCYXNlKSB7XG4gICAgIFxuICAgIH1cblxuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKuKAlOKAlCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy3liIbnlYznur8tLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xufSIsImltcG9ydCB7dWl9IGZyb20gXCIuLi8uLi8uLi91aS9sYXlhTWF4VUlcIjtcbmltcG9ydCBCcm93c2VyID0gTGF5YS5Ccm93c2VyO1xuaW1wb3J0IGdhbWVVSSA9IHVpLnZpZXcubWFpbi5nYW1lVUk7XG5pbXBvcnQgeyBEYXRhQmFzZSB9IGZyb20gJy4uLy4uLy4uL2ZyYW1ld29yay9tYW5hZ2VyL2RhdGEvZGF0YS1iYXNlJztcblxuLyoqXG4gKiBAYXV0aG9yIFN1blxuICogQHRpbWUgMjAxOS0wOC0xMSAxODowOFxuICogQHByb2plY3QgU0ZyYW1ld29ya19MYXlhQWlyXG4gKiBAZGVzY3JpcHRpb24g5Li76aG1XG4gKlxuICovXG5leHBvcnQgY2xhc3MgR2FtZVZpZXcgZXh0ZW5kcyBnYW1lVUkge1xuXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq6aG16Z2i5bGe5oCn566h55CGKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cblxuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKuKAlOKAlCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy/liIbnlYznur8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKumhtemdoueUn+WRveWRqOacnyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgIHByaXZhdGUgc3RhdGljIGluc3RhbmNlOiBHYW1lVmlldztcblxuICAgIHB1YmxpYyBzdGF0aWMgZ2V0ICQoKTogR2FtZVZpZXcge1xuICAgICAgICBpZiAoIXRoaXMuaW5zdGFuY2UpIHRoaXMuaW5zdGFuY2UgPSBuZXcgR2FtZVZpZXcoKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuaW5zdGFuY2U7XG4gICAgfVxuXG5cblxuXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKCk7XG4gICAgfVxuXG4gICAgb25Bd2FrZSgpOiB2b2lkIHtcbiAgICAgICAgc3VwZXIub25Bd2FrZSgpO1xuICAgICAgICB0aGlzLkluaXQoKTtcbiAgICAgICAgdGhpcy5zdWl0SW5pdCgpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOWIneWni+WMluS4gOasoVxuICAgICAqL1xuICAgIHB1YmxpYyBJbml0KCkge1xuXG4gICAgICAgIHRoaXMuaW5pdE9uY2UoKTtcblxuICAgICAgICAvLyAvL+aVsOaNruebkeWQrFxuICAgICAgICAvLyB0aGlzLmFkZERhdGFXYXRjaChEYXRhRGVmaW5lLlVzZXJJbmZvKTtcblxuICAgICAgICBpZiAoQnJvd3Nlci5vbldlaVhpbikge1xuICAgICAgICAgICAgdGhpcy5pbml0TGluaygpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5q+P5qyh5by55Ye65Yid5aeL5YyW5LiA5qyhXG4gICAgICovXG4gICAgcG9wdXBJbml0KCkge1xuICAgICAgICB0aGlzLmluaXRBbGwoKTtcbiAgICB9XG5cblxuICAgIC8qKlxuICAgICAqIOmAgumFjVxuICAgICAqL1xuICAgIHN1aXRJbml0KClcbiAgICB7XG4gICAgXG4gICAgfVxuXG5cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirigJTigJQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8v5YiG55WM57q/Ly8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirpobXpnaLliJ3lp4vmlbDmja4qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxuICAgIC8qKiBEZXM65p6E6YCg5piv5Yid5aeL5YyW5LiA5qyhICovXG4gICAgcHJpdmF0ZSBpbml0T25jZSgpXG4gICAge1xuICAgIH1cblxuICAgIC8qKiBEZXM65q+P5qyh5by55Ye65Yid5aeL5YyWICovXG4gICAgcHJpdmF0ZSBpbml0QWxsKClcbiAgICB7XG5cbiAgICB9XG5cblxuXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq4oCU4oCUKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL+WIhueVjOe6vy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKuWklumDqOi/nuaOpei/m+WFpeWIpOaWrSoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxuICAgIC8qKiBEZXM65Yik5pat6L+b5YWl6L+e5o6l5L+h5oGvICovXG4gICAgcHJpdmF0ZSBpbml0TGluaygpIHtcblxuXG4gICAgfVxuXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq4oCU4oCUKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL+WIhueVjOe6vy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq6aG16Z2i54K55Ye75LqL5Lu2KioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cblxuIFxuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKuKAlOKAlCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy/liIbnlYznur8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq5pWw5o2u5pS55Y+Y55qE55uR5ZCsKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxuICAgIC8qKlxuICAgICAqIOWIt+aWsOaVsOaNrlxuICAgICAqL1xuICAgIHByb3RlY3RlZCBvbkRhdGEoZGF0YTogRGF0YUJhc2UpIHtcbiAgICAgICAgXG4gICAgfVxuXG5cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirigJTigJQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8t5YiG55WM57q/LS8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cblxufVxuXG5jbGFzcyBHcmVldGVyIHtcbiAgICBncmVldGluZzogc3RyaW5nO1xuICAgIGNvbnN0cnVjdG9yKG1lc3NhZ2U6IHN0cmluZykge1xuICAgICAgICB0aGlzLmdyZWV0aW5nID0gbWVzc2FnZTtcbiAgICB9XG4gICAgZ3JlZXQoKSB7XG4gICAgICAgIHJldHVybiBcIkhlbGxvLCBcIiArIHRoaXMuZ3JlZXRpbmc7XG4gICAgfVxufVxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cbiIsImltcG9ydCB7dWl9IGZyb20gXCIuLi8uLi8uLi91aS9sYXlhTWF4VUlcIjtcbmltcG9ydCBsb2FkaW5nVUkgPSB1aS52aWV3Lm1haW4ubG9hZGluZ1VJO1xuaW1wb3J0IHsgSUxvYWluZyB9IGZyb20gJy4uLy4uLy4uL2ZyYW1ld29yay9pbnRlcmZhY2UvaS1Mb2FkaW5nJztcbmltcG9ydCB7IEJnVmlldyB9IGZyb20gJy4vYmctdmlldyc7XG5pbXBvcnQgeyBEM1ZpZXcgfSBmcm9tICcuL2QzLXZpZXcnO1xuaW1wb3J0IHsgRGF0YUJhc2UgfSBmcm9tICcuLi8uLi8uLi9mcmFtZXdvcmsvbWFuYWdlci9kYXRhL2RhdGEtYmFzZSc7XG5pbXBvcnQgeyBDb25maWdVSSwgQ29uZmlnR2FtZSwgQ29uZmlnUmVzIH0gZnJvbSAnLi4vLi4vLi4vZnJhbWV3b3JrL3NldHRpbmcvY29uZmlnJztcbmltcG9ydCB7IEV2ZW50TWFuYWdlciB9IGZyb20gJy4uLy4uLy4uL2ZyYW1ld29yay9tYW5hZ2VyL2V2ZW50L2V2ZW50LW1hbmFnZXInO1xuaW1wb3J0IHsgVXRpbE51bWJlciB9IGZyb20gJy4uLy4uLy4uL2ZyYW1ld29yay91dGlsL251bWJlcic7XG5pbXBvcnQgeyBlbnVtRGltZW5zaW9uIH0gZnJvbSAnLi4vLi4vLi4vZnJhbWV3b3JrL3NldHRpbmcvZW51bSc7XG5pbXBvcnQgeyBHYW1lVmlldyB9IGZyb20gJy4vZ2FtZS12aWV3JztcbmltcG9ydCB7IEVmZmVjdFZpZXcgfSBmcm9tICcuL2VmZmVjdC12aWV3JztcbmltcG9ydCB7IEV2ZW50RnVuYyB9IGZyb20gJy4uLy4uLy4uL2ZyYW1ld29yay9tYW5hZ2VyL2V2ZW50L2V2ZW50LWRhdGEnO1xuaW1wb3J0IHsgTG9nIH0gZnJvbSAnLi4vLi4vLi4vZnJhbWV3b3JrL2NvcmUvbG9nJztcbmltcG9ydCB7IFJlc01hbmFnZXIgfSBmcm9tICcuLi8uLi8uLi9mcmFtZXdvcmsvbWFuYWdlci9yZXMvcmVzLW1hbmFnZXInO1xuaW1wb3J0IHsgUmVzR3JvdXAgfSBmcm9tICcuLi8uLi8uLi9mcmFtZXdvcmsvbWFuYWdlci9yZXMvcmVzLWdyb3VwJztcblxuXG5cbmV4cG9ydCBjbGFzcyBMb2FkaW5nVmlldyBleHRlbmRzIGxvYWRpbmdVSSBpbXBsZW1lbnRzIElMb2Fpbmd7XG5cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirpobXpnaLlsZ7mgKfnrqHnkIYqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxuXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq4oCU4oCUKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL+WIhueVjOe6vy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq6aG16Z2i55Sf5ZG95ZGo5pyfKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgc3VwZXIoKTtcbiAgICB9XG5cbiAgICBvbkF3YWtlKCk6IHZvaWQge1xuICAgICAgICAvLyBzdXBlci5vbkF3YWtlKCk7XG4gICAgICAgIHRoaXMuSW5pdCgpO1xuICAgICAgICB0aGlzLnN1aXRJbml0KCk7XG4gICAgfVxuXG4gICAgICAvKipcbiAgICAgKiDliqDovb3pobXpnaLlkK/liqjpoblcbiAgICAgKi9cbiAgICBvblN0YXJ0KCk6IHZvaWQge1xuICAgICAgXG4gICAgICAgIC8v5Yqg6L295Li75Zy65pmv5omA6ZyA6KaB55qE6LWE5rqQ5L+h5oGvXG4gICAgICAgIFJlc01hbmFnZXIuJC5sb2FkR3JvdXAoXG4gICAgICAgICAgICBDb25maWdSZXMuJC5kZWZhdWx0TWFpblJlcyxcbiAgICAgICAgICAgIG5ldyBFdmVudEZ1bmModGhpcyx0aGlzLm9uUHJvZ3Jlc3MpLFxuICAgICAgICAgICAgbmV3IEV2ZW50RnVuYyh0aGlzLHRoaXMub25Db21wbGV0ZWQpXG4gICAgICAgICk7XG4gICAgICAgIHRoaXMubGJsTG9hZGluZy50ZXh0ID0gXCLmuLjmiI/nmbvlvZXkuK0uLi5cIjtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDliqDovb3lrozmiJDlm57osINcbiAgICAgKiBAcGFyYW0gc3VjY2Vzc1xuICAgICAqL1xuICAgIG9uQ29tcGxldGVkKHN1Y2Nlc3M6IGJvb2xlYW4pOiB2b2lkIHtcblxuICAgICAgICAvL0Jn6aG16Z2iXG4gICAgICAgIGxldCBiZ1ZpZXcgPSBCZ1ZpZXcuJDtcbiAgICAgICAgTGF5YS5zdGFnZS5hZGRDaGlsZChiZ1ZpZXcpO1xuXG4gICAgICAgIGlmKENvbmZpZ0dhbWUuJC5kaW1lbnNpb249PWVudW1EaW1lbnNpb24uRGltMylcbiAgICAgICAge1xuICAgICAgICAgICAgLy8zROmhtemdolxuICAgICAgICAgICAgbGV0IGQzVmlldyA9IEQzVmlldy4kO1xuICAgICAgICAgICAgTGF5YS5zdGFnZS5hZGRDaGlsZChkM1ZpZXcpO1xuICAgICAgICAgICAgZDNWaWV3LmxvYWQzRFNjZW5lKHRoaXMsdGhpcy5zaG93Vmlldyk7XG4gICAgICAgIH1lbHNle1xuICAgICAgICAgICAgdGhpcy5zaG93VmlldygpO1xuICAgICAgICB9XG4gICAgfVxuICAgIHByaXZhdGUgc2hvd1ZpZXcoKVxuICAgIHtcbiAgICAgICAgLy/kuLvpobVcbiAgICAgICAgbGV0IGdhbWVWaWV3ID0gR2FtZVZpZXcuJDtcbiAgICAgICAgTGF5YS5zdGFnZS5hZGRDaGlsZChnYW1lVmlldyk7XG4gICAgICAgIC8v5pWI5p6c6aG1XG4gICAgICAgIGxldCBlZmZlY3RWaWV3ID0gRWZmZWN0Vmlldy4kO1xuICAgICAgICBMYXlhLnN0YWdlLmFkZENoaWxkKGVmZmVjdFZpZXcpO1xuICAgICAgICAvL+e7k+adn+mUgOavgeWKoOi9vemhtVxuICAgICAgICB0aGlzLmRlc3Ryb3koKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDliqDovb3ov5vluqZcbiAgICAgKiBAcGFyYW0gcHJvZ3Jlc3NcbiAgICAgKi9cbiAgICBvblByb2dyZXNzKHByb2dyZXNzOiBudW1iZXIpOiB2b2lkIHtcblxuICAgICAgICBsZXQgZml4ZWQgPSBVdGlsTnVtYmVyLnRvRml4ZWQocHJvZ3Jlc3MqMTAwLCAwKTtcbiAgICAgICAgdGhpcy5sYmxMb2FkaW5nLnRleHQgPSBmaXhlZCArIFwiJVwiO1xuICAgICAgICB0aGlzLnByb19Mb2FkaW5nLnZhbHVlID0gZml4ZWQvMTAwO1xuICAgIH1cblxuICBcblxuXG4gICAgLyoqXG4gICAgICog5Yid5aeL5YyW5LiA5qyhXG4gICAgICovXG4gICAgcHVibGljIEluaXQoKSB7XG4gICAgICAgIHRoaXMuaW5pdE9uY2UoKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDmr4/mrKHlvLnlh7rliJ3lp4vljJbkuIDmrKFcbiAgICAgKi9cbiAgICBwb3B1cEluaXQoKSB7XG4gICAgICAgIHRoaXMuaW5pdEFsbCgpO1xuICAgIH1cblxuXG4gICAgLyoqXG4gICAgICog6YCC6YWNXG4gICAgICovXG4gICAgc3VpdEluaXQoKVxuICAgIHtcbiAgICAgICAgdGhpcy53aWR0aCA9IExheWEuc3RhZ2Uud2lkdGg7XG4gICAgICAgIHRoaXMuaGVpZ2h0ID0gTGF5YS5zdGFnZS5oZWlnaHQ7XG4gICAgICAgIHRoaXMuaW1nX2JnLndpZHRoID0gdGhpcy53aWR0aDtcbiAgICAgICAgdGhpcy5pbWdfYmcuaGVpZ2h0ID0gdGhpcy5oZWlnaHQ7XG4gICAgICAgIHRoaXMuaW1nX2JnLnggPSAwO1xuICAgICAgICB0aGlzLmltZ19iZy55ID0gMDtcbiAgICB9XG5cblxuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKuKAlOKAlCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy/liIbnlYznur8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKumhtemdouWIneWni+aVsOaNrioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG4gICAgLyoqIERlczrmnoTpgKDmmK/liJ3lp4vljJbkuIDmrKEgKi9cbiAgICBwcml2YXRlIGluaXRPbmNlKClcbiAgICB7XG5cbiAgICB9XG5cbiAgICAvKiogRGVzOuavj+asoeW8ueWHuuWIneWni+WMliAqL1xuICAgIHByaXZhdGUgaW5pdEFsbCgpXG4gICAge1xuXG4gICAgfVxuXG5cblxuXG5cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirigJTigJQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8v5YiG55WM57q/Ly8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq5aSW6YOo6L+e5o6l6L+b5YWl5Yik5patKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG4gICAgLyoqIERlczrliKTmlq3ov5vlhaXov57mjqXkv6Hmga8gKi9cbiAgICBwcml2YXRlIGluaXRMaW5rKCkge1xuXG5cbiAgICB9XG5cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirigJTigJQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8v5YiG55WM57q/Ly8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirpobXpnaLngrnlh7vkuovku7YqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxuXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq4oCU4oCUKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL+WIhueVjOe6vy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirmlbDmja7mlLnlj5jnmoTnm5HlkKwqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG4gICAgLyoqXG4gICAgICog5Yi35paw5pWw5o2uXG4gICAgICovXG4gICAgcHJvdGVjdGVkIG9uRGF0YShkYXRhOiBEYXRhQmFzZSkge1xuICAgICBcbiAgICB9XG5cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirigJTigJQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8t5YiG55WM57q/LS8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq6ZSA5q+B6Ieq6LqrKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG4gICAgZGVzdHJveSgpXG4gICAge1xuICAgICAgICAvLyB0aGlzLnJlbW92ZVNlbGYoKTtcbiAgICAgICAgLy8gUmVzTWFuYWdlci4kLnJlbGVhc2VHcm91cChDb25maWdSZXMuJC5kZWZhdWx0TG9hZFJlcyk7XG4gICAgfVxuXG4gICAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirigJTigJQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8t5YiG55WM57q/LS8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbn0iLCJpbXBvcnQgeyBDb25maWdEZWJ1ZyB9IGZyb20gJy4uL3NldHRpbmcvY29uZmlnJztcblxuIC8qKlxuICogQGF1dGhvciBTdW5cbiAqIEB0aW1lIDIwMTktMDgtMDkgMTU6NTlcbiAqIEBwcm9qZWN0IFNGcmFtZXdvcmtfTGF5YUFpclxuICogQGRlc2NyaXB0aW9uIOi+k+WHuuS/oeaBr+euoeeQhlxuICovXG5leHBvcnQgY2xhc3MgTG9nIHtcblxuICAgIHB1YmxpYyBzdGF0aWMgZGVidWcoLi4uYXJnczogYW55W10pIHtcbiAgICAgICAgaWYgKENvbmZpZ0RlYnVnLiQuaXNEZWJ1ZykgY29uc29sZS5kZWJ1ZyhcIltkZWJ1Z11cIiwgYXJncy50b1N0cmluZygpKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIGluZm8oLi4uYXJnczogYW55W10pIHtcbiAgICAgICAgaWYgKENvbmZpZ0RlYnVnLiQuaXNEZWJ1ZykgY29uc29sZS5pbmZvKFwiW2luZm9dXCIsIGFyZ3MudG9TdHJpbmcoKSk7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyB3YXJuKC4uLmFyZ3M6IGFueVtdKSB7XG4gICAgICAgIGlmIChDb25maWdEZWJ1Zy4kLmlzRGVidWcpIGNvbnNvbGUud2FybihcIlt3YXJuXVwiLCBhcmdzLnRvU3RyaW5nKCkpO1xuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgZXJyb3IoLi4uYXJnczogYW55W10pIHtcbiAgICAgICAgaWYgKENvbmZpZ0RlYnVnLiQuaXNEZWJ1ZykgY29uc29sZS5lcnJvcihcIltlcnJvcl1cIiwgYXJncy50b1N0cmluZygpKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIGV4Y2VwdGlvbiguLi5hcmdzOiBhbnlbXSkge1xuICAgICAgICBpZiAoQ29uZmlnRGVidWcuJC5pc0RlYnVnKSBjb25zb2xlLmV4Y2VwdGlvbihcIltleGNlXVwiLCBhcmdzLnRvU3RyaW5nKCkpO1xuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgbG9nKC4uLmFyZ3M6IGFueVtdKSB7XG4gICAgICAgIGlmIChDb25maWdEZWJ1Zy4kLmlzRGVidWcpIGNvbnNvbGUubG9nKFwiW2xvZ11cIiwgYXJncy50b1N0cmluZygpKTtcbiAgICB9XG5cblxuICAgIC8qKuaJk+WNsOiuvuWkh+S/oeaBryovXG4gICAgcHVibGljIHN0YXRpYyBwcmludERldmljZUluZm8oKSB7XG4gICAgICAgIGlmIChDb25maWdEZWJ1Zy4kLmlzRGVidWcgJiYgbmF2aWdhdG9yKSB7XG4gICAgICAgICAgICBsZXQgYWdlbnRTdHIgPSBuYXZpZ2F0b3IudXNlckFnZW50O1xuXG4gICAgICAgICAgICBsZXQgc3RhcnQgPSBhZ2VudFN0ci5pbmRleE9mKFwiKFwiKTtcbiAgICAgICAgICAgIGxldCBlbmQgPSBhZ2VudFN0ci5pbmRleE9mKFwiKVwiKTtcblxuICAgICAgICAgICAgaWYgKHN0YXJ0IDwgMCB8fCBlbmQgPCAwIHx8IGVuZCA8IHN0YXJ0KSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBsZXQgaW5mb1N0ciA9IGFnZW50U3RyLnN1YnN0cmluZyhzdGFydCArIDEsIGVuZCk7XG5cbiAgICAgICAgICAgIGxldCBkZXZpY2U6IHN0cmluZywgc3lzdGVtOiBzdHJpbmcsIHZlcnNpb246IHN0cmluZztcbiAgICAgICAgICAgIGxldCBpbmZvcyA9IGluZm9TdHIuc3BsaXQoXCI7XCIpO1xuICAgICAgICAgICAgaWYgKGluZm9zLmxlbmd0aCA9PSAzKSB7XG4gICAgICAgICAgICAgICAgLy/lpoLmnpzmmK/kuInkuKrnmoTor53vvIwg5Y+v6IO95pivYW5kcm9pZOeahO+8jCDpgqPkuYjnrKzkuInkuKrmmK/orr7lpIflj7dcbiAgICAgICAgICAgICAgICBkZXZpY2UgPSBpbmZvc1syXTtcbiAgICAgICAgICAgICAgICAvL+esrOS6jOS4quaYr+ezu+e7n+WPt+WSjOeJiOacrFxuICAgICAgICAgICAgICAgIGxldCBzeXN0ZW1faW5mbyA9IGluZm9zWzFdLnNwbGl0KFwiIFwiKTtcbiAgICAgICAgICAgICAgICBpZiAoc3lzdGVtX2luZm8ubGVuZ3RoID49IDIpIHtcbiAgICAgICAgICAgICAgICAgICAgc3lzdGVtID0gc3lzdGVtX2luZm9bMV07XG4gICAgICAgICAgICAgICAgICAgIHZlcnNpb24gPSBzeXN0ZW1faW5mb1syXTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2UgaWYgKGluZm9zLmxlbmd0aCA9PSAyKSB7XG4gICAgICAgICAgICAgICAgc3lzdGVtID0gaW5mb3NbMF07XG4gICAgICAgICAgICAgICAgZGV2aWNlID0gaW5mb3NbMF07XG4gICAgICAgICAgICAgICAgdmVyc2lvbiA9IGluZm9zWzFdO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBzeXN0ZW0gPSBuYXZpZ2F0b3IucGxhdGZvcm07XG4gICAgICAgICAgICAgICAgZGV2aWNlID0gbmF2aWdhdG9yLnBsYXRmb3JtO1xuICAgICAgICAgICAgICAgIHZlcnNpb24gPSBpbmZvU3RyO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgTG9nLmluZm8oc3lzdGVtLCBkZXZpY2UsIHZlcnNpb24pO1xuICAgICAgICB9XG4gICAgfVxuXG59XG5cbiIsImltcG9ydCB7IExvZyB9IGZyb20gJy4vbG9nJztcblxuLyoqXG4gKiBAYXV0aG9yIFN1blxuICogQHRpbWUgMjAxOS0wOC0wOSAyMzoyNVxuICogQHByb2plY3QgU0ZyYW1ld29ya19MYXlhQWlyXG4gKiBAZGVzY3JpcHRpb24gIOWvueixoeaxoFxuICpcbiAqL1xuZXhwb3J0IGNsYXNzIE9iamVjdFBvb2wge1xuICAgIFxuICAgIC8qKlxuICAgICAqIOiOt+WPluS4gOS4quWvueixoe+8jOS4jeWtmOWcqOWImeWIm+W7ulxuICAgICAqIEBwYXJhbSBjbGFzc0RlZiAg57G75ZCNXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyBnZXQoY2xhc3NEZWY6IGFueSk6IGFueSB7XG4gICAgICAgIGxldCBzaWduOiBzdHJpbmcgPSBcImRjLlwiICsgY2xhc3NEZWYubmFtZTtcbiAgICAgICAgbGV0IG9iajogYW55ID0gTGF5YS5Qb29sLmdldEl0ZW0oc2lnbik7XG4gICAgICAgIGlmICghb2JqKSB7XG4gICAgICAgICAgICBpZiAoIUxheWEuQ2xhc3NVdGlscy5nZXRSZWdDbGFzcyhzaWduKSkge1xuICAgICAgICAgICAgICAgIExvZy5kZWJ1ZyhcIltwb29sc13ms6jlhozlr7nosaHmsaA6XCIgKyBzaWduKTtcbiAgICAgICAgICAgICAgICBMYXlhLkNsYXNzVXRpbHMucmVnQ2xhc3Moc2lnbiwgY2xhc3NEZWYpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgb2JqID0gTGF5YS5DbGFzc1V0aWxzLmdldEluc3RhbmNlKHNpZ24pO1xuICAgICAgICB9XG4gICAgICAgIGlmIChvYmogJiYgb2JqW1wiaW5pdFwiXSkgb2JqLmluaXQoKTtcbiAgICAgICAgcmV0dXJuIG9iajtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDlm57mlLblr7nosaFcbiAgICAgKiBAcGFyYW0gb2JqICDlr7nosaHlrp7kvotcbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIHJlY292ZXIob2JqOiBhbnkpOiB2b2lkIHtcbiAgICAgICAgaWYgKCFvYmopIHJldHVybjtcblxuICAgICAgICBsZXQgcHJvdG86IGFueSA9IE9iamVjdC5nZXRQcm90b3R5cGVPZihvYmopO1xuICAgICAgICBsZXQgY2xheno6IGFueSA9IHByb3RvW1wiY29uc3RydWN0b3JcIl07XG4gICAgICAgIGxldCBzaWduOiBzdHJpbmcgPSBcImRjLlwiICsgY2xhenoubmFtZTtcbiAgICAgICAgb2JqLmNsb3NlKCk7XG4gICAgICAgIExheWEuUG9vbC5yZWNvdmVyKHNpZ24sIG9iaik7XG4gICAgfVxufVxuXG4vKirlr7nosaHmsaDln7rnsbsqL1xuZXhwb3J0IGludGVyZmFjZSBJUG9vbE9iamVjdCB7XG4gICAgLy8g5Yid5aeL5YyWXG4gICAgaW5pdCgpO1xuICAgIC8vIOWFs+mXrVxuICAgIGNsb3NlKCk7XG59XG4iLCJpbXBvcnQgeyBMb2cgfSBmcm9tICcuL2xvZyc7XG5cbiAvKipcbiAqIEBhdXRob3IgU3VuXG4gKiBAdGltZSAyMDE5LTA4LTA5IDE1OjU3XG4gKiBAcHJvamVjdCBTRnJhbWV3b3JrX0xheWFBaXJcbiAqIEBkZXNjcmlwdGlvbiDljZXkvovlt6XlhbfnsbtcbiAqL1xuZXhwb3J0IGNsYXNzIFNpbmdsZXRvbiB7XG5cbiAgICBwcml2YXRlIHN0YXRpYyBjbGFzc0tleXM6IEZ1bmN0aW9uW10gPSBbXTtcbiAgICBwcml2YXRlIHN0YXRpYyBjbGFzc1ZhbHVlczogYW55W10gPSBbXTtcblxuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICBsZXQgY2xheno6IGFueSA9IHRoaXNbXCJjb25zdHJ1Y3RvclwiXTtcbiAgICAgICAgaWYgKCFjbGF6eikge1xuICAgICAgICAgICAgY29uc29sZS53YXJuKFwiTm90IHN1cHBvcnQgY29uc3RydWN0b3IhXCIpO1xuICAgICAgICAgICAgTG9nLndhcm4oXCJOb3Qgc3VwcG9ydCBjb25zdHJ1Y3RvciFcIik7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgLy8g6Ziy5q2i6YeN5aSN5a6e5L6L5YyWXG4gICAgICAgIGlmIChTaW5nbGV0b24uY2xhc3NLZXlzLmluZGV4T2YoY2xhenopICE9IC0xKVxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKHRoaXMgKyBcIk9ubHkgaW5zdGFuY2Ugb25jZSFcIik7XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgU2luZ2xldG9uLmNsYXNzS2V5cy5wdXNoKGNsYXp6KTtcbiAgICAgICAgICAgIFNpbmdsZXRvbi5jbGFzc1ZhbHVlcy5wdXNoKHRoaXMpO1xuICAgICAgICB9XG4gICAgfVxuXG59XG4iLCJcbiBcbi8qKlxuICogQGF1dGhvciBTdW5cbiAqIEB0aW1lIDIwMTktMDgtMDkgMjM6MzFcbiAqIEBwcm9qZWN0IFNGcmFtZXdvcmtfTGF5YUFpclxuICogQGRlc2NyaXB0aW9uICDkuovku7bku7vliqHlsZ7mgKdcbiAqXG4gKi9cbmV4cG9ydCBjbGFzcyBUaW1lRGVsYXlEYXRhIHtcblxuICAgIC8qKumHjeWkjeasoeaVsCAqL1xuICAgIHB1YmxpYyByZXBlYXQ6IG51bWJlcjtcbiAgICAvKirpl7TpmpQgKi9cbiAgICBwdWJsaWMgaW50ZXJ2YWw6IG51bWJlcjtcbiAgICAvKirlj4LmlbAgKi9cbiAgICBwdWJsaWMgcGFyYW06IGFueTtcbiAgICAvKirlm57osIMgKi9cbiAgICBwdWJsaWMgY2FsbGJhY2s6IFRpbWVyQ2FsbGJhY2s7XG4gICAgLyoq5L2c55So5Z+fICovXG4gICAgcHVibGljIHRoaXNPYmo6IGFueTtcbiAgICAvKirmmK/lkKblt7LliKDpmaQgKi9cbiAgICBwdWJsaWMgZGVsZXRlZDogYm9vbGVhbjtcbiAgICAvKirov5DooYzkuovku7YgKi9cbiAgICBwdWJsaWMgZWxhcHNlZDogbnVtYmVyO1xuXG4gICAgcHVibGljIHNldChpbnRlcnZhbDogbnVtYmVyLCByZXBlYXQ6IG51bWJlciwgY2FsbGJhY2s6IFRpbWVyQ2FsbGJhY2ssIHRoaXNPYmo6IGFueSwgcGFyYW06IGFueSk6IHZvaWQge1xuICAgICAgICB0aGlzLmludGVydmFsID0gaW50ZXJ2YWw7XG4gICAgICAgIHRoaXMucmVwZWF0ID0gcmVwZWF0O1xuICAgICAgICB0aGlzLmNhbGxiYWNrID0gY2FsbGJhY2s7XG4gICAgICAgIHRoaXMucGFyYW0gPSBwYXJhbTtcbiAgICAgICAgdGhpcy50aGlzT2JqID0gdGhpc09iajtcbiAgICB9XG59XG5cbmV4cG9ydCB0eXBlIFRpbWVyQ2FsbGJhY2sgPSAocGFyYW06IGFueSkgPT4gdm9pZFxuXG4gLyoqXG4gKiBAYXV0aG9yIFN1blxuICogQHRpbWUgMjAxOS0wOC0wOSAyMzoyNVxuICogQHByb2plY3QgU0ZyYW1ld29ya19MYXlhQWlyXG4gKiBAZGVzY3JpcHRpb24gIOaXtumXtOaOp+WItuaguOW/g+exu1xuICpcbiAqL1xuaW1wb3J0IHtTaW5nbGV0b259IGZyb20gXCIuL3NpbmdsZXRvblwiO1xuXG5leHBvcnQgY2xhc3MgVGltZURlbGF5IGV4dGVuZHMgU2luZ2xldG9uIHtcblxuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICBzdXBlcigpO1xuICAgICAgICBMYXlhLnRpbWVyLmZyYW1lTG9vcCgwLjAxLCB0aGlzLCB0aGlzLnVwZGF0ZSk7XG4gICAgfVxuXG4gICAgLyoq5b2T5YmN5LqL5Lu25omn6KGM55qE5qyh5pWwICovXG4gICAgcHVibGljIHJlcGVhdDogbnVtYmVyID0gMFxuICAgIHByaXZhdGUgaXRlbXM6IEFycmF5PFRpbWVEZWxheURhdGE+ID0gbmV3IEFycmF5PFRpbWVEZWxheURhdGE+KCk7XG4gICAgcHJpdmF0ZSB0b0FkZDogQXJyYXk8VGltZURlbGF5RGF0YT4gPSBuZXcgQXJyYXk8VGltZURlbGF5RGF0YT4oKTtcbiAgICBwcml2YXRlIHRvUmVtb3ZlOiBBcnJheTxUaW1lRGVsYXlEYXRhPiA9IG5ldyBBcnJheTxUaW1lRGVsYXlEYXRhPigpO1xuICAgIHByaXZhdGUgcG9vbDogQXJyYXk8VGltZURlbGF5RGF0YT4gPSBuZXcgQXJyYXk8VGltZURlbGF5RGF0YT4oKTtcblxuICAgIFxuICAgIHByaXZhdGUgc3RhdGljIG1JbnN0YW5jZTogVGltZURlbGF5ID0gbnVsbDtcbiAgICBwdWJsaWMgc3RhdGljIGdldCAkKCkge1xuICAgICAgICBpZiAodGhpcy5tSW5zdGFuY2UgPT0gbnVsbCkge1xuICAgICAgICAgICAgdGhpcy5tSW5zdGFuY2UgPSBuZXcgVGltZURlbGF5KCk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMubUluc3RhbmNlXG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5LuO5rGg5a2Q5Lit6I635Y+WZGF0Yeexu1xuICAgICAqL1xuICAgIHByaXZhdGUgZ2V0RnJvbVBvb2woKTogVGltZURlbGF5RGF0YSB7XG4gICAgICAgIGxldCB0OiBUaW1lRGVsYXlEYXRhO1xuICAgICAgICBpZiAodGhpcy5wb29sLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIHQgPSB0aGlzLnBvb2wucG9wKClcbiAgICAgICAgfSBlbHNlXG4gICAgICAgICAgICB0ID0gbmV3IFRpbWVEZWxheURhdGEoKTtcbiAgICAgICAgcmV0dXJuIHQ7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogZGF0Yeexu+aUvuWbnuaxoOWtkFxuICAgICAqIEBwYXJhbSB0IFxuICAgICAqL1xuICAgIHByaXZhdGUgcmV0dXJuVG9Qb29sKHQ6IFRpbWVEZWxheURhdGEpIHtcbiAgICAgICAgdC5zZXQoMCwgMCwgbnVsbCwgbnVsbCwgbnVsbClcbiAgICAgICAgdC5lbGFwc2VkID0gMFxuICAgICAgICB0LmRlbGV0ZWQgPSBmYWxzZVxuICAgICAgICB0aGlzLnBvb2wucHVzaCh0KVxuICAgIH1cblxuICAgIHB1YmxpYyBleGlzdHMoY2FsbGJhY2s6IFRpbWVyQ2FsbGJhY2ssIHRoaXNPYmo6IGFueSkge1xuICAgICAgICBsZXQgdCA9IHRoaXMudG9BZGQuZmluZCgodmFsdWU6IFRpbWVEZWxheURhdGEsIGluZGV4OiBudW1iZXIsIG9iajogQXJyYXk8VGltZURlbGF5RGF0YT4pID0+IHtcbiAgICAgICAgICAgIHJldHVybiB2YWx1ZS5jYWxsYmFjayA9PSBjYWxsYmFjayAmJiB2YWx1ZS50aGlzT2JqID09IHRoaXNPYmpcbiAgICAgICAgfSlcblxuICAgICAgICBpZiAodCAhPSBudWxsKSB7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZVxuICAgICAgICB9XG4gICAgICAgIHQgPSB0aGlzLml0ZW1zLmZpbmQoKHZhbHVlOiBUaW1lRGVsYXlEYXRhLCBpbmRleDogbnVtYmVyLCBvYmo6IEFycmF5PFRpbWVEZWxheURhdGE+KSA9PiB7XG4gICAgICAgICAgICByZXR1cm4gdmFsdWUuY2FsbGJhY2sgPT0gY2FsbGJhY2sgJiYgdmFsdWUudGhpc09iaiA9PSB0aGlzT2JqXG4gICAgICAgIH0pXG4gICAgICAgIGlmICh0ICE9IG51bGwgJiYgIXQuZGVsZXRlZCkge1xuICAgICAgICAgICAgcmV0dXJuIHRydWVcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZmFsc2VcbiAgICB9XG5cbiAgICBwdWJsaWMgYWRkKGludGVydmFsOiBudW1iZXIsIHJlcGVhdDogbnVtYmVyLCBjYWxsYmFjazogVGltZXJDYWxsYmFjaywgdGhpc09iajogYW55LCBjYWxsYmFja1BhcmFtOiBhbnkgPSBudWxsKSB7XG4gICAgICAgIGxldCB0OiBUaW1lRGVsYXlEYXRhO1xuICAgICAgICB0ID0gdGhpcy5pdGVtcy5maW5kKCh2YWx1ZTogVGltZURlbGF5RGF0YSwgaW5kZXg6IG51bWJlciwgb2JqOiBBcnJheTxUaW1lRGVsYXlEYXRhPikgPT4ge1xuICAgICAgICAgICAgcmV0dXJuIHZhbHVlLmNhbGxiYWNrID09IGNhbGxiYWNrICYmIHZhbHVlLnRoaXNPYmogPT0gdGhpc09ialxuICAgICAgICB9KTtcblxuICAgICAgICBpZiAodCA9PSBudWxsKSB7XG4gICAgICAgICAgICB0ID0gdGhpcy50b0FkZC5maW5kKCh2YWx1ZTogVGltZURlbGF5RGF0YSwgaW5kZXg6IG51bWJlciwgb2JqOiBBcnJheTxUaW1lRGVsYXlEYXRhPikgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiB2YWx1ZS5jYWxsYmFjayA9PSBjYWxsYmFjayAmJiB2YWx1ZS50aGlzT2JqID09IHRoaXNPYmpcbiAgICAgICAgICAgIH0pXG4gICAgICAgIH1cblxuICAgICAgICBpZiAodCA9PSBudWxsKSB7XG4gICAgICAgICAgICB0ID0gdGhpcy5nZXRGcm9tUG9vbCgpO1xuICAgICAgICAgICAgdGhpcy50b0FkZC5wdXNoKHQpO1xuICAgICAgICB9XG5cbiAgICAgICAgdC5zZXQoaW50ZXJ2YWwsIHJlcGVhdCwgY2FsbGJhY2ssIHRoaXNPYmosIGNhbGxiYWNrUGFyYW0pO1xuICAgICAgICB0LmRlbGV0ZWQgPSBmYWxzZTtcbiAgICAgICAgdC5lbGFwc2VkID0gMDtcbiAgICB9XG5cbiAgICBwdWJsaWMgYWRkVXBkYXRlKGNhbGxiYWNrOiBUaW1lckNhbGxiYWNrLCB0aGlzT2JqOiBhbnksIGNhbGxiYWNrUGFyYW06IGFueSA9IG51bGwpIHtcbiAgICAgICAgdGhpcy5hZGQoMC4wMDEsIDAsIGNhbGxiYWNrLCB0aGlzT2JqLCBjYWxsYmFja1BhcmFtKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgcmVtb3ZlKGNhbGxiYWNrOiBUaW1lckNhbGxiYWNrLCB0aGlzT2JqOiBhbnkpIHtcbiAgICAgICAgbGV0IGZpbmRpbmRleCA9IC0xXG4gICAgICAgIGxldCB0ID0gdGhpcy50b0FkZC5maW5kKCh2YWx1ZTogVGltZURlbGF5RGF0YSwgaW5kZXg6IG51bWJlciwgb2JqOiBBcnJheTxUaW1lRGVsYXlEYXRhPikgPT4ge1xuICAgICAgICAgICAgaWYgKHZhbHVlLmNhbGxiYWNrID09IGNhbGxiYWNrICYmIHZhbHVlLnRoaXNPYmogPT0gdGhpc09iaikge1xuICAgICAgICAgICAgICAgIGZpbmRpbmRleCA9IGluZGV4O1xuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICBpZiAodCAhPSBudWxsKSB7XG4gICAgICAgICAgICB0aGlzLnRvQWRkLnNwbGljZShmaW5kaW5kZXgsIDEpO1xuICAgICAgICAgICAgdGhpcy5yZXR1cm5Ub1Bvb2wodCk7XG4gICAgICAgIH1cblxuICAgICAgICB0ID0gdGhpcy5pdGVtcy5maW5kKCh2YWx1ZTogVGltZURlbGF5RGF0YSwgaW5kZXg6IG51bWJlciwgb2JqOiBBcnJheTxUaW1lRGVsYXlEYXRhPikgPT4ge1xuICAgICAgICAgICAgcmV0dXJuIHZhbHVlLmNhbGxiYWNrID09IGNhbGxiYWNrICYmIHZhbHVlLnRoaXNPYmogPT0gdGhpc09iajtcbiAgICAgICAgfSk7XG4gICAgICAgIGlmICh0ICE9IG51bGwpXG4gICAgICAgICAgICB0LmRlbGV0ZWQgPSB0cnVlO1xuICAgIH1cblxuICAgIHByaXZhdGUgbGFzdFRpbWU6IG51bWJlciA9IDA7XG4gICAgcHJpdmF0ZSBkZWx0YVRpbWU6IG51bWJlciA9IDA7XG5cbiAgICBzdGFydCgpIHtcbiAgICAgICAgdGhpcy5sYXN0VGltZSA9IExheWEudGltZXIuY3VyclRpbWVyO1xuICAgIH1cblxuICAgIHVwZGF0ZSgpIHtcbiAgICAgICAgdGhpcy5kZWx0YVRpbWUgPSAoTGF5YS50aW1lci5jdXJyVGltZXIgLSB0aGlzLmxhc3RUaW1lKSAvIDEwMDA7XG4gICAgICAgIHRoaXMubGFzdFRpbWUgPSBMYXlhLnRpbWVyLmN1cnJUaW1lcjtcblxuICAgICAgICBmb3IgKGxldCBpbmRleCA9IDA7IGluZGV4IDwgdGhpcy5pdGVtcy5sZW5ndGg7IGluZGV4KyspIHtcbiAgICAgICAgICAgIGxldCB0ID0gdGhpcy5pdGVtc1tpbmRleF07XG4gICAgICAgICAgICBpZiAodC5kZWxldGVkKSB7XG4gICAgICAgICAgICAgICAgdGhpcy50b1JlbW92ZS5wdXNoKHQpO1xuICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0LmVsYXBzZWQgKz0gdGhpcy5kZWx0YVRpbWU7XG4gICAgICAgICAgICBpZiAodC5lbGFwc2VkIDwgdC5pbnRlcnZhbCkge1xuICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0LmVsYXBzZWQgPSAwO1xuXG4gICAgICAgICAgICBpZiAodC5yZXBlYXQgPiAwKSB7XG4gICAgICAgICAgICAgICAgdC5yZXBlYXQtLTtcbiAgICAgICAgICAgICAgICBpZiAodC5yZXBlYXQgPT0gMCkge1xuICAgICAgICAgICAgICAgICAgICB0LmRlbGV0ZWQgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnRvUmVtb3ZlLnB1c2godCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5yZXBlYXQgPSB0LnJlcGVhdDtcbiAgICAgICAgICAgIGlmICh0LmNhbGxiYWNrICE9IG51bGwpIHtcbiAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICB0LmNhbGxiYWNrLmNhbGwodC50aGlzT2JqLCB0LnBhcmFtKTtcbiAgICAgICAgICAgICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICAgICAgICAgICAgICB0LmRlbGV0ZWQgPSB0cnVlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBsZXQgbGVuID0gdGhpcy50b1JlbW92ZS5sZW5ndGg7XG4gICAgICAgIHdoaWxlIChsZW4pIHtcbiAgICAgICAgICAgIGxldCB0ID0gdGhpcy50b1JlbW92ZS5wb3AoKTtcbiAgICAgICAgICAgIGxldCBpbmRleCA9IHRoaXMuaXRlbXMuaW5kZXhPZih0KTtcbiAgICAgICAgICAgIGlmICh0LmRlbGV0ZWQgJiYgaW5kZXggIT0gLTEpIHtcbiAgICAgICAgICAgICAgICB0aGlzLml0ZW1zLnNwbGljZShpbmRleCwgMSk7XG4gICAgICAgICAgICAgICAgdGhpcy5yZXR1cm5Ub1Bvb2wodCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBsZW4tLTtcbiAgICAgICAgfVxuICAgICAgICBsZW4gPSB0aGlzLnRvQWRkLmxlbmd0aDtcbiAgICAgICAgd2hpbGUgKGxlbikge1xuICAgICAgICAgICAgbGV0IHQgPSB0aGlzLnRvQWRkLnBvcCgpO1xuICAgICAgICAgICAgdGhpcy5pdGVtcy5wdXNoKHQpO1xuICAgICAgICAgICAgbGVuLS07XG4gICAgICAgIH1cbiAgICB9XG59XG5cblxuXG4iLCJpbXBvcnQgeyBFdmVudE5vZGUgfSBmcm9tICcuLi9ldmVudC9ldmVudC1ub2RlJztcbmltcG9ydCB7IEV2ZW50RGF0YSB9IGZyb20gJy4uL2V2ZW50L2V2ZW50LWRhdGEnO1xuaW1wb3J0IHsgRGF0YUJhc2UgfSBmcm9tICcuL2RhdGEtYmFzZSc7XG5pbXBvcnQgeyBJTWFuYWdlciB9IGZyb20gJy4uLy4uL2ludGVyZmFjZS9pLW1hbmFnZXInO1xuXG5cbi8qKlxuICogQGF1dGhvciBTdW5cbiAqIEB0aW1lIDIwMTktMDgtMDkgMTU6NTFcbiAqIEBwcm9qZWN0IFNGcmFtZXdvcmtfTGF5YUFpclxuICogQGRlc2NyaXB0aW9uIOaVsOaNrueuoeeQhuexu1xuICovXG5leHBvcnQgY2xhc3MgRGF0YU1hbmFnZXIgZXh0ZW5kcyBFdmVudE5vZGUgaW1wbGVtZW50cyBJTWFuYWdlciB7XG5cblxuXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKCk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBzdGF0aWMgaW5zdGFuY2U6IERhdGFNYW5hZ2VyID0gbnVsbDtcblxuICAgIHB1YmxpYyBzdGF0aWMgZ2V0ICQoKTpEYXRhTWFuYWdlciB7XG4gICAgICAgIGlmICghdGhpcy5pbnN0YW5jZSkgdGhpcy5pbnN0YW5jZSA9IG5ldyBEYXRhTWFuYWdlcigpO1xuICAgICAgICByZXR1cm4gdGhpcy5pbnN0YW5jZTtcbiAgICB9XG5cbiAgICBwcm90ZWN0ZWQgZGF0YXM6IE1hcDxzdHJpbmcsIERhdGFCYXNlPiA9IG5ldyBNYXA8c3RyaW5nLCBEYXRhQmFzZT4oKTtcblxuICAgIHNldHVwKCk6IHZvaWQge1xuICAgIH1cblxuICAgIHVwZGF0ZSgpOiB2b2lkIHtcbiAgICB9XG5cbiAgICBkZXN0cm95KCk6IHZvaWQge1xuICAgICAgICB0aGlzLmRhdGFzLmNsZWFyKCk7XG4gICAgfVxuICBcblxuICAgIHB1YmxpYyByZWdpc3RlcihkYXRhOiBEYXRhQmFzZSk6IERhdGFNYW5hZ2VyIHtcbiAgICAgICAgdGhpcy5kYXRhcy5zZXQoZGF0YS5jbWQsIGRhdGEpO1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9XG5cbiAgICBwdWJsaWMgZ2V0KGNtZDogc3RyaW5nKTogRGF0YUJhc2Uge1xuICAgICAgICByZXR1cm4gdGhpcy5kYXRhcy5nZXQoY21kKTtcbiAgICB9XG59XG5cblxuXG4iLCJcblxuIC8qKlxuICogQGF1dGhvciBTdW5cbiAqIEB0aW1lIDIwMTktMDgtMTIgMTc6MTNcbiAqIEBwcm9qZWN0IFNGcmFtZXdvcmtfTGF5YUFpclxuICogQGRlc2NyaXB0aW9uIOS6i+S7tuaVsOaNruWumuS5ieexu1xuICovXG5leHBvcnQgY2xhc3MgRXZlbnREYXRhIHtcblxuICAgIGNvbnN0cnVjdG9yKGNtZDogc3RyaW5nLCBvYmo6IGFueSA9IG51bGwsIGlzU3RvcDogYm9vbGVhbiA9IGZhbHNlKSB7XG4gICAgICAgIHRoaXMuY21kID0gY21kO1xuICAgICAgICB0aGlzLmRhdGEgPSBvYmo7XG4gICAgICAgIHRoaXMuaXNTdG9wID0gZmFsc2U7XG4gICAgfVxuXG4gICAgcHVibGljIGNtZDogc3RyaW5nO1xuICAgIHB1YmxpYyBkYXRhOiBhbnk7XG4gICAgcHVibGljIGlzU3RvcCA9IGZhbHNlO1xuXG4gICAgLyoqXG4gICAgICog5b+r6YCf5Yib5bu65LqL5Lu25pWw5o2uXG4gICAgICogQHBhcmFtIGNtZFxuICAgICAqIEBwYXJhbSBkYXRhXG4gICAgICogQHBhcmFtIGlzU3RvcFxuICAgICAqL1xuICAgIHB1YmxpYyBzdGF0aWMgY3JlYXRlKGNtZDogc3RyaW5nLCBkYXRhOiBhbnkgPSBudWxsLCBpc1N0b3A6IGJvb2xlYW4gPSBmYWxzZSk6IEV2ZW50RGF0YSB7XG4gICAgICAgIHJldHVybiBuZXcgRXZlbnREYXRhKGNtZCwgZGF0YSwgaXNTdG9wKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgc3RvcCgpIHtcbiAgICAgICAgdGhpcy5pc1N0b3AgPSB0cnVlXG4gICAgfVxufVxuXG5cbiAvKipcbiAqIEBhdXRob3IgU3VuXG4gKiBAdGltZSAyMDE5LTAxLTIwIDAwOjI0XG4gKiBAcHJvamVjdCBTRnJhbWV3b3JrX0xheWFBaXJcbiAqIEBkZXNjcmlwdGlvbiDkuovku7blm57osIPlh73mlbDlrprkuYlcbiAqL1xuZXhwb3J0IGNsYXNzIEV2ZW50RnVuYyB7XG5cbiAgICBwcml2YXRlIG1fdGhpczogYW55O1xuICAgIHByaXZhdGUgbV9jYjogRnVuY3Rpb247XG5cbiAgICBwdWJsaWMgY29uc3RydWN0b3IodGhpc09iajogYW55LCBjYWxsQmFjazogRnVuY3Rpb24pIHtcbiAgICAgICAgdGhpcy5tX3RoaXMgPSB0aGlzT2JqO1xuICAgICAgICB0aGlzLm1fY2IgPSBjYWxsQmFjaztcbiAgICB9XG5cbiAgICBwdWJsaWMgaW52b2tlKC4uLmFyZ3M6IGFueVtdKSB7XG4gICAgICAgIHRoaXMubV9jYi5jYWxsKHRoaXMubV90aGlzLCAuLi5hcmdzKTtcbiAgICB9XG59XG5cblxuIiwiaW1wb3J0IHsgRXZlbnROb2RlLCBFdmVudENvbnRleHQsIEV2ZW50Q2FsbGJhY2tMaXN0ZW5lciB9IGZyb20gJy4vZXZlbnQtbm9kZSc7XG5pbXBvcnQgeyBFdmVudERhdGEgfSBmcm9tICcuL2V2ZW50LWRhdGEnO1xuaW1wb3J0IHsgSU1hbmFnZXIgfSBmcm9tICcuLi8uLi9pbnRlcmZhY2UvaS1tYW5hZ2VyJztcblxuXG4gLyoqXG4gKiBAYXV0aG9yIFN1blxuICogQHRpbWUgMjAxOS0wMS0xOCAxNjoyMFxuICogQHByb2plY3QgU0ZyYW1ld29ya19MYXlhQWlyXG4gKiBAZGVzY3JpcHRpb24g5LqL5Lu2566h55CG5ZmoXG4gKi9cbmV4cG9ydCBjbGFzcyBFdmVudE1hbmFnZXIgZXh0ZW5kcyBFdmVudE5vZGUgaW1wbGVtZW50cyBJTWFuYWdlciB7XG5cblxuICAgIHByaXZhdGUgc3RhdGljIGluc3RhbmNlOiBFdmVudE1hbmFnZXIgPSBudWxsO1xuICAgIHB1YmxpYyBzdGF0aWMgZ2V0ICQoKTpFdmVudE1hbmFnZXIge1xuICAgICAgICBpZiAoIXRoaXMuaW5zdGFuY2UpIHRoaXMuaW5zdGFuY2UgPSBuZXcgRXZlbnRNYW5hZ2VyKCk7XG4gICAgICAgIHJldHVybiB0aGlzLmluc3RhbmNlO1xuICAgIH1cbiBcbiAgICBwdWJsaWMgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKCk7XG4gICAgfVxuXG4gIFxuXG4gICAgc2V0dXAoKTogdm9pZCB7XG4gICAgICAgIEV2ZW50Q29udGV4dC5ldmVudE5vZGVzLmNsZWFyKCk7XG4gICAgfVxuXG4gICAgdXBkYXRlKCk6IHZvaWQge1xuICAgIH1cblxuICAgIGRlc3Ryb3koKTogdm9pZCB7XG4gICAgICAgIEV2ZW50Q29udGV4dC5ldmVudE5vZGVzLmNsZWFyKCk7XG4gICAgfVxuICAgIFxuICAgIC8qKlxuICAgICAqIOenu+mZpOS4gOS4qua2iOaBr+ebkeWQrOiKgueCuVxuICAgICAqIEBwYXJhbSBub2RlXG4gICAgICovXG4gICAgcmVtb3ZlKG5vZGU6IEV2ZW50Tm9kZSk6IHZvaWQge1xuICAgICAgICBub2RlLnJlbW92ZUV2ZW50TGlzdGVuZXJBbGwoKTtcbiAgICAgICAgRXZlbnRDb250ZXh0LmV2ZW50Tm9kZXMuZGVsZXRlKG5vZGUpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOe7meaJgOacieacrOWcsOa2iOaBr+iKgueCuemAmuefpea2iOaBr1xuICAgICAqIEBwYXJhbSBlZFxuICAgICAqL1xuICAgIGRpc3BhdGNoRXZlbnRMb2NhbEFsbChlZDogRXZlbnREYXRhKSB7XG4gICAgICAgIEV2ZW50Q29udGV4dC5ldmVudE5vZGVzLmZvckVhY2goKGVuOiBFdmVudE5vZGUpID0+IHtcbiAgICAgICAgICAgIGVuLmRpc3BhdGNoRXZlbnQoZWQpO1xuICAgICAgICB9KVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOe7meaJgOacieacrOWcsOa2iOaBr+iKgueCuemAmuefpea2iOaBr1xuICAgICAqIEBwYXJhbSBjbWRcbiAgICAgKiBAcGFyYW0gZGF0YVxuICAgICAqL1xuICAgIGRpc3BhdGNoRXZlbnRMb2NhbEFsbEJ5Q21kKGNtZDogc3RyaW5nIHwgbnVtYmVyLCBkYXRhOiBhbnkgPSBudWxsKSB7XG4gICAgICAgIEV2ZW50Q29udGV4dC5ldmVudE5vZGVzLmZvckVhY2goKGVuOiBFdmVudE5vZGUpID0+IHtcbiAgICAgICAgICAgIGVuLmRpc3BhdGNoRXZlbnRCeUNtZChjbWQsIGRhdGEpO1xuICAgICAgICB9KVxuICAgIH1cblxuXG4gICAgLyoqXG4gICAgICog5re75Yqg5LiA5Liq5raI5oGv55uR5ZCs5ZmoXG4gICAgICogQHBhcmFtIHR5cGUg5raI5oGv57G75Z6LXG4gICAgICogQHBhcmFtIGNhbGxCYWNrIOWbnuiwg+WHveaVsFxuICAgICAqIEBwYXJhbSB0YXJnZXQg5L2c55So5a+56LGhXG4gICAgICogQHBhcmFtIHByaW9yaXR5IOa2iOaBr+eahOS8mOWFiOe6p1xuICAgICAqIEBwYXJhbSBvbmNlIOaYr+WQpuWPquebkeWQrOS4gOasoVxuICAgICAqL1xuICAgIHB1YmxpYyBhZGRMaXN0ZW5lcih0eXBlOiBzdHJpbmcgfCBudW1iZXIsIGNhbGxCYWNrOiBFdmVudENhbGxiYWNrTGlzdGVuZXIsIHRhcmdldDogYW55LCBwcmlvcml0eTogbnVtYmVyID0gMCwgb25jZTogYm9vbGVhbiA9IGZhbHNlKSB7XG4gICAgICAgIEV2ZW50Tm9kZS5hZGRHbG9iYWxMaXN0ZW5lcih0eXBlLGNhbGxCYWNrLHRhcmdldCxwcmlvcml0eSxvbmNlKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDnp7vpmaTkuIDkuKrmtojmga/nm5HlkKzlmahcbiAgICAgKiBAcGFyYW0gdHlwZSDmtojmga9pZFxuICAgICAqIEBwYXJhbSBjYWxsQmFjayDlm57osIPlh73mlbBcbiAgICAgKiBAcGFyYW0gdGFyZ2V0IOS9nOeUqOeahOWvueixoVxuICAgICAqL1xuICAgIHB1YmxpYyByZW1vdmVMaXN0ZW5lcih0eXBlOiBzdHJpbmcgfCBudW1iZXIsIGNhbGxCYWNrOiBFdmVudENhbGxiYWNrTGlzdGVuZXIsIHRhcmdldDogYW55KSB7XG4gICAgICAgIEV2ZW50Tm9kZS5yZW1vdmVHbG9iYWxMaXN0ZW5lcih0eXBlLGNhbGxCYWNrLHRhcmdldCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5piv5ZCm5a2Y5Zyo6L+Z5Liq55uR5ZCs5raI5oGvXG4gICAgICogQHBhcmFtIHR5cGUg5raI5oGv57G75Z6LXG4gICAgICogQHBhcmFtIGNhbGxCYWNrIOWbnuiwg+exu+Wei1xuICAgICAqIEBwYXJhbSB0YXJnZXQg5Zue6LCD5a+56LGhXG4gICAgICovXG4gICAgcHVibGljIGhhc0xpc3RlbmVyKHR5cGU6IHN0cmluZyB8IG51bWJlciwgY2FsbEJhY2s6IEV2ZW50Q2FsbGJhY2tMaXN0ZW5lciwgdGFyZ2V0OiBhbnkpIHtcbiAgICAgICAgRXZlbnROb2RlLmhhc0dsb2JhbExpc3RlbmVyKHR5cGUsY2FsbEJhY2ssdGFyZ2V0KTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDmtL7lj5Hmtojmga9cbiAgICAgKiBAcGFyYW0gY21kIOa2iOaBr2lkXG4gICAgICogQHBhcmFtIGRhdGEg5raI5oGv5YaF5a65XG4gICAgICovXG4gICAgcHVibGljIGRpc3BhdGNoRXZlbnRCeUNtZChjbWQ6IHN0cmluZyB8IG51bWJlciwgZGF0YTogYW55ID0gbnVsbCkge1xuICAgICAgICBFdmVudE5vZGUuZGlzcGF0Y2hHbG9iYWxCeUNtZChjbWQsZGF0YSk7XG4gICAgfVxuXG59XG4iLCJpbXBvcnQgeyBFdmVudERhdGEgfSBmcm9tICcuL2V2ZW50LWRhdGEnO1xuaW1wb3J0IHsgTG9nIH0gZnJvbSAnLi4vLi4vY29yZS9sb2cnO1xuaW1wb3J0IHsgU2luZ2xldG9uIH0gZnJvbSAnLi4vLi4vY29yZS9zaW5nbGV0b24nO1xuXG5cbiAvKipcbiAqIEBhdXRob3IgU3VuXG4gKiBAdGltZSAyMDE5LTAxLTE4IDE2OjIwXG4gKiBAcHJvamVjdCBTRnJhbWV3b3JrX0xheWFBaXJcbiAqIEBkZXNjcmlwdGlvbiDmiYDmnInpnIDopoHnm5Hmjqfkuovku7boioLngrnnmoTln7rnsbtcbiAqL1xuZXhwb3J0IGNsYXNzIEV2ZW50Tm9kZSBleHRlbmRzIFNpbmdsZXRvbiB7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgc3VwZXIoKTtcbiAgICAgICAgRXZlbnRDb250ZXh0LmV2ZW50Tm9kZXMuc2V0KHRoaXMsIHRoaXMpO1xuICAgIH1cblxuICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gICAgLy8gPT09PT09PT09PT09PT0gIExvY2FsIEV2ZW50IFNlY3Rpb24gPT09PT09PT09PT09PT1cbiAgICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG4gICAgcHJpdmF0ZSBzdGF0aWMgbV9nbG9iYWxFdmVudERhdGE6IEFycmF5PEV2ZW50RGF0YT4gPSBuZXcgQXJyYXk8RXZlbnREYXRhPigpO1xuICAgIHByaXZhdGUgc3RhdGljIG1fZ2xvYmFsRXZlbnREaWN0OiBFdmVudExpc3RlbmVyQ2xhc3NEaWN0ID0ge307XG5cbiAgICBwcml2YXRlIHN0YXRpYyBjcmVhdGVHbG9iYWxEYXRhKGNtZCwgZGF0YSk6IEV2ZW50RGF0YSB7XG4gICAgICAgIGxldCBlZDogRXZlbnREYXRhO1xuICAgICAgICBpZiAoRXZlbnROb2RlLm1fZ2xvYmFsRXZlbnREYXRhLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIGVkID0gRXZlbnROb2RlLm1fZ2xvYmFsRXZlbnREYXRhLnBvcCgpO1xuICAgICAgICAgICAgZWQuY21kID0gY21kO1xuICAgICAgICAgICAgZWQuZGF0YSA9IGRhdGE7XG4gICAgICAgICAgICBlZC5pc1N0b3AgPSBmYWxzZTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGVkID0gbmV3IEV2ZW50RGF0YShjbWQsIGRhdGEpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBlZDtcbiAgICB9XG5cbiAgICBwcml2YXRlIHN0YXRpYyByZXR1cm5HbG9iYWxFdmVudERhdGEoZWQ6IEV2ZW50RGF0YSkge1xuICAgICAgICBlZC5kYXRhID0gbnVsbDtcbiAgICAgICAgZWQuY21kID0gbnVsbDtcbiAgICAgICAgZWQuaXNTdG9wID0gZmFsc2U7XG4gICAgICAgIEV2ZW50Tm9kZS5tX2dsb2JhbEV2ZW50RGF0YS5wdXNoKGVkKVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOa3u+WKoOS4gOS4qua2iOaBr+ebkeWQrOWZqFxuICAgICAqIEBwYXJhbSB0eXBlIOa2iOaBr+exu+Wei1xuICAgICAqIEBwYXJhbSBjYWxsQmFjayDlm57osIPlh73mlbBcbiAgICAgKiBAcGFyYW0gdGFyZ2V0IOS9nOeUqOWvueixoVxuICAgICAqIEBwYXJhbSBwcmlvcml0eSDmtojmga/nmoTkvJjlhYjnuqdcbiAgICAgKiBAcGFyYW0gb25jZSDmmK/lkKblj6rnm5HlkKzkuIDmrKFcbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIGFkZEdsb2JhbExpc3RlbmVyKHR5cGU6IHN0cmluZyB8IG51bWJlciwgY2FsbEJhY2s6IEV2ZW50Q2FsbGJhY2tMaXN0ZW5lciwgdGFyZ2V0OiBhbnksIHByaW9yaXR5OiBudW1iZXIgPSAwLCBvbmNlOiBib29sZWFuID0gZmFsc2UpIHtcbiAgICAgICAgdHlwZSA9IHR5cGUudG9TdHJpbmcoKTtcbiAgICAgICAgbGV0IGluZm86IEV2ZW50TGlzdGVuZXJJbmZvRGF0YSA9IHtcbiAgICAgICAgICAgIHR5cGU6IHR5cGUsXG4gICAgICAgICAgICBjYWxsQmFjazogY2FsbEJhY2ssXG4gICAgICAgICAgICB0YXJnZXQ6IHRhcmdldCxcbiAgICAgICAgICAgIHByaW9yaXR5OiBwcmlvcml0eSxcbiAgICAgICAgICAgIG9uY2U6IG9uY2VcbiAgICAgICAgfTtcblxuICAgICAgICBsZXQgYXJyYXkgPSBFdmVudE5vZGUubV9nbG9iYWxFdmVudERpY3RbdHlwZV07XG4gICAgICAgIGxldCBoYXMgPSBmYWxzZTtcbiAgICAgICAgbGV0IHBvcyA9IDA7XG4gICAgICAgIGlmIChhcnJheSAhPSBudWxsKSB7XG4gICAgICAgICAgICBhcnJheS5mb3JFYWNoKGVsZW1lbnQgPT4ge1xuICAgICAgICAgICAgICAgIGlmIChlbGVtZW50LnRhcmdldCA9PSB0YXJnZXQgJiYgZWxlbWVudC5jYWxsQmFjayA9PSBjYWxsQmFjaykge1xuICAgICAgICAgICAgICAgICAgICBoYXMgPSB0cnVlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZiAoZWxlbWVudC5wcmlvcml0eSA+IGluZm8ucHJpb3JpdHkpIHtcbiAgICAgICAgICAgICAgICAgICAgcG9zKys7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBhcnJheSA9IG5ldyBBcnJheTxFdmVudExpc3RlbmVySW5mb0RhdGE+KCk7XG4gICAgICAgICAgICBFdmVudE5vZGUubV9nbG9iYWxFdmVudERpY3RbdHlwZV0gPSBhcnJheTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoaGFzKSB7XG4gICAgICAgICAgICAvLyBjb25zb2xlLmVycm9yKFwi6YeN5aSN5rOo5YaM5raI5oGv77yadHlwZT1cIiArIHR5cGUpO1xuICAgICAgICAgICAgTG9nLmVycm9yKFwi6YeN5aSN5rOo5YaM5raI5oGv77yadHlwZT1cIiArIHR5cGUpXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBhcnJheS5zcGxpY2UocG9zLCAwLCBpbmZvKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOenu+mZpOS4gOS4qua2iOaBr+ebkeWQrOWZqFxuICAgICAqIEBwYXJhbSB0eXBlIOa2iOaBr2lkXG4gICAgICogQHBhcmFtIGNhbGxCYWNrIOWbnuiwg+WHveaVsFxuICAgICAqIEBwYXJhbSB0YXJnZXQg5L2c55So55qE5a+56LGhXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyByZW1vdmVHbG9iYWxMaXN0ZW5lcih0eXBlOiBzdHJpbmcgfCBudW1iZXIsIGNhbGxCYWNrOiBFdmVudENhbGxiYWNrTGlzdGVuZXIsIHRhcmdldDogYW55KSB7XG4gICAgICAgIHR5cGUgPSB0eXBlLnRvU3RyaW5nKCk7XG4gICAgICAgIGxldCBpbmZvOiBFdmVudExpc3RlbmVySW5mb0RhdGEgPSBudWxsO1xuICAgICAgICBsZXQgYXJyYXkgPSBFdmVudE5vZGUubV9nbG9iYWxFdmVudERpY3RbdHlwZV07XG4gICAgICAgIGlmIChhcnJheSAhPSBudWxsKSB7XG4gICAgICAgICAgICBsZXQgaW5mb0luZGV4ID0gLTE7XG4gICAgICAgICAgICBhcnJheS5ldmVyeSgodmFsdWU6IEV2ZW50TGlzdGVuZXJJbmZvRGF0YSwgaW5kZXg6IG51bWJlciwgYXJyYXk6IEV2ZW50TGlzdGVuZXJJbmZvRGF0YVtdKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKHZhbHVlLnRhcmdldCA9PSB0YXJnZXQgJiYgdmFsdWUuY2FsbEJhY2sgPT0gY2FsbEJhY2spIHtcbiAgICAgICAgICAgICAgICAgICAgaW5mb0luZGV4ID0gaW5kZXg7XG4gICAgICAgICAgICAgICAgICAgIGluZm8gPSB2YWx1ZTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBpZiAoaW5mb0luZGV4ICE9IC0xKSB7XG4gICAgICAgICAgICAgICAgYXJyYXkuc3BsaWNlKGluZm9JbmRleCwgMSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDmmK/lkKblrZjlnKjov5nkuKrnm5HlkKzmtojmga9cbiAgICAgKiBAcGFyYW0gdHlwZSDmtojmga/nsbvlnotcbiAgICAgKiBAcGFyYW0gY2FsbEJhY2sg5Zue6LCD57G75Z6LXG4gICAgICogQHBhcmFtIHRhcmdldCDlm57osIPlr7nosaFcbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIGhhc0dsb2JhbExpc3RlbmVyKHR5cGU6IHN0cmluZyB8IG51bWJlciwgY2FsbEJhY2s6IEV2ZW50Q2FsbGJhY2tMaXN0ZW5lciwgdGFyZ2V0OiBhbnkpIHtcbiAgICAgICAgbGV0IGZsYWcgPSBmYWxzZTtcbiAgICAgICAgbGV0IGFycmF5ID0gRXZlbnROb2RlLm1fZ2xvYmFsRXZlbnREaWN0W3R5cGVdO1xuICAgICAgICBpZiAoYXJyYXkpIHtcbiAgICAgICAgICAgIC8vIEB0cy1pZ25vcmVcbiAgICAgICAgICAgIGxldCBpbmRleCA9IGFycmF5LmZpbmRJbmRleCgob2JqLCBpbmRleCwgYW55KSA9PiB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIG9iai50YXJnZXQgPT0gdGFyZ2V0ICYmIG9iai5jYWxsQmFjayA9PSBjYWxsQmFjaztcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgZmxhZyA9IGluZGV4ICE9IC0xO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmbGFnO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOa0vuWPkea2iOaBr1xuICAgICAqIEBwYXJhbSBlZCDmtL7lj5HnmoTmtojmga/lhoXlrrlcbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIGRpc3BhdGNoR2xvYmFsKGVkOiBFdmVudERhdGEpIHtcbiAgICAgICAgRXZlbnROb2RlLl9kaXNwYXRjaEdsb2JhbChlZCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5rS+5Y+R5raI5oGvXG4gICAgICogQHBhcmFtIGNtZCDmtojmga9pZFxuICAgICAqIEBwYXJhbSBkYXRhIOa2iOaBr+WGheWuuVxuICAgICAqL1xuICAgIHB1YmxpYyBzdGF0aWMgZGlzcGF0Y2hHbG9iYWxCeUNtZChjbWQ6IHN0cmluZyB8IG51bWJlciwgZGF0YTogYW55ID0gbnVsbCkge1xuICAgICAgICBsZXQgZWQgPSBFdmVudE5vZGUuY3JlYXRlR2xvYmFsRGF0YShjbWQsIGRhdGEpO1xuICAgICAgICBFdmVudE5vZGUuX2Rpc3BhdGNoR2xvYmFsKGVkKTtcbiAgICAgICAgaWYgKGVkICE9IG51bGwpIHtcbiAgICAgICAgICAgIEV2ZW50Tm9kZS5yZXR1cm5HbG9iYWxFdmVudERhdGEoZWQpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBzdGF0aWMgX2Rpc3BhdGNoR2xvYmFsKGVkOiBFdmVudERhdGEpIHtcbiAgICAgICAgbGV0IGFycmF5ID0gRXZlbnROb2RlLm1fZ2xvYmFsRXZlbnREaWN0W2VkLmNtZF07XG4gICAgICAgIGlmIChhcnJheSAhPSBudWxsKSB7XG5cbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgYXJyYXkubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICBsZXQgaW5mbyA9IGFycmF5W2ldO1xuICAgICAgICAgICAgICAgIGlmIChpbmZvLmNhbGxCYWNrICE9IG51bGwpIHtcbiAgICAgICAgICAgICAgICAgICAgaW5mby5jYWxsQmFjay5jYWxsKGluZm8udGFyZ2V0LCBlZCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmIChpbmZvLm9uY2UpIHtcbiAgICAgICAgICAgICAgICAgICAgYXJyYXkuc3BsaWNlKGktLSwgMSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmIChlZC5pc1N0b3ApIHtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAgICAvLyA9PT09PT09PT09PT09PSAgTG9jYWwgRXZlbnQgU2VjdGlvbiA9PT09PT09PT09PT09PVxuICAgIC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgICBwcml2YXRlIG1fZXZlbnREYXRhOiBBcnJheTxFdmVudERhdGE+ID0gbmV3IEFycmF5PEV2ZW50RGF0YT4oKTtcbiAgICBwcml2YXRlIG1fZXZlbnREaWN0OiBFdmVudExpc3RlbmVyQ2xhc3NEaWN0ID0ge307XG5cbiAgICBwcml2YXRlIGNyZWF0ZUV2ZW50RGF0YShjbWQsIGRhdGEpOiBFdmVudERhdGEge1xuICAgICAgICBsZXQgZWQ6IEV2ZW50RGF0YTtcbiAgICAgICAgaWYgKHRoaXMubV9ldmVudERhdGEubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgZWQgPSB0aGlzLm1fZXZlbnREYXRhLnBvcCgpO1xuICAgICAgICAgICAgZWQuY21kID0gY21kO1xuICAgICAgICAgICAgZWQuZGF0YSA9IGRhdGE7XG4gICAgICAgICAgICBlZC5pc1N0b3AgPSBmYWxzZTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGVkID0gbmV3IEV2ZW50RGF0YShjbWQsIGRhdGEpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBlZDtcbiAgICB9XG5cbiAgICBwcml2YXRlIHJldHVybkV2ZW50RGF0YShlZDogRXZlbnREYXRhKSB7XG4gICAgICAgIGVkLmRhdGEgPSBudWxsO1xuICAgICAgICBlZC5jbWQgPSBudWxsO1xuICAgICAgICBlZC5pc1N0b3AgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5tX2V2ZW50RGF0YS5wdXNoKGVkKVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOa3u+WKoOS4gOS4qua2iOaBr+ebkeWQrOWZqFxuICAgICAqIEBwYXJhbSB0eXBlIOa2iOaBr+exu+Wei1xuICAgICAqIEBwYXJhbSBjYWxsQmFjayDlm57osIPlh73mlbBcbiAgICAgKiBAcGFyYW0gdGFyZ2V0IOS9nOeUqOWvueixoVxuICAgICAqIEBwYXJhbSBwcmlvcml0eSDmtojmga/nmoTkvJjlhYjnuqdcbiAgICAgKiBAcGFyYW0gb25jZSDmmK/lkKblj6rnm5HlkKzkuIDmrKFcbiAgICAgKi9cbiAgICBwdWJsaWMgYWRkRXZlbnRMaXN0ZW5lcih0eXBlOiBzdHJpbmcgfCBudW1iZXIsIGNhbGxCYWNrOiBFdmVudENhbGxiYWNrTGlzdGVuZXIsIHRhcmdldDogYW55LCBwcmlvcml0eTogbnVtYmVyID0gMCwgb25jZTogYm9vbGVhbiA9IGZhbHNlKTpFdmVudExpc3RlbmVySW5mb0RhdGEgICB7XG4gICAgICAgIHR5cGUgPSB0eXBlLnRvU3RyaW5nKCk7XG4gICAgICAgIGxldCBpbmZvOiBFdmVudExpc3RlbmVySW5mb0RhdGEgPSB7XG4gICAgICAgICAgICB0eXBlOiB0eXBlLFxuICAgICAgICAgICAgY2FsbEJhY2s6IGNhbGxCYWNrLFxuICAgICAgICAgICAgdGFyZ2V0OiB0YXJnZXQsXG4gICAgICAgICAgICBwcmlvcml0eTogcHJpb3JpdHksXG4gICAgICAgICAgICBvbmNlOiBvbmNlXG4gICAgICAgIH07XG5cbiAgICAgICAgbGV0IGFycmF5ID0gdGhpcy5tX2V2ZW50RGljdFt0eXBlXTtcbiAgICAgICAgbGV0IGhhcyA9IGZhbHNlO1xuICAgICAgICBsZXQgcG9zID0gMDtcbiAgICAgICAgaWYgKGFycmF5ICE9IG51bGwpIHtcbiAgICAgICAgICAgIGFycmF5LmZvckVhY2goZWxlbWVudCA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKGVsZW1lbnQudGFyZ2V0ID09IHRhcmdldCAmJiBlbGVtZW50LmNhbGxCYWNrID09IGNhbGxCYWNrKSB7XG4gICAgICAgICAgICAgICAgICAgIGhhcyA9IHRydWU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmIChlbGVtZW50LnByaW9yaXR5ID4gaW5mby5wcmlvcml0eSkge1xuICAgICAgICAgICAgICAgICAgICBwb3MrKztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGFycmF5ID0gbmV3IEFycmF5PEV2ZW50TGlzdGVuZXJJbmZvRGF0YT4oKTtcbiAgICAgICAgICAgIHRoaXMubV9ldmVudERpY3RbdHlwZV0gPSBhcnJheTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoaGFzKSB7XG4gICAgICAgICAgICAvLyBjb25zb2xlLmVycm9yKFwi6YeN5aSN5rOo5YaM5raI5oGv77yadHlwZT1cIiArIHR5cGUpO1xuICAgICAgICAgICAgTG9nLmVycm9yKFwi6YeN5aSN5rOo5YaM5raI5oGv77yadHlwZT1cIiArIHR5cGUpXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGFycmF5LnNwbGljZShwb3MsIDAsIGluZm8pO1xuICAgICAgICAgICAgcmV0dXJuIGluZm87XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDnp7vpmaTkuIDkuKrmtojmga/nm5HlkKzlmahcbiAgICAgKiBAcGFyYW0gdHlwZSDmtojmga9pZFxuICAgICAqIEBwYXJhbSBjYWxsQmFjayDlm57osIPlh73mlbBcbiAgICAgKiBAcGFyYW0gdGFyZ2V0IOS9nOeUqOeahOWvueixoVxuICAgICAqL1xuICAgIHB1YmxpYyByZW1vdmVFdmVudExpc3RlbmVyKHR5cGU6IHN0cmluZyB8IG51bWJlciwgY2FsbEJhY2s6IEV2ZW50Q2FsbGJhY2tMaXN0ZW5lciwgdGFyZ2V0OiBhbnkpIHtcbiAgICAgICAgdHlwZSA9IHR5cGUudG9TdHJpbmcoKTtcbiAgICAgICAgbGV0IGluZm86IEV2ZW50TGlzdGVuZXJJbmZvRGF0YSA9IG51bGw7XG4gICAgICAgIGxldCBhcnJheSA9IHRoaXMubV9ldmVudERpY3RbdHlwZV07XG4gICAgICAgIGlmIChhcnJheSAhPSBudWxsKSB7XG4gICAgICAgICAgICBsZXQgaW5mb0luZGV4ID0gLTE7XG4gICAgICAgICAgICBhcnJheS5ldmVyeSgodmFsdWU6IEV2ZW50TGlzdGVuZXJJbmZvRGF0YSwgaW5kZXg6IG51bWJlciwgYXJyYXk6IEV2ZW50TGlzdGVuZXJJbmZvRGF0YVtdKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKHZhbHVlLnRhcmdldCA9PSB0YXJnZXQgJiYgdmFsdWUuY2FsbEJhY2sgPT0gY2FsbEJhY2spIHtcbiAgICAgICAgICAgICAgICAgICAgaW5mb0luZGV4ID0gaW5kZXg7XG4gICAgICAgICAgICAgICAgICAgIGluZm8gPSB2YWx1ZTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBpZiAoaW5mb0luZGV4ICE9IC0xKSB7XG4gICAgICAgICAgICAgICAgYXJyYXkuc3BsaWNlKGluZm9JbmRleCwgMSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgcmVtb3ZlRXZlbnRMaXN0ZW5lckFsbCgpIHtcbiAgICAgICAgdGhpcy5tX2V2ZW50RGF0YSA9IG5ldyBBcnJheTxFdmVudERhdGE+KCk7XG4gICAgICAgIHRoaXMubV9ldmVudERpY3QgPSB7fTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDmmK/lkKblrZjlnKjov5nkuKrnm5HlkKzmtojmga9cbiAgICAgKiBAcGFyYW0gdHlwZSDmtojmga/nsbvlnotcbiAgICAgKiBAcGFyYW0gY2FsbEJhY2sg5Zue6LCD57G75Z6LXG4gICAgICogQHBhcmFtIHRhcmdldCDlm57osIPlr7nosaFcbiAgICAgKi9cbiAgICBwdWJsaWMgaGFzRXZlbnRMaXN0ZW5lcih0eXBlOiBzdHJpbmcgfCBudW1iZXIsIGNhbGxCYWNrOiBFdmVudENhbGxiYWNrTGlzdGVuZXIsIHRhcmdldDogYW55KSB7XG4gICAgICAgIGxldCBmbGFnID0gZmFsc2U7XG4gICAgICAgIGxldCBhcnJheSA9IHRoaXMubV9ldmVudERpY3RbdHlwZV07XG4gICAgICAgIGlmIChhcnJheSkge1xuICAgICAgICAgICAgLy8gQHRzLWlnbm9yZVxuICAgICAgICAgICAgbGV0IGluZGV4ID0gYXJyYXkuZmluZEluZGV4KChvYmosIGluZGV4LCBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICByZXR1cm4gb2JqLnRhcmdldCA9PSB0YXJnZXQgJiYgb2JqLmNhbGxCYWNrID09IGNhbGxCYWNrO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBmbGFnID0gaW5kZXggIT0gLTE7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZsYWc7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5rS+5Y+R5raI5oGvXG4gICAgICogQHBhcmFtIGVkIOa0vuWPkeeahOa2iOaBr+WGheWuuVxuICAgICAqL1xuICAgIHB1YmxpYyBkaXNwYXRjaEV2ZW50KGVkOiBFdmVudERhdGEpIHtcbiAgICAgICAgdGhpcy5fZGlzcGF0Y2hFdmVudChlZCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5rS+5Y+R5raI5oGvXG4gICAgICogQHBhcmFtIGNtZCDmtojmga9pZFxuICAgICAqIEBwYXJhbSBkYXRhIOa2iOaBr+WGheWuuVxuICAgICAqL1xuICAgIHB1YmxpYyBkaXNwYXRjaEV2ZW50QnlDbWQoY21kOiBzdHJpbmcgfCBudW1iZXIsIGRhdGE6IGFueSA9IG51bGwpIHtcbiAgICAgICAgbGV0IGVkID0gdGhpcy5jcmVhdGVFdmVudERhdGEoY21kLCBkYXRhKTtcbiAgICAgICAgdGhpcy5fZGlzcGF0Y2hFdmVudChlZCk7XG4gICAgICAgIGlmIChlZCAhPSBudWxsKSB7XG4gICAgICAgICAgICB0aGlzLnJldHVybkV2ZW50RGF0YShlZCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcml2YXRlIF9kaXNwYXRjaEV2ZW50KGVkOiBFdmVudERhdGEpIHtcbiAgICAgICAgbGV0IGFycmF5ID0gdGhpcy5tX2V2ZW50RGljdFtlZC5jbWRdO1xuICAgICAgICBpZiAoYXJyYXkgIT0gbnVsbCkge1xuXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGFycmF5Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgbGV0IGluZm8gPSBhcnJheVtpXTtcbiAgICAgICAgICAgICAgICBpZiAoaW5mby5jYWxsQmFjayAhPSBudWxsKSB7XG4gICAgICAgICAgICAgICAgICAgIGluZm8uY2FsbEJhY2suY2FsbChpbmZvLnRhcmdldCwgZWQpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZiAoaW5mby5vbmNlKSB7XG4gICAgICAgICAgICAgICAgICAgIGFycmF5LnNwbGljZShpLS0sIDEpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZiAoZWQuaXNTdG9wKSB7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuXG59XG5cbmV4cG9ydCB0eXBlIEV2ZW50TGlzdGVuZXJJbmZvRGF0YSA9XG4gICAge1xuICAgICAgICB0eXBlOiBzdHJpbmcsXG4gICAgICAgIGNhbGxCYWNrOiBFdmVudENhbGxiYWNrTGlzdGVuZXIsXG4gICAgICAgIHRhcmdldDogYW55LFxuICAgICAgICBwcmlvcml0eTogbnVtYmVyLFxuICAgICAgICBvbmNlOiBib29sZWFuXG4gICAgfVxuXG5leHBvcnQgdHlwZSBFdmVudExpc3RlbmVyQ2xhc3NEaWN0ID0ge1xuICAgIFtrZXk6IHN0cmluZ106IEFycmF5PEV2ZW50TGlzdGVuZXJJbmZvRGF0YT5cbn1cblxuXG5leHBvcnQgdHlwZSBFdmVudENhbGxiYWNrTGlzdGVuZXIgPSAoKGVkOiBFdmVudERhdGEpID0+IHZvaWQpO1xuXG5leHBvcnQgY2xhc3MgRXZlbnRDb250ZXh0IHtcblxuICAgIHB1YmxpYyBzdGF0aWMgZXZlbnROb2RlczogTWFwPEV2ZW50Tm9kZSwgRXZlbnROb2RlPiA9IG5ldyBNYXA8RXZlbnROb2RlLCBFdmVudE5vZGU+KCk7XG5cbn1cblxuIiwiaW1wb3J0IEhhbmRsZXIgPSBMYXlhLkhhbmRsZXI7XG5pbXBvcnQge1V0aWxTdHJpbmd9IGZyb20gXCIuLi8uLi91dGlsL3N0cmluZ1wiO1xuaW1wb3J0IHtSZXNNYW5hZ2VyfSBmcm9tIFwiLi4vcmVzL3Jlcy1tYW5hZ2VyXCI7XG5pbXBvcnQgeyBTaW5nbGV0b24gfSBmcm9tICcuLi8uLi9jb3JlL3NpbmdsZXRvbic7XG5pbXBvcnQgeyBJTWFuYWdlciB9IGZyb20gJy4uLy4uL2ludGVyZmFjZS9pLW1hbmFnZXInO1xuaW1wb3J0IHsgRGljdGlvbmFyeSB9IGZyb20gJy4uLy4uL3N0cnVjdHVyZS9kaWN0aW9uYXJ5JztcbmltcG9ydCB7IEpzb25UZW1wbGF0ZSB9IGZyb20gJy4vanNvbi10ZW1wbGF0ZSc7XG5pbXBvcnQgeyBDb25maWdEYXRhIH0gZnJvbSAnLi4vLi4vc2V0dGluZy9jb25maWcnO1xuaW1wb3J0IHsgTG9nIH0gZnJvbSAnLi4vLi4vY29yZS9sb2cnO1xuXG4gIC8qKlxuICogQGF1dGhvciBTdW5cbiAqIEB0aW1lIDIwMTktMDgtMTIgMTQ6NDBcbiAqIEBwcm9qZWN0IFNGcmFtZXdvcmtfTGF5YUFpclxuICogQGRlc2NyaXB0aW9uIOmFjee9ruihqOeuoeeQhlxuICpcbiAqL1xuZXhwb3J0IGNsYXNzIEpzb25NYW5hZ2VyIGV4dGVuZHMgU2luZ2xldG9uIGltcGxlbWVudHMgSU1hbmFnZXIge1xuXG4gICAgLyoqXG4gICAgICog5a2Y5pS+5omA5pyJ6YWN572u6KGo5qih5p2/XG4gICAgICovXG4gICAgcHJpdmF0ZSBtX0RpY1RlbXBsYXRlOiBEaWN0aW9uYXJ5PEpzb25UZW1wbGF0ZT4gPSBudWxsO1xuICAgIC8qKlxuICAgICAqIOWtmOaUvuaJgOacieino+aekOi/h+eahOmFjee9ruihqFxuICAgICAqL1xuICAgIHByaXZhdGUgbV9EaWNEYXRhOiBEaWN0aW9uYXJ5PGFueT4gPSBudWxsO1xuXG4gICAgcHVibGljIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICBzdXBlcigpO1xuICAgIH1cbiAgICBwcml2YXRlIHN0YXRpYyBpbnN0YW5jZTogSnNvbk1hbmFnZXIgPSBudWxsO1xuICAgIHB1YmxpYyBzdGF0aWMgZ2V0ICQoKTogSnNvbk1hbmFnZXIge1xuICAgICAgICBpZiAoIXRoaXMuaW5zdGFuY2UpIHRoaXMuaW5zdGFuY2UgPSBuZXcgSnNvbk1hbmFnZXIoKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuaW5zdGFuY2U7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog566h55CG5Zmo57uf5LiA6K6+572u5pa55rOVXG4gICAgICovXG4gICAgcHVibGljIHNldHVwKCk6IHZvaWQge1xuICAgICAgICB0aGlzLm1fRGljVGVtcGxhdGUgPSBuZXcgRGljdGlvbmFyeTxKc29uVGVtcGxhdGU+KCk7XG4gICAgICAgIHRoaXMubV9EaWNEYXRhID0gbmV3IERpY3Rpb25hcnk8YW55PigpO1xuICAgICAgICB0aGlzLmxvYWQoQ29uZmlnRGF0YS4kLmpzb25UZW1wbGF0ZUxpc3QpO1xuICAgIH1cblxuICAgIHVwZGF0ZSgpOiB2b2lkIHtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDnrqHnkIblmajnu5/kuIDplIDmr4Hmlrnms5VcbiAgICAgKi9cbiAgICBwdWJsaWMgZGVzdHJveSgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy51bmxvYWRBbGwoKTtcbiAgICAgICAgaWYgKHRoaXMubV9EaWNUZW1wbGF0ZSkge1xuICAgICAgICAgICAgdGhpcy5tX0RpY1RlbXBsYXRlLmNsZWFyKCk7XG4gICAgICAgICAgICB0aGlzLm1fRGljVGVtcGxhdGUgPSBudWxsO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLm1fRGljRGF0YSkge1xuICAgICAgICAgICAgdGhpcy5tX0RpY0RhdGEuY2xlYXIoKTtcbiAgICAgICAgICAgIHRoaXMubV9EaWNEYXRhID0gbnVsbDtcbiAgICAgICAgfVxuICAgIH1cblxuICAgICAvKipcbiAgICAgKiDliqDovb3miYDmnInnmoTmlbDmja7mqKHmnb9cbiAgICAgKiBAcGFyYW0gbGlzdFxuICAgICAqL1xuICAgIHByaXZhdGUgbG9hZChsaXN0OiBKc29uVGVtcGxhdGVbXSk6IHZvaWQge1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGxpc3QubGVuZ3RoOyArK2kpIHtcbiAgICAgICAgICAgIExvZy5sb2coXCJbbG9hZF3liqDovb3phY3nva7ooag6XCIgKyBsaXN0W2ldLnVybCk7XG4gICAgICAgICAgICB0aGlzLm1fRGljVGVtcGxhdGUuYWRkKGxpc3RbaV0ubmFtZSwgbGlzdFtpXSk7XG4gICAgICAgIH1cbiAgICB9XG5cblxuICAgIC8qKlxuICAgICAqIOiOt+WPluS4gOS4quWNleS4gOe7k+aehOeahOaVsOaNrlxuICAgICAqIEBwYXJhbSBuYW1lXG4gICAgICovXG4gICAgcHVibGljIGdldFRhYmxlKG5hbWU6IHN0cmluZyk6IGFueSB7XG4gICAgICAgIFxuICAgICAgICBsZXQgZGF0YSA9IHRoaXMubV9EaWNEYXRhLnZhbHVlKG5hbWUpO1xuICAgICAgICBpZihkYXRhPT1udWxsKXtcbiAgICAgICAgICAgIGRhdGEgPSBSZXNNYW5hZ2VyLiQuZ2V0UmVzKHRoaXMubV9EaWNUZW1wbGF0ZS52YWx1ZShuYW1lKS51cmwpO1xuICAgICAgICAgICAgdGhpcy5tX0RpY0RhdGEuYWRkKG5hbWUsZGF0YSk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGRhdGE7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog6I635Y+W5LiA6KGM5aSN5ZCI6KGo55qE5pWw5o2uXG4gICAgICogQHBhcmFtIG5hbWVcbiAgICAgKiBAcGFyYW0ga2V5XG4gICAgICovXG4gICAgcHVibGljIGdldFRhYmxlUm93KG5hbWU6IHN0cmluZywga2V5OiBzdHJpbmd8bnVtYmVyKTogYW55IHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0VGFibGUobmFtZSlba2V5XTtcbiAgICB9XG5cbiAgIFxuXG4gICAgLyoqXG4gICAgICog5Y246L295oyH5a6a55qE5qih5p2/XG4gICAgICogQHBhcmFtIHVybFxuICAgICAqL1xuICAgIHB1YmxpYyB1bmxvYWQobmFtZTogc3RyaW5nKTogdm9pZCB7XG4gICAgICAgIGxldCB0ZW1wbGF0ZSA9IHRoaXMubV9EaWNUZW1wbGF0ZS52YWx1ZShuYW1lKTtcbiAgICAgICAgaWYgKHRlbXBsYXRlKSB7XG4gICAgICAgICAgICB0aGlzLm1fRGljRGF0YS5yZW1vdmUobmFtZSk7XG4gICAgICAgIH1cbiAgICAgICAgUmVzTWFuYWdlci4kLnJlbGVhc2VVcmwodGVtcGxhdGUudXJsKTtcbiAgICAgICAgdGhpcy5tX0RpY1RlbXBsYXRlLnJlbW92ZShuYW1lKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDljbjovb3miYDmnInnmoTmqKHmnb9cbiAgICAgKiBAY29uc3RydWN0b3JcbiAgICAgKi9cbiAgICBwdWJsaWMgdW5sb2FkQWxsKCk6IHZvaWQge1xuICAgICAgICBpZiAoIXRoaXMubV9EaWNUZW1wbGF0ZSkgcmV0dXJuO1xuXG4gICAgICAgIHRoaXMubV9EaWNUZW1wbGF0ZS5mb3JlYWNoKGZ1bmN0aW9uIChrZXksIHZhbHVlKSB7XG4gICAgICAgICAgICB0aGlzLnVubG9hZChrZXkpO1xuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH0pO1xuICAgICAgICB0aGlzLm1fRGljRGF0YS5jbGVhcigpO1xuICAgICAgICB0aGlzLm1fRGljVGVtcGxhdGUuY2xlYXIoKTtcbiAgICB9XG59XG4iLCJcblxuIC8qKlxuICogQGF1dGhvciBTdW5cbiAqIEB0aW1lIDIwMTktMDgtMTIgMTA6NTlcbiAqIEBwcm9qZWN0IFNGcmFtZXdvcmtfTGF5YUFpclxuICogQGRlc2NyaXB0aW9uIOmFjee9ruihqOaooeadv+mhuVxuICpcbiAqL1xuZXhwb3J0IGNsYXNzIEpzb25UZW1wbGF0ZSB7XG5cbiAgICBwdWJsaWMgdXJsOiBzdHJpbmc7XHQvL+i1hOa6kHVybFxuICAgIHB1YmxpYyBuYW1lOiBzdHJpbmc7IC8v5ZCN56ew77ya55So5LqO5p+l5om+6K+l5pWw5o2u57uT5p6EXG5cbiAgICBjb25zdHJ1Y3Rvcih1cmw6IHN0cmluZywgbmFtZTogc3RyaW5nKSB7XG4gICAgICAgIHRoaXMudXJsID0gdXJsO1xuICAgICAgICB0aGlzLm5hbWUgPSBuYW1lO1xuICAgIH1cbn1cbiIsImltcG9ydCB7IEV2ZW50RnVuYyB9IGZyb20gJy4uL2V2ZW50L2V2ZW50LWRhdGEnO1xuaW1wb3J0IHsgUmVzSXRlbSB9IGZyb20gJy4vcmVzLWl0ZW0nO1xuXG4gLyoqXG4gKiBAYXV0aG9yIFN1blxuICogQHRpbWUgMjAxOS0wOC0wOSAxOTozMVxuICogQHByb2plY3QgU0ZyYW1ld29ya19MYXlhQWlyXG4gKiBAZGVzY3JpcHRpb24g5Zy65pmv566h55CG5Zmo5omA6ZyA55qE6LWE5rqQ5YyF5a6a5LmJXG4gKlxuICovXG5leHBvcnQgY2xhc3MgUmVzR3JvdXAge1xuXG4gICAgLyoq5Yqg6L296L+b5bqmICovXG4gICAgcHVibGljIHByb2dyZXNzOiBudW1iZXIgPSAwO1xuICAgIC8qKuWKoOi9vei1hOa6kCAqL1xuICAgIHB1YmxpYyBuZWVkTG9hZDogQXJyYXk8UmVzSXRlbT4gPSBuZXcgQXJyYXk8UmVzSXRlbT4oKTtcbiAgICAvKirliqDovb3ml7bnmoTlm57osIPmjqXlj6Ms5LiA6Iis55So5L2c57uZ5Yqg6L2956qX6K6+572u6L+b5bqmICovXG4gICAgcHVibGljIGxvYWRJdGVtOiBFdmVudEZ1bmM7XG4gICAgLyoq57uT5p2f5pe255qE5Zue6LCD5o6l5Y+jICovXG4gICAgcHVibGljIGZpbmlzaDogRXZlbnRGdW5jO1xuXG4gICAgLyoqXG4gICAgICog5ZCR6LWE5rqQ57uE5re75Yqg55uu5qCHXG4gICAgICogQHBhcmFtIHVybCDnm7jlr7not6/lvoRcbiAgICAgKiBAcGFyYW0gdHlwZSDnsbvlnosgXG4gICAgICogQHBhcmFtIGlzS2VlcE1lbW9yeSDmmK/lkKbluLjpqbvlhoXlrZggXG4gICAgICovXG4gICAgcHVibGljIGFkZCh1cmw6IHN0cmluZywgdHlwZTogc3RyaW5nLCBpc0tlZXBNZW1vcnkgPSBmYWxzZSkge1xuXG4gICAgICAgIGxldCBpbmRleCA9IHRoaXMubmVlZExvYWQuZmluZEluZGV4KCh2YWx1ZTogUmVzSXRlbSwgaW5kZXg6IG51bWJlciwgb2JqOiBBcnJheTxSZXNJdGVtPikgPT4ge1xuICAgICAgICAgICAgcmV0dXJuIHZhbHVlLlVybCA9PSB1cmxcbiAgICAgICAgfSk7XG4gICAgICAgIGlmIChpbmRleCA9PSAtMSkge1xuICAgICAgICAgICAgbGV0IGluZm8gPSBuZXcgUmVzSXRlbSh1cmwsdHlwZSxpc0tlZXBNZW1vcnkpO1xuICAgICAgICAgICAgdGhpcy5uZWVkTG9hZC5wdXNoKGluZm8pO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzXG4gICAgfVxufVxuXG4iLCJcbi8qKlxuICogQGF1dGhvciBTdW5cbiAqIEB0aW1lIDIwMTktMDgtMDkgMTk6MThcbiAqIEBwcm9qZWN0IFNGcmFtZXdvcmtfTGF5YUFpclxuICogQGRlc2NyaXB0aW9uIOi1hOa6kOWxnuaAp1xuICpcbiAqL1xuZXhwb3J0IGNsYXNzIFJlc0l0ZW0ge1xuICAgIHByaXZhdGUgdXJsOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSB0eXBlOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBpc0tlZXBNZW1vcnkgPSBmYWxzZTtcblxuICAgIGNvbnN0cnVjdG9yKHVybCx0eXBlLGlza2VlcE1lbW9yeSlcbiAgICB7XG4gICAgICAgIHRoaXMudXJsID0gdXJsO1xuICAgICAgICB0aGlzLnR5cGUgPSB0eXBlO1xuICAgICAgICB0aGlzLmlzS2VlcE1lbW9yeSA9IGlza2VlcE1lbW9yeTtcbiAgICB9XG5cbiAgICBwdWJsaWMgZ2V0IFVybCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMudXJsXG4gICAgfVxuXG4gICAgcHVibGljIGdldCBUeXBlKClcbiAgICB7XG4gICAgICAgIHJldHVybiB0aGlzLnR5cGVcbiAgICB9XG5cbiAgICBwdWJsaWMgZ2V0IElzS2VlcE1lbW9yeSgpXG4gICAge1xuICAgICAgICByZXR1cm4gdGhpcy5pc0tlZXBNZW1vcnlcbiAgICB9XG59XG5cblxuIiwiaW1wb3J0IEhhbmRsZXIgPSBMYXlhLkhhbmRsZXI7XG5pbXBvcnQgeyBFdmVudE5vZGUgfSBmcm9tICcuLi9ldmVudC9ldmVudC1ub2RlJztcbmltcG9ydCB7IElNYW5hZ2VyIH0gZnJvbSAnLi4vLi4vaW50ZXJmYWNlL2ktbWFuYWdlcic7XG5pbXBvcnQgeyBEaWN0aW9uYXJ5IH0gZnJvbSAnLi4vLi4vc3RydWN0dXJlL2RpY3Rpb25hcnknO1xuaW1wb3J0IHsgTG9nIH0gZnJvbSAnLi4vLi4vY29yZS9sb2cnO1xuaW1wb3J0IHsgUmVzSXRlbSB9IGZyb20gJy4vcmVzLWl0ZW0nO1xuaW1wb3J0IHsgVXRpbFRpbWUgfSBmcm9tICcuLi8uLi91dGlsL3RpbWUnO1xuaW1wb3J0IHsgUmVzR3JvdXAgfSBmcm9tICcuL3Jlcy1ncm91cCc7XG5pbXBvcnQgeyBSZXNMb2FkZWQgfSBmcm9tICcuL3Jlcy1sb2FkZWQnO1xuaW1wb3J0IHsgZW51bUNsZWFyU3RyYXRlZ3ksIGVudW1BcnJheVNvcnRPcmRlciB9IGZyb20gJy4uLy4uL3NldHRpbmcvZW51bSc7XG5pbXBvcnQgeyBVdGlsQXJyYXkgfSBmcm9tICcuLi8uLi91dGlsL2FycmF5JztcbmltcG9ydCB7IEV2ZW50RnVuYyB9IGZyb20gJy4uL2V2ZW50L2V2ZW50LWRhdGEnO1xuXG5cbi8qKlxuICogQGF1dGhvciBTdW5cbiAqIEB0aW1lIDIwMTktMDgtMTIgMTM6MzNcbiAqIEBwcm9qZWN0IFNGcmFtZXdvcmtfTGF5YUFpclxuICogQGRlc2NyaXB0aW9uICDotYTmupDnrqHnkIYgIO+8iOaJgOaciei1hOa6kOWdh+mAmui/h1Jlc0dyb3Vw55qE5b2i5byP5p2l5Yqg6L2977yJXG4gKlxuICovXG5leHBvcnQgY2xhc3MgUmVzTWFuYWdlciBleHRlbmRzIEV2ZW50Tm9kZSBpbXBsZW1lbnRzIElNYW5hZ2VyIHtcblxuXG4gICAgcHJpdmF0ZSBzdGF0aWMgaW5zdGFuY2U6IFJlc01hbmFnZXIgPSBudWxsO1xuICAgIHB1YmxpYyBzdGF0aWMgZ2V0ICQoKTogUmVzTWFuYWdlciB7XG4gICAgICAgIGlmICh0aGlzLmluc3RhbmNlID09IG51bGwpIHRoaXMuaW5zdGFuY2UgPSBuZXcgUmVzTWFuYWdlcigpO1xuICAgICAgICByZXR1cm4gdGhpcy5pbnN0YW5jZTtcbiAgICB9XG4gICBcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgc3VwZXIoKTtcbiAgICB9XG5cbiAgICAvL+WtmOaUvuaJgOacieW3suWKoOi9veeahOi1hOa6kFxuICAgIHByaXZhdGUgbV9kaWN0UmVzSXRlbTogTWFwPHN0cmluZywgUmVzSXRlbT4gPSBuZXcgTWFwPHN0cmluZywgUmVzSXRlbT4oKTtcblxuIFxuXG4gICAgcHVibGljIHNldHVwKCk6IHZvaWQge1xuICAgIH1cblxuICAgIHVwZGF0ZSgpOiB2b2lkIHtcbiAgICB9XG5cbiAgICBwdWJsaWMgZGVzdHJveSgpOiB2b2lkIHtcbiAgICB9XG5cblxuICAgIC8qKlxuICAgICAqIOmAmui/h1VSTOiOt+WPlui1hOa6kFxuICAgICAqIEBwYXJhbSB1cmxcbiAgICAgKi9cbiAgICBwdWJsaWMgZ2V0UmVzKHVybDogc3RyaW5nKSB7XG4gICAgICAgIHJldHVybiBMYXlhLmxvYWRlci5nZXRSZXModXJsKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDliqDovb3ljZXkuKrotYTmupBcbiAgICAgKiBAcGFyYW0gcmVzSXRlbSDotYTmupDkv6Hmga9cbiAgICAgKiBAcGFyYW0gcHJvZ3Jlc3NGdWMg5Yqg6L296L+b5bqm5Zue6LCDXG4gICAgICogQHBhcmFtIGNvbXBsZXRlRnVjIOWKoOi9veWujOaIkOWbnuiwg1xuICAgICAqL1xuICAgIHB1YmxpYyBsb2FkUmVzKHJlc0l0ZW06UmVzSXRlbSxwcm9ncmVzc0Z1YzpFdmVudEZ1bmMsY29tcGxldGVGdWM6RXZlbnRGdW5jKSB7XG5cblxuICAgICAgICBMYXlhLmxvYWRlci5sb2FkKHJlc0l0ZW0uVXJsLCBIYW5kbGVyLmNyZWF0ZSh0aGlzLCAoc3VjY2VzczogYm9vbGVhbikgPT4ge1xuXG4gICAgICAgICAgICBpZiAoc3VjY2Vzcykge1xuICAgICAgICAgICAgICAgIC8v5a6M5oiQ5Zue6LCDXG4gICAgICAgICAgICAgICAgaWYoY29tcGxldGVGdWMhPW51bGwpIGNvbXBsZXRlRnVjLmludm9rZSgpO1xuICAgICAgICAgICAgICAgIC8v5qCH6K6w6LWE5rqQXG4gICAgICAgICAgICAgICAgaWYgKCF0aGlzLm1fZGljdFJlc0l0ZW0uaGFzKHJlc0l0ZW0uVXJsKSkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLm1fZGljdFJlc0l0ZW0uc2V0KHJlc0l0ZW0uVXJsLCByZXNJdGVtKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIExvZy5lcnJvcihcIkxvYWQgUmVzb3VyY2UgRXJyb3LvvJpcIik7XG4gICAgICAgICAgICAgICAgTG9nLmRlYnVnKHJlc0l0ZW0uVXJsKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICB9KSwgSGFuZGxlci5jcmVhdGUodGhpcywgKHByb2dyZXNzOiBudW1iZXIpID0+IHtcbiAgICAgICAgICAgIC8v6L+b5bqm5Zue6LCDXG4gICAgICAgICAgICBpZihwcm9ncmVzc0Z1YyE9bnVsbCkgcHJvZ3Jlc3NGdWMuaW52b2tlKHByb2dyZXNzKTtcblxuICAgICAgICB9LCBudWxsLCBmYWxzZSkpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOWKoOi9vei1hOa6kOe7hFxuICAgICAqIEBwYXJhbSBsb2FkcyDotYTmupDkv6Hmga8gXG4gICAgICogQHBhcmFtIHByb2dyZXNzRnVjIOWKoOi9vei/m+W6puWbnuiwg1xuICAgICAqIEBwYXJhbSBjb21wbGV0ZUZ1YyDliqDovb3lrozmiJDlm57osINcbiAgICAgKi9cbiAgICBwdWJsaWMgbG9hZEdyb3VwKGxvYWRzOiBSZXNHcm91cCxwcm9ncmVzc0Z1YzpFdmVudEZ1bmMsY29tcGxldGVGdWM6RXZlbnRGdW5jKSB7XG4gICAgICAgIGxldCB1cmxzOiBBcnJheTxhbnk+ID0gbmV3IEFycmF5PGFueT4oKTtcbiAgICAgICAgbG9hZHMubmVlZExvYWQuZm9yRWFjaChlbGVtZW50ID0+IHtcbiAgICAgICAgICAgIHVybHMucHVzaCh7dXJsOiBlbGVtZW50LlVybCwgdHlwZTogZWxlbWVudC5UeXBlfSlcbiAgICAgICAgfSk7XG5cbiAgICAgICAgTGF5YS5sb2FkZXIubG9hZCh1cmxzLCBIYW5kbGVyLmNyZWF0ZSh0aGlzLCAoc3VjY2VzczogYm9vbGVhbikgPT4ge1xuXG4gICAgICAgICAgICBpZiAoc3VjY2Vzcykge1xuICAgICAgICAgICAgICAgIC8v5a6M5oiQ5Zue6LCDXG4gICAgICAgICAgICAgICAgaWYoY29tcGxldGVGdWMhPW51bGwpIGNvbXBsZXRlRnVjLmludm9rZSgpO1xuICAgICAgICAgICAgICAgIC8v5qCH6K6w6LWE5rqQXG4gICAgICAgICAgICAgICAgZm9yIChsZXQgaW5kZXggPSAwOyBpbmRleCA8IGxvYWRzLm5lZWRMb2FkLmxlbmd0aDsgaW5kZXgrKykge1xuICAgICAgICAgICAgICAgICAgICBsZXQgaW5mbyA9IGxvYWRzLm5lZWRMb2FkW2luZGV4XTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCF0aGlzLm1fZGljdFJlc0l0ZW0uaGFzKGluZm8uVXJsKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5tX2RpY3RSZXNJdGVtLnNldChpbmZvLlVybCwgaW5mbyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIExvZy5lcnJvcihcIkxvYWQgUmVzb3VyY2UgRXJyb3LvvJpcIik7XG4gICAgICAgICAgICAgICAgTG9nLmRlYnVnKHVybHMpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgIH0pLCBIYW5kbGVyLmNyZWF0ZSh0aGlzLCAocHJvZ3Jlc3M6IG51bWJlcikgPT4ge1xuICAgICAgICAgICAgLy/ov5vluqblm57osINcbiAgICAgICAgICAgIGlmKHByb2dyZXNzRnVjIT1udWxsKSBwcm9ncmVzc0Z1Yy5pbnZva2UocHJvZ3Jlc3MpO1xuICAgICAgICAgICAgXG4gICAgICAgIH0sIG51bGwsIGZhbHNlKSk7XG5cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDliqDovb3pooTorr7nialcbiAgICAgKiBAcGFyYW0gZmlsZVBhdGhcbiAgICAgKiBAcGFyYW0gY29tcGxldGVcbiAgICAgKi9cbiAgICBwdWJsaWMgbG9hZFByZWZhYihmaWxlUGF0aDpTdHJpbmcsY29tcGxldGU6RXZlbnRGdW5jKVxuICAgIHtcbiAgICAgICAgTGF5YS5sb2FkZXIubG9hZChmaWxlUGF0aCxMYXlhLkhhbmRsZXIuY3JlYXRlKHRoaXMsZnVuY3Rpb24gKHByZTogTGF5YS5QcmVmYWIpIHtcbiAgICAgICAgICAgIHZhciBwbGF5UHJlOkxheWEuUHJlZmFiID0gbmV3IExheWEuUHJlZmFiKCk7XG4gICAgICAgICAgICBwbGF5UHJlLmpzb24gPSBwcmU7XG4gICAgICAgICAgICBsZXQgY2VsbCA9IExheWEuUG9vbC5nZXRJdGVtQnlDcmVhdGVGdW4oXCJDZWxsXCIsIHBsYXlQcmUuY3JlYXRlLCBwbGF5UHJlKTtcbiAgICAgICAgICAgIGlmIChjb21wbGV0ZSkgY29tcGxldGUuaW52b2tlKGNlbGwpO1xuICAgICAgICB9KSk7XG4gICAgfVxuXG5cbiAgICAvKipcbiAgICAgKiDph4rmlL7otYTmupDnu4RcbiAgICAgKiBAcGFyYW0gbG9hZHMg6LWE5rqQ57uEIFxuICAgICAqL1xuICAgIHB1YmxpYyByZWxlYXNlR3JvdXAobG9hZHM6UmVzR3JvdXApXG4gICAge1xuICAgICAgICBsZXQgdXJscyA9IG5ldyBBcnJheTxzdHJpbmc+KCk7XG4gICAgICAgIGxvYWRzLm5lZWRMb2FkLmZvckVhY2goZWxlbWVudCA9PiB7XG4gICAgICAgICAgICB1cmxzLnB1c2goZWxlbWVudC5VcmwpXG4gICAgICAgIH0pO1xuICAgICAgICBcbiAgICAgICAgZm9yKGxldCBpPTA7aTx1cmxzLmxlbmd0aDtpKyspe1xuICAgICAgICAgICAgTGF5YS5sb2FkZXIuY2xlYXJSZXModXJsc1tpXSk7XG4gICAgICAgICAgICB0aGlzLm1fZGljdFJlc0l0ZW0uZm9yRWFjaCgodjogUmVzSXRlbSwga2V5OiBzdHJpbmcpID0+IHtcbiAgICAgICAgICAgICAgIGlmKGtleT09dXJsc1tpXSl7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubV9kaWN0UmVzSXRlbS5kZWxldGUoa2V5KTtcbiAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog6YeK5pS+5oyH5a6a6LWE5rqQXG4gICAgICogQHBhcmFtIHVybCBcbiAgICAgKi9cbiAgICBwdWJsaWMgcmVsZWFzZVVybCh1cmw6c3RyaW5nKVxuICAgIHtcbiAgICAgICAgIGxldCBpc0FjdGl2ZTpib29sZWFuID0gZmFsc2U7XG4gICAgICAgICB0aGlzLm1fZGljdFJlc0l0ZW0uZm9yRWFjaCgodjogUmVzSXRlbSwga2V5OiBzdHJpbmcpID0+IHtcbiAgICAgICAgICAgIGlmKGtleT09dXJsKXtcbiAgICAgICAgICAgICAgICBpc0FjdGl2ZSA9IHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICB9KTtcblxuICAgICAgICAgaWYoaXNBY3RpdmUpe1xuICAgICAgICAgICAgTGF5YS5sb2FkZXIuY2xlYXJSZXModXJsKTtcbiAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgTG9nLmVycm9yKFwi5Yqg6L296LWE5rqQ57uE5YaF5LiN5a2Y5Zyo6K+l6LWE5rqQXCIpO1xuICAgICAgICAgfVxuICAgIH1cbn1cblxuIiwiaW1wb3J0IHtVdGlsU3RyaW5nfSBmcm9tIFwiLi4vLi4vdXRpbC9zdHJpbmdcIjtcbmltcG9ydCBTb3VuZENoYW5uZWwgPSBMYXlhLlNvdW5kQ2hhbm5lbDtcbmltcG9ydCBIYW5kbGVyID0gTGF5YS5IYW5kbGVyO1xuaW1wb3J0IHtSZXNNYW5hZ2VyfSBmcm9tIFwiLi4vcmVzL3Jlcy1tYW5hZ2VyXCI7XG5pbXBvcnQgeyBFdmVudE5vZGUgfSBmcm9tICcuLi9ldmVudC9ldmVudC1ub2RlJztcbmltcG9ydCB7IElNYW5hZ2VyIH0gZnJvbSAnLi4vLi4vaW50ZXJmYWNlL2ktbWFuYWdlcic7XG5pbXBvcnQgeyBMb2cgfSBmcm9tICcuLi8uLi9jb3JlL2xvZyc7XG5pbXBvcnQgeyBEaWN0aW9uYXJ5IH0gZnJvbSAnLi4vLi4vc3RydWN0dXJlL2RpY3Rpb25hcnknO1xuaW1wb3J0IHsgQ29uZmlnU291bmQgfSBmcm9tICcuLi8uLi9zZXR0aW5nL2NvbmZpZyc7XG5cblxuLyoqXG4gKiBAYXV0aG9yIFN1blxuICogQHRpbWUgMjAxOS0wOC0xMiAxNTowOFxuICogQHByb2plY3QgU0ZyYW1ld29ya19MYXlhQWlyXG4gKiBAZGVzY3JpcHRpb24g6Z+z5pWI566h55CGXG4gKlxuICovXG5leHBvcnQgY2xhc3MgU291bmRNYW5hZ2VyIGV4dGVuZHMgRXZlbnROb2RlIGltcGxlbWVudHMgSU1hbmFnZXIge1xuXG5cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirigJTigJQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8v5YiG55WM57q/Ly8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq5bGe5oCn5L+h5oGvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxuICAgIC8qKiBEZXM66IOM5pmv6Z+z5LmQICovXG4gICAgcHJpdmF0ZSBtX0N1ckJHU291bmQ6IFNvdW5kQ2hhbm5lbCA9IG51bGw7XG4gICAgLyoq6Z+z5pWI5ZCN5a2X5a+55bqUVXJsICovXG4gICAgcHJpdmF0ZSBkaWN0U291bmRVcmw6RGljdGlvbmFyeTxzdHJpbmc+ID0gbnVsbDtcblxuXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq4oCU4oCUKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL+WIhueVjOe6vy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKueUn+WRveWRqOacnyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbiAgICBwcml2YXRlIHN0YXRpYyBpbnN0YW5jZTogU291bmRNYW5hZ2VyID0gbnVsbDtcblxuICAgIHB1YmxpYyBzdGF0aWMgZ2V0ICQoKTogU291bmRNYW5hZ2VyIHtcbiAgICAgICAgaWYgKCF0aGlzLmluc3RhbmNlKSB0aGlzLmluc3RhbmNlID0gbmV3IFNvdW5kTWFuYWdlcigpO1xuICAgICAgICByZXR1cm4gdGhpcy5pbnN0YW5jZTtcbiAgICB9XG4gXG4gICAgc2V0dXAoKTogdm9pZCB7XG4gICAgICAgIHRoaXMubV9DdXJCR1NvdW5kID0gbmV3IFNvdW5kQ2hhbm5lbCgpO1xuICAgICAgICB0aGlzLmRpY3RTb3VuZFVybCA9IG5ldyBEaWN0aW9uYXJ5PHN0cmluZz4oKTtcbiAgICAgICAgQ29uZmlnU291bmQuJC5zb3VuZFJlc0xpc3QuZm9yRWFjaChpdGVtPT57XG4gICAgICAgICAgICB0aGlzLmRpY3RTb3VuZFVybC5hZGQoaXRlbS5uYW1lLGl0ZW0udXJsKTtcbiAgICAgICAgfSk7XG4gICAgICAgIGlmKCFVdGlsU3RyaW5nLmlzRW1wdHkoQ29uZmlnU291bmQuJC5iZ1NvdW5kTmFtZSkpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHRoaXMucGxheUJHU291bmQoQ29uZmlnU291bmQuJC5iZ1NvdW5kTmFtZSwwKTtcbiAgICAgICAgICAgIHRoaXMuc2V0QWxsVm9sdW1lKENvbmZpZ1NvdW5kLiQudm9sdW1lVm9pY2VTb3VuZCk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgdXBkYXRlKCk6IHZvaWQge1xuICAgIH1cbiAgICBkZXN0cm95KCk6IHZvaWQge1xuICAgIH1cblxuXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq4oCU4oCUKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL+WIhueVjOe6vy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKirorr7nva7mlbTkvZPpn7Pph48qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxuICAgIC8qKlxuICAgICAqIOiuvue9ruaVtOS9k+mfs+mHj1xuICAgICAqIEBwYXJhbSBudW1iZXJcbiAgICAgKi9cbiAgICBwdWJsaWMgc2V0QWxsVm9sdW1lKG51bWJlcilcbiAgICB7XG4gICAgICAgIENvbmZpZ1NvdW5kLiQudm9sdW1lVm9pY2VTb3VuZCA9IG51bWJlcjtcbiAgICAgICAgdGhpcy5tX0N1ckJHU291bmQudm9sdW1lID0gbnVtYmVyO1xuICAgIH1cblxuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKuKAlOKAlCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy/liIbnlYznur8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKuaOp+WItuiDjOaZr+mfs+S5kCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG4gICAgLyoqXG4gICAgICog5pKt5pS+6IOM5pmv5aOw6Z+zXG4gICAgICogQHBhcmFtICAgIGZpbGVfbmFtZSAgICDotYTmupDlkI3lrZdcbiAgICAgKiBAcGFyYW0gICAgY291bnQgICAgICAgIOaSreaUvuasoeaVsCgw5Li65b6q546vKVxuICAgICAqL1xuICAgIHB1YmxpYyBwbGF5QkdTb3VuZChmaWxlX25hbWU6IHN0cmluZywgY291bnQ6IG51bWJlcik6IHZvaWQge1xuICAgICAgICBpZiAoVXRpbFN0cmluZy5pc0VtcHR5KGZpbGVfbmFtZSkpIHtcbiAgICAgICAgICAgIExvZy5lcnJvcihcIlNvdW5kIGZpbGUgZXJyb3IhXCIpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMubV9DdXJCR1NvdW5kID0gTGF5YS5Tb3VuZE1hbmFnZXIucGxheU11c2ljKHRoaXMuZGljdFNvdW5kVXJsLnZhbHVlKGZpbGVfbmFtZSksY291bnQpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOWBnOatouiDjOaZr+mfs+aSreaUvlxuICAgICAqL1xuICAgIHB1YmxpYyBzdG9wQkdTb3VuZCgpOiB2b2lkIHtcbiAgICAgICAgaWYgKHRoaXMubV9DdXJCR1NvdW5kKSB7XG4gICAgICAgICAgICB0aGlzLm1fQ3VyQkdTb3VuZC5zdG9wKClcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOaaguWBnOiDjOaZr+mfs+S5kFxuICAgICAqL1xuICAgIHB1YmxpYyBwYXVzZUJHU291bmQoKTogdm9pZCB7XG4gICAgICAgIGlmICh0aGlzLm1fQ3VyQkdTb3VuZCkge1xuICAgICAgICAgICAgdGhpcy5tX0N1ckJHU291bmQucGF1c2UoKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOaBouWkjeiDjOaZr+mfs+S5kOaSreaUvlxuICAgICAqL1xuICAgIHB1YmxpYyByZXN1bWVCR1NvdW5kKCk6IHZvaWQge1xuICAgICAgICBpZiAodGhpcy5tX0N1ckJHU291bmQpIHtcbiAgICAgICAgICAgIHRoaXMubV9DdXJCR1NvdW5kLnJlc3VtZSgpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog6K6+572u6IOM5pmv6Z+z6YePXG4gICAgICogQHBhcmFtIHZvbHVtZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXRCR1NvdW5kVm9sdW1lKHZvbHVtZTogbnVtYmVyKTogdm9pZCB7XG4gICAgICAgIGlmICh0aGlzLm1fQ3VyQkdTb3VuZCkge1xuICAgICAgICAgICAgdGhpcy5tX0N1ckJHU291bmQudm9sdW1lID0gdm9sdW1lO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq4oCU4oCUKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL+WIhueVjOe6vy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioq5o6n5Yi26Z+z5pWI5pKt5pS+KioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbiAgICAvKipcbiAgICAgKiDmkq3mlL7mlYjmnpzlo7Dpn7NcbiAgICAgKiBAcGFyYW0gICAgZmlsZV9uYW1lICAgIOi1hOa6kFxuICAgICAqIEBwYXJhbSAgICBjb3VudCAgICAgICAg5pKt5pS+5qyh5pWwXG4gICAgICovXG4gICAgcHVibGljIHBsYXlTb3VuZEVmZmVjdChmaWxlX25hbWU6IHN0cmluZywgY291bnQ6IG51bWJlcil7XG4gICAgICAgIGlmIChVdGlsU3RyaW5nLmlzRW1wdHkoZmlsZV9uYW1lKSkge1xuICAgICAgICAgICAgTG9nLmVycm9yKFwi5aOw6Z+z5paH5Lu26ZSZ6K+vXCIpO1xuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIH1cbiAgICAgICAgbGV0IHNvdW5kOiBTb3VuZENoYW5uZWwgPSBMYXlhLlBvb2wuZ2V0SXRlbUJ5Q2xhc3MoXCJTb3VuZFwiLFNvdW5kQ2hhbm5lbCk7XG5cbiAgICAgICAgc291bmQgPSBMYXlhLlNvdW5kTWFuYWdlci5wbGF5U291bmQodGhpcy5kaWN0U291bmRVcmwudmFsdWUoZmlsZV9uYW1lKSxjb3VudCxIYW5kbGVyLmNyZWF0ZSh0aGlzLCgpPT57XG4gICAgICAgICAgICBMYXlhLlBvb2wucmVjb3ZlcihcIlNvdW5kXCIsc291bmQpO1xuICAgICAgICB9KSk7XG4gICAgICAgIHNvdW5kLnZvbHVtZSA9IENvbmZpZ1NvdW5kLiQudm9sdW1lVm9pY2VTb3VuZDtcbiAgICAgICAgcmV0dXJuIHNvdW5kO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOWBnOatouaSreaUvlxuICAgICAqIEBwYXJhbSBzb3VuZFxuICAgICAqL1xuICAgIHB1YmxpYyBzdG9wU291bmRFZmZlY3Qoc291bmQ6IFNvdW5kQ2hhbm5lbCk6IHZvaWQge1xuICAgICAgICBpZiAoc291bmQpIHtcbiAgICAgICAgICAgIHNvdW5kLnN0b3AoKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKuKAlOKAlCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy/liIbnlYznur8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuXG59IiwiaW1wb3J0IHtVdGlsVGltZX0gZnJvbSBcIi4uLy4uL3V0aWwvdGltZVwiO1xuaW1wb3J0IEhhbmRsZXIgPSBMYXlhLkhhbmRsZXI7XG5pbXBvcnQgeyBJUG9vbE9iamVjdCB9IGZyb20gJy4uLy4uL2NvcmUvb2JqZWN0LXBvb2wnO1xuaW1wb3J0IHsgVGltZXJJbnRlcnZhbCB9IGZyb20gJy4vdGltZXItaW50ZXJ2YWwnO1xuXG4vKipcbiAqIEBhdXRob3IgU3VuXG4gKiBAdGltZSAyMDE5LTA4LTEwIDIwOjA2XG4gKiBAcHJvamVjdCBTRnJhbWV3b3JrX0xheWFBaXJcbiAqIEBkZXNjcmlwdGlvbiAg6K6h5pe25Zmo5Z+657G7XG4gKlxuICovXG5leHBvcnQgY2xhc3MgVGltZXJFbnRpdHkgaW1wbGVtZW50cyBJUG9vbE9iamVjdCB7XG4gICAgcHVibGljIGlkOiBudW1iZXI7XG4gICAgcHVibGljIGlzQWN0aXZlOiBib29sZWFuO1xuXG4gICAgcHVibGljIG1SYXRlOiBudW1iZXI7XG4gICAgcHVibGljIG1UaWNrczogbnVtYmVyO1xuICAgIHB1YmxpYyBtVGlja3NFbGFwc2VkOiBudW1iZXI7XG4gICAgcHVibGljIGhhbmRsZTogSGFuZGxlcjtcblxuICAgIHB1YmxpYyBtVGltZTogVGltZXJJbnRlcnZhbDtcblxuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICB0aGlzLm1UaW1lID0gbmV3IFRpbWVySW50ZXJ2YWwoKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgaW5pdCgpOiB2b2lkIHtcbiAgICB9XG5cbiAgICBwdWJsaWMgY2xvc2UoKSB7XG4gICAgfVxuXG5cbiAgICBwdWJsaWMgY2xlYXIoKTogdm9pZCB7XG4gICAgICAgIGlmICh0aGlzLmhhbmRsZSAhPSBudWxsKSB7XG4gICAgICAgICAgICB0aGlzLmhhbmRsZS5yZWNvdmVyKCk7XG4gICAgICAgICAgICB0aGlzLmhhbmRsZSA9IG51bGw7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgc2V0KGlkOiBudW1iZXIsIHJhdGU6IG51bWJlciwgdGlja3M6IG51bWJlciwgaGFuZGxlOiBIYW5kbGVyKSB7XG4gICAgICAgIHRoaXMuaWQgPSBpZDtcbiAgICAgICAgdGhpcy5tUmF0ZSA9IHJhdGUgPCAwID8gMCA6IHJhdGU7XG4gICAgICAgIHRoaXMubVRpY2tzID0gdGlja3MgPCAwID8gMCA6IHRpY2tzO1xuICAgICAgICB0aGlzLmhhbmRsZSA9IGhhbmRsZTtcbiAgICAgICAgdGhpcy5tVGlja3NFbGFwc2VkID0gMDtcbiAgICAgICAgdGhpcy5pc0FjdGl2ZSA9IHRydWU7XG4gICAgICAgIHRoaXMubVRpbWUuaW5pdCh0aGlzLm1SYXRlLCBmYWxzZSk7XG4gICAgfVxuXG4gICAgcHVibGljIHVwZGF0ZShyZW1vdmVUaW1lcjogYW55KTogdm9pZCB7XG4gICAgICAgIGlmICh0aGlzLmlzQWN0aXZlICYmIHRoaXMubVRpbWUudXBkYXRlKFV0aWxUaW1lLmRlbHRhVGltZSkpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmhhbmRsZSAhPSBudWxsKSB0aGlzLmhhbmRsZS5ydW4oKTtcblxuICAgICAgICAgICAgdGhpcy5tVGlja3NFbGFwc2VkKys7XG4gICAgICAgICAgICBpZiAodGhpcy5tVGlja3MgPiAwICYmIHRoaXMubVRpY2tzID09IHRoaXMubVRpY2tzRWxhcHNlZCkge1xuICAgICAgICAgICAgICAgIHRoaXMuaXNBY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICByZW1vdmVUaW1lcih0aGlzLmlkKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn1cbiIsIi8qKlxuICogQGF1dGhvciBTdW5cbiAqIEB0aW1lIDIwMTktMDgtMTAgMjA6MDJcbiAqIEBwcm9qZWN0IFNGcmFtZXdvcmtfTGF5YUFpclxuICogQGRlc2NyaXB0aW9uICDlrprml7bmiafooYxcbiAqXG4gKi9cbmV4cG9ydCBjbGFzcyBUaW1lckludGVydmFsIHtcblxuICAgIHByaXZhdGUgbV9pbnRlcnZhbF90aW1lOiBudW1iZXI7Ly/mr6vnp5JcbiAgICBwcml2YXRlIG1fbm93X3RpbWU6IG51bWJlcjtcblxuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICB0aGlzLm1fbm93X3RpbWUgPSAwO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOWIneWni+WMluWumuaXtuWZqFxuICAgICAqIEBwYXJhbSAgICBpbnRlcnZhbCAgICDop6blj5Hpl7TpmpRcbiAgICAgKiBAcGFyYW0gICAgZmlyc3RfZnJhbWUgICAg5piv5ZCm56ys5LiA5bin5byA5aeL5omn6KGMXG4gICAgICovXG4gICAgcHVibGljIGluaXQoaW50ZXJ2YWw6IG51bWJlciwgZmlyc3RfZnJhbWU6IGJvb2xlYW4pOiB2b2lkIHtcbiAgICAgICAgdGhpcy5tX2ludGVydmFsX3RpbWUgPSBpbnRlcnZhbDtcbiAgICAgICAgaWYgKGZpcnN0X2ZyYW1lKSB0aGlzLm1fbm93X3RpbWUgPSB0aGlzLm1faW50ZXJ2YWxfdGltZTtcbiAgICB9XG5cbiAgICBwdWJsaWMgcmVzZXQoKTogdm9pZCB7XG4gICAgICAgIHRoaXMubV9ub3dfdGltZSA9IDA7XG4gICAgfVxuXG4gICAgcHVibGljIHVwZGF0ZShlbGFwc2VfdGltZTogbnVtYmVyKTogYm9vbGVhbiB7XG4gICAgICAgIHRoaXMubV9ub3dfdGltZSArPSBlbGFwc2VfdGltZTtcbiAgICAgICAgaWYgKHRoaXMubV9ub3dfdGltZSA+PSB0aGlzLm1faW50ZXJ2YWxfdGltZSkge1xuICAgICAgICAgICAgdGhpcy5tX25vd190aW1lIC09IHRoaXMubV9pbnRlcnZhbF90aW1lO1xuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbn1cbiIsImltcG9ydCBIYW5kbGVyID0gTGF5YS5IYW5kbGVyO1xuaW1wb3J0IHtVdGlsQXJyYXl9IGZyb20gXCIuLi8uLi91dGlsL2FycmF5XCI7XG5pbXBvcnQgeyBFdmVudE5vZGUgfSBmcm9tICcuLi9ldmVudC9ldmVudC1ub2RlJztcbmltcG9ydCB7IElNYW5hZ2VyIH0gZnJvbSAnLi4vLi4vaW50ZXJmYWNlL2ktbWFuYWdlcic7XG5pbXBvcnQgeyBUaW1lRGVsYXkgfSBmcm9tICcuLi8uLi9jb3JlL3RpbWUtZGVsYXknO1xuaW1wb3J0IHsgT2JqZWN0UG9vbCB9IGZyb20gJy4uLy4uL2NvcmUvb2JqZWN0LXBvb2wnO1xuaW1wb3J0IHsgVGltZXJFbnRpdHkgfSBmcm9tICcuL3RpbWVyLWVudGl0eSc7XG5cbi8qKlxuICogQGF1dGhvciBTdW5cbiAqIEB0aW1lIDIwMTktMDgtMDkgMjM6MjJcbiAqIEBwcm9qZWN0IFNGcmFtZXdvcmtfTGF5YUFpclxuICogQGRlc2NyaXB0aW9uICDlrprml7bnrqHnkIblmahcbiAqXG4gKi9cbmV4cG9ydCBjbGFzcyBUaW1lck1hbmFnZXIgZXh0ZW5kcyBFdmVudE5vZGUgaW1wbGVtZW50cyBJTWFuYWdlciB7XG4gIFxuICAgIHByaXZhdGUgbV9pZENvdW50ZXI6IG51bWJlciA9IDA7XG4gICAgcHJpdmF0ZSBtX1JlbW92YWxQZW5kaW5nOiBBcnJheTxudW1iZXI+ID0gW107XG4gICAgcHJpdmF0ZSBtX1RpbWVyczogQXJyYXk8VGltZXJFbnRpdHk+ID0gW107XG5cbiAgICBwcml2YXRlIHN0YXRpYyBpbnN0YW5jZTogVGltZXJNYW5hZ2VyID0gbnVsbDtcblxuICAgIHB1YmxpYyBzdGF0aWMgZ2V0ICQoKTogVGltZXJNYW5hZ2VyIHtcbiAgICAgICAgaWYgKCF0aGlzLmluc3RhbmNlKSB0aGlzLmluc3RhbmNlID0gbmV3IFRpbWVyTWFuYWdlcigpO1xuICAgICAgICByZXR1cm4gdGhpcy5pbnN0YW5jZTtcbiAgICB9XG5cbiAgICBwdWJsaWMgc2V0dXAoKTogdm9pZCB7XG4gICAgICAgIHRoaXMubV9pZENvdW50ZXIgPSAwO1xuICAgICAgICBUaW1lRGVsYXkuJC5hZGQoMC4xLCAwLCB0aGlzLnJlbW92ZSwgdGhpcyk7XG4gICAgICAgIFRpbWVEZWxheS4kLmFkZFVwZGF0ZSh0aGlzLnRpY2ssIHRoaXMpO1xuICAgIH1cblxuICAgIHVwZGF0ZSgpOiB2b2lkIHtcbiAgICB9XG5cbiAgICBwdWJsaWMgZGVzdHJveSgpOiB2b2lkIHtcbiAgICAgICAgVXRpbEFycmF5LmNsZWFyKHRoaXMubV9SZW1vdmFsUGVuZGluZyk7XG4gICAgICAgIFV0aWxBcnJheS5jbGVhcih0aGlzLm1fVGltZXJzKTtcbiAgICAgICAgVGltZURlbGF5LiQucmVtb3ZlKHRoaXMucmVtb3ZlLCB0aGlzKTtcbiAgICAgICAgVGltZURlbGF5LiQucmVtb3ZlKHRoaXMudGljaywgdGhpcyk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSB0aWNrKCk6IHZvaWQge1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMubV9UaW1lcnMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIHRoaXMubV9UaW1lcnNbaV0udXBkYXRlKHRoaXMucmVtb3ZlVGltZXIpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5a6a5pe26YeN5aSN5omn6KGMXG4gICAgICogQHBhcmFtICAgIHJhdGUgICAg6Ze06ZqU5pe26Ze0KOWNleS9jeavq+enkinjgIJcbiAgICAgKiBAcGFyYW0gICAgdGlja3MgICAg5omn6KGM5qyh5pWwXG4gICAgICogQHBhcmFtICAgIGNhbGxlciAgICDmiafooYzln58odGhpcynjgIJcbiAgICAgKiBAcGFyYW0gICAgbWV0aG9kICAgIOWumuaXtuWZqOWbnuiwg+WHveaVsO+8muazqOaEj++8jOi/lOWbnuWHveaVsOesrOS4gOS4quWPguaVsOS4uuWumuaXtuWZqGlk77yM5ZCO6Z2i5Y+C5pWw5L6d5qyh5pe25Lyg5YWl55qE5Y+C5pWw44CC5L6LT25UaW1lKHRpbWVyX2lkOm51bWJlciwgYXJnczE6YW55LCBhcmdzMjphbnksLi4uKTp2b2lkXG4gICAgICogQHBhcmFtICAgIGFyZ3MgICAg5Zue6LCD5Y+C5pWw44CCXG4gICAgICovXG4gICAgcHVibGljIGFkZExvb3AocmF0ZTogbnVtYmVyLCB0aWNrczogbnVtYmVyLCBjYWxsZXI6IGFueSwgbWV0aG9kOiBGdW5jdGlvbiwgYXJnczogQXJyYXk8YW55PiA9IG51bGwpOiBudW1iZXIge1xuICAgICAgICBpZiAodGlja3MgPD0gMCkgdGlja3MgPSAwO1xuICAgICAgICBsZXQgbmV3VGltZXI6IFRpbWVyRW50aXR5ID0gT2JqZWN0UG9vbC5nZXQoVGltZXJFbnRpdHkpO1xuICAgICAgICArK3RoaXMubV9pZENvdW50ZXI7XG4gICAgICAgIGlmIChhcmdzICE9IG51bGwpIFV0aWxBcnJheS5pbnNlcnQoYXJncywgdGhpcy5tX2lkQ291bnRlciwgMCk7XG4gICAgICAgIG5ld1RpbWVyLnNldCh0aGlzLm1faWRDb3VudGVyLCByYXRlLCB0aWNrcywgSGFuZGxlci5jcmVhdGUoY2FsbGVyLCBtZXRob2QsIGFyZ3MsIGZhbHNlKSk7XG4gICAgICAgIHRoaXMubV9UaW1lcnMucHVzaChuZXdUaW1lcik7XG4gICAgICAgIHJldHVybiBuZXdUaW1lci5pZDtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDljZXmrKHmiafooYxcbiAgICAgKi9cbiAgICBwdWJsaWMgYWRkT25jZShyYXRlOiBudW1iZXIsIGNhbGxlcjogYW55LCBtZXRob2Q6IEZ1bmN0aW9uLCBhcmdzOiBBcnJheTxhbnk+ID0gbnVsbCk6IG51bWJlciB7XG4gICAgICAgIGxldCBuZXdUaW1lcjogVGltZXJFbnRpdHkgPSBPYmplY3RQb29sLmdldChUaW1lckVudGl0eSk7XG4gICAgICAgICsrdGhpcy5tX2lkQ291bnRlcjtcbiAgICAgICAgaWYgKGFyZ3MgIT0gbnVsbCkgVXRpbEFycmF5Lmluc2VydChhcmdzLCB0aGlzLm1faWRDb3VudGVyLCAwKTtcbiAgICAgICAgbmV3VGltZXIuc2V0KHRoaXMubV9pZENvdW50ZXIsIHJhdGUsIDEsIEhhbmRsZXIuY3JlYXRlKGNhbGxlciwgbWV0aG9kLCBhcmdzLCBmYWxzZSkpO1xuICAgICAgICB0aGlzLm1fVGltZXJzLnB1c2gobmV3VGltZXIpO1xuICAgICAgICByZXR1cm4gbmV3VGltZXIuaWQ7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog56e76Zmk5a6a5pe25ZmoXG4gICAgICogQHBhcmFtICAgIHRpbWVySWQgICAg5a6a5pe25ZmoaWRcbiAgICAgKi9cbiAgICBwdWJsaWMgcmVtb3ZlVGltZXIodGltZXJJZDogbnVtYmVyKTogdm9pZCB7XG4gICAgICAgIHRoaXMubV9SZW1vdmFsUGVuZGluZy5wdXNoKHRpbWVySWQpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOenu+mZpOi/h+acn+WumuaXtuWZqFxuICAgICAqL1xuICAgIHByaXZhdGUgcmVtb3ZlKCk6IHZvaWQge1xuICAgICAgICBsZXQgdGltZXI6IFRpbWVyRW50aXR5O1xuICAgICAgICBpZiAodGhpcy5tX1JlbW92YWxQZW5kaW5nLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIGZvciAobGV0IGlkIG9mIHRoaXMubV9SZW1vdmFsUGVuZGluZykge1xuICAgICAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5tX1RpbWVycy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICB0aW1lciA9IHRoaXMubV9UaW1lcnNbaV07XG4gICAgICAgICAgICAgICAgICAgIGlmICh0aW1lci5pZCA9PSBpZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGltZXIuY2xlYXIoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIE9iamVjdFBvb2wucmVjb3Zlcih0aW1lcik7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1fVGltZXJzLnNwbGljZShpLCAxKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBVdGlsQXJyYXkuY2xlYXIodGhpcy5tX1JlbW92YWxQZW5kaW5nKTtcbiAgICAgICAgfVxuICAgIH1cbn1cblxuIiwiXG5cbmltcG9ydCBTcHJpdGUgPSBMYXlhLlNwcml0ZTtcbmltcG9ydCBUd2VlbiA9IExheWEuVHdlZW47XG5pbXBvcnQgRWFzZSA9IExheWEuRWFzZTtcbmltcG9ydCBIYW5kbGVyID0gTGF5YS5IYW5kbGVyO1xuaW1wb3J0IHsgVXRpbERpc3BsYXkgfSBmcm9tIFwiLi4vLi4vdXRpbC9kaXNwbGF5XCI7XG5pbXBvcnQgeyBFdmVudEZ1bmMgfSBmcm9tICcuLi9ldmVudC9ldmVudC1kYXRhJztcblxuZXhwb3J0IG1vZHVsZSBDdXN0b21EaWFsb2d7XG5cbiAgICAvKipcbiAgICAgKiBAYXV0aG9yIFN1blxuICAgICAqIEB0aW1lIDIwMTktMDgtMDkgMTc6NDFcbiAgICAgKiBAcHJvamVjdCBTRnJhbWV3b3JrX0xheWFBaXJcbiAgICAgKiBAZGVzY3JpcHRpb24gIFVJ57uE5Lu255qE5Z+657G777yM57un5om/6IeqTGF5YS5WaWV3XG4gICAgICpcbiAgICAgKi9cbiAgICBleHBvcnQgY2xhc3MgRGlhbG9nQmFzZSBleHRlbmRzIExheWEuRGlhbG9nIHtcbiAgICAgICAgXG4gICAgICAgIC8qKumBrue9qeWxgiAqL1xuICAgICAgICBwcml2YXRlIG1hc2tMYXllcjogU3ByaXRlID0gbnVsbDtcbiAgICAgICAgLyoq5by556qX5YaF54mp5L2TICovXG4gICAgICAgIHByaXZhdGUgY29udGVudFBubDogTGF5YS5Ob2RlID0gbnVsbDtcbiAgICAgICAgLyoq5by556qX5pWw5o2uICovXG4gICAgICAgIHB1YmxpYyBwb3B1cERhdGEgPSBuZXcgUG9wdXBEYXRhKCk7XG5cbiAgICAgICAgY3JlYXRlVmlldyh2aWV3OiBhbnkpOiB2b2lkIHtcbiAgICAgICAgICAgIHN1cGVyLmNyZWF0ZVZpZXcodmlldyk7XG4gICAgICAgIH1cblxuXG4gICAgICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICAgICAgc3VwZXIoKTtcbiAgICAgICAgICAgIHRoaXMuYnVuZGxlQnV0dG9ucygpO1xuXG4gICAgICAgICAgICB0aGlzLmNvbnRlbnRQbmwgPSB0aGlzLmdldENoaWxkQXQoMCk7XG4gICAgICAgIH1cblxuICAgICAgICAvKipcbiAgICAgICAgICog5re75Yqg6YGu572p5bGCXG4gICAgICAgICAqL1xuICAgICAgICBjcmF0ZU1hc2tMYXllcigpOiB2b2lkIHtcbiAgICAgICAgICAgIHRoaXMubWFza0xheWVyID0gVXRpbERpc3BsYXkuY3JlYXRlTWFza0xheWVyKCk7XG4gICAgICAgICAgICB0aGlzLm1hc2tMYXllci5tb3VzZUVuYWJsZWQgPSB0cnVlO1xuXG4gICAgICAgICAgICBsZXQgdCA9IHRoaXMubWFza0xheWVyO1xuICAgICAgICAgICAgdC54ID0gTWF0aC5yb3VuZCgoKExheWEuc3RhZ2Uud2lkdGggLSB0LndpZHRoKSA+PiAxKSArIHQucGl2b3RYKTtcbiAgICAgICAgICAgIHQueSA9IE1hdGgucm91bmQoKChMYXlhLnN0YWdlLmhlaWdodCAtIHQuaGVpZ2h0KSA+PiAxKSArIHQucGl2b3RZKTtcblxuICAgICAgICAgICAgdGhpcy5hZGRDaGlsZCh0aGlzLm1hc2tMYXllcik7XG4gICAgICAgICAgICB0aGlzLm1hc2tMYXllci56T3JkZXIgPSAtMTtcblxuICAgICAgICB9XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIOWcqOWcuuaZr+S4reWxheS4ree7hOS7tlxuICAgICAgICAgKi9cbiAgICAgICAgcHJvdGVjdGVkIGNlbnRlcih2aWV3PzogTGF5YS5TcHJpdGUpOiB2b2lkIHtcbiAgICAgICAgICAgIGlmICh2aWV3ID09IG51bGwpIHZpZXcgPSB0aGlzO1xuICAgICAgICAgICAgdmlldy54ID0gTWF0aC5yb3VuZCgoKExheWEuc3RhZ2Uud2lkdGggLSB2aWV3LndpZHRoKSA+PiAxKSArIHZpZXcucGl2b3RYKTtcbiAgICAgICAgICAgIHZpZXcueSA9IE1hdGgucm91bmQoKChMYXlhLnN0YWdlLmhlaWdodCAtIHZpZXcuaGVpZ2h0KSA+PiAxKSArIHZpZXcucGl2b3RZKTtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIOa3u+WKoOm7mOiupOaMiemSruS6i+S7tlxuICAgICAgICAgKi9cbiAgICAgICAgYnVuZGxlQnV0dG9ucygpOiB2b2lkIHtcbiAgICAgICAgICAgIGlmICh0aGlzW1wiYnRuQ2xvc2VcIl0gIT0gbnVsbCkge1xuICAgICAgICAgICAgICAgIHRoaXNbXCJidG5DbG9zZVwiXS5vbihMYXlhLkV2ZW50LkNMSUNLLCB0aGlzLCB0aGlzLmNsb3NlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiDlhbPpl63nqbrnmb3lpITngrnlh7vlhbPpl63kuovku7ZcbiAgICAgICAgICovXG4gICAgICAgIGNsb3NlT3V0c2llQ2xpY2soKXtcbiAgICAgICAgICAgIGlmICh0aGlzLm1hc2tMYXllciAhPSBudWxsKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5tYXNrTGF5ZXIub2ZmKExheWEuRXZlbnQuQ0xJQ0ssIHRoaXMsIHRoaXMuY2xvc2UpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIOWvueivneahhuW8ueWHuuaWueazlVxuICAgICAgICAgKiBAcGFyYW0gdGltZSDlvLnlh7rml7bpl7RcbiAgICAgICAgICogQHBhcmFtIGRhdGEg5pWw5o2uXG4gICAgICAgICAqIEBwYXJhbSBpc01hc2sg5piv5ZCm55Sf5oiQ6YGu572pXG4gICAgICAgICAqIEBwYXJhbSBjbG9zZU91dHNpZGUg5piv5ZCm54K55Ye756m655m95aSE5YWz6ZetXG4gICAgICAgICAqL1xuICAgICAgICBwb3B1cERpYWxvZyhwb3B1cERhdGE6UG9wdXBEYXRhID0gbnVsbCk6IHZvaWQge1xuICAgICAgICAgICAgLy8gdGhpcy5wb3B1cChmYWxzZSxmYWxzZSk7XG4gICAgICAgICAgICBpZihwb3B1cERhdGE9PW51bGwpIHtcbiAgICAgICAgICAgICAgICBwb3B1cERhdGEgPSB0aGlzLnBvcHVwRGF0YTtcbiAgICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgICAgIHRoaXMucG9wdXBEYXRhID0gcG9wdXBEYXRhO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgTGF5YS5zdGFnZS5hZGRDaGlsZCh0aGlzKTtcbiAgICAgICAgICAgIHRoaXMucG9wdXBJbml0KCk7XG4gICAgICAgICAgICBpZiAocG9wdXBEYXRhLmlzTWFzayAmJiB0aGlzLm1hc2tMYXllciA9PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5jcmF0ZU1hc2tMYXllcigpO1xuICAgICAgICAgICAgICAgIGlmICghcG9wdXBEYXRhLmNsb3NlT3V0c2lkZSkgdGhpcy5tYXNrTGF5ZXIub24oTGF5YS5FdmVudC5DTElDSywgdGhpcywgdGhpcy5jbG9zZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLm9uU2hvd0FuaW1hdGlvbihwb3B1cERhdGEudGltZSwoKT0+e1xuICAgICAgICAgICAgICAgIGlmKHBvcHVwRGF0YS5jYWxsQmFjaykgcG9wdXBEYXRhLmNhbGxCYWNrLmludm9rZSgpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICAvKiogRGVzOuW8ueWHuuiwg+eUqCAqL1xuICAgICAgICBwb3B1cEluaXQoKSB7XG4gICAgICAgIH1cblxuXG4gICAgICAgIG9uU2hvd0FuaW1hdGlvbih0aW1lOiBudW1iZXIgPSAzMDAsY2IpIHtcbiAgICAgICAgICAgIGxldCB0YXJnZXQgPSB0aGlzLmNvbnRlbnRQbmw7XG4gICAgICAgICAgICB0aGlzLmNlbnRlcigpO1xuICAgICAgICAgICAgXG4gICAgICAgICAgICAvLyBAdHMtaWdub3JlXG4gICAgICAgICAgICB0YXJnZXQuc2NhbGUoMCwgMCk7XG4gICAgICAgICAgICBUd2Vlbi50byh0YXJnZXQsIHtcbiAgICAgICAgICAgICAgICBzY2FsZVg6IDEsXG4gICAgICAgICAgICAgICAgc2NhbGVZOiAxXG4gICAgICAgICAgICB9LCB0aW1lLCBFYXNlLmJhY2tPdXQsIEhhbmRsZXIuY3JlYXRlKHRoaXMsIGNiLCBbdGhpc10pLCAwLCBmYWxzZSwgZmFsc2UpO1xuICAgICAgICB9XG5cbiAgICAgICAgY2xvc2UoKTogdm9pZCB7XG4gICAgICAgICAgICB0aGlzLnJlbW92ZVNlbGYoKTtcbiAgICAgICAgfVxuXG5cbiAgICB9XG59XG5cblxuICAgIC8qKlxuICAgICAqIEBhdXRob3IgU3VuXG4gICAgICogQHRpbWUgMjAxOS0wOC0xMiAxNzo0M1xuICAgICAqIEBwcm9qZWN0IFNGcmFtZXdvcmtfTGF5YUFpclxuICAgICAqIEBkZXNjcmlwdGlvbiAg56qX5L2T5by55Ye65pWw5o2uXG4gICAgICp0aW1lOiBudW1iZXIgPSAzMDAsIGRhdGE6IGFueSA9IG51bGwsIGlzTWFzazogYm9vbGVhbiA9IHRydWUsIGNsb3NlT3V0c2lkZTogYm9vbGVhbiA9IHRydWUsY2I/XG4gICAgICovXG4gICAgZXhwb3J0IGNsYXNzIFBvcHVwRGF0YXtcbiAgICAgICAgcHVibGljIHRpbWU6bnVtYmVyID0gMzAwO1xuICAgICAgICBwdWJsaWMgZGF0YTphbnkgPSBudWxsO1xuICAgICAgICBwdWJsaWMgaXNNYXNrOmJvb2xlYW4gPSB0cnVlO1xuICAgICAgICBwdWJsaWMgY2xvc2VPdXRzaWRlOmJvb2xlYW4gPSB0cnVlO1xuICAgICAgICBwdWJsaWMgY2FsbEJhY2s6RXZlbnRGdW5jID0gbnVsbDtcblxuICAgICAgICBjb25zdHJ1Y3Rvcih0aW1lOiBudW1iZXIgPSAzMDAsIGRhdGE6IGFueSA9IG51bGwsIGlzTWFzazogYm9vbGVhbiA9IHRydWUsIGNsb3NlT3V0c2lkZTogYm9vbGVhbiA9IHRydWUsY2I6RXZlbnRGdW5jID1udWxsKVxuICAgICAgICB7XG4gICAgICAgICAgICBpZih0aW1lIT1udWxsKSB0aGlzLnRpbWUgPSB0aW1lO1xuICAgICAgICAgICAgaWYoZGF0YSE9bnVsbCkgdGhpcy5kYXRhID0gZGF0YTtcbiAgICAgICAgICAgIGlmKGlzTWFzayE9bnVsbCkgdGhpcy5pc01hc2sgPSBpc01hc2s7XG4gICAgICAgICAgICBpZihjbG9zZU91dHNpZGUhPW51bGwpIHRoaXMuY2xvc2VPdXRzaWRlID0gY2xvc2VPdXRzaWRlO1xuICAgICAgICAgICAgaWYoY2IhPW51bGwpIHRoaXMuY2FsbEJhY2sgPSBjYjtcbiAgICAgICAgfVxuICAgIH0iLCJpbXBvcnQgeyBSZXNHcm91cCB9IGZyb20gJy4uL3Jlcy9yZXMtZ3JvdXAnO1xuaW1wb3J0IHsgUmVzTWFuYWdlciB9IGZyb20gJy4uL3Jlcy9yZXMtbWFuYWdlcic7XG5pbXBvcnQgeyBMb2cgfSBmcm9tICcuLi8uLi9jb3JlL2xvZyc7XG5pbXBvcnQgeyBUaW1lck1hbmFnZXIgfSBmcm9tICcuLi90aW1lci90aW1lci1tYW5hZ2VyJztcbmltcG9ydCB7IEV2ZW50RnVuYyB9IGZyb20gJy4uL2V2ZW50L2V2ZW50LWRhdGEnO1xuXG5leHBvcnQgbW9kdWxlIEN1c3RvbVNjZW5le1xuXG4gICAgLyoqXG4gICAgICogQGF1dGhvciBTdW5cbiAgICAgKiBAdGltZSAyMDE5LTA4LTA5IDE5OjEyXG4gICAgICogQHByb2plY3QgU0ZyYW1ld29ya19MYXlhQWlyXG4gICAgICogQGRlc2NyaXB0aW9uICBTY2VuZeeahOWfuuexu1xuICAgICAqXG4gICAgICovXG4gICAgZXhwb3J0IGNsYXNzIEx5U2NlbmUgZXh0ZW5kcyBMYXlhLlNjZW5lIHtcblxuICAgICAgICAvKipcbiAgICAgICAgICog5YaF5bWM5qih5byP56m655qE5Zy65pmv6LWE5rqQ77yM5b+F6aG75a6e546w6L+Z5LiqY3JlYXRlVmlld++8jOWQpuWImeaciemXrumimFxuICAgICAgICAgKi9cbiAgICAgICAgcHVibGljIHN0YXRpYyAgdWlWaWV3OmFueSA9e1widHlwZVwiOlwiU2NlbmVcIixcInByb3BzXCI6e1wid2lkdGhcIjoxMzM0LFwiaGVpZ2h0XCI6NzUwfSxcImxvYWRMaXN0XCI6W10sXCJsb2FkTGlzdDNEXCI6W119O1xuXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIOWcuuaZr+esrOS4gOS4quWKoOi9veeahOeql+WPo1xuICAgICAgICAgKi9cbiAgICAgICAgcHJvdGVjdGVkIGZpcnN0VmlldzogYW55ID0gbnVsbDtcbiAgICAgICAgLyoqXG4gICAgICAgICAqIOWcuuaZr+S+nei1lueahOi1hOa6kOe7hFxuICAgICAgICAgKi9cbiAgICAgICAgcHVibGljIG5lZWRMb2FkUmVzOiBSZXNHcm91cDtcblxuICAgICAgICBwcml2YXRlIG1fcGFyYW06IGFueTtcbiAgICAgICAgcHJpdmF0ZSBtX2xvYWRlZCA9IGZhbHNlO1xuXG4gICAgICAgIHB1YmxpYyBzY2VuZVRpbWVyczogQXJyYXk8bnVtYmVyPiA9IG5ldyBBcnJheTxudW1iZXI+KCk7XG5cbiAgICAgICAgcHVibGljIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICAgICAgc3VwZXIoKTtcbiAgICAgICAgICAgIHRoaXMubmVlZExvYWRSZXMgPSBuZXcgUmVzR3JvdXAoKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGNyZWF0ZUNoaWxkcmVuKCk6dm9pZCB7XG4gICAgICAgICAgICBzdXBlci5jcmVhdGVDaGlsZHJlbigpO1xuICAgICAgICAgICAgdGhpcy5jcmVhdGVWaWV3KEx5U2NlbmUudWlWaWV3KTtcbiAgICAgICAgICAgIHRoaXMud2lkdGggPSBMYXlhLnN0YWdlLndpZHRoO1xuICAgICAgICAgICAgdGhpcy5oZWlnaHQgPSBMYXlhLnN0YWdlLmhlaWdodDtcbiAgICAgICAgfVxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiDov5vlhaXlnLrmma9cbiAgICAgICAgICogQHBhcmFtIHBhcmFtIOWPguaVsCBcbiAgICAgICAgICogQHBhcmFtIHByb2dyZXNzRnVjIOi/m+W6puWbnuiwgyBcbiAgICAgICAgICogQHBhcmFtIGNvbXBsZXRlRnVjIOWujOaIkOWbnuiwg1xuICAgICAgICAgKi9cbiAgICAgICAgcHVibGljIGVudGVyKHBhcmFtOiBhbnkscHJvZ3Jlc3NGdWM6RXZlbnRGdW5jLGNvbXBsZXRlRnVjOkV2ZW50RnVuYykge1xuXG4gICAgICAgICAgICB0aGlzLm1fbG9hZGVkID0gZmFsc2U7XG4gICAgICAgICAgICB0aGlzLm1fcGFyYW0gPSBwYXJhbTtcbiAgICAgICAgICAgIHRoaXMub25Jbml0KHBhcmFtKTtcblxuICAgICAgICAgICAgUmVzTWFuYWdlci4kLmxvYWRHcm91cCh0aGlzLm5lZWRMb2FkUmVzLHByb2dyZXNzRnVjLGNvbXBsZXRlRnVjKTtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgcHVibGljIGxlYXZlKCkge1xuICAgICAgICAgICAgdGhpcy5vbkxlYXZlKCk7XG4gICAgICAgICAgICB0aGlzLmRlc3Ryb3koKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHB1YmxpYyBkZXN0cm95KCk6IHZvaWQge1xuICAgICAgICAgICAgdGhpcy5vbkNsZWFuKCk7XG4gICAgICAgICAgICB0aGlzLnNjZW5lVGltZXJzLmZvckVhY2goKHRpbWVyOiBudW1iZXIpID0+IHtcbiAgICAgICAgICAgICAgICBUaW1lck1hbmFnZXIuJC5yZW1vdmVUaW1lcih0aW1lcik7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgc3VwZXIuZGVzdHJveSgpO1xuICAgICAgICB9XG5cblxuICAgICAgICAvKipcbiAgICAgICAgICog5Yqg6L295a6M5oiQXG4gICAgICAgICAqIEBwYXJhbSBlcnJvciDliqDovb3plJnor69cbiAgICAgICAgICovXG4gICAgICAgIHByb3RlY3RlZCBsb2FkZWQoZXJyb3IpIHtcblxuICAgICAgICAgICAgaWYgKGVycm9yICE9IG51bGwpIHtcbiAgICAgICAgICAgICAgICBMb2cuZXJyb3IoZXJyb3IpXG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMub25Mb2FkZWQoKTtcbiAgICAgICAgICAgICAgICB0aGlzLm1fbG9hZGVkID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICB0aGlzLmNoZWNoRW50ZXIoKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICB9XG5cblxuICAgICAgICBwcml2YXRlIGNoZWNoRW50ZXIoKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5tX2xvYWRlZCkge1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLmZpcnN0VmlldyAhPSBudWxsKSB7XG4gICAgICAgICAgICAgICAgICAgIGxldCBjbHMgPSB0aGlzLmZpcnN0VmlldztcbiAgICAgICAgICAgICAgICAgICAgbGV0IHdpbiA9IG5ldyBjbHMoKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hZGRDaGlsZCh3aW4pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB0aGlzLm9uRW50ZXIodGhpcy5tX3BhcmFtKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIOWKoOi9veWujOaIkFxuICAgICAgICAgKi9cbiAgICAgICAgcHJvdGVjdGVkIG9uTG9hZGVkKCkge1xuXG4gICAgICAgIH1cblxuICAgICAgICAvKipcbiAgICAgICAgICog5Zy65pmv5Yid5aeL5YyWXG4gICAgICAgICAqIEBwYXJhbSBwYXJhbSDlj4LmlbBcbiAgICAgICAgICovXG4gICAgICAgIHByb3RlY3RlZCBvbkluaXQocGFyYW06IGFueSkge1xuXG4gICAgICAgIH1cblxuICAgICAgICAvKipcbiAgICAgICAgICog6L+b5YWl5Zy65pmvXG4gICAgICAgICAqL1xuICAgICAgICBwcm90ZWN0ZWQgb25FbnRlcihwYXJhbTogYW55KTogdm9pZCB7XG5cbiAgICAgICAgfVxuXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIOmAkOW4p+W+queOr1xuICAgICAgICAgKi9cbiAgICAgICAgcHVibGljIHVwZGF0ZSgpOiB2b2lkIHtcblxuICAgICAgICB9XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIOemu+W8gOWcuuaZr1xuICAgICAgICAgKi9cbiAgICAgICAgcHJvdGVjdGVkIG9uTGVhdmUoKTogdm9pZCB7XG5cbiAgICAgICAgfVxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiDlvZPlnLrmma/ooqvplIDmr4HnmoTml7blgJlcbiAgICAgICAgICovXG4gICAgICAgIHByb3RlY3RlZCBvbkNsZWFuKCk6IHZvaWQge1xuXG4gICAgICAgIH1cblxuICAgIH1cbn0iLCJpbXBvcnQgeyBEYXRhTWFuYWdlciB9IGZyb20gJy4uL2RhdGEvZGF0YS1tYW5hZ2VyJztcbmltcG9ydCB7IERhdGFCYXNlIH0gZnJvbSAnLi4vZGF0YS9kYXRhLWJhc2UnO1xuXG5leHBvcnQgbW9kdWxlIEN1c3RvbVZpZXd7XG5cbiAgICAvKipcbiAgICAgKiBAYXV0aG9yIFN1blxuICAgICAqIEB0aW1lIDIwMTktMDgtMDkgMTU6NTFcbiAgICAgKiBAcHJvamVjdCBTRnJhbWV3b3JrX0xheWFBaXJcbiAgICAgKiBAZGVzY3JpcHRpb24gIFVJ57uE5Lu255qE5Z+657G777yM57un5om/6IeqTGF5YS5WaWV3XG4gICAgICpcbiAgICAgKi9cbiAgICBleHBvcnQgY2xhc3MgVmlld0Jhc2UgZXh0ZW5kcyBMYXlhLlZpZXcge1xuXG4gICAgICAgIC8q5omA5pyJ5pWw5o2u6KeC5a+f6ICFKi9cbiAgICAgICAgcHJvdGVjdGVkIGRhdGFXYXRjaHM6IEFycmF5PHN0cmluZz4gPSBbXTtcblxuICAgICAgICBwdWJsaWMgZGF0YTogYW55ID0gbnVsbDtcblxuICAgICAgICAvL292ZXJyaWRlXG4gICAgICAgIGNyZWF0ZVZpZXcodmlldzogYW55KTogdm9pZCB7XG4gICAgICAgICAgICBzdXBlci5jcmVhdGVWaWV3KHZpZXcpO1xuICAgICAgICAgICAgdGhpcy5mdWxsU2NyZWVuKCk7XG4gICAgICAgICAgICB0aGlzLnBhcnNlRWxlbWVudCgpO1xuICAgICAgICB9XG5cbiAgICAgICAgb25EaXNhYmxlKCk6IHZvaWQge1xuXG4gICAgICAgICAgICB0aGlzLmRhdGFXYXRjaHMuZm9yRWFjaCgoY21kOiBzdHJpbmcpID0+IHtcbiAgICAgICAgICAgICAgICBEYXRhTWFuYWdlci4kLnJlbW92ZUV2ZW50TGlzdGVuZXIoY21kLCB0aGlzLm9uRGF0YSwgdGhpcyk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiDog4zmma/lm77pgILlupRcbiAgICAgICAgICovXG4gICAgICAgIHByb3RlY3RlZCBwYXJzZUVsZW1lbnQoKTogdm9pZCB7XG4gICAgICAgICAgICBpZiAodGhpc1tcImltZ0JnXCJdICE9IG51bGwpIHtcbiAgICAgICAgICAgICAgICBsZXQgaW1nQmcgPSB0aGlzW1wiaW1nQmdcIl0gYXMgTGF5YS5TcHJpdGVcbiAgICAgICAgICAgICAgICB0aGlzLmZ1bGxTY3JlZW4oaW1nQmcpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIOWcqOWcuuaZr+S4reWxheS4ree7hOS7tlxuICAgICAgICAgKi9cbiAgICAgICAgcHJvdGVjdGVkIGNlbnRlcih2aWV3PzogTGF5YS5TcHJpdGUpOiB2b2lkIHtcbiAgICAgICAgICAgIGlmICh2aWV3ID09IG51bGwpIHZpZXcgPSB0aGlzO1xuICAgICAgICAgICAgdmlldy54ID0gTWF0aC5yb3VuZCgoKExheWEuc3RhZ2Uud2lkdGggLSB2aWV3LndpZHRoKSA+PiAxKSArIHZpZXcucGl2b3RYKTtcbiAgICAgICAgICAgIHZpZXcueSA9IE1hdGgucm91bmQoKChMYXlhLnN0YWdlLmhlaWdodCAtIHZpZXcuaGVpZ2h0KSA+PiAxKSArIHZpZXcucGl2b3RZKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiDorr7nva7lpKflsI/kuLrlhajlsY9cbiAgICAgICAgICogQHBhcmFtIHZpZXcgTGF5YS5TcHJpdGVcbiAgICAgICAgICovXG4gICAgICAgIHByb3RlY3RlZCBmdWxsU2NyZWVuKHZpZXc/OiBMYXlhLlNwcml0ZSk6IHZvaWQge1xuICAgICAgICAgICAgaWYgKHZpZXcgPT0gbnVsbCkgdmlldyA9IHRoaXM7XG4gICAgICAgICAgICB2aWV3LndpZHRoID0gTGF5YS5zdGFnZS53aWR0aDtcbiAgICAgICAgICAgIHZpZXcuaGVpZ2h0ID0gTGF5YS5zdGFnZS5oZWlnaHQ7XG4gICAgICAgIH1cblxuICAgICAgICAvKipcbiAgICAgICAgICog57uR5a6a5pWw5o2u55uR5ZCsXG4gICAgICAgICAqIEBwYXJhbSBjbWQg55uR5ZCs57G75Z6LXG4gICAgICAgICAqL1xuICAgICAgICBwcm90ZWN0ZWQgYWRkRGF0YVdhdGNoKGNtZDogc3RyaW5nKSB7XG4gICAgICAgICAgICB0aGlzLmRhdGFXYXRjaHMucHVzaChjbWQpO1xuICAgICAgICAgICAgRGF0YU1hbmFnZXIuJC5hZGRFdmVudExpc3RlbmVyKGNtZCwgdGhpcy5vbkRhdGEsIHRoaXMpO1xuICAgICAgICAgICAgRGF0YU1hbmFnZXIuJC5nZXQoY21kKS5ub3RpZnkoKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiDlvZPmlbDmja7liLfmlrDmmK/ph43nu5hcbiAgICAgICAgICovXG4gICAgICAgIHByb3RlY3RlZCBvbkRhdGEoZGF0YTogRGF0YUJhc2UpIHtcbiAgICAgICAgICAgIC8vIGlmIChkYXRhLmNtZCA9PSBEYXRhRGVmaW5lLkNvaW5JbmZvKXtcbiAgICAgICAgICAgIC8vXG4gICAgICAgICAgICAvLyB9XG4gICAgICAgIH1cblxuICAgICAgICAvKipcbiAgICAgICAgICog5re75Yqg5Yiw55S75biDXG4gICAgICAgICAqIEBwYXJhbSBkYXRhIOaVsOaNriBcbiAgICAgICAgICovXG4gICAgICAgIGFkZChkYXRhOiBhbnkgPSBudWxsKVxuICAgICAgICB7XG4gICAgICAgICAgICB0aGlzLmRhdGEgPSBkYXRhO1xuICAgICAgICAgICAgTGF5YS5zdGFnZS5hZGRDaGlsZCh0aGlzKTtcbiAgICAgICAgICAgIHRoaXMuc2hvdygpO1xuICAgICAgICB9XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIOaYvuekunZpZXdcbiAgICAgICAgICovXG4gICAgICAgIHNob3coKTogdm9pZCB7XG4gICAgICAgICAgICB0aGlzLnZpc2libGUgPSB0cnVlO1xuICAgICAgICB9XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIOmakOiXj3ZpZXdcbiAgICAgICAgICovXG4gICAgICAgIGhpZGUoKTp2b2lke1xuICAgICAgICAgICAgdGhpcy52aXNpYmxlID0gZmFsc2U7XG4gICAgICAgIH1cblxuICAgIH1cbn1cbiIsImltcG9ydCB7IEV2ZW50Tm9kZSB9IGZyb20gJy4uL21hbmFnZXIvZXZlbnQvZXZlbnQtbm9kZSc7XG5pbXBvcnQgeyBDb25maWdMYXlvdXQsIENvbmZpZ1VJLCBDb25maWdEZWJ1ZywgQ29uZmlnR2FtZSwgQ29uZmlnVmVyc2lvbiwgQ29uZmlnUmVzIH0gZnJvbSAnLi4vc2V0dGluZy9jb25maWcnO1xuaW1wb3J0IHsgTG9nIH0gZnJvbSAnLi4vY29yZS9sb2cnO1xuaW1wb3J0IHsgVXRpbFRpbWUgfSBmcm9tICcuLi91dGlsL3RpbWUnO1xuaW1wb3J0IHsgZW51bURpbWVuc2lvbiwgZW51bUFsaWdlLCBlbnVtU2NyZWVuTW9kZWwsIGVudW1TY2FsZVR5cGUgfSBmcm9tICcuLi9zZXR0aW5nL2VudW0nO1xuaW1wb3J0IEJyb3dzZXIgPSBMYXlhLkJyb3dzZXI7XG5pbXBvcnQgeyBSZXNNYW5hZ2VyIH0gZnJvbSAnLi4vbWFuYWdlci9yZXMvcmVzLW1hbmFnZXInO1xuaW1wb3J0IHsgRXZlbnRGdW5jIH0gZnJvbSAnLi4vbWFuYWdlci9ldmVudC9ldmVudC1kYXRhJztcbmltcG9ydCB7IExvYWRpbmdWaWV3IH0gZnJvbSAnLi4vLi4vY2xpZW50L3ZpZXcvbGF5ZXItdmlldy9sb2FkaW5nLXZpZXcnO1xuaW1wb3J0IHsgRGF0YU1hbmFnZXIgfSBmcm9tICcuLi9tYW5hZ2VyL2RhdGEvZGF0YS1tYW5hZ2VyJztcbmltcG9ydCB7IEV2ZW50TWFuYWdlciB9IGZyb20gJy4uL21hbmFnZXIvZXZlbnQvZXZlbnQtbWFuYWdlcic7XG5pbXBvcnQgeyBKc29uTWFuYWdlciB9IGZyb20gJy4uL21hbmFnZXIvanNvbi9qc29uLW1hbmFnZXInO1xuaW1wb3J0IHsgU291bmRNYW5hZ2VyIH0gZnJvbSAnLi4vbWFuYWdlci9zb3VuZC9zb3VuZC1tYW5hZ2VyJztcbmltcG9ydCB7IFRpbWVyTWFuYWdlciB9IGZyb20gJy4uL21hbmFnZXIvdGltZXIvdGltZXItbWFuYWdlcic7XG5pbXBvcnQge0dhbWVTZXR0aW5nfSBmcm9tIFwiLi4vLi4vY2xpZW50L3NldHRpbmcvZ2FtZVNldHRpbmdcIjtcbi8qKlxuICogQGF1dGhvciBTdW5cbiAqIEB0aW1lIDIwMTktMDgtMTEgMTg6MDhcbiAqIEBwcm9qZWN0IFNGcmFtZXdvcmtfTGF5YUFpclxuICogQGRlc2NyaXB0aW9uIOahhuaetuWIneWni+WMluWSjOa4uOaIj+WFpeWPo1xuICpcbiAqL1xuZXhwb3J0IGNsYXNzIEVuZ2luZXtcblxuXG4gICAgcHVibGljIGxheW91dDogQ29uZmlnTGF5b3V0ID0gQ29uZmlnTGF5b3V0LiQ7XG4gICAgcHVibGljIGdhbWU6IENvbmZpZ0dhbWUgPSBDb25maWdHYW1lLiQ7XG4gICAgcHVibGljIHVpOiBDb25maWdVSSA9IENvbmZpZ1VJLiQ7XG4gICAgcHVibGljIGRlYnVnOiBDb25maWdEZWJ1ZyA9IENvbmZpZ0RlYnVnLiQ7XG5cblxuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgIH1cblxuICAgIHByaXZhdGUgc3RhdGljIGluc3RhbmNlOiBFbmdpbmUgPSBudWxsO1xuICAgIHB1YmxpYyBzdGF0aWMgZ2V0ICQoKTogRW5naW5lIHtcbiAgICAgICAgaWYgKHRoaXMuaW5zdGFuY2U9PW51bGwpIHRoaXMuaW5zdGFuY2UgPSBuZXcgRW5naW5lKCk7XG4gICAgICAgIHJldHVybiB0aGlzLmluc3RhbmNlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOW8leaTjuWQr+WKqOWFpeWPo1xuICAgICAqL1xuICAgIHB1YmxpYyBydW4oKTogdm9pZCB7XG4gICAgICAgIExvZy5pbmZvKFwiOjo6IEdhbWUgRW5naW5lIFJ1biA6OjpcIik7XG4gICAgICAgIEdhbWVTZXR0aW5nLiQuaW5pdCgpO1xuICAgICAgICBpZiAoQ29uZmlnVUkuJC5kZWZhdWx0TG9hZFZpZXcgIT0gbnVsbCAmJiBDb25maWdSZXMuJC5kZWZhdWx0TG9hZFJlcyAhPSBudWxsKSB7XG4gICAgICAgICAgICB0aGlzLmVuZ2luZVNldHVwKCgpPT57XG4gICAgICAgICAgICAgICAgLy/muLjmiI/lvIDlp4tcbiAgICAgICAgICAgICAgICBVdGlsVGltZS5zdGFydCgpO1xuICAgICAgICAgICAgICAgIC8v5Yid5aeL5YyW5ri45oiP566h55CG5ZmoXG4gICAgICAgICAgICAgICAgdGhpcy5tYW5hZ2VyU2V0VXAoKTtcbiAgICAgICAgICAgICAgICAvL+WIneWni+WMlua4uOaIj+S4u+W+queOr1xuICAgICAgICAgICAgICAgIExheWEudGltZXIuZnJhbWVMb29wKDEsIHRoaXMsIHRoaXMubWFuYWdlclVwZGF0ZSk7XG4gICAgICAgICAgICAgICAgLy/liqDovb1Mb2FkaW5n6aG155qE6buY6K6k6LWE5rqQ5bm25pi+56S6TG9hZGluZ+mhtVxuICAgICAgICAgICAgICAgIFJlc01hbmFnZXIuJC5sb2FkR3JvdXAoQ29uZmlnUmVzLiQuZGVmYXVsdExvYWRSZXMsbnVsbCxuZXcgRXZlbnRGdW5jKHRoaXMsKCk9PntcbiAgICAgICAgICAgICAgICAgICAgbGV0IHNjcnB0ID0gQ29uZmlnVUkuJC5kZWZhdWx0TG9hZFZpZXc7XG4gICAgICAgICAgICAgICAgICAgIGlmIChzY3JwdCAhPSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCBsb2FkaW5nVmlldyA9IG5ldyBzY3JwdCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgTGF5YS5zdGFnZS5hZGRDaGlsZChsb2FkaW5nVmlldyk7XG4gICAgICAgICAgICAgICAgICAgICAgICBsb2FkaW5nVmlldy5vblN0YXJ0KCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KSlcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICBcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgTG9nLmVycm9yKFwiRXJyb3I6TG9hZGluZ+i1hOa6kOS4uuepuuWKoOi9veWksei0pe+8gVwiKTtcbiAgICAgICAgfVxuICAgICAgIFxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOW8leaTjueahOWIneWni+WMluiuvue9rlxuICAgICAqL1xuICAgIHByaXZhdGUgZW5naW5lU2V0dXAoc3RhcnRDYWxsYmFjaylcbiAgICB7XG4gICAgICAgIC8qKuWIneWni+WMlkxheWEgKi9cbiAgICAgICAgaWYgKHRoaXMuZ2FtZS5kaW1lbnNpb24gPT0gZW51bURpbWVuc2lvbi5EaW0zKSB7XG4gICAgICAgICAgICBMYXlhM0QuaW5pdChDb25maWdMYXlvdXQuJC5kZXNpZ25XaWR0aCwgQ29uZmlnTGF5b3V0LiQuZGVzaWduSGVpZ2h0KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIExheWEuaW5pdChDb25maWdMYXlvdXQuJC5kZXNpZ25XaWR0aCwgQ29uZmlnTGF5b3V0LiQuZGVzaWduSGVpZ2h0LCBMYXlhLldlYkdMKTtcbiAgICAgICAgfVxuICAgICAgICAvKirog4zmma/popzoibIgKi9cbiAgICAgICAgTGF5YS5zdGFnZS5iZ0NvbG9yID0gXCJub25lXCI7XG4gICAgICAgIC8qKue8qeaUvuaooeW8jyAqL1xuICAgICAgICBMYXlhLnN0YWdlLnNjYWxlTW9kZSA9IGVudW1TY2FsZVR5cGUuU2NhbGVTaG93QWxsLnRvU3RyaW5nKCk7XG4gICAgICAgIC8qKuiuvue9ruWxj+W5leWkp+WwjyAqL1xuICAgICAgICBMYXlhLnN0YWdlLnNldFNjcmVlblNpemUoQnJvd3Nlci5jbGllbnRXaWR0aCwgQnJvd3Nlci5jbGllbnRIZWlnaHQpO1xuICAgICAgICAvKirorr7nva7mqKrnq5blsY8gKi9cbiAgICAgICAgTGF5YS5zdGFnZS5zY3JlZW5Nb2RlID0gZW51bVNjcmVlbk1vZGVsLlNjcmVlbk5vbmU7XG4gICAgICAgIC8qKuawtOW5s+Wvuem9kOaWueW8jyAqL1xuICAgICAgICBMYXlhLnN0YWdlLmFsaWduSCA9IGVudW1BbGlnZS5BbGlnZUNlbnRlcjtcbiAgICAgICAgLyoq5Z6C55u05a+56b2Q5pa55byPICovXG4gICAgICAgIExheWEuc3RhZ2UuYWxpZ25WID0gZW51bUFsaWdlLkFsaWdlTWlkZGxlO1xuICAgICAgICAvKirlvIDlkK/niannkIblvJXmk44gKi9cbiAgICAgICAgaWYoQ29uZmlnR2FtZS4kLnBoeXNpY3MpIExheWFbXCJQaHlzaWNzXCJdICYmIExheWFbXCJQaHlzaWNzXCJdLmVuYWJsZSgpO1xuXHRcdC8qKuaJk+W8gOiwg+ivlemdouadv++8iOmAmui/h0lEReiuvue9ruiwg+ivleaooeW8j++8jOaIluiAhXVybOWcsOWdgOWinuWKoGRlYnVnPXRydWXlj4LmlbDvvIzlnYflj6/miZPlvIDosIPor5XpnaLmnb/vvIkgKi9cbiAgICAgICAgaWYgKENvbmZpZ0RlYnVnLiQuaXNFbmFibGVEZWJ1Z1BhbmVsIHx8IExheWEuVXRpbHMuZ2V0UXVlcnlTdHJpbmcoXCJkZWJ1Z1wiKSA9PSBcInRydWVcIikgTGF5YS5lbmFibGVEZWJ1Z1BhbmVsKCk7XG4gICAgICAgIC8qKueJqeeQhui+heWKqee6vyAqL1xuICAgICAgICBpZiAoQ29uZmlnRGVidWcuJC5pc1BoeXNpY3NEZWJ1ZyAmJiBMYXlhW1wiUGh5c2ljc0RlYnVnRHJhd1wiXSkgTGF5YVtcIlBoeXNpY3NEZWJ1Z0RyYXdcIl0uZW5hYmxlKCk7XG4gICAgICAgIC8qKuaAp+iDveWQjOe6p+mdouadvyAqL1xuICAgICAgICBpZiAoQ29uZmlnRGVidWcuJC5pc1N0YXQpIExheWEuU3RhdC5zaG93KENvbmZpZ0RlYnVnLiQucGFuZWxYLENvbmZpZ0RlYnVnLiQucGFuZWxZKTtcbiAgICAgICAgLyoq5b6u5L+h5byA5pS+5Z+f5a2Q5Z+f6K6+572uKi9cbiAgICAgICAgaWYgKEJyb3dzZXIub25XZWlYaW4gfHwgQnJvd3Nlci5vbk1pbmlHYW1lKSB7XG4gICAgICAgICAgICBMYXlhLk1pbmlBZHB0ZXIuaW5pdCgpO1xuICAgICAgICAgICAgTGF5YS5pc1dYT3BlbkRhdGFDb250ZXh0ID0gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgLyoq5qih5byP56qX5Y+j54K55Ye76L6557yYICovXG4gICAgICAgIFVJQ29uZmlnLmNsb3NlRGlhbG9nT25TaWRlID0gdHJ1ZTtcbiAgICAgICAgLyoq5piv5ZCm5pi+56S65rua5Yqo5p2h5oyJ6ZKuICovXG4gICAgICAgIFVJQ29uZmlnLnNob3dCdXR0b25zID0gdHJ1ZTtcbiAgICAgICAgLyoq5oyJ6ZKu55qE54K55Ye75pWI5p6cICovXG4gICAgICAgIFVJQ29uZmlnLnNpbmdsZUJ1dHRvblN0eWxlID0gXCJzY2FsZVwiOyAvL1wiY29sb3JcIixcInNjYWxlXCJcbiAgICAgICAgLyoq5by55Ye65qGG6IOM5pmv6YCP5piO5bqmICovXG4gICAgICAgIFVJQ29uZmlnLnBvcHVwQmdBbHBoYSA9IDAuNzU7XG4gICAgICAgIC8qKuWFvOWuuVNjZW5l5ZCO57yA5Zy65pmvICovXG4gICAgICAgIExheWEuVVJMLmV4cG9ydFNjZW5lVG9Kc29uID0gdHJ1ZTtcbiAgICAgICAgLyoq5piv5ZCm5byA5ZCv54mI5pys566h55CGICovXG4gICAgICAgIGlmKENvbmZpZ1ZlcnNpb24uJC5pc09wZW5WZXJzaW9uKXtcbiAgICAgICAgICAgIExheWEuUmVzb3VyY2VWZXJzaW9uLmVuYWJsZShDb25maWdWZXJzaW9uLiQudmVyc2lvbkZsb2RlcixcbiAgICAgICAgICAgIExheWEuSGFuZGxlci5jcmVhdGUodGhpcywgc3RhcnRDYWxsYmFjayksIExheWEuUmVzb3VyY2VWZXJzaW9uLkZJTEVOQU1FX1ZFUlNJT04pO1xuICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgIHN0YXJ0Q2FsbGJhY2suY2FsbCgpO1xuICAgICAgICB9XG4gICAgICAgXG5cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDnrqHnkIblmajnmoTliJ3lp4vljJZcbiAgICAgKi9cbiAgICBwcml2YXRlICBtYW5hZ2VyU2V0VXAoKTogdm9pZCB7XG4gICAgICAgIERhdGFNYW5hZ2VyLiQuc2V0dXAoKTtcbiAgICAgICAgRXZlbnRNYW5hZ2VyLiQuc2V0dXAoKTtcbiAgICAgICAgUmVzTWFuYWdlci4kLnNldHVwKCk7XG4gICAgICAgIEpzb25NYW5hZ2VyLiQuc2V0dXAoKTtcbiAgICAgICAgU291bmRNYW5hZ2VyLiQuc2V0dXAoKTtcbiAgICAgICAgVGltZXJNYW5hZ2VyLiQuc2V0dXAoKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDnrqHnkIblmajnmoRVcGRhdGVcbiAgICAgKi9cbiAgICBwcml2YXRlIG1hbmFnZXJVcGRhdGUoKTogdm9pZCB7XG4gICAgICAgXG4gICAgfVxuXG59IiwiaW1wb3J0IEJyb3dzZXIgPSBsYXlhLnV0aWxzLkJyb3dzZXI7XG5pbXBvcnQgeyBlbnVtRGltZW5zaW9uLCBlbnVtU2NhbGVUeXBlLCBlbnVtSnNvbkRlZmluZSwgZW51bVNvdW5kTmFtZSB9IGZyb20gJy4vZW51bSc7XG5pbXBvcnQgeyBTaW5nbGV0b24gfSBmcm9tICcuLi9jb3JlL3NpbmdsZXRvbic7XG5pbXBvcnQgeyBNYWluU2NlbmUgfSBmcm9tICcuLi8uLi9jbGllbnQvc2NlbmUvbWFpbi1zY2VuZSc7XG5pbXBvcnQgeyBSZXNHcm91cCB9IGZyb20gJy4uL21hbmFnZXIvcmVzL3Jlcy1ncm91cCc7XG5pbXBvcnQgeyBMb2FkaW5nVmlldyB9IGZyb20gJy4uLy4uL2NsaWVudC92aWV3L2xheWVyLXZpZXcvbG9hZGluZy12aWV3JztcbmltcG9ydCB7IEpzb25UZW1wbGF0ZSB9IGZyb20gJy4uL21hbmFnZXIvanNvbi9qc29uLXRlbXBsYXRlJztcbmltcG9ydCB7IFNvdW5kVGVtcGxhdGUgfSBmcm9tICcuLi9tYW5hZ2VyL3NvdW5kL3NvdW5kLXRlbXBsYXRlJztcbiAvKipcbiAqIEBhdXRob3IgU3VuXG4gKiBAdGltZSAyMDE5LTA4LTA5IDE0OjAxXG4gKiBAcHJvamVjdCBTRnJhbWV3b3JrX0xheWFBaXJcbiAqIEBkZXNjcmlwdGlvbiDmuLjmiI/phY3nva7kv6Hmga9cbiAqL1xuXG5cbi8qKlxuICog55WM6Z2i6YWN572uXG4gKi9cbmV4cG9ydCBjbGFzcyBDb25maWdVSSBleHRlbmRzIFNpbmdsZXRvbiB7XG5cbiAgICAvKirpu5jorqTlrZfkvZMgKi9cbiAgICBwdWJsaWMgZGVmYXVsdEZvbnROYW1lOiBzdHJpbmcgPSAn6buR5L2TJztcbiAgICAvKirpu5jorqTlrZfkvZPlpKflsI8gKi9cbiAgICBwdWJsaWMgZGVmYXVsdEZvbnRTaXplOiBudW1iZXIgPSAxNjtcbiAgICAvKirpu5jorqTliqDovb3lnLrmma8gKi9cbiAgICBwdWJsaWMgZGVmYXVsdE1haW5TY2VuZTogYW55ID0gTWFpblNjZW5lO1xuICAgIC8qKum7mOiupOWKoOi9veeahExvYWRpbmfpobXpnaIgKi9cbiAgICBwdWJsaWMgZGVmYXVsdExvYWRWaWV3OiBhbnkgPSBMb2FkaW5nVmlldztcbiAgIFxuXG4gICAgcHJpdmF0ZSBzdGF0aWMgaW5zdGFuY2U6IENvbmZpZ1VJID0gbnVsbDtcbiAgICBwdWJsaWMgc3RhdGljIGdldCAkKCk6Q29uZmlnVUkge1xuICAgICAgICBpZiAoIXRoaXMuaW5zdGFuY2UpIHRoaXMuaW5zdGFuY2UgPSBuZXcgQ29uZmlnVUkoKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuaW5zdGFuY2U7XG4gICAgfVxuICAgXG59XG5cbi8qKlxuICog6LWE5rqQ6YWN572uXG4gKi9cbmV4cG9ydCBjbGFzcyBDb25maWdSZXMgZXh0ZW5kcyBTaW5nbGV0b257XG5cbiAgICAvKirpu5jorqRMb2FkaW5n6aG16Z2i55qE6LWE5rqQ5L+h5oGvICovXG4gICAgcHVibGljIGRlZmF1bHRMb2FkUmVzOiBSZXNHcm91cCA9IG5ldyBSZXNHcm91cCgpO1xuICAgIC8qKum7mOiupOeahOWfuuehgOmhtemdoui1hOa6kOS/oeaBryAqL1xuICAgIHB1YmxpYyBkZWZhdWx0TWFpblJlczpSZXNHcm91cCA9IG5ldyBSZXNHcm91cCgpO1xuXG4gICAgcHJpdmF0ZSBzdGF0aWMgaW5zdGFuY2U6IENvbmZpZ1JlcyA9IG51bGw7XG4gICAgcHVibGljIHN0YXRpYyBnZXQgJCgpOkNvbmZpZ1JlcyB7XG4gICAgICAgIGlmICghdGhpcy5pbnN0YW5jZSkgdGhpcy5pbnN0YW5jZSA9IG5ldyBDb25maWdSZXMoKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuaW5zdGFuY2U7XG4gICAgfVxuXG4gICAgY29uc3RydWN0b3IoKXtcbiAgICAgICAgc3VwZXIoKTtcbiAgICAgICAgLy/liqDovb1Kc29u6YWN572u5paH5Lu2XG4gICAgICAgIENvbmZpZ0RhdGEuJC5qc29uVGVtcGxhdGVMaXN0LmZvckVhY2goaXRlbT0+e1xuICAgICAgICAgICAgdGhpcy5kZWZhdWx0TWFpblJlc1xuICAgICAgICAgICAgLmFkZChpdGVtLnVybCwgTGF5YS5Mb2FkZXIuSlNPTik7XG4gICAgICAgIH0pO1xuICAgICAgICAvL+WKoOi9vemfs+aViOi1hOa6kFxuICAgICAgICBDb25maWdTb3VuZC4kLnNvdW5kUmVzTGlzdC5mb3JFYWNoKGl0ZW09PntcbiAgICAgICAgICAgIHRoaXMuZGVmYXVsdE1haW5SZXNcbiAgICAgICAgICAgIC5hZGQoaXRlbS51cmwsIExheWEuTG9hZGVyLlNPVU5EKTtcbiAgICAgICAgfSk7XG4gICAgfVxufVxuXG4vKipcbiAqIOWjsOmfs+mFjee9rlxuICovXG5leHBvcnQgY2xhc3MgQ29uZmlnU291bmQgZXh0ZW5kcyBTaW5nbGV0b24ge1xuXG4gICAgLyoq6IOM5pmv6Z+z5LmQ5ZCN5a2XICovXG4gICAgcHVibGljIGJnU291bmROYW1lID0gXCJcIjtcbiAgICAvKirog4zmma/pn7PlvIDlhbMgKi9cbiAgICBwdWJsaWMgaXNDbG9zZUJHU291bmQgPSBmYWxzZTtcbiAgICAvKirmlYjmnpzpn7PlvIDlhbMgKi9cbiAgICBwdWJsaWMgaXNDbG9zZUVmZmVjdFNvdW5kID0gZmFsc2U7XG4gICAgLyoq5omA5pyJ6Z+z5pWI5byA5YWzICovXG4gICAgcHVibGljIGlzQ2xvc2VWb2ljZVNvdW5kID0gZmFsc2U7XG4gICAgLyoq5oC76Z+z6YePICovXG4gICAgcHVibGljIHZvbHVtZVZvaWNlU291bmQgPSAxO1xuICAgIC8qKumfs+aViOi1hOa6kCAqL1xuICAgIHB1YmxpYyBzb3VuZFJlc0xpc3Q6QXJyYXk8U291bmRUZW1wbGF0ZT4gPSBudWxsO1xuICBcbiAgICBwcml2YXRlIHN0YXRpYyBpbnN0YW5jZTogQ29uZmlnU291bmQgPSBudWxsO1xuICAgIHB1YmxpYyBzdGF0aWMgZ2V0ICQoKTpDb25maWdTb3VuZCB7XG4gICAgICAgIGlmICghdGhpcy5pbnN0YW5jZSkgdGhpcy5pbnN0YW5jZSA9IG5ldyBDb25maWdTb3VuZCgpO1xuICAgICAgICByZXR1cm4gdGhpcy5pbnN0YW5jZTtcbiAgICB9XG5cbiAgICBjb25zdHJ1Y3RvcigpXG4gICAge1xuICAgICAgICBzdXBlcigpO1xuICAgICAgICB0aGlzLnNvdW5kUmVzTGlzdCA9IG5ldyBBcnJheTxTb3VuZFRlbXBsYXRlPigpO1xuICAgICAgICAvLyB0aGlzLnNvdW5kUmVzTGlzdC5wdXNoKG5ldyBTb3VuZFRlbXBsYXRlKFwicmVzL3NvdW5kL2JnLm1wM1wiLGVudW1Tb3VuZE5hbWUuYmcpKTtcbiAgICB9XG59XG5cbi8qKlxuICog5pWw5o2u6KGo6YWN572uXG4gKi9cbmV4cG9ydCBjbGFzcyBDb25maWdEYXRhIGV4dGVuZHMgU2luZ2xldG9ue1xuXG4gICAgLyoqanNvbumFjee9ruihqOS/oeaBryAqL1xuICAgIHB1YmxpYyBqc29uVGVtcGxhdGVMaXN0OkFycmF5PEpzb25UZW1wbGF0ZT4gPSBuZXcgQXJyYXk8SnNvblRlbXBsYXRlPigpO1xuICAgIGNvbnN0cnVjdG9yKClcbiAgICB7XG4gICAgICAgIHN1cGVyKCk7XG4gICAgfVxuICAgIHByaXZhdGUgc3RhdGljIGluc3RhbmNlOiBDb25maWdEYXRhID0gbnVsbDtcbiAgICBwdWJsaWMgc3RhdGljIGdldCAkKCk6Q29uZmlnRGF0YSB7XG4gICAgICAgIGlmICghdGhpcy5pbnN0YW5jZSkgdGhpcy5pbnN0YW5jZSA9IG5ldyBDb25maWdEYXRhKCk7XG4gICAgICAgIHJldHVybiB0aGlzLmluc3RhbmNlO1xuICAgIH1cbn1cblxuLyoqXG4gKiDmuLjmiI/phY3nva5cbiAqL1xuZXhwb3J0IGNsYXNzIENvbmZpZ0dhbWUgZXh0ZW5kcyBTaW5nbGV0b24ge1xuIFxuICAgIC8qKum7mOiupOaooeW8j+S/oeaBryAyRC8zRCAqL1xuICAgIHB1YmxpYyBkaW1lbnNpb246IGVudW1EaW1lbnNpb24gPSBlbnVtRGltZW5zaW9uLkRpbTI7XG4gICAgLyoq54mp55CG5byA5YWzICovXG4gICAgcHVibGljIHBoeXNpY3M6Ym9vbGVhbiA9IGZhbHNlO1xuICBcbiAgICBcbiAgICBwcml2YXRlIHN0YXRpYyBpbnN0YW5jZTogQ29uZmlnR2FtZSA9IG51bGw7XG4gICAgcHVibGljIHN0YXRpYyBnZXQgJCgpOkNvbmZpZ0dhbWUge1xuICAgICAgICBpZiAoIXRoaXMuaW5zdGFuY2UpIHRoaXMuaW5zdGFuY2UgPSBuZXcgQ29uZmlnR2FtZSgpO1xuICAgICAgICByZXR1cm4gdGhpcy5pbnN0YW5jZTtcbiAgICB9XG59XG5cbi8qKlxuICog54mI5pys6YWN572uXG4gKi9cbmV4cG9ydCBjbGFzcyBDb25maWdWZXJzaW9uIGV4dGVuZHMgU2luZ2xldG9uIHtcbiBcbiAgICAvKirniYjmnKzmjqfliLblvIDlhbMgKi9cbiAgICBwdWJsaWMgaXNPcGVuVmVyc2lvbjpib29sZWFuID0gZmFsc2U7XG4gICAgLyoq54mI5pys5Y+3ICovXG4gICAgcHVibGljIHZlcnNpb25OdW06bnVtYmVyID0gMDtcbiAgICAvKirniYjmnKzmjqfliLbmlofku7blkI0gKi9cbiAgICBwdWJsaWMgdmVyc2lvbkZsb2RlcjpzdHJpbmcgPSBcIlZlcnNpb25cIit0aGlzLnZlcnNpb25OdW07XG4gICAgXG4gICAgcHJpdmF0ZSBzdGF0aWMgaW5zdGFuY2U6IENvbmZpZ1ZlcnNpb24gPSBudWxsO1xuICAgIHB1YmxpYyBzdGF0aWMgZ2V0ICQoKTpDb25maWdWZXJzaW9uIHtcbiAgICAgICAgaWYgKCF0aGlzLmluc3RhbmNlKSB0aGlzLmluc3RhbmNlID0gbmV3IENvbmZpZ1ZlcnNpb24oKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuaW5zdGFuY2U7XG4gICAgfVxufVxuXG5cbi8qKlxuICog5biD5bGA6YWN572uXG4gKi9cbmV4cG9ydCBjbGFzcyBDb25maWdMYXlvdXQgZXh0ZW5kcyBTaW5nbGV0b24ge1xuXG4gICAgLyoq6K6+6K6h5YiG6L6o546HWCAqL1xuICAgIHB1YmxpYyBkZXNpZ25XaWR0aDogbnVtYmVyID0gNzUwO1xuICAgIC8qKuiuvuiuoeWIhui+qOeOh1kgKi9cbiAgICBwdWJsaWMgZGVzaWduSGVpZ2h0OiBudW1iZXIgPSAxMzM0O1xuICAgIC8qKue8qeaUvuaooeW8jyAqL1xuICAgIHB1YmxpYyBzY2FsZU1vZGU6IGVudW1TY2FsZVR5cGUgPSBlbnVtU2NhbGVUeXBlLlNjYWxlRml4ZWRBdXRvO1xuXG4gICAgcHJpdmF0ZSBzdGF0aWMgaW5zdGFuY2U6IENvbmZpZ0xheW91dCA9IG51bGw7XG4gICAgcHVibGljIHN0YXRpYyBnZXQgJCgpOkNvbmZpZ0xheW91dCB7XG4gICAgICAgIGlmICghdGhpcy5pbnN0YW5jZSkgdGhpcy5pbnN0YW5jZSA9IG5ldyBDb25maWdMYXlvdXQoKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuaW5zdGFuY2U7XG4gICAgfVxuXG59XG5cblxuLyoqXG4gKiBEZWJ1Z+mFjee9rlxuICovXG5leHBvcnQgY2xhc3MgQ29uZmlnRGVidWcgZXh0ZW5kcyBTaW5nbGV0b24ge1xuXG4gICAgLyoq6LCD6K+V5L+h5oGv5byA5YWzICovXG4gICAgcHVibGljIGlzRGVidWc6IGJvb2xlYW4gPSB0cnVlO1xuICAgIC8qKueJqeeQhui+heWKqee6v+W8gOWFsyAqL1xuICAgIHB1YmxpYyBpc1BoeXNpY3NEZWJ1ZzogYm9vbGVhbiA9IGZhbHNlOyBcbiAgICAvKirosIPor5XpnaLmnb8gKi9cbiAgICBwdWJsaWMgaXNFbmFibGVEZWJ1Z1BhbmVsOmJvb2xlYW4gPSBmYWxzZTtcbiAgICAvKirmgKfog73pnaLmnb/lvIDlhbMgKi9cbiAgICBwdWJsaWMgaXNTdGF0OiBib29sZWFuID0gdHJ1ZTtcbiAgICAvKirmgKfog73nu5/orqHpnaLmnb9YICovXG4gICAgcHVibGljIHBhbmVsWDpudW1iZXIgPSAwO1xuICAgIC8qKuaAp+iDvee7n+iuoemdouadv1kgKi9cbiAgICBwdWJsaWMgcGFuZWxZOm51bWJlciA9IDEwMDtcblxuICAgIHByaXZhdGUgc3RhdGljIGluc3RhbmNlOiBDb25maWdEZWJ1ZyA9IG51bGw7XG4gICAgcHVibGljIHN0YXRpYyBnZXQgJCgpOkNvbmZpZ0RlYnVnIHtcbiAgICAgICAgaWYgKCF0aGlzLmluc3RhbmNlKSB0aGlzLmluc3RhbmNlID0gbmV3IENvbmZpZ0RlYnVnKCk7XG4gICAgICAgIHJldHVybiB0aGlzLmluc3RhbmNlO1xuICAgIH1cbn1cblxuLyoqXG4gKiAzROmFjee9rlxuICovXG5leHBvcnQgY2xhc3MgQ29uZmlnM0QgZXh0ZW5kcyBTaW5nbGV0b257XG5cbiAgICAvKirlnLrmma/otYTmupDot6/lvoQgKi9cbiAgICBwdWJsaWMgc2NlbmVQYXRoOnN0cmluZyA9IFwicmVzL3UzZC9MYXlhU2NlbmVfTWFpbi9Db252ZW50aW9uYWwvTWFpbi5sc1wiO1xuXG4gICAgcHJpdmF0ZSBzdGF0aWMgaW5zdGFuY2U6IENvbmZpZzNEID0gbnVsbDtcbiAgICBwdWJsaWMgc3RhdGljIGdldCAkKCk6Q29uZmlnM0Qge1xuICAgICAgICBpZiAoIXRoaXMuaW5zdGFuY2UpIHRoaXMuaW5zdGFuY2UgPSBuZXcgQ29uZmlnM0QoKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuaW5zdGFuY2U7XG4gICAgfVxufVxuXG5cblxuLy8gLyoqXG4vLyAgKiBOZXR3b3Jr6YWN572uXG4vLyAgKi9cbi8vIGV4cG9ydCBjbGFzcyBDb25maWdOZXQgZXh0ZW5kcyBTaW5nbGV0b24ge1xuXG4vLyAgICAgcHVibGljIGh0dHBVcmw6IHN0cmluZyA9IFwiaHR0cDovLzEyNy4wLjAuMTozNDU2OFwiO1xuLy8gICAgIHB1YmxpYyB3c1VybDogc3RyaW5nID0gXCJ3c3M6Ly93eC5kb25vcG8uY29tL3dzL3dzXCI7XG4vLyAgICAgcHVibGljIHJlc1VybDogc3RyaW5nID0gXCJ3czovLzEyNy4wLjAuMToxNjY2OVwiO1xuLy8gICAgIHB1YmxpYyB0aW1lT3V0OiBudW1iZXIgPSAxMDtcbi8vICAgICBwdWJsaWMgaGVhcnRCZWF0OiBudW1iZXIgPSAxMDtcbi8vICAgICBwdWJsaWMgc2VydmVySGVhcnRCZWF0OiBudW1iZXIgPSAzO1xuXG4vLyAgICAgcHJpdmF0ZSBzdGF0aWMgaW5zdGFuY2U6IENvbmZpZ05ldCA9IG51bGw7XG5cbi8vICAgICBwdWJsaWMgc3RhdGljIGdldCAkKCk6Q29uZmlnTmV0IHtcbi8vICAgICAgICAgaWYgKCF0aGlzLmluc3RhbmNlKSB0aGlzLmluc3RhbmNlID0gbmV3IENvbmZpZ05ldCgpO1xuLy8gICAgICAgICByZXR1cm4gdGhpcy5pbnN0YW5jZTtcbi8vICAgICB9XG5cbi8vIH1cblxuLy8gLyoqXG4vLyAgKiDlvq7kv6HphY3nva5cbi8vICAqL1xuLy8gZXhwb3J0IGNsYXNzIENvbmZXZWNoYXQgZXh0ZW5kcyBTaW5nbGV0b24ge1xuXG4vLyAgICAgcHVibGljIGFwcGlkOiBzdHJpbmcgPSBcIlwiO1xuLy8gICAgIHB1YmxpYyBzZWNyZXQ6IHN0cmluZyA9IFwiXCI7XG4vLyAgICAgcHVibGljIGFkVW5pdElkOiBzdHJpbmcgPSBcIlwiO1xuLy8gICAgIHB1YmxpYyBjb2RlMnNlc3Npb25VcmwgPSBcImh0dHBzOi8vYXBpLndlaXhpbi5xcS5jb20vc25zL2pzY29kZTJzZXNzaW9uP2FwcGlkPXswfSZzZWNyZXQ9ezF9JmpzX2NvZGU9ezJ9JmdyYW50X3R5cGU9YXV0aG9yaXphdGlvbl9jb2RlXCI7XG5cblxuLy8gICAgIHByaXZhdGUgc3RhdGljIGluc3RhbmNlOiBDb25mV2VjaGF0ID0gbnVsbDtcblxuLy8gICAgIHB1YmxpYyBzdGF0aWMgZ2V0ICQoKTpDb25mV2VjaGF0IHtcbi8vICAgICAgICAgaWYgKCF0aGlzLmluc3RhbmNlKSB0aGlzLmluc3RhbmNlID0gbmV3IENvbmZXZWNoYXQoKTtcbi8vICAgICAgICAgcmV0dXJuIHRoaXMuaW5zdGFuY2U7XG4vLyAgICAgfVxuLy8gfVxuIiwiLyoqXG4gKiDph43opoHnmoTmnprkuL7lrprkuYks5qGG5p6257qn5YirXG4gKlxuICogQGF1dGhvciBUaW0gV2Fyc1xuICogQGRhdGUgMjAxOS0wMS0xOCAxNjoyMFxuICogQHByb2plY3QgZmlyZWJvbHRcbiAqIEBjb3B5cmlnaHQgKEMpIERPTk9QT1xuICpcbiAqL1xuXG5pbXBvcnQgU3RhZ2UgPSBMYXlhLlN0YWdlO1xuXG4vKipcbiAqIOiInuWPsOeahOe8qeaUvuagvOW8j1xuICovXG5leHBvcnQgZW51bSBlbnVtU2NhbGVUeXBlIHtcbiAgICAvLyBAdHMtaWdub3JlXG4gICAgU2NhbGVOb1NjYWxlID0gU3RhZ2UuU0NBTEVfRlVMTCxcbiAgICAvLyBAdHMtaWdub3JlXG4gICAgU2NhbGVFeGFjdEZpdCA9IFN0YWdlLlNDQUxFX0VYQUNURklULFxuICAgIC8vIEB0cy1pZ25vcmVcbiAgICBTY2FsZVNob3dBbGwgPSBTdGFnZS5TQ0FMRV9TSE9XQUxMLFxuICAgIC8vIEB0cy1pZ25vcmVcbiAgICBTY2FsZU5vQm9yZGVyID0gU3RhZ2UuU0NBTEVfTk9CT1JERVIsXG4gICAgLy8gQHRzLWlnbm9yZVxuICAgIFNjYWxlRnVsbCA9IFN0YWdlLlNDQUxFX0ZVTEwsXG4gICAgLy8gQHRzLWlnbm9yZVxuICAgIFNjYWxlRml4ZWRXaWR0aCA9IFN0YWdlLlNDQUxFX0ZJWEVEX1dJRFRILFxuICAgIC8vIEB0cy1pZ25vcmVcbiAgICBTY2FsZUZpeGVkSGVpZ2h0ID0gU3RhZ2UuU0NBTEVfRklYRURfSEVJR0hULFxuICAgIC8vIEB0cy1pZ25vcmVcbiAgICBTY2FsZUZpeGVkQXV0byA9IFN0YWdlLlNDQUxFX0ZJWEVEX0FVVE8sXG4gICAgLy8gQHRzLWlnbm9yZVxuICAgIFNjYWxlTm9TY2FsZSA9IFN0YWdlLlNDQUxFX05PU0NBTEVcbn1cblxuLyoqXG4gKiDlsY/luZXnmoToh6rpgILlupTmlrnlvI9cbiAqL1xuZXhwb3J0IGVudW0gZW51bVNjcmVlbk1vZGVsIHtcbiAgICBTY3JlZW5Ob25lID0gJ25vbmUnLFxuICAgIFNjcmVlbkhvcml6b250YWwgPSAnaG9yaXpvbnRhbCcsXG4gICAgU2NyZWVuVmVydGljYWwgPSAndmVydGljYWwnXG59XG5cbi8qKlxuICog5pWw57uE5o6S5bqP5pa55byPXG4gKiAqL1xuZXhwb3J0IGVudW0gZW51bUFycmF5U29ydE9yZGVyIHtcbiAgICBBc2NlbmRpbmcsXHQvL+WNh+W6j1xuICAgIERlc2NlbmRpbmcsXHQvL+mZjeW6j1xufVxuXG4vKipcbiAqIOa4uOaIj+eahOi/kOihjOWuueWZqFxuICovXG5leHBvcnQgZW51bSBlbnVtR2FtZVBsYXRmb3JtIHtcbiAgICBXZWIsXG4gICAgUGhvbmUsXG4gICAgV2VpeGluXG59XG5cbi8qKlxuICog5a+56b2Q5pa55byPXG4gKi9cbmV4cG9ydCBlbnVtIGVudW1BbGlnZVR5cGUge1xuICAgIE5PTkUgPSAwLFxuICAgIFJJR0hULFxuICAgIFJJR0hUX0JPVFRPTSxcbiAgICBCT1RUT00sXG4gICAgTEVGVF9CT1RUT00sXG4gICAgTEVGVCxcbiAgICBMRUZUX1RPUCxcbiAgICBUT1AsXG4gICAgUklHSFRfVE9QLFxuICAgIE1JRCxcbn1cblxuLyoqXG4gKiDlr7npvZDmoIfms6hcbiAqL1xuZXhwb3J0IGVudW0gZW51bUFsaWdlIHtcbiAgICBBbGlnZUxlZnQgPSAnbGVmdCcsXG4gICAgQWxpZ2VDZW50ZXIgPSAnY2VudGVyJyxcbiAgICBBbGlnZVJpZ2h0ID0gJ3JpZ2h0JyxcbiAgICBBbGlnZVRvcCA9ICd0b3AnLFxuICAgIEFsaWdlTWlkZGxlID0gJ21pZGRsZScsXG4gICAgQWxpZ2VCb3R0b20gPSAnYm90dG9tJ1xufVxuXG4vKipcbiAqIOa4heeQhui1hOa6kOeahOasoeW6j+etlueVpVxuICovXG5leHBvcnQgZW51bSBlbnVtQ2xlYXJTdHJhdGVneSB7XG4gICAgRklGTyA9IDAsICAgLy/lhYjov5vlhYjlh7pcbiAgICBGSUxPLCAgICAgICAvL+WFiOi/m+WQjuWHulxuICAgIExSVSxcdFx0Ly/mnIDov5HmnIDlsJHkvb/nlKhcbiAgICBVTl9VU0VELFx0Ly/mnKrkvb/nlKhcbiAgICBBTEwsXHRcdC8v5riF55CG5omA5pyJXG59XG5cbi8qKlxuICog5ri45oiP5piv5ZCm6YeH55So55qEMkTmiJbogIUzRFxuICovXG5leHBvcnQgZW51bSBlbnVtRGltZW5zaW9uIHtcbiAgICBEaW0yID0gJzJkJyxcbiAgICBEaW0zID0gJzNkJ1xufVxuXG4vKipcbiAqIOa4uOaIj+eahOeKtuaAgVxuICovXG5leHBvcnQgZW51bSBlbnVtR2FtZVN0YXR1cyB7XG4gICAgU3RhcnQgPSAnR0FNRS1TVEFUVVMtU1RBUlQnLFxuICAgIFN0b3AgPSAnR0FNRS1TVEFUVVMtU1RPUCcsXG4gICAgUmVzdGFydCA9ICdHQU1FLVNUQVRVUy1SRVNUQVJUJyxcbn1cblxuLyoqXG4gbGJsICAtLS0+TGFiZWwo5paH5pysKVxuIHR4dCAgLS0tPlRleHQo5paH5pysKVxuIHJ0eHQgIC0tLT5SaWNoVGV4dCjlr4zmlofmnKwpXG4gaXB0ICAtLS0+SW5wdXQo6L6T5YWl5qGGKVxuIGltZyAgLS0tPkltYWdlKOWbvueJhylcbiBzcHQgIC0tLT5TcHJpdGUo57K+54G1KVxuIGdyaCAgLS0tPkdyYXBoKOWbvuW9oilcbiBsaXN0IC0tLT5MaXN0KOWIl+ihqClcbiBsb2FkIC0tLT5Mb2FkKOijhei9veWZqClcbiBndXAgIC0tLT5Hcm91cCjnu4QpXG4gY29tICAtLS0+Q29tcG9uZW50KOe7hOS7tilcbiBidG4gIC0tLT5CdXR0b24o5oyJ6ZKuKVxuIGNvYiAgLS0tPkNvbWJvQm93KOS4i+aLieahhilcbiBwYmFyIC0tLT5Qcm9ncmVzc0Jhcijov5vluqbmnaEpXG4gc2xkICAtLS0+U2xpZGVyKOa7keWKqOadoSlcbiB3aW4gIC0tLT5XaW5kb3fvvIjnqpflj6PvvIlcbiBhbmkgIC0tLT5Nb3ZpZSjliqjnlLspXG4gZWZ0ICAtLS0+VHJhbnNpdGlvbijliqjmlYgpXG4gY3RsICAtLS0+Q29udHJvbGxlcijmjqfliLblmagpXG4gKi9cblxuLyoqXG4gKiDmjqfku7bliY3nvIBcbiAqL1xuZXhwb3J0IGVudW0gZW51bUVsZW1lbnRQcmVmaXgge1xuICAgIExhYmxlID0gJ2xibF8nLFxuICAgIElucHV0ID0gJ2lwdF8nLFxuICAgIFRleHQgPSAndHh0XycsXG4gICAgUmljaFRleHQgPSAncnR4dF8nLFxuICAgIEltYWdlID0gJ2ltZ18nLFxuICAgIFNwcml0ZSA9ICdzcHRfJyxcbiAgICBHcmFwaCA9ICdncmhfJyxcbiAgICBMaXN0ID0gJ2xpc3RfJyxcbiAgICBMb2FkID0gJ2xvYWRfJyxcbiAgICBHcm91cCA9ICdndXBfJyxcbiAgICBDb21wb25lbnQgPSAnY29tXycsXG4gICAgQnV0dG9uID0gJ2J0bl8nLFxuICAgIENvbWJvQm93ID0gJ2NvYl8nLFxuICAgIFByb2dyZXNzQmFyID0gJ3BiYXJfJyxcbiAgICBTbGlkZXIgPSAnc2xkXycsXG4gICAgV2luZG93ID0gJ3dpbl8nLFxuICAgIE1vdmllID0gJ2FuaV8nLFxuICAgIFRyYW5zaXRpb24gPSAnZWZ0XycsXG4gICAgQ29udHJvbGxlciA9ICdjdGxfJ1xufVxuXG4vKipcbiAqIOaVsOaNruihqOmFjee9rlxuICovXG5leHBvcnQgZW51bSBlbnVtSnNvbkRlZmluZSB7XG4gICAgaW52aXRlID0gXCJpbnZpdGVcIixcbiAgICBsZXZlbCA9IFwibGV2ZWxcIixcbiAgICBsb3R0ZXJ5ID0gXCJsb3R0ZXJ5XCIsXG4gICAgb2ZmbGluZSA9IFwib2ZmbGluZVwiLFxufVxuXG4vKipcbiAqIOmfs+aViOagh+iusFxuICovXG5leHBvcnQgZW51bSBlbnVtU291bmROYW1le1xuICAgIGJnID0gXCJiZ1NvdW5kXCIsXG4gICAgYm90dG9uID0gXCJidG5Tb3VuZFwiLFxufVxuXG5cbiIsImltcG9ydCB7IFV0aWxEaWN0IH0gZnJvbSAnLi4vdXRpbC9kaWN0JztcblxuLyoqXG4gKiBAYXV0aG9yIFN1blxuICogQHRpbWUgMjAxOS0wNS0yMSAxOToyMlxuICogQHByb2plY3QgU0ZyYW1ld29ya19MYXlhQWlyXG4gKiBAZGVzY3JpcHRpb24gIOWtl+WFuFxuICpcbiAqL1xuZXhwb3J0IGNsYXNzIERpY3Rpb25hcnk8VD4ge1xuXG4gICAgcHJpdmF0ZSBtX2RpY3Q6IE9iamVjdCA9IHt9O1xuXG4gICAgcHVibGljIGFkZChrZXk6IGFueSwgdmFsdWU6IFQpOiBib29sZWFuIHtcbiAgICAgICAgaWYgKHRoaXMuaGFzS2V5KGtleSkpIHJldHVybiBmYWxzZTtcbiAgICAgICAgdGhpcy5tX2RpY3Rba2V5XSA9IHZhbHVlO1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG5cbiAgICBwdWJsaWMgcmVtb3ZlKGtleTogYW55KTogdm9pZCB7XG4gICAgICAgIGRlbGV0ZSB0aGlzLm1fZGljdFtrZXldO1xuICAgIH1cblxuICAgIHB1YmxpYyBoYXNLZXkoa2V5OiBhbnkpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuICh0aGlzLm1fZGljdFtrZXldICE9IG51bGwpO1xuICAgIH1cblxuICAgIHB1YmxpYyB2YWx1ZShrZXk6IGFueSk6IFQge1xuICAgICAgICBpZiAoIXRoaXMuaGFzS2V5KGtleSkpIHJldHVybiBudWxsO1xuICAgICAgICByZXR1cm4gdGhpcy5tX2RpY3Rba2V5XTtcbiAgICB9XG5cbiAgICBwdWJsaWMga2V5cygpOiBBcnJheTxhbnk+IHtcbiAgICAgICAgbGV0IGxpc3Q6IEFycmF5PHN0cmluZyB8IG51bWJlcj4gPSBbXTtcbiAgICAgICAgZm9yIChsZXQga2V5IGluIHRoaXMubV9kaWN0KSB7XG4gICAgICAgICAgICBsaXN0LnB1c2goa2V5KTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbGlzdDtcbiAgICB9XG5cbiAgICBwdWJsaWMgdmFsdWVzKCk6IEFycmF5PFQ+IHtcbiAgICAgICAgbGV0IGxpc3Q6IEFycmF5PFQ+ID0gW107XG4gICAgICAgIGZvciAobGV0IGtleSBpbiB0aGlzLm1fZGljdCkge1xuICAgICAgICAgICAgbGlzdC5wdXNoKHRoaXMubV9kaWN0W2tleV0pO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBsaXN0O1xuICAgIH1cblxuICAgIHB1YmxpYyBjbGVhcigpOiB2b2lkIHtcbiAgICAgICAgZm9yIChsZXQga2V5IGluIHRoaXMubV9kaWN0KSB7XG4gICAgICAgICAgICBkZWxldGUgdGhpcy5tX2RpY3Rba2V5XTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBmb3JlYWNoKGNvbXBhcmVGbjogKGtleTogYW55LCB2YWx1ZTogVCk9PnZvaWQpOiB2b2lkIHtcbiAgICAgICAgZm9yIChsZXQga2V5IGluIHRoaXMubV9kaWN0KSB7XG4gICAgICAgICAgICBjb21wYXJlRm4uY2FsbChudWxsLCBrZXksIHRoaXMubV9kaWN0W2tleV0pO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIGZvcmVhY2hCcmVhayhjb21wYXJlRm46IChrZXk6YW55LCB2YWx1ZTogVCkgPT4gYm9vbGVhbik6IHZvaWQge1xuICAgICAgICBmb3IgKGxldCBrZXkgaW4gdGhpcy5tX2RpY3QpIHtcbiAgICAgICAgICAgIGlmICghY29tcGFyZUZuLmNhbGwobnVsbCwga2V5LCB0aGlzLm1fZGljdFtrZXldKSlcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBnZXQgbGVuZ3RoKCk6IG51bWJlciB7XG4gICAgICAgIHJldHVybiBVdGlsRGljdC5zaXplKHRoaXMubV9kaWN0KTtcbiAgICB9XG59XG4iLCJpbXBvcnQgeyBlbnVtQXJyYXlTb3J0T3JkZXIgfSBmcm9tICcuLi9zZXR0aW5nL2VudW0nO1xuXG4gLyoqXG4gKiBAYXV0aG9yIFN1blxuICogQHRpbWUgMjAxOS0wOC0wOSAyMzoxNVxuICogQHByb2plY3QgU0ZyYW1ld29ya19MYXlhQWlyXG4gKiBAZGVzY3JpcHRpb24g5pWw57uE5bel5YW357G7XG4gKi9cbmV4cG9ydCBjbGFzcyBVdGlsQXJyYXkge1xuXG4gICAgLyoqIOaPkuWFpeWFg+e0oFxuICAgICAqIEBwYXJhbSBhcnIg6ZyA6KaB5pON5L2c55qE5pWw57uEXG4gICAgICogQHBhcmFtIHZhbHVlIOmcgOimgeaPkuWFpeeahOWFg+e0oFxuICAgICAqIEBwYXJhbSBpbmRleCDmj5LlhaXkvY3nva5cbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIGluc2VydChhcnI6IGFueVtdLCB2YWx1ZTogYW55LCBpbmRleDogbnVtYmVyKTogdm9pZCB7XG4gICAgICAgIGlmIChpbmRleCA+IGFyci5sZW5ndGggLSAxKSB7XG4gICAgICAgICAgICBhcnIucHVzaCh2YWx1ZSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBhcnIuc3BsaWNlKGluZGV4LCAwLCB2YWx1ZSk7XG4gICAgICAgIH1cblxuICAgIH1cblxuICAgIC8qKuS7juaVsOe7hOenu+mZpOWFg+e0oCovXG4gICAgcHVibGljIHN0YXRpYyByZW1vdmUoYXJyOiBhbnlbXSwgdjogYW55KTogdm9pZCB7XG4gICAgICAgIGxldCBpOiBudW1iZXIgPSBhcnIuaW5kZXhPZih2KTtcbiAgICAgICAgaWYgKGkgIT0gLTEpIHtcbiAgICAgICAgICAgIGFyci5zcGxpY2UoaSwgMSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvKirnp7vpmaTmiYDmnInlgLznrYnkuo5255qE5YWD57SgKi9cbiAgICBwdWJsaWMgc3RhdGljIHJlbW92ZUFsbChhcnI6IGFueVtdLCB2OiBhbnkpOiB2b2lkIHtcbiAgICAgICAgbGV0IGk6IG51bWJlciA9IGFyci5pbmRleE9mKHYpO1xuICAgICAgICB3aGlsZSAoaSA+PSAwKSB7XG4gICAgICAgICAgICBhcnIuc3BsaWNlKGksIDEpO1xuICAgICAgICAgICAgaSA9IGFyci5pbmRleE9mKHYpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoq5YyF5ZCr5YWD57SgKi9cbiAgICBwdWJsaWMgc3RhdGljIGNvbnRhaW4oYXJyOiBhbnlbXSwgdjogYW55KTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiBhcnIubGVuZ3RoID4gMCA/IGFyci5pbmRleE9mKHYpICE9IC0xIDogZmFsc2U7XG4gICAgfVxuXG4gICAgLyoq5aSN5Yi2Ki9cbiAgICBwdWJsaWMgc3RhdGljIGNvcHkoYXJyOiBhbnlbXSk6IGFueVtdIHtcbiAgICAgICAgcmV0dXJuIGFyci5zbGljZSgpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOaOkuW6j1xuICAgICAqIEBwYXJhbSBhcnIg6ZyA6KaB5o6S5bqP55qE5pWw57uEXG4gICAgICogQHBhcmFtIGtleSDmjpLluo/lrZfmrrVcbiAgICAgKiBAcGFyYW0gb3JkZXIg5o6S5bqP5pa55byPXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyBzb3J0KGFycjogYW55W10sIGtleTogc3RyaW5nLCBvcmRlcjogZW51bUFycmF5U29ydE9yZGVyID0gZW51bUFycmF5U29ydE9yZGVyLkRlc2NlbmRpbmcpOiB2b2lkIHtcbiAgICAgICAgaWYgKGFyciA9PSBudWxsKSByZXR1cm47XG4gICAgICAgIGFyci5zb3J0KGZ1bmN0aW9uIChpbmZvMSwgaW5mbzIpIHtcbiAgICAgICAgICAgIHN3aXRjaCAob3JkZXIpIHtcbiAgICAgICAgICAgICAgICBjYXNlIGVudW1BcnJheVNvcnRPcmRlci5Bc2NlbmRpbmc6IHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGluZm8xW2tleV0gPCBpbmZvMltrZXldKVxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIC0xO1xuICAgICAgICAgICAgICAgICAgICBpZiAoaW5mbzFba2V5XSA+IGluZm8yW2tleV0pXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gMTtcbiAgICAgICAgICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIDA7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGNhc2UgZW51bUFycmF5U29ydE9yZGVyLkRlc2NlbmRpbmc6IHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGluZm8xW2tleV0gPiBpbmZvMltrZXldKVxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIC0xO1xuICAgICAgICAgICAgICAgICAgICBpZiAoaW5mbzFba2V5XSA8IGluZm8yW2tleV0pXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gMTtcbiAgICAgICAgICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIDA7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICAvKirmuIXnqbrmlbDnu4QqL1xuICAgIHB1YmxpYyBzdGF0aWMgY2xlYXIoYXJyOiBhbnlbXSk6IHZvaWQge1xuICAgICAgICBsZXQgaTogbnVtYmVyID0gMDtcbiAgICAgICAgbGV0IGxlbjogbnVtYmVyID0gYXJyLmxlbmd0aDtcbiAgICAgICAgZm9yICg7IGkgPCBsZW47ICsraSkge1xuICAgICAgICAgICAgYXJyW2ldID0gbnVsbDtcbiAgICAgICAgfVxuICAgICAgICBhcnIuc3BsaWNlKDApO1xuICAgIH1cblxuICAgIC8qKuaVsOaNruaYr+WQpuS4uuepuiovXG4gICAgcHVibGljIHN0YXRpYyBpc0VtcHR5KGFycjogYW55W10pOiBCb29sZWFuIHtcbiAgICAgICAgaWYgKGFyciA9PSBudWxsIHx8IGFyci5sZW5ndGggPT0gMCkge1xuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZhbHNlXG4gICAgfVxufVxuIiwiXG4gLyoqXG4gKiBAYXV0aG9yIFN1blxuICogQHRpbWUgMjAxOS0wOC0xMCAyMDoyMlxuICogQHByb2plY3QgU0ZyYW1ld29ya19MYXlhQWlyXG4gKiBAZGVzY3JpcHRpb24gIOWtl+WFuOW3peWFt+exu1xuICpcbiAqL1xuZXhwb3J0IGNsYXNzIFV0aWxEaWN0IHtcbiAgICAvKipcbiAgICAgKiDplK7liJfooahcbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIGtleXMoZDogT2JqZWN0KTogYW55W10ge1xuICAgICAgICBsZXQgYTogYW55W10gPSBbXTtcbiAgICAgICAgZm9yIChsZXQga2V5IGluIGQpIHtcbiAgICAgICAgICAgIGEucHVzaChrZXkpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIGE7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5YC85YiX6KGoXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyB2YWx1ZXMoZDogT2JqZWN0KTogYW55W10ge1xuICAgICAgICBsZXQgYTogYW55W10gPSBbXTtcblxuICAgICAgICBmb3IgKGxldCBrZXkgaW4gZCkge1xuICAgICAgICAgICAgYS5wdXNoKGRba2V5XSk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gYTtcbiAgICB9XG4gXG4gICAgLyoqXG4gICAgICog5riF56m65a2X5YW4XG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyBjbGVhcihkaWM6IE9iamVjdCk6IHZvaWQge1xuICAgICAgICBsZXQgdjogYW55O1xuICAgICAgICBmb3IgKGxldCBrZXkgaW4gZGljKSB7XG4gICAgICAgICAgICB2ID0gZGljW2tleV07XG4gICAgICAgICAgICBpZiAodiBpbnN0YW5jZW9mIE9iamVjdCkge1xuICAgICAgICAgICAgICAgIFV0aWxEaWN0LmNsZWFyKHYpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZGVsZXRlIGRpY1trZXldO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5YWo6YOo5bqU55SoXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyBmb3JlYWNoKGRpYzogT2JqZWN0LCBjb21wYXJlRm46IChrZXk6IGFueSwgdmFsdWU6IGFueSkgPT4gYm9vbGVhbik6IHZvaWQge1xuICAgICAgICBmb3IgKGxldCBrZXkgaW4gZGljKSB7XG4gICAgICAgICAgICBpZiAoIWNvbXBhcmVGbi5jYWxsKG51bGwsIGtleSwgZGljW2tleV0pKVxuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBpc0VtcHR5KGRpYzogT2JqZWN0KTogQm9vbGVhbiB7XG4gICAgICAgIGlmIChkaWMgPT0gbnVsbCkgcmV0dXJuIHRydWU7XG4gICAgICAgIC8vIEB0cy1pZ25vcmVcbiAgICAgICAgZm9yIChsZXQga2V5IGluIGRpYykge1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgc2l6ZShkaWM6IE9iamVjdCk6IG51bWJlciB7XG4gICAgICAgIGlmIChkaWMgPT0gbnVsbCkgcmV0dXJuIDA7XG5cbiAgICAgICAgbGV0IGNvdW50OiBudW1iZXIgPSAwO1xuXG4gICAgICAgIGZvciAobGV0IGtleSBpbiBkaWMpIHtcbiAgICAgICAgICAgICsrY291bnQ7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGNvdW50O1xuICAgIH1cbn1cbiIsImltcG9ydCBOb2RlID0gTGF5YS5Ob2RlO1xuaW1wb3J0IFNwcml0ZSA9IExheWEuU3ByaXRlO1xuaW1wb3J0IFJlY3RhbmdsZSA9IExheWEuUmVjdGFuZ2xlO1xuaW1wb3J0IExhYmVsID0gTGF5YS5MYWJlbDtcblxuZXhwb3J0IGNsYXNzIFV0aWxEaXNwbGF5IHtcblxuICAgIC8qKlxuICAgICAqIOenu+mZpOWFqOmDqOWtkOWvueixoVxuICAgICAqIEBwYXJhbSBjb250YWluZXIgXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyByZW1vdmVBbGxDaGlsZChjb250YWluZXI6IExheWEuU3ByaXRlKTogdm9pZCB7XG4gICAgICAgIGlmICghY29udGFpbmVyKSByZXR1cm47XG4gICAgICAgIGlmIChjb250YWluZXIubnVtQ2hpbGRyZW4gPD0gMCkgcmV0dXJuO1xuXG4gICAgICAgIHdoaWxlIChjb250YWluZXIubnVtQ2hpbGRyZW4gPiAwKSB7XG4gICAgICAgICAgICBjb250YWluZXIucmVtb3ZlQ2hpbGRBdCgwKVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogXG4gICAgICogQHBhcmFtIG5vZGUg6ZSA5q+BVUnoioLngrlcbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIGRlc3Ryb3lVSU5vZGUobm9kZTogTm9kZSk6IHZvaWQge1xuICAgICAgICBpZiAobm9kZSkge1xuICAgICAgICAgICAgbm9kZS5yZW1vdmVTZWxmKCk7XG4gICAgICAgICAgICBub2RlLmRlc3Ryb3koKTtcbiAgICAgICAgICAgIG5vZGUgPSBudWxsO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog6YCa6L+H5ZCN5a2X6I635b6X5a2Q6IqC54K5XG4gICAgICogQHBhcmFtIHBhcmVudCBcbiAgICAgKiBAcGFyYW0gbmFtZSBcbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIGdldENoaWxkQnlOYW1lKHBhcmVudDogTm9kZSwgbmFtZTogc3RyaW5nKTogTm9kZSB7XG4gICAgICAgIGlmICghcGFyZW50KSByZXR1cm4gbnVsbDtcbiAgICAgICAgaWYgKHBhcmVudC5uYW1lID09PSBuYW1lKSByZXR1cm4gcGFyZW50O1xuICAgICAgICBsZXQgY2hpbGQ6IE5vZGUgPSBudWxsO1xuICAgICAgICBsZXQgbnVtOiBudW1iZXIgPSBwYXJlbnQubnVtQ2hpbGRyZW47XG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgbnVtOyArK2kpIHtcbiAgICAgICAgICAgIGNoaWxkID0gVXRpbERpc3BsYXkuZ2V0Q2hpbGRCeU5hbWUocGFyZW50LmdldENoaWxkQXQoaSksIG5hbWUpO1xuICAgICAgICAgICAgaWYgKGNoaWxkKSByZXR1cm4gY2hpbGQ7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuXG4gICAgLy8gLyoqXG4gICAgLy8gICog6K6+572u5a+56b2Q5pa55byPXG4gICAgLy8gICogQHBhcmFtIGFsaWdlIOWvuem9kOaWueW8j1xuICAgIC8vICAqL1xuICAgIC8vIHB1YmxpYyBzdGF0aWMgc2V0QWxpZ2Uobm9kZTogU3ByaXRlLCBhbGlnZTogZW51bUFsaWdlVHlwZSwgdzogbnVtYmVyID0gMCwgaDogbnVtYmVyID0gMCkge1xuICAgIC8vICAgICBpZiAoIW5vZGUpIHJldHVybjtcbiAgICAvLyAgICAgbGV0IHJlY3Q6IFJlY3RhbmdsZTtcbiAgICAvLyAgICAgaWYgKHcgPD0gMCB8fCBoIDw9IDApIHJlY3QgPSBub2RlLmdldEJvdW5kcygpO1xuICAgIC8vICAgICBsZXQgd2lkdGg6IG51bWJlciA9IHcgPiAwID8gdyA6IHJlY3Qud2lkdGg7XG4gICAgLy8gICAgIGxldCBoZWlndGg6IG51bWJlciA9IGggPiAwID8gaCA6IHJlY3QuaGVpZ2h0O1xuICAgIC8vICAgICBzd2l0Y2ggKGFsaWdlKSB7XG4gICAgLy8gICAgICAgICBjYXNlIGVudW1BbGlnZVR5cGUuTEVGVF9UT1A6XG4gICAgLy8gICAgICAgICAgICAgbm9kZS5waXZvdCgwLCAwKTtcbiAgICAvLyAgICAgICAgICAgICBicmVhaztcbiAgICAvLyAgICAgICAgIGNhc2UgZW51bUFsaWdlVHlwZS5MRUZUOlxuICAgIC8vICAgICAgICAgICAgIG5vZGUucGl2b3QoMCwgaGVpZ3RoICogMC41KTtcbiAgICAvLyAgICAgICAgICAgICBicmVhaztcbiAgICAvLyAgICAgICAgIGNhc2UgZW51bUFsaWdlVHlwZS5MRUZUX0JPVFRPTTpcbiAgICAvLyAgICAgICAgICAgICBub2RlLnBpdm90KDAsIGhlaWd0aCk7XG4gICAgLy8gICAgICAgICAgICAgYnJlYWs7XG4gICAgLy8gICAgICAgICBjYXNlIGVudW1BbGlnZVR5cGUuVE9QOlxuICAgIC8vICAgICAgICAgICAgIG5vZGUucGl2b3Qod2lkdGggKiAwLjUsIDApO1xuICAgIC8vICAgICAgICAgICAgIGJyZWFrO1xuICAgIC8vICAgICAgICAgY2FzZSBlbnVtQWxpZ2VUeXBlLk1JRDpcbiAgICAvLyAgICAgICAgICAgICBub2RlLnBpdm90KHdpZHRoICogMC41LCBoZWlndGggKiAwLjUpO1xuICAgIC8vICAgICAgICAgICAgIGJyZWFrO1xuICAgIC8vICAgICAgICAgY2FzZSBlbnVtQWxpZ2VUeXBlLkJPVFRPTTpcbiAgICAvLyAgICAgICAgICAgICBub2RlLnBpdm90KHdpZHRoICogMC41LCBoZWlndGgpO1xuICAgIC8vICAgICAgICAgICAgIGJyZWFrO1xuICAgIC8vICAgICAgICAgY2FzZSBlbnVtQWxpZ2VUeXBlLlJJR0hUX1RPUDpcbiAgICAvLyAgICAgICAgICAgICBub2RlLnBpdm90KHdpZHRoLCAwKTtcbiAgICAvLyAgICAgICAgICAgICBicmVhaztcbiAgICAvLyAgICAgICAgIGNhc2UgZW51bUFsaWdlVHlwZS5SSUdIVDpcbiAgICAvLyAgICAgICAgICAgICBub2RlLnBpdm90KHdpZHRoLCBoZWlndGggKiAwLjUpO1xuICAgIC8vICAgICAgICAgICAgIGJyZWFrO1xuICAgIC8vICAgICAgICAgY2FzZSBlbnVtQWxpZ2VUeXBlLlJJR0hUX0JPVFRPTTpcbiAgICAvLyAgICAgICAgICAgICBub2RlLnBpdm90KHdpZHRoLCBoZWlndGgpO1xuICAgIC8vICAgICAgICAgICAgIGJyZWFrO1xuICAgIC8vICAgICB9XG4gICAgLy8gfVxuXG4gICAgLyoqXG4gICAgICog5Yib5bu66YCP5piO6YGu572pXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyBjcmVhdGVNYXNrTGF5ZXIoKTogU3ByaXRlIHtcbiAgICAgICAgbGV0IGxheWVyID0gbmV3IFNwcml0ZSgpO1xuICAgICAgICBsYXllci5tb3VzZUVuYWJsZWQgPSB0cnVlO1xuXG4gICAgICAgIGxldCB3aWR0aCA9IGxheWVyLndpZHRoID0gTGF5YS5zdGFnZS53aWR0aCArIDIwMDtcbiAgICAgICAgdmFyIGhlaWdodCA9IGxheWVyLmhlaWdodCA9IExheWEuc3RhZ2UuaGVpZ2h0ICsgNDAwO1xuICAgICAgICBsYXllci5ncmFwaGljcy5jbGVhcih0cnVlKTtcbiAgICAgICAgbGF5ZXIuZ3JhcGhpY3MuZHJhd1JlY3QoMCwgMCwgd2lkdGgsIGhlaWdodCwgVUlDb25maWcucG9wdXBCZ0NvbG9yKTtcbiAgICAgICAgbGF5ZXIuYWxwaGEgPSBVSUNvbmZpZy5wb3B1cEJnQWxwaGE7XG5cbiAgICAgICAgcmV0dXJuIGxheWVyO1xuICAgIH1cbn0iLCJpbXBvcnQgeyBMb2cgfSBmcm9tICcuLi9jb3JlL2xvZyc7XG5pbXBvcnQgeyBFdmVudEZ1bmMgfSBmcm9tICcuLi9tYW5hZ2VyL2V2ZW50L2V2ZW50LWRhdGEnO1xuXG4gLyoqXG4gKiBAYXV0aG9yIFN1blxuICogQHRpbWUgMjAxOS0wMi0yNSAxNzoyMlxuICogQHByb2plY3QgU0ZyYW1ld29ya19MYXlhQWlyXG4gKiBAZGVzY3JpcHRpb24gIDNE5qih5Z6L5Yqg6L295bel5YW357G7XG4gKlxuICovXG5leHBvcnQgY2xhc3MgVXRpbExvYWQzRCB7XG5cbiAgICAvKipcbiAgICAgKiDliqDovb1VM0TlnLrmma9cbiAgICAgKiBAcGFyYW0gYXJlYSDkvZznlKjln59cbiAgICAgKiBAcGFyYW0gcGF0aCDlnLrmma/mlofku7bot6/lvoRcbiAgICAgKiBAcGFyYW0gY2IgICDliqDovb3lrozmiJDlm57osINcbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIGxvYWRTY2VuZShwYXRoLGFyZWEsY2IpOmFueVxuICAgIHtcbiAgICAgICAgTGF5YS5sb2FkZXIuY3JlYXRlKHBhdGgsTGF5YS5IYW5kbGVyLmNyZWF0ZSh0aGlzLCgpPT57XG4gICAgICAgICAgICBMYXlhLnN0YWdlLmFkZENoaWxkKExheWEubG9hZGVyLmdldFJlcyhwYXRoKSk7XG4gICAgICAgICAgICBpZiAoY2IpIHtcbiAgICAgICAgICAgICAgICBjYi5jYWxsKGFyZWEpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KSk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog6I635Y+W5Zy65pmv5YaF54mp5L2TXG4gICAgICogQHBhcmFtIHNjZW5lM2Qg5Zy65pmvXG4gICAgICogQHBhcmFtIGNoaWxkTmFtZSDlrZDniankvZPlkI3lrZdcbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIGdldFNjZW5lM0RDaGlsZDxUPihzY2VuZTNkLGNoaWxkTmFtZSk6VFxuICAgIHtcbiAgICAgICAgbGV0IG1zID0gc2NlbmUzZC5nZXRDaGlsZEJ5TmFtZShjaGlsZE5hbWUpIGFzIFQ7XG4gICAgICAgIGlmIChtcykge1xuICAgICAgICAgICAgcmV0dXJuIG1zO1xuICAgICAgICB9XG4gICAgICAgIExvZy5lcnJvcihcIkVycm9yOuiOt+WPluWcuuaZr+eJqeS9k+Wksei0pVwiKTtcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog6I635Y+W5qih5Z6L55qE5a2Q54mp5L2T5qih5Z6LXG4gICAgICogQHBhcmFtIGZhdFNQIOeItuaWuVxuICAgICAqIEBwYXJhbSBjaGlsZE5hbWUg5a2Q5pa55ZCN5a2XXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyBnZXRNb2RlbENoaWxkQnlOYW1lPFQ+KGZhdFNQLGNoaWxkTmFtZSk6VFxuICAgIHtcbiAgICAgICAgbGV0IGNoaWxkID0gZmF0U1AuZ2V0Q2hpbGRCeU5hbWUoY2hpbGROYW1lKSBhcyBUO1xuICAgICAgICBpZiAoY2hpbGQpIHtcbiAgICAgICAgICAgIHJldHVybiBjaGlsZDtcbiAgICAgICAgfVxuICAgICAgICBMb2cuZXJyb3IoXCJFcnJvcjrojrflj5bmqKHlnovlrZDniankvZPkv6Hmga/plJnor69cIik7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOabv+aNouaooeWei1xuICAgICAqIEBwYXJhbSB0YXJnZXRTUCDooqvmm7/mjaLmqKHlnotcbiAgICAgKiBAcGFyYW0gbWlhblNQICAg5pu/5o2i5qih5Z6LXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyByZXBsYWNlTW9kZWwodGFyZ2V0U1AsbWFpblNQKVxuICAgIHtcbiAgICAgICAgaWYgKCF0YXJnZXRTUCB8fCAhbWFpblNQKSB7XG4gICAgICAgICAgICBMb2cuZXJyb3IoXCJFcnJvcjrmm7/mjaLmiJbooqvmm7/mjaLmqKHlnovkv6Hmga/plJnor69cIik7XG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGFyZ2V0U1AucGFyZW50KSB7XG4gICAgICAgICAgICB0YXJnZXRTUC5wYXJlbnQuYWRkQ2hpbGQobWFpblNQKTtcbiAgICAgICAgfVxuICAgICAgICBtYWluU1AudHJhbnNmb3JtLnBvc2l0aW9uID0gbmV3IExheWEuVmVjdG9yMyh0YXJnZXRTUC50cmFuc2Zvcm0ucG9zaXRpb24ueCx0YXJnZXRTUC50cmFuc2Zvcm0ucG9zaXRpb24ueSx0YXJnZXRTUC50cmFuc2Zvcm0ucG9zaXRpb24ueik7XG4gICAgICAgIG1haW5TUC50cmFuc2Zvcm0uc2NhbGUgPSBuZXcgTGF5YS5WZWN0b3IzKHRhcmdldFNQLnRyYW5zZm9ybS5zY2FsZS54LHRhcmdldFNQLnRyYW5zZm9ybS5zY2FsZS55LHRhcmdldFNQLnRyYW5zZm9ybS5zY2FsZS55KTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDmm7/mjaJNZXNo5qih5Z6L5p2Q6LSoXG4gICAgICogQHBhcmFtIHRhcmdldFNQIOebruagh+aooeWei1xuICAgICAqIEBwYXJhbSB0YXJnZXRNYXQg55uu5qCH5p2Q6LSoXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyByZXBsYWNlTW9kZWxNZXNoTWF0KHRhcmdldFNQLHRhcmdldE1hdClcbiAgICB7XG4gICAgICAgIGlmICghdGFyZ2V0U1AgfHwgIXRhcmdldE1hdCkge1xuICAgICAgICAgICAgTG9nLmVycm9yKFwiRXJyb3I65qih5Z6L5oiW5p2Q6LSo5L+h5oGv6ZSZ6K+vXCIpO1xuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIH1cbiAgICAgICAgdGFyZ2V0U1AgYXMgTGF5YS5NZXNoU3ByaXRlM0Q7XG4gICAgICAgIHRhcmdldFNQLm1lc2hSZW5kZXJlci5tYXRlcmlhbCA9IHRhcmdldE1hdDtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDmm7/mjaJTa2luTWVzaOaooeWei+adkOi0qFxuICAgICAqIEBwYXJhbSB0YXJnZXRTUCDnm67moIfmqKHlnotcbiAgICAgKiBAcGFyYW0gdGFyZ2V0TWF0IOebruagh+adkOi0qFxuICAgICAqL1xuICAgIHB1YmxpYyBzdGF0aWMgcmVwbGFjZU1vZGVsU2tpbk1lc2hNYXQodGFyZ2V0U1AsdGFyZ2V0TWF0KVxuICAgIHtcbiAgICAgICAgaWYgKCF0YXJnZXRTUCB8fCAhdGFyZ2V0TWF0KSB7XG4gICAgICAgICAgICBMb2cuZXJyb3IoXCJFcnJvcjrmqKHlnovmiJbmnZDotKjkv6Hmga/plJnor69cIik7XG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgfVxuICAgICAgICB0YXJnZXRTUCBhcyBMYXlhLlNraW5uZWRNZXNoU3ByaXRlM0Q7XG4gICAgICAgIHRhcmdldFNQLnNraW5uZWRNZXNoUmVuZGVyZXIubWF0ZXJpYWwgPSB0YXJnZXRNYXQ7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog6I635Y+W5a655Zmo5LiK55qE5p2Q6LSo5bm25a2Y5YWl5ZOI5biM6KGoXG4gICAgICogQHBhcmFtIHRhcmdldE9iaiDmib/ovb3mnZDotKjnmoTlrrnlmahcbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIGdldE1hdGVyaWFscyhzY2VuZTNkKTphbnlcbiAgICB7XG4gICAgICAgIC8qKlVuaXR55Zy65pmv5a2Y6LSu5LiA5Liq56m654mp5L2T77yM6ZmE5bimTWVzaFJlbmRlcueUqOadpeWtmOWCqOadkOi0qCoqL1xuICAgICAgICB2YXIgY29udGFpbmVyID0gVXRpbExvYWQzRC5nZXRTY2VuZTNEQ2hpbGQ8TGF5YS5NZXNoU3ByaXRlM0Q+KHNjZW5lM2QsXCJNYXRDb250YWluZXJcIik7XG4gICAgICAgIGlmICghY29udGFpbmVyKSB7XG4gICAgICAgICAgICBMb2cuZXJyb3IoXCJFcnJvcjrmnZDotKjlrrnlmajplJnor6/miJbkuI3lrZjlnKhcIik7XG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgfVxuICAgICAgICB2YXIgdXNlckluZm86IHtbaW5kZXg6c3RyaW5nXTogTGF5YS5CbGlublBob25nTWF0ZXJpYWx9ID0ge31cbiAgICAgICAgdmFyIG1hdEFycmFyeSA9IGNvbnRhaW5lci5tZXNoUmVuZGVyZXIubWF0ZXJpYWxzO1xuICAgICAgICBmb3IgKHZhciBpID0gMDtpPG1hdEFycmFyeS5sZW5ndGg7aSsrKVxuICAgICAgICB7XG4gICAgICAgICAgICB2YXIgbmFtZSA9IG1hdEFycmFyeVtpXS5uYW1lLnNsaWNlKDAsbWF0QXJyYXJ5W2ldLm5hbWUubGVuZ3RoLTEwKTtcbiAgICAgICAgICAgIHVzZXJJbmZvW25hbWVdID0gbWF0QXJyYXJ5W2ldIGFzIExheWEuQmxpbm5QaG9uZ01hdGVyaWFsO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB1c2VySW5mbztcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDov5Tlm57mjIflrprlkI3lrZfnmoTmnZDotKhcbiAgICAgKiBAcGFyYW0gbWF0QXJyYXR5IOWtmOaUvuadkOi0qOeahOWTiOW4jOihqFxuICAgICAqIEBwYXJhbSBtYXROYW1lICAg5p2Q6LSo5ZCN5a2XXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyBnZXRNYXRlcmlhbEJ5TmFtZShtYXRBcnJhcnksbWF0TmFtZSk6TGF5YS5CbGlublBob25nTWF0ZXJpYWxcbiAgICB7XG4gICAgICAgIGlmICghbWF0QXJyYXJ5KSB7XG4gICAgICAgICAgICBMb2cuZXJyb3IoXCJFcnJvcjrmnZDotKjlk4jluIzooajkv6Hmga/plJnor69cIik7XG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgfVxuICAgICAgICBpZiAoIW1hdEFycmFyeVttYXROYW1lXSlcbiAgICAgICAge1xuICAgICAgICAgICAgTG9nLmVycm9yKFwiRXJyb3I65oyH5a6a5ZOI5biM6KGo5YaF5LiN5a2Y5ZyoW1wiK21hdE5hbWUrXCJd5p2Q6LSoXCIpO1xuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG1hdEFycmFyeVttYXROYW1lXTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDmkq3mlL7mqKHlnovliqjnlLtcbiAgICAgKiBAcGFyYW0gdGFyZ2V0U3Ag5pKt5pS+54mp5L2TXG4gICAgICogQHBhcmFtIGFuaU5hbWUgIOWKqOeUu+WQjeWtl1xuICAgICAqIEBwYXJhbSBpc0Nyb3NzICDmmK/lkKbov4fluqZcbiAgICAgKiDpgJrov4d0aGlzLmFuaW1hdG9yLmdldEN1cnJlbnRBbmltYXRvclBsYXlTdGF0ZSgwKS5ub3JtYWxpemVkVGltZT49MeWOu+WIpOaWreW9k+WJjeWKqOeUu+aYr+WQpuaSreaUvuWujOaIkFxuICAgICAqL1xuICAgIHB1YmxpYyBzdGF0aWMgcGxheUFuaW1hdG9yQnlOYW1lKHRhcmdldFNwLGFuaU5hbWUsaXNDcm9zcz8pOkxheWEuQW5pbWF0b3JcbiAgICB7XG4gICAgICAgIHZhciBhbmltYXRvcjpMYXlhLkFuaW1hdG9yID0gdGFyZ2V0U3AuZ2V0Q29tcG9uZW50KExheWEuQW5pbWF0b3IpO1xuICAgICAgICBpZiAoIWFuaW1hdG9yKVxuICAgICAgICB7XG4gICAgICAgICAgICBMb2cuZXJyb3IoXCJFcnJvcjrliqjnlLvmnLrkv6Hmga/plJnor69cIik7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGlzQ3Jvc3MgIT0gbnVsbCAmJiBpc0Nyb3NzID09IGZhbHNlKSB7XG4gICAgICAgICAgICBhbmltYXRvci5wbGF5KGFuaU5hbWUpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGFuaW1hdG9yLmNyb3NzRmFkZShhbmlOYW1lLDAuMik7XG4gICAgICAgIHJldHVybiBhbmltYXRvcjtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDmjqfliLbliqjnlLvpgJ/luqZcbiAgICAgKiBAcGFyYW0gdGFyZ2V0U3Ag55uu5qCH54mp5L2TXG4gICAgICogQHBhcmFtIHNwZWVkIOaSreaUvumAn+W6plxuICAgICAqL1xuICAgIHB1YmxpYyBzdGF0aWMgY29udHJvbEFuaW1hdG9yU3BlZWQodGFyZ2V0U3Asc3BlZWQpXG4gICAge1xuICAgICAgICB2YXIgYW5pbWF0b3I6TGF5YS5BbmltYXRvciA9IHRhcmdldFNwLmdldENvbXBvbmVudChMYXlhLkFuaW1hdG9yKTtcbiAgICAgICAgaWYgKCFhbmltYXRvcilcbiAgICAgICAge1xuICAgICAgICAgICAgTG9nLmVycm9yKFwiRXJyb3I65Yqo55S75py65L+h5oGv6ZSZ6K+vXCIpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGlmIChzcGVlZCkge1xuICAgICAgICAgICAgYW5pbWF0b3Iuc3BlZWQgPSBzcGVlZDtcbiAgICAgICAgfWVsc2Uge1xuICAgICAgICAgICAgYW5pbWF0b3Iuc3BlZWQgPSAxO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5Yik5pat5Yqo55S75piv5ZCm5a6M5oiQXG4gICAgICogQHBhcmFtIGFuaW1hdG9yXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyBjb25maXJtQW5pQ29tcGxldGUoYW5pbWF0b3I6TGF5YS5BbmltYXRvcik6Ym9vbGVhblxuICAgIHtcbiAgICAgICAgdmFyIGJvb2wgPSBmYWxzZTtcbiAgICAgICAgbGV0IGluZGV4ID0gYW5pbWF0b3IuZ2V0Q3VycmVudEFuaW1hdG9yUGxheVN0YXRlKDApLm5vcm1hbGl6ZWRUaW1lO1xuICAgICAgICBpZiAoaW5kZXggPj0gMSkge1xuICAgICAgICAgICAgYm9vbCA9IHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGJvb2w7XG4gICAgfVxuXG59IiwiaW1wb3J0IHtVdGlsTWF0aDNEfSBmcm9tIFwiLi9tYXRoM2RcIjtcblxuLyoqXG4gKiBAYXV0aG9yIFN1blxuICogQHRpbWUgMjAxOS0wMS0xOCAxNjoyMFxuICogQHByb2plY3QgU0ZyYW1ld29ya19MYXlhQWlyXG4gKiBAZGVzY3JpcHRpb24g566X5rOV5bel5YW357G7XG4gKlxuICovXG5leHBvcnQgY2xhc3MgVXRpbE1hdGgge1xuXG4gICAgLyoq5a2X6IqC6L2s5o2iTSovXG4gICAgcHVibGljIHN0YXRpYyBCWVRFX1RPX006IG51bWJlciA9IDEgLyAoMTAyNCAqIDEwMjQpO1xuICAgIC8qKuWtl+iKgui9rOaNoksqL1xuICAgIHB1YmxpYyBzdGF0aWMgQllURV9UT19LOiBudW1iZXIgPSAxIC8gKDEwMjQpO1xuXG4gICAgcHVibGljIHN0YXRpYyBEZWcyUmFkOiBudW1iZXIgPSAwLjAxNzQ1MzI5O1xuXG4gICAgcHVibGljIHN0YXRpYyBSYWQyRGVnOiBudW1iZXIgPSA1Ny4yOTU3ODtcblxuICAgIHB1YmxpYyBzdGF0aWMgU2lnbihmOiBudW1iZXIpOiBudW1iZXIge1xuICAgICAgICByZXR1cm4gKChmIDwgMCkgPyAtMSA6IDEpO1xuICAgIH1cblxuXG4gICAgLyoqXG4gICAgICog6ZmQ5a6a5pWw5a2X5Zyo6IyD5Zu05Yy66Ze05bm26L+U5ZueXG4gICAgICogQHBhcmFtIG51bVxuICAgICAqIEBwYXJhbSBtaW5cbiAgICAgKiBAcGFyYW0gbWF4XG4gICAgICogQGNvbnN0cnVjdG9yXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyBjbGFtcChudW06IG51bWJlciwgbWluOiBudW1iZXIsIG1heDogbnVtYmVyKTogbnVtYmVyIHtcbiAgICAgICAgaWYgKG51bSA8IG1pbikge1xuICAgICAgICAgICAgbnVtID0gbWluO1xuICAgICAgICB9IGVsc2UgaWYgKG51bSA+IG1heCkge1xuICAgICAgICAgICAgbnVtID0gbWF4O1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBudW07XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBjbGFtcDAxKHZhbHVlOiBudW1iZXIpOiBudW1iZXIge1xuICAgICAgICBpZiAodmFsdWUgPCAwKSByZXR1cm4gMDtcbiAgICAgICAgaWYgKHZhbHVlID4gMSkgcmV0dXJuIDE7XG4gICAgICAgIHJldHVybiB2YWx1ZTtcbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIGxlcnAoZnJvbTogbnVtYmVyLCB0bzogbnVtYmVyLCB0OiBudW1iZXIpOiBudW1iZXIge1xuICAgICAgICByZXR1cm4gKGZyb20gKyAoKHRvIC0gZnJvbSkgKiBVdGlsTWF0aC5jbGFtcDAxKHQpKSk7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBsZXJwQW5nbGUoYTogbnVtYmVyLCBiOiBudW1iZXIsIHQ6IG51bWJlcik6IG51bWJlciB7XG4gICAgICAgIGxldCBudW06IG51bWJlciA9IFV0aWxNYXRoLnJlcGVhdChiIC0gYSwgMzYwKTtcbiAgICAgICAgaWYgKG51bSA+IDE4MCkgbnVtIC09IDM2MDtcbiAgICAgICAgcmV0dXJuIChhICsgKG51bSAqIFV0aWxNYXRoLmNsYW1wMDEodCkpKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIHJlcGVhdCh0OiBudW1iZXIsIGxlbmd0aDogbnVtYmVyKTogbnVtYmVyIHtcbiAgICAgICAgcmV0dXJuICh0IC0gKE1hdGguZmxvb3IodCAvIGxlbmd0aCkgKiBsZW5ndGgpKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDkuqfnlJ/pmo/mnLrmlbBcbiAgICAgKiDnu5PmnpzvvJp4Pj1wYXJhbTEgJiYgeDxwYXJhbTJcbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIHJhbmRSYW5nZShwYXJhbTE6IG51bWJlciwgcGFyYW0yOiBudW1iZXIpOiBudW1iZXIge1xuICAgICAgICBsZXQgbG9jOiBudW1iZXIgPSBNYXRoLnJhbmRvbSgpICogKHBhcmFtMiAtIHBhcmFtMSkgKyBwYXJhbTE7XG4gICAgICAgIHJldHVybiBsb2M7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5Lqn55Sf6ZqP5py65pWwXG4gICAgICog57uT5p6c77yaeD49cGFyYW0xICYmIHg8PXBhcmFtMlxuICAgICAqL1xuICAgIHB1YmxpYyBzdGF0aWMgcmFuZFJhbmdlSW50KHBhcmFtMTogbnVtYmVyLCBwYXJhbTI6IG51bWJlcik6IG51bWJlciB7XG4gICAgICAgIGxldCBsb2M6IG51bWJlciA9IE1hdGgucmFuZG9tKCkgKiAocGFyYW0yIC0gcGFyYW0xICsgMSkgKyBwYXJhbTE7XG4gICAgICAgIHJldHVybiBNYXRoLmZsb29yKGxvYyk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5LuO5pWw57uE5Lit5Lqn55Sf6ZqP5py65pWwWy0xLDEsMl1cbiAgICAgKiDnu5PmnpzvvJotMS8xLzLkuK3nmoTkuIDkuKpcbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIHJhbmRSYW5nZUFycmF5PFQ+KGFycjogQXJyYXk8VD4pOiBUIHtcbiAgICAgICAgaWYgKGFyci5sZW5ndGggPT0gMClcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICBsZXQgbG9jOiBUID0gYXJyW1V0aWxNYXRoLnJhbmRSYW5nZUludCgwLCBhcnIubGVuZ3RoIC0gMSldO1xuICAgICAgICByZXR1cm4gbG9jO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOi9rOaNouS4ujM2MOW6puinkuW6plxuICAgICAqL1xuICAgIHB1YmxpYyBzdGF0aWMgY2xhbXBEZWdyZWVzKGRlZ3JlZXM6IG51bWJlcik6IG51bWJlciB7XG4gICAgICAgIHdoaWxlIChkZWdyZWVzIDwgMCkgZGVncmVlcyA9IGRlZ3JlZXMgKyAzNjA7XG4gICAgICAgIHdoaWxlIChkZWdyZWVzID49IDM2MCkgZGVncmVlcyA9IGRlZ3JlZXMgLSAzNjA7XG4gICAgICAgIHJldHVybiBkZWdyZWVzO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOi9rOaNouS4ujM2MOW6puW8p+W6plxuICAgICAqL1xuICAgIHB1YmxpYyBzdGF0aWMgY2xhbXBSYWRpYW5zKHJhZGlhbnM6IG51bWJlcik6IG51bWJlciB7XG4gICAgICAgIHdoaWxlIChyYWRpYW5zIDwgMCkgcmFkaWFucyA9IHJhZGlhbnMgKyAyICogTWF0aC5QSTtcbiAgICAgICAgd2hpbGUgKHJhZGlhbnMgPj0gMiAqIE1hdGguUEkpIHJhZGlhbnMgPSByYWRpYW5zIC0gMiAqIE1hdGguUEk7XG4gICAgICAgIHJldHVybiByYWRpYW5zO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOS4pOeCuemXtOeahOi3neemu1xuICAgICAqL1xuICAgIHB1YmxpYyBzdGF0aWMgZ2V0RGlzdGFuY2UoeDE6IG51bWJlciwgeTE6IG51bWJlciwgeDI6IG51bWJlciwgeTI6IG51bWJlcik6IG51bWJlciB7XG4gICAgICAgIHJldHVybiBNYXRoLnNxcnQoTWF0aC5wb3coeTIgLSB5MSwgMikgKyBNYXRoLnBvdyh4MiAtIHgxLCAyKSk7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBnZXRTcXVhcmVEaXN0YW5jZSh4MTogbnVtYmVyLCB5MTogbnVtYmVyLCB4MjogbnVtYmVyLCB5MjogbnVtYmVyKTogbnVtYmVyIHtcbiAgICAgICAgcmV0dXJuIE1hdGgucG93KHkyIC0geTEsIDIpICsgTWF0aC5wb3coeDIgLSB4MSwgMik7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5Lik54K56Ze055qE5byn5bqm77yaeOato+aWueW9ouS4ujDvvIxZ6L205ZCR5LiLLOmhuuaXtumSiOS4uuato1xuICAgICAqL1xuICAgIHB1YmxpYyBzdGF0aWMgZ2V0TGluZVJhZGlhbnMoeDE6IG51bWJlciwgeTE6IG51bWJlciwgeDI6IG51bWJlciwgeTI6IG51bWJlcik6IG51bWJlciB7XG4gICAgICAgIHJldHVybiBNYXRoLmF0YW4yKHkyIC0geTEsIHgyIC0geDEpO1xuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgZ2V0TGluZURlZ3JlZSh4MTogbnVtYmVyLCB5MTogbnVtYmVyLCB4MjogbnVtYmVyLCB5MjogbnVtYmVyKTogbnVtYmVyIHtcbiAgICAgICAgbGV0IGRlZ3JlZTogbnVtYmVyID0gVXRpbE1hdGgudG9EZWdyZWUoVXRpbE1hdGguZ2V0TGluZVJhZGlhbnMoeDEsIHkxLCB4MiwgeTIpKTtcbiAgICAgICAgcmV0dXJuIFV0aWxNYXRoLmNsYW1wRGVncmVlcyhkZWdyZWUpO1xuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgZ2V0UG9pbnRSYWRpYW5zKHg6IG51bWJlciwgeTogbnVtYmVyKTogbnVtYmVyIHtcbiAgICAgICAgcmV0dXJuIE1hdGguYXRhbjIoeSwgeCk7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBnZXRQb2ludERlZ3JlZSh4OiBudW1iZXIsIHk6IG51bWJlcik6IG51bWJlciB7XG4gICAgICAgIGxldCBkZWdyZWU6IG51bWJlciA9IFV0aWxNYXRoLnRvRGVncmVlKFV0aWxNYXRoLmdldFBvaW50UmFkaWFucyh4LCB5KSk7XG4gICAgICAgIHJldHVybiBVdGlsTWF0aC5jbGFtcERlZ3JlZXMoZGVncmVlKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDlvKfluqbovazljJbkuLrluqZcbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIHRvRGVncmVlKHJhZGlhbjogbnVtYmVyKTogbnVtYmVyIHtcbiAgICAgICAgcmV0dXJuIHJhZGlhbiAqICgxODAuMCAvIE1hdGguUEkpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOW6pui9rOWMluS4uuW8p+W6plxuICAgICAqL1xuICAgIHB1YmxpYyBzdGF0aWMgdG9SYWRpYW4oZGVncmVlOiBudW1iZXIpOiBudW1iZXIge1xuICAgICAgICByZXR1cm4gZGVncmVlICogKE1hdGguUEkgLyAxODAuMCk7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBtb3ZlVG93YXJkcyhjdXJyZW50OiBudW1iZXIsIHRhcmdldDogbnVtYmVyLCBtYXhEZWx0YTogbnVtYmVyKTogbnVtYmVyIHtcbiAgICAgICAgaWYgKE1hdGguYWJzKHRhcmdldCAtIGN1cnJlbnQpIDw9IG1heERlbHRhKSB7XG4gICAgICAgICAgICByZXR1cm4gdGFyZ2V0O1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiAoY3VycmVudCArIChVdGlsTWF0aC5TaWduKHRhcmdldCAtIGN1cnJlbnQpICogbWF4RGVsdGEpKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDojrflj5bkuIDlrprojIPlm7TlhoXnmoTpmo/mnLrmlbTmlbBcbiAgICAgKiBAcGFyYW0gbWluIOacgOWwj+WAvFxuICAgICAqIEBwYXJhbSBtYXgg5pyA5aSn5YC8XG4gICAgICogQGNvbnN0cnVjdG9yXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyByYW5kb20obWluOiBudW1iZXIsIG1heDogbnVtYmVyKTogbnVtYmVyIHtcbiAgICAgICAgcmV0dXJuIE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSAqIChtYXggLSBtaW4pICsgbWluKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDkuoznu7TlkJHph4/lvZLkuIDljJZcbiAgICAgKiBAcGFyYW0geFxuICAgICAqIEBwYXJhbSB5XG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyBub3JtYWxpemUoeDpudW1iZXIseTpudW1iZXIpOm51bWJlcntcbiAgICAgICAgcmV0dXJuIE1hdGguc3FydCh4KngreSp5KTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDov5Tlm57kuKTlkJHph4/lpLnop5JcbiAgICAgKiBAcGFyYW0geDFcbiAgICAgKiBAcGFyYW0geTFcbiAgICAgKiBAcGFyYW0geDJcbiAgICAgKiBAcGFyYW0geTJcbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIHZlY3RvckFuZ2xlKHgxOm51bWJlcix5MTpudW1iZXIseDI6bnVtYmVyLHkyOm51bWJlcik6bnVtYmVyXG4gICAge1xuICAgICAgICBpZiAoeDEgPT0geDIgJiYgeTEgPT0geTIpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICB2YXIgY29zQW5nbGUgPSAoeDEqeDIreTEqeTIpLyhVdGlsTWF0aC5ub3JtYWxpemUoeDEseTEpKlV0aWxNYXRoLm5vcm1hbGl6ZSh4Mix5MikpO1xuICAgICAgICB2YXIgYUNvc0FuZ2xlID0gTWF0aC5hY29zKGNvc0FuZ2xlKTtcbiAgICAgICAgdmFyIGFuZ2xlID0gVXRpbE1hdGgzRC5SYWQyRGVnKGFDb3NBbmdsZSk7XG4gICAgICAgIGlmICh4MSAvIHkxIDwgeDIgLyB5MikgYW5nbGUgPSAtIGFuZ2xlO1xuICAgICAgICByZXR1cm4gYW5nbGU7XG4gICAgfVxuXG59XG5cbiIsImltcG9ydCBSYXkgPSBMYXlhLlJheTtcbmltcG9ydCBWZWN0b3IyID0gTGF5YS5WZWN0b3IyO1xuaW1wb3J0IFZlY3RvcjMgPSBMYXlhLlZlY3RvcjM7XG5pbXBvcnQgVmVjdG9yNCA9IExheWEuVmVjdG9yNDtcbmltcG9ydCB7VXRpbFN0cmluZ30gZnJvbSBcIi4vc3RyaW5nXCI7XG5cbi8qKlxuICogQGF1dGhvciBTdW5cbiAqIEB0aW1lIDIwMTktMDgtMTEgMTg6MDhcbiAqIEBwcm9qZWN0IFNGcmFtZXdvcmtfTGF5YUFpclxuICogQGRlc2NyaXB0aW9uIDNk566X5rOV5bel5YW357G7XG4gKlxuICovXG5leHBvcnQgY2xhc3MgVXRpbE1hdGgzRCB7XG5cbiAgICBwcml2YXRlIHN0YXRpYyBfVmVjMlRlbXA6IFZlY3RvcjIgPSBudWxsO1xuICAgIHByaXZhdGUgc3RhdGljIF9WZWMzVGVtcDogVmVjdG9yMyA9IG51bGw7XG4gICAgcHJpdmF0ZSBzdGF0aWMgX1ZlYzRUZW1wOiBWZWN0b3I0ID0gbnVsbDtcblxuICAgIHB1YmxpYyBzdGF0aWMgZ2V0IFRlbXBWZWMyKCk6IFZlY3RvcjIge1xuICAgICAgICBpZiAoIVV0aWxNYXRoM0QuX1ZlYzJUZW1wKSBVdGlsTWF0aDNELl9WZWMyVGVtcCA9IG5ldyBWZWN0b3IyKDAsIDApO1xuICAgICAgICByZXR1cm4gVXRpbE1hdGgzRC5fVmVjMlRlbXA7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBnZXQgVGVtcFZlYzMoKTogVmVjdG9yMyB7XG4gICAgICAgIGlmICghVXRpbE1hdGgzRC5fVmVjM1RlbXApIFV0aWxNYXRoM0QuX1ZlYzNUZW1wID0gbmV3IFZlY3RvcjMoMCwgMCwgMCk7XG4gICAgICAgIHJldHVybiBVdGlsTWF0aDNELl9WZWMzVGVtcDtcbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIGdldCBUZW1wVmVjNCgpOiBWZWN0b3I0IHtcbiAgICAgICAgaWYgKCFVdGlsTWF0aDNELl9WZWM0VGVtcCkgVXRpbE1hdGgzRC5fVmVjNFRlbXAgPSBuZXcgVmVjdG9yNCgwLCAwLCAwLCAwKTtcbiAgICAgICAgcmV0dXJuIFV0aWxNYXRoM0QuX1ZlYzRUZW1wO1xuICAgIH1cblxuICAgIC8qKui9rOaNouS4uuawtOW5s+aWueWQkSovXG4gICAgcHVibGljIHN0YXRpYyBUb0hvcml6b250YWwodmVjOiBWZWN0b3IzKTogVmVjdG9yMyB7XG4gICAgICAgIHZlYy55ID0gMDtcbiAgICAgICAgcmV0dXJuIHZlYztcbiAgICB9XG5cbiAgICAvKirmsLTlubPot53nprsqL1xuICAgIHB1YmxpYyBzdGF0aWMgSG9yaXpvbnRhbERpc3RhbmNlKHZlYzE6IFZlY3RvcjMsIHZlYzI6IFZlY3RvcjMpOiBudW1iZXIge1xuICAgICAgICB2ZWMxLnkgPSAwO1xuICAgICAgICB2ZWMyLnkgPSAwO1xuICAgICAgICByZXR1cm4gVmVjdG9yMy5zY2FsYXJMZW5ndGgoVmVjM1N1Yih2ZWMxLCB2ZWMyKSk7XG4gICAgfVxuXG4gICAgLyoq5bCE57q/5LiK55qE5LiA54K5Ki9cbiAgICBwdWJsaWMgc3RhdGljIEdldFJheVBvaW50KHJheTogUmF5LCBkaXN0YW5jZTogbnVtYmVyKTogVmVjdG9yMyB7XG4gICAgICAgIHJldHVybiBWZWMzQWRkKHJheS5vcmlnaW4sIFZlYzNNdWwocmF5LmRpcmVjdGlvbiwgZGlzdGFuY2UpKTtcbiAgICB9XG5cbiAgICAvKiogRGVzOuS4iee7tOaxguS4pOeCuei3neemuyAqL1xuICAgIHB1YmxpYyBzdGF0aWMgVmVjM01hZ25pdHVkZSh2ZWMxOiBWZWN0b3IzLHZlYzI6VmVjdG9yMyk6IG51bWJlciB7XG4gICAgICAgIHJldHVybiBNYXRoLnNxcnQoKHZlYzEueC12ZWMyLngpICogKHZlYzEueC12ZWMyLngpICsgKCh2ZWMxLnktdmVjMi55KSAqICh2ZWMxLnktdmVjMi55KSkgKyAoKHZlYzEuei12ZWMyLnopICogKHZlYzEuei12ZWMyLnopKSk7XG4gICAgfVxuXG4gICAgLyoqIERlczrkuoznu7TmsYLkuKTngrnot53nprsgKi9cbiAgICBwdWJsaWMgc3RhdGljIFZlYzJNYWduaXR1ZGUodmVjMTogVmVjdG9yMix2ZWMyOlZlY3RvcjIpOiBudW1iZXIge1xuICAgICAgICByZXR1cm4gTWF0aC5zcXJ0KCh2ZWMxLngtdmVjMi54KSAqICh2ZWMxLngtdmVjMi54KSArICgodmVjMS55LXZlYzIueSkgKiAodmVjMS55LXZlYzIueSkpKTtcbiAgICB9XG5cbiAgICAvKiogRGVzOuinkuW6pui9rOW8p+W6piAqL1xuICAgIHB1YmxpYyBzdGF0aWMgRGVnMlJhZChhbmdsZTpudW1iZXIpOm51bWJlcntcbiAgICAgICAgcmV0dXJuIExheWEuVXRpbHMudG9SYWRpYW4oYW5nbGUpO1xuICAgIH1cblxuICAgIC8qKiBEZXM65byn5bqm6L2s6KeS5bqmICovXG4gICAgcHVibGljIHN0YXRpYyBSYWQyRGVnKHJhZGlhbjpudW1iZXIpOm51bWJlcntcbiAgICAgICAgcmV0dXJuIExheWEuVXRpbHMudG9BbmdsZShyYWRpYW4pO1xuICAgIH1cblxuICAgIC8qKiBEZXM65q2j5bymICovXG4gICAgcHVibGljIHN0YXRpYyBzaW4oYW5nbGU6bnVtYmVyKTpudW1iZXJ7XG4gICAgICAgIHZhciByYWRpYW4gPSBVdGlsTWF0aDNELkRlZzJSYWQoYW5nbGUpO1xuICAgICAgICByZXR1cm4gTWF0aC5zaW4ocmFkaWFuKTtcbiAgICB9XG4gICAgLyoqIERlczrkvZnlvKYgKi9cbiAgICBwdWJsaWMgc3RhdGljIGNvcyhhbmdsZTpudW1iZXIpOm51bWJlcntcbiAgICAgICAgdmFyIHJhZGlhbiA9IFV0aWxNYXRoM0QuRGVnMlJhZChhbmdsZSk7XG4gICAgICAgIHJldHVybiBNYXRoLmNvcyhyYWRpYW4pO1xuICAgIH1cbiAgICAvKiogRGVzOuato+WIhyAqL1xuICAgIHB1YmxpYyBzdGF0aWMgdGFuKGFuZ2xlOm51bWJlcik6bnVtYmVye1xuICAgICAgICB2YXIgcmFkaWFuID0gVXRpbE1hdGgzRC5EZWcyUmFkKGFuZ2xlKTtcbiAgICAgICAgcmV0dXJuIE1hdGgudGFuKHJhZGlhbik7XG4gICAgfVxuICAgIC8qKiBEZXM65Y+N5q2j5bymICovXG4gICAgcHVibGljIHN0YXRpYyBhc2luKGFuZ2xlOm51bWJlcik6bnVtYmVye1xuICAgICAgICB2YXIgcmFkaWFuID0gVXRpbE1hdGgzRC5EZWcyUmFkKGFuZ2xlKTtcbiAgICAgICAgcmV0dXJuIE1hdGguYXNpbihyYWRpYW4pO1xuICAgIH1cbiAgICAvKiogRGVzOuWPjeS9meW8piAqL1xuICAgIHB1YmxpYyBzdGF0aWMgYWNvcyhhbmdsZTpudW1iZXIpOm51bWJlcntcbiAgICAgICAgdmFyIHJhZGlhbiA9IFV0aWxNYXRoM0QuRGVnMlJhZChhbmdsZSk7XG4gICAgICAgIHJldHVybiBNYXRoLmFjb3MocmFkaWFuKTtcbiAgICB9XG4gICAgLyoqIERlczrlj43mraPliIcgKi9cbiAgICBwdWJsaWMgc3RhdGljIGF0YW4oYW5nbGU6bnVtYmVyKTpudW1iZXJ7XG4gICAgICAgIHZhciByYWRpYW4gPSBVdGlsTWF0aDNELkRlZzJSYWQoYW5nbGUpO1xuICAgICAgICByZXR1cm4gTWF0aC5hdGFuKHJhZGlhbik7XG4gICAgfVxuXG5cblxuXG59XG5cbi8v772e772e772e772e772e772e772e772e772e772e772e772e772e772e772e772e772e772e772e772e772e772e772edmVjMu+9nu+9nu+9nu+9nu+9nu+9nu+9nu+9nu+9nu+9nu+9nu+9nu+9nu+9nu+9nu+9nu+9nu+9nu+9nu+9nu+9nu+9nu+9ni8vXG5leHBvcnQgZnVuY3Rpb24gVmVjMkFkZChhOiBWZWN0b3IyLCBiOiBWZWN0b3IyKTogVmVjdG9yMiB7XG4gICAgcmV0dXJuIG5ldyBWZWN0b3IyKGEueCArIGIueCwgYS55ICsgYi55KTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIFZlYzJTdWIoYTogVmVjdG9yMiwgYjogVmVjdG9yMik6IFZlY3RvcjIge1xuICAgIHJldHVybiBuZXcgVmVjdG9yMihhLnggLSBiLngsIGEueSAtIGIueSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBWZWMyTXVsdGlwbHkoYTogVmVjdG9yMiwgYjogVmVjdG9yMik6IFZlY3RvcjIge1xuICAgIHJldHVybiBuZXcgVmVjdG9yMihhLnggKiBiLngsIGEueSAqIGIueSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBWZWMyTXVsKGE6IFZlY3RvcjIsIGQ6IG51bWJlcik6IFZlY3RvcjIge1xuICAgIHJldHVybiBuZXcgVmVjdG9yMihhLnggKiBkLCBhLnkgKiBkKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIFZlYzJEaXYoYTogVmVjdG9yMiwgZDogbnVtYmVyKTogVmVjdG9yMiB7XG4gICAgcmV0dXJuIG5ldyBWZWN0b3IyKGEueCAvIGQsIGEueSAvIGQpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gVmVjMkRvdChsaHM6IFZlY3RvcjIsIHJoczogVmVjdG9yMik6IG51bWJlciB7XG4gICAgcmV0dXJuICgobGhzLnggKiByaHMueCkgKyAobGhzLnkgKiByaHMueSkpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gVmVjMlByb2plY3QodmVjdG9yOiBWZWN0b3IyLCBvbk5vcm1hbDogVmVjdG9yMik6IFZlY3RvcjIge1xuICAgIGxldCBudW06IG51bWJlciA9IFZlYzJEb3Qob25Ob3JtYWwsIG9uTm9ybWFsKTtcbiAgICBpZiAobnVtIDwgMUUtMDUpIHtcbiAgICAgICAgcmV0dXJuIFZlY3RvcjIuWkVSTztcbiAgICB9XG4gICAgcmV0dXJuIChWZWMyRGl2KFZlYzJNdWwob25Ob3JtYWwsIFZlYzJEb3QodmVjdG9yLCBvbk5vcm1hbCkpLCBudW0pKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIFZlYzJNaW4obGhzOiBWZWN0b3IyLCByaHM6IFZlY3RvcjIpOiBWZWN0b3IyIHtcbiAgICByZXR1cm4gbmV3IFZlY3RvcjIoTWF0aC5taW4obGhzLngsIHJocy54KSwgTWF0aC5taW4obGhzLnksIHJocy55KSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBWZWMyTWF4KGxoczogVmVjdG9yMiwgcmhzOiBWZWN0b3IyKTogVmVjdG9yMiB7XG4gICAgcmV0dXJuIG5ldyBWZWN0b3IyKE1hdGgubWF4KGxocy54LCByaHMueCksIE1hdGgubWF4KGxocy55LCByaHMueSkpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gVmVjMk1hZ25pdHVkZSh2ZWM6IFZlY3RvcjIpOiBudW1iZXIge1xuICAgIHJldHVybiBNYXRoLnNxcnQoKHZlYy54ICogdmVjLngpICsgKHZlYy55ICogdmVjLnkpKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIFZlYzJTcXJNYWduaXR1ZGUodmVjOiBWZWN0b3IyKTogbnVtYmVyIHtcbiAgICByZXR1cm4gKHZlYy54ICogdmVjLngpICsgKHZlYy55ICogdmVjLnkpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gVmVjMk5vcm1hbGl6ZWQodmVjOiBWZWN0b3IyKTogVmVjdG9yMiB7XG4gICAgbGV0IG1hZ25pdHVkZTogbnVtYmVyID0gVmVjMk1hZ25pdHVkZSh2ZWMpO1xuICAgIGxldCB2OiBWZWN0b3IyO1xuICAgIGlmIChtYWduaXR1ZGUgPiAxRS0wNSlcbiAgICAgICAgdiA9IFZlYzJEaXYodmVjLCBtYWduaXR1ZGUpO1xuICAgIGVsc2VcbiAgICAgICAgdiA9IG5ldyBWZWN0b3IyKDAsIDApO1xuICAgIHJldHVybiB2O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gVmVjMk5vcm1hbCh2ZWM6IFZlY3RvcjIpOiB2b2lkIHtcbiAgICBsZXQgbWFnbml0dWRlOiBudW1iZXIgPSBWZWMyTWFnbml0dWRlKHZlYyk7XG4gICAgaWYgKG1hZ25pdHVkZSA+IDFFLTA1KSB7XG4gICAgICAgIGxldCB2OiBWZWN0b3IyID0gVmVjMkRpdih2ZWMsIG1hZ25pdHVkZSk7XG4gICAgICAgIFZlYzJTZXQodmVjLCB2LngsIHYueSk7XG4gICAgfSBlbHNlXG4gICAgICAgIFZlYzJTZXQodmVjLCAwLCAwKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIFZlYzJTZXQodjogVmVjdG9yMiwgeDogbnVtYmVyLCB5OiBudW1iZXIpOiBWZWN0b3IyIHtcbiAgICB2LnggPSB4O1xuICAgIHYueSA9IHk7XG4gICAgO1xuICAgIHJldHVybiB2O1xufVxuXG4vLyBleHBvcnQgZnVuY3Rpb24gVmVjMkFuZ2xlKGZyb206IFZlY3RvcjIsIHRvOiBWZWN0b3IyKTogbnVtYmVyIHtcbi8vICAgICByZXR1cm4gKE1hdGguYWNvcyhVdGlsTWF0aC5jbGFtcChWZWMyRG90KFZlYzJOb3JtYWxpemVkKGZyb20pLCBWZWMyTm9ybWFsaXplZCh0bykpLCAtMSwgMSkpICogNTcuMjk1NzgpO1xuLy8gfVxuXG5leHBvcnQgZnVuY3Rpb24gVmVjMkNsYW1wTWFnbml0dWRlKHZlY3RvcjogVmVjdG9yMiwgbWF4TGVuZ3RoKTogVmVjdG9yMiB7XG4gICAgaWYgKFZlYzJTcXJNYWduaXR1ZGUodmVjdG9yKSA+IChtYXhMZW5ndGggKiBtYXhMZW5ndGgpKSB7XG4gICAgICAgIHJldHVybiAoVmVjMk11bChWZWMyTm9ybWFsaXplZCh2ZWN0b3IpLCBtYXhMZW5ndGgpKTtcbiAgICB9XG4gICAgcmV0dXJuIHZlY3Rvcjtcbn1cblxuLy8gZXhwb3J0IGZ1bmN0aW9uIFZlYzJMZXJwKGZyb206IFZlY3RvcjIsIHRvOiBWZWN0b3IyLCB0OiBudW1iZXIpOiBWZWN0b3IyIHtcbi8vICAgICB0ID0gVXRpbE1hdGguY2xhbXAodCwgMCwgMSk7XG4vLyAgICAgcmV0dXJuIG5ldyBWZWN0b3IyKGZyb20ueCArICgodG8ueCAtIGZyb20ueCkgKiB0KSwgZnJvbS55ICsgKCh0by55IC0gZnJvbS55KSAqIHQpKTtcbi8vIH1cblxuZXhwb3J0IGZ1bmN0aW9uIFZlYzJNb3ZlVG93YXJkcyhjdXJyZW50OiBWZWN0b3IyLCB0YXJnZXQ6IFZlY3RvcjIsIG1heERpc3RhbmNlRGVsdGE6IG51bWJlcik6IFZlY3RvcjIge1xuICAgIGxldCB2ZWN0b3I6IFZlY3RvcjIgPSBWZWMyU3ViKHRhcmdldCwgY3VycmVudCk7XG4gICAgbGV0IG1hZ25pdHVkZTogbnVtYmVyID0gVmVjMk1hZ25pdHVkZSh2ZWN0b3IpO1xuICAgIGlmICgobWFnbml0dWRlID4gbWF4RGlzdGFuY2VEZWx0YSkgJiYgKG1hZ25pdHVkZSAhPSAwKSkge1xuICAgICAgICByZXR1cm4gVmVjMkFkZChjdXJyZW50LCAoVmVjMk11bChWZWMyRGl2KHZlY3RvciwgbWFnbml0dWRlKSwgbWF4RGlzdGFuY2VEZWx0YSkpKTtcbiAgICB9XG4gICAgcmV0dXJuIHRhcmdldDtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIFZlYzJUb1N0cmluZyh2ZWM6IFZlY3RvcjIpOiBzdHJpbmcge1xuICAgIHJldHVybiBVdGlsU3RyaW5nLmZvcm1hdChcIih7MH0sIHsxfSlcIiwgdmVjLngsIHZlYy55KTtcbn1cblxuLy/vvZ7vvZ7vvZ7vvZ7vvZ7vvZ7vvZ7vvZ7vvZ7vvZ7vvZ7vvZ7vvZ7vvZ7vvZ7vvZ7vvZ7vvZ7vvZ7vvZ7vvZ7vvZ7vvZ52ZWMz772e772e772e772e772e772e772e772e772e772e772e772e772e772e772e772e772e772e772e772e772e772e772eLy9cbmV4cG9ydCBmdW5jdGlvbiBWZWMzQWRkKGE6IFZlY3RvcjMsIGI6IFZlY3RvcjMpOiBWZWN0b3IzIHtcbiAgICByZXR1cm4gbmV3IFZlY3RvcjMoYS54ICsgYi54LCBhLnkgKyBiLnksIGEueiArIGIueik7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBWZWMzU3ViKGE6IFZlY3RvcjMsIGI6IFZlY3RvcjMpOiBWZWN0b3IzIHtcbiAgICByZXR1cm4gbmV3IFZlY3RvcjMoYS54IC0gYi54LCBhLnkgLSBiLnksIGEueiAtIGIueik7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBWZWMzTXVsdGlwbHkoYTogVmVjdG9yMywgYjogVmVjdG9yMyk6IFZlY3RvcjMge1xuICAgIHJldHVybiBuZXcgVmVjdG9yMyhhLnggKiBiLngsIGEueSAqIGIueSwgYS56ICogYi56KTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIFZlYzNNdWwoYTogVmVjdG9yMywgZDogbnVtYmVyKTogVmVjdG9yMyB7XG4gICAgcmV0dXJuIG5ldyBWZWN0b3IzKGEueCAqIGQsIGEueSAqIGQsIGEueiAqIGQpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gVmVjM0RpdihhOiBWZWN0b3IzLCBkOiBudW1iZXIpOiBWZWN0b3IzIHtcbiAgICByZXR1cm4gbmV3IFZlY3RvcjMoYS54IC8gZCwgYS55IC8gZCwgYS56IC8gZCk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBWZWMzQ3Jvc3MobGhzOiBWZWN0b3IzLCByaHM6IFZlY3RvcjMpOiBWZWN0b3IzIHtcbiAgICByZXR1cm4gbmV3IFZlY3RvcjMoKGxocy55ICogcmhzLnopIC0gKGxocy56ICogcmhzLnkpLCAobGhzLnogKiByaHMueCkgLSAobGhzLnggKiByaHMueiksIChsaHMueCAqIHJocy55KSAtIChsaHMueSAqIHJocy54KSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBWZWMzUHJvamVjdCh2ZWN0b3I6IFZlY3RvcjMsIG9uTm9ybWFsOiBWZWN0b3IzKTogVmVjdG9yMyB7XG4gICAgbGV0IG51bTogbnVtYmVyID0gVmVjdG9yMy5kb3Qob25Ob3JtYWwsIG9uTm9ybWFsKTtcbiAgICBpZiAobnVtIDwgMUUtMDUpIHtcbiAgICAgICAgcmV0dXJuIG5ldyBWZWN0b3IzKCk7XG4gICAgfVxuICAgIHJldHVybiAoVmVjM0RpdihWZWMzTXVsKG9uTm9ybWFsLCBWZWN0b3IzLmRvdCh2ZWN0b3IsIG9uTm9ybWFsKSksIG51bSkpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gVmVjM01pbihsaHM6IFZlY3RvcjMsIHJoczogVmVjdG9yMyk6IFZlY3RvcjMge1xuICAgIHJldHVybiBuZXcgVmVjdG9yMyhNYXRoLm1pbihsaHMueCwgcmhzLngpLCBNYXRoLm1pbihsaHMueSwgcmhzLnkpLCBNYXRoLm1pbihsaHMueiwgcmhzLnopKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIFZlYzNNYXgobGhzOiBWZWN0b3IzLCByaHM6IFZlY3RvcjMpOiBWZWN0b3IzIHtcbiAgICByZXR1cm4gbmV3IFZlY3RvcjMoTWF0aC5tYXgobGhzLngsIHJocy54KSwgTWF0aC5tYXgobGhzLnksIHJocy55KSwgTWF0aC5tYXgobGhzLnosIHJocy56KSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBWZWMzTWFnbml0dWRlKHZlYzogVmVjdG9yMyk6IG51bWJlciB7XG4gICAgcmV0dXJuIE1hdGguc3FydCgodmVjLnggKiB2ZWMueCkgKyAodmVjLnkgKiB2ZWMueSkgKyAodmVjLnogKiB2ZWMueikpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gVmVjM1Nxck1hZ25pdHVkZSh2ZWM6IFZlY3RvcjMpOiBudW1iZXIge1xuICAgIHJldHVybiAodmVjLnggKiB2ZWMueCkgKyAodmVjLnkgKiB2ZWMueSkgKyAodmVjLnogKiB2ZWMueik7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBWZWMzTm9ybWFsaXplZCh2ZWM6IFZlY3RvcjMpOiBWZWN0b3IzIHtcbiAgICBsZXQgbWFnbml0dWRlOiBudW1iZXIgPSBWZWN0b3IzLnNjYWxhckxlbmd0aCh2ZWMpO1xuICAgIGxldCB2OiBWZWN0b3IzO1xuICAgIGlmIChtYWduaXR1ZGUgPiAxRS0wNSlcbiAgICAgICAgdiA9IFZlYzNEaXYodmVjLCBtYWduaXR1ZGUpO1xuICAgIGVsc2VcbiAgICAgICAgdiA9IG5ldyBWZWN0b3IzKDAsIDAsIDApO1xuICAgIHJldHVybiB2O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gVmVjM05vcm1hbCh2ZWM6IFZlY3RvcjMpOiB2b2lkIHtcbiAgICBsZXQgbWFnbml0dWRlOiBudW1iZXIgPSBWZWN0b3IzLnNjYWxhckxlbmd0aCh2ZWMpO1xuICAgIGlmIChtYWduaXR1ZGUgPiAxRS0wNSkge1xuICAgICAgICBsZXQgdjogVmVjdG9yMyA9IFZlYzNEaXYodmVjLCBtYWduaXR1ZGUpO1xuICAgICAgICBWZWMzU2V0KHZlYywgdi54LCB2LnksIHYueik7XG4gICAgfSBlbHNlXG4gICAgICAgIFZlYzNTZXQodmVjLCAwLCAwLCAwKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIFZlYzNTZXQodjogVmVjdG9yMywgeDogbnVtYmVyLCB5OiBudW1iZXIsIHo6IG51bWJlcik6IFZlY3RvcjMge1xuICAgIHYueCA9IHg7XG4gICAgdi55ID0geTtcbiAgICB2LnogPSB6O1xuICAgIHJldHVybiB2O1xufVxuXG4vLyBleHBvcnQgZnVuY3Rpb24gVmVjM0FuZ2xlKGZyb206IFZlY3RvcjMsIHRvOiBWZWN0b3IzKTogbnVtYmVyIHtcbi8vICAgICByZXR1cm4gKE1hdGguYWNvcyhVdGlsTWF0aC5jbGFtcChWZWN0b3IzLmRvdChWZWMzTm9ybWFsaXplZChmcm9tKSwgVmVjM05vcm1hbGl6ZWQodG8pKSwgLTEsIDEpKSAqIDU3LjI5NTc4KTtcbi8vIH1cblxuZXhwb3J0IGZ1bmN0aW9uIFZlYzNDbGFtcE1hZ25pdHVkZSh2ZWN0b3I6IFZlY3RvcjMsIG1heExlbmd0aCk6IFZlY3RvcjMge1xuICAgIGlmIChWZWN0b3IzLnNjYWxhckxlbmd0aFNxdWFyZWQodmVjdG9yKSA+IChtYXhMZW5ndGggKiBtYXhMZW5ndGgpKSB7XG4gICAgICAgIHJldHVybiAoVmVjM011bChWZWMzTm9ybWFsaXplZCh2ZWN0b3IpLCBtYXhMZW5ndGgpKTtcbiAgICB9XG4gICAgcmV0dXJuIHZlY3Rvcjtcbn1cblxuLy8gZXhwb3J0IGZ1bmN0aW9uIFZlYzNMZXJwKGZyb206IFZlY3RvcjMsIHRvOiBWZWN0b3IzLCB0OiBudW1iZXIpOiBWZWN0b3IzIHtcbi8vICAgICB0ID0gVXRpbE1hdGguY2xhbXAodCwgMCwgMSk7XG4vLyAgICAgcmV0dXJuIG5ldyBWZWN0b3IzKGZyb20ueCArICgodG8ueCAtIGZyb20ueCkgKiB0KSwgZnJvbS55ICsgKCh0by55IC0gZnJvbS55KSAqIHQpLCBmcm9tLnogKyAoKHRvLnogLSBmcm9tLnopICogdCkpO1xuLy8gfVxuXG5leHBvcnQgZnVuY3Rpb24gVmVjM01vdmVUb3dhcmRzKGN1cnJlbnQ6IFZlY3RvcjMsIHRhcmdldDogVmVjdG9yMywgbWF4RGlzdGFuY2VEZWx0YTogbnVtYmVyKTogVmVjdG9yMyB7XG4gICAgbGV0IHZlY3RvcjogVmVjdG9yMyA9IFZlYzNTdWIodGFyZ2V0LCBjdXJyZW50KTtcbiAgICBsZXQgbWFnbml0dWRlOiBudW1iZXIgPSBWZWN0b3IzLnNjYWxhckxlbmd0aCh2ZWN0b3IpO1xuICAgIGlmICgobWFnbml0dWRlID4gbWF4RGlzdGFuY2VEZWx0YSkgJiYgKG1hZ25pdHVkZSAhPSAwKSkge1xuICAgICAgICByZXR1cm4gVmVjM0FkZChjdXJyZW50LCAoVmVjM011bChWZWMzRGl2KHZlY3RvciwgbWFnbml0dWRlKSwgbWF4RGlzdGFuY2VEZWx0YSkpKTtcbiAgICB9XG4gICAgcmV0dXJuIHRhcmdldDtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIFZlYzNUb1N0cmluZyh2ZWM6IFZlY3RvcjMpOiBzdHJpbmcge1xuICAgIHJldHVybiBVdGlsU3RyaW5nLmZvcm1hdChcIih7MH0sIHsxfSwgezJ9KVwiLCB2ZWMueCwgdmVjLnksIHZlYy56KTtcbn1cblxuLyoqXG4gKiDlvKfluqbovazlkJHph49cbiAqIEBwYXJhbSAgICByYWRpYW5zICAgIOW8p+W6plxuICovXG5leHBvcnQgZnVuY3Rpb24gZ2V0TGluZUZyb21SYWRpYW5zKHJhZGlhbnM6IG51bWJlcik6IFZlY3RvcjIge1xuICAgIGxldCB4OiBudW1iZXIgPSBNYXRoLmNvcyhyYWRpYW5zKTtcbiAgICBsZXQgeTogbnVtYmVyID0gTWF0aC5zaW4ocmFkaWFucyk7XG4gICAgbGV0IGRpcjogVmVjdG9yMiA9IG5ldyBWZWN0b3IyKHgsIHkpO1xuICAgIFZlYzJOb3JtYWwoZGlyKTtcbiAgICByZXR1cm4gZGlyO1xufVxuXG4iLCJpbXBvcnQgeyBVdGlsU3RyaW5nIH0gZnJvbSAnLi9zdHJpbmcnO1xuXG4vKipcbiAqIEBhdXRob3IgU3VuXG4gKiBAdGltZSAyMDE5LTA4LTExIDE4OjU0XG4gKiBAcHJvamVjdCBTRnJhbWV3b3JrX0xheWFBaXJcbiAqIEBkZXNjcmlwdGlvbiDmlbDlgLzlt6XlhbfnsbtcbiAqXG4gKi9cbmV4cG9ydCBjbGFzcyBVdGlsTnVtYmVyIHtcbiAgICAvKipcbiAgICAgKiDkv53nlZnlsI/mlbDngrnlkI7lh6DkvY1cbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIHRvRml4ZWQodmFsdWU6IG51bWJlciwgcDogbnVtYmVyKTogbnVtYmVyIHtcbiAgICAgICAgcmV0dXJuIFV0aWxTdHJpbmcudG9OdW1iZXIodmFsdWUudG9GaXhlZChwKSk7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyB0b0ludCh2YWx1ZTogbnVtYmVyKTogbnVtYmVyIHtcbiAgICAgICAgcmV0dXJuIE1hdGguZmxvb3IodmFsdWUpO1xuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgaXNJbnQodmFsdWU6IG51bWJlcik6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gTWF0aC5jZWlsKHZhbHVlKSA9PSB2YWx1ZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDkv53nlZnmnInmlYjmlbDlgLxcbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIHJlc2VydmVOdW1iZXIobnVtOiBudW1iZXIsIHNpemU6IG51bWJlcik6IG51bWJlciB7XG4gICAgICAgIGxldCBzdHIgPSBTdHJpbmcobnVtKTtcbiAgICAgICAgbGV0IGwgPSBzdHIubGVuZ3RoO1xuICAgICAgICBsZXQgcF9pbmRleDogbnVtYmVyID0gc3RyLmluZGV4T2YoXCIuXCIpO1xuICAgICAgICBpZiAocF9pbmRleCA8IDApIHtcbiAgICAgICAgICAgIHJldHVybiBudW07XG4gICAgICAgIH1cbiAgICAgICAgbGV0IHJldDogc3RyaW5nID0gc3RyLnNsaWNlKDAsIHBfaW5kZXggKyAxKTtcblxuICAgICAgICBsZXQgbGFzdE51bSA9IGwgLSBwX2luZGV4O1xuICAgICAgICBpZiAobGFzdE51bSA+IHNpemUpIHtcbiAgICAgICAgICAgIGxhc3ROdW0gPSBzaXplO1xuICAgICAgICB9XG4gICAgICAgIGxldCBsYXN0U3RyOiBzdHJpbmcgPSBzdHIuc2xpY2UocF9pbmRleCArIDEsIHBfaW5kZXggKyAxICsgbGFzdE51bSk7XG4gICAgICAgIHJldHVybiBVdGlsU3RyaW5nLnRvTnVtYmVyKHJldCArIGxhc3RTdHIpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOS/neeVmeacieaViOaVsOWAvO+8jOS4jeWkn+ihpTDvvJvms6jmhI/ov5Tlm57nmoTmmK/lrZfnrKbkuLJcbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIHJlc2VydmVOdW1iZXJXaXRoWmVybyhudW06IG51bWJlciwgc2l6ZTogbnVtYmVyKTogc3RyaW5nIHtcbiAgICAgICAgbGV0IHN0ciA9IFN0cmluZyhudW0pO1xuICAgICAgICBsZXQgbCA9IHN0ci5sZW5ndGg7XG4gICAgICAgIGxldCBwX2luZGV4OiBudW1iZXIgPSBzdHIuaW5kZXhPZihcIi5cIik7XG4gICAgICAgIGlmIChwX2luZGV4IDwgMCkgey8v5piv5pW05pWwXG4gICAgICAgICAgICBzdHIgKz0gJy4nO1xuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBzaXplOyArK2kpIHN0ciArPSAnMCc7XG4gICAgICAgICAgICByZXR1cm4gc3RyO1xuICAgICAgICB9XG4gICAgICAgIGxldCByZXQ6IHN0cmluZyA9IHN0ci5zbGljZSgwLCBwX2luZGV4ICsgMSk7XG5cbiAgICAgICAgbGV0IGxhc3ROdW0gPSBsIC0gcF9pbmRleCAtIDE7XG4gICAgICAgIGlmIChsYXN0TnVtID4gc2l6ZSkgey8v6LaF6L+HXG4gICAgICAgICAgICBsYXN0TnVtID0gc2l6ZTtcbiAgICAgICAgICAgIGxldCBsYXN0U3RyOiBzdHJpbmcgPSBzdHIuc2xpY2UocF9pbmRleCArIDEsIHBfaW5kZXggKyAxICsgbGFzdE51bSk7XG4gICAgICAgICAgICByZXR1cm4gcmV0ICsgbGFzdFN0cjtcbiAgICAgICAgfSBlbHNlIGlmIChsYXN0TnVtIDwgc2l6ZSkgey8v5LiN6Laz6KGlMFxuICAgICAgICAgICAgbGV0IGRpZmY6IG51bWJlciA9IHNpemUgLSBsYXN0TnVtO1xuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBkaWZmOyArK2kpIHN0ciArPSAnMCc7XG4gICAgICAgICAgICByZXR1cm4gc3RyO1xuICAgICAgICB9IGVsc2VcbiAgICAgICAgICAgIHJldHVybiBzdHI7XG4gICAgfVxuXG5cbiAgICAvKipcbiAgICAgKlxuICAgICAqL1xuICAgIHB1YmxpYyBzdGF0aWMgZm9ybWF0VGhvdXNhbmRzTnVtYmVyKG51bTogbnVtYmVyKSB7XG4gICAgICAgIGlmIChudW0gPCAxMDAwMDAwKSB7XG4gICAgICAgICAgICByZXR1cm4gbnVtLnRvTG9jYWxlU3RyaW5nKCk7XG4gICAgICAgIH0gZWxzZSBpZiAobnVtIDwgMTAwMDAwMDAwMCkge1xuICAgICAgICAgICAgbGV0IHQgPSBNYXRoLmZsb29yKG51bSAvIDEwMDApXG4gICAgICAgICAgICByZXR1cm4gdC50b0xvY2FsZVN0cmluZygpICsgXCJLXCI7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBsZXQgdCA9IE1hdGguZmxvb3IobnVtIC8gMTAwMDAwMClcbiAgICAgICAgICAgIHJldHVybiB0LnRvTG9jYWxlU3RyaW5nKCkgKyBcIk1cIjtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyBmb3JtYXROdW1iZXJTaG9ydChudW0sIGZpeGVkOiBudW1iZXIgPSAwKSB7XG5cbiAgICAgICAgaWYgKG51bSA8IDEwMDApIHtcbiAgICAgICAgICAgIHJldHVybiBudW07XG4gICAgICAgIH0gZWxzZSBpZiAobnVtIDwgMTAwMDAwMCkge1xuICAgICAgICAgICAgbGV0IHQgPSBNYXRoLmZsb29yKG51bSAvIDEwMDApLnRvRml4ZWQoZml4ZWQpO1xuICAgICAgICAgICAgcmV0dXJuIHQgKyBcIktcIjtcbiAgICAgICAgfSBlbHNlIGlmIChudW0gPCAxMDAwMDAwMDAwKSB7XG4gICAgICAgICAgICBsZXQgdCA9IE1hdGguZmxvb3IobnVtIC8gMTAwMDAwMCkudG9GaXhlZChmaXhlZCk7XG4gICAgICAgICAgICByZXR1cm4gdCArIFwiTVwiO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgbGV0IHQgPSBNYXRoLmZsb29yKG51bSAvIDEwMDAwMDAwMDApLnRvRml4ZWQoZml4ZWQpO1xuICAgICAgICAgICAgcmV0dXJuIHQudG9Mb2NhbGVTdHJpbmcoKSArIFwiR1wiO1xuICAgICAgICB9XG5cbiAgICB9XG5cblxuICAgIC8qKlxuICAgICAqIOenkeWtpuiuoeaVsOazleaYvuekulxuICAgICAqIEBwYXJhbSBudW0xXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyBiaWdOdW1iZXJGb3JtYXQobnVtOiBzdHJpbmcsZml4ZWQ6bnVtYmVyID0gMikge1xuICAgICAgICBsZXQgZXh0cyA9IFtcbiAgICAgICAgICAgICcnLCAnSycsIFwiTVwiLCBcIkdcIiwgXCJUXCIsIFwiUFwiLCBcIkVcIiwgXCJaXCIsIFwiWVwiLCBcIkFBXCIsXG4gICAgICAgICAgICBcIkJCXCIsIFwiQ0NcIiwgJ0REJywgJ0VFJywgXCJGRlwiLCBcIkdHXCIsIFwiSEhcIiwgXCJJSVwiLFxuICAgICAgICAgICAgXCJKSlwiLCBcIktLXCIsICdMTCcsICdNTScsIFwiTk5cIiwgXCJPT1wiLCBcIlBQXCIsIFwiUVFcIixcbiAgICAgICAgICAgIFwiUlJcIiwgXCJTU1wiLCAnVFQnLCAnVVUnLCBcIlZWXCIsIFwiV1dcIiwgXCJYWFwiLCBcIllZXCIsXG4gICAgICAgICAgICBcIlpaXCIsIFwiYWFcIiwgJ2JiJywgJ2NjJywgXCJkZFwiLCBcImVlXCIsIFwiZmZcIiwgXCJnZ1wiLFxuICAgICAgICAgICAgXCJoaFwiLCBcImlpXCIsICdnZycsICdraycsIFwibGxcIiwgXCJtbVwiLCBcIm5uXCIsIFwib29cIixcbiAgICAgICAgICAgIFwicHBcIiwgXCJxcVwiLCAncnInLCAnc3MnLCBcInR0XCIsIFwidXVcIiwgXCJ2dlwiLCBcInd3XCJcbiAgICAgICAgXTtcblxuICAgICAgICBsZXQgdDEsIHQyO1xuICAgICAgICBsZXQgbjEgPSBudW0uaW5kZXhPZihcImUrXCIpO1xuICAgICAgICBpZiAobjEgPT0gLTEpIG4xID0gbnVtLmluZGV4T2YoXCJFXCIpO1xuICAgICAgICBpZiAobjEgJiYgbjEgIT0gLTEpIHtcbiAgICAgICAgICAgIHQxID0gcGFyc2VGbG9hdChudW0uc3Vic3RyKDAsIG4xKSk7XG4gICAgICAgICAgICB0MiA9IHBhcnNlSW50KG51bS5zdWJzdHIobjEgKyAyKSk7XG5cbiAgICAgICAgICAgIGxldCBleHQgPSBNYXRoLmZsb29yKHQyIC8gMyk7XG4gICAgICAgICAgICBsZXQgbSA9IHQyICUgMztcblxuICAgICAgICAgICAgcmV0dXJuICh0MSAqIE1hdGgucG93KDEwLG0pKS50b0ZpeGVkKGZpeGVkKSArIGV4dHNbZXh0XTtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgcmV0dXJuIG51bTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDmlbDlrZfnm7jliqBcbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIGJpZ051bWJlckFkZChudW0xOiBzdHJpbmcsIG51bTI6IHN0cmluZykge1xuICAgICAgICBsZXQgYiA9IE51bWJlcihudW0xKSArIE51bWJlcihudW0yKTtcbiAgICAgICAgcmV0dXJuIGIudG9FeHBvbmVudGlhbCg0KTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDmlbDlrZfnm7jlh49cbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIGJpZ051bWJlclN1YihudW0xOiBzdHJpbmcsIG51bTI6IHN0cmluZykge1xuICAgICAgICBsZXQgbjEgPSBOdW1iZXIobnVtMSk7XG4gICAgICAgIGxldCBuMiA9IE51bWJlcihudW0yKTtcbiAgICAgICAgaWYgKG4xIDwgbjIpIHtcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIChuMSAtIG4yKS50b0V4cG9uZW50aWFsKDQpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOaVsOWtl+ebuOS5mOazlVxuICAgICAqL1xuICAgIHB1YmxpYyBzdGF0aWMgYmlnTnVtYmVyTXVsKG51bTE6IHN0cmluZywgbnVtMjogbnVtYmVyKSB7XG4gICAgICAgIHJldHVybiAoTnVtYmVyKG51bTEpICogbnVtMikudG9FeHBvbmVudGlhbCg0KTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDmlbDlrZfnm7jpmaRcbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIGJpZ051bWJlckRpdihudW0xOiBzdHJpbmcsIG51bTI6IG51bWJlcikge1xuICAgICAgICByZXR1cm4gKE51bWJlcihudW0xKSAvIG51bTIpLnRvRXhwb25lbnRpYWwoNCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5Lik5Liq56eR5a2m6K6h5pWw55u46ZmkXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyBiaWdOdW1iZXJEaXZEb3VuYmxlKG51bTE6IHN0cmluZywgbnVtMjogc3RyaW5nKSB7XG4gICAgICAgIHJldHVybiAoTnVtYmVyKG51bTEpIC8gTnVtYmVyKG51bTIpKTtcbiAgICB9XG5cbn1cbiIsIi8qKlxuICogQGF1dGhvciBTdW5cbiAqIEB0aW1lIDIwMTktMDgtMTEgMTg6NTVcbiAqIEBwcm9qZWN0IFNGcmFtZXdvcmtfTGF5YUFpclxuICogQGRlc2NyaXB0aW9uIOWtl+espuS4suW3peWFt+exu1xuICpcbiAqL1xuZXhwb3J0IGNsYXNzIFV0aWxTdHJpbmcge1xuXG4gICAgcHVibGljIHN0YXRpYyBnZXQgZW1wdHkoKTogc3RyaW5nIHtcbiAgICAgICAgcmV0dXJuIFwiXCI7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5a2X56ym5Liy5piv5ZCm5pyJ5YC8XG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyBpc0VtcHR5KHM6IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gKHMgIT0gbnVsbCAmJiBzLmxlbmd0aCA+IDApID8gZmFsc2UgOiB0cnVlO1xuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgdG9JbnQoc3RyOiBzdHJpbmcpOiBudW1iZXIge1xuICAgICAgICBpZiAoIXN0ciB8fCBzdHIubGVuZ3RoID09IDApIHJldHVybiAwO1xuICAgICAgICByZXR1cm4gcGFyc2VJbnQoc3RyKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIHRvTnVtYmVyKHN0cjogc3RyaW5nKTogbnVtYmVyIHtcbiAgICAgICAgaWYgKCFzdHIgfHwgc3RyLmxlbmd0aCA9PSAwKSByZXR1cm4gMDtcbiAgICAgICAgcmV0dXJuIHBhcnNlRmxvYXQoc3RyKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDojrflj5blrZfnrKbkuLLnnJ/lrp7plb/luqYs5rOo77yaXG4gICAgICogMS7mma7pgJrmlbDnu4TvvIzlrZfnrKbljaAx5a2X6IqC77yb5rGJ5a2Q5Y2g5Lik5Liq5a2X6IqCXG4gICAgICogMi7lpoLmnpzlj5jmiJDnvJbnoIHvvIzlj6/og73orqHnrpfmjqXlj6PkuI3lr7lcbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIGdldE51bUJ5dGVzKHN0cjogc3RyaW5nKTogbnVtYmVyIHtcbiAgICAgICAgbGV0IHJlYWxMZW5ndGggPSAwLCBsZW4gPSBzdHIubGVuZ3RoLCBjaGFyQ29kZSA9IC0xO1xuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICAgICAgICBjaGFyQ29kZSA9IHN0ci5jaGFyQ29kZUF0KGkpO1xuICAgICAgICAgICAgaWYgKGNoYXJDb2RlID49IDAgJiYgY2hhckNvZGUgPD0gMTI4KSByZWFsTGVuZ3RoICs9IDE7XG4gICAgICAgICAgICBlbHNlIHJlYWxMZW5ndGggKz0gMjtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcmVhbExlbmd0aDtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDooaXpm7ZcbiAgICAgKiBAcGFyYW0gc3RyXG4gICAgICogQHBhcmFtIGxlblxuICAgICAqIEBwYXJhbSBkaXIgMC3lkI7vvJsxLeWJjVxuICAgICAqIEByZXR1cm5cbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIGFkZFplcm8oc3RyOiBzdHJpbmcsIGxlbjogbnVtYmVyLCBkaXI6IG51bWJlciA9IDApOiBzdHJpbmcge1xuICAgICAgICBsZXQgX3N0cjogc3RyaW5nID0gXCJcIjtcbiAgICAgICAgbGV0IF9sZW46IG51bWJlciA9IHN0ci5sZW5ndGg7XG4gICAgICAgIGxldCBzdHJfcHJlX3plcm86IHN0cmluZyA9IFwiXCI7XG4gICAgICAgIGxldCBzdHJfZW5kX3plcm86IHN0cmluZyA9IFwiXCI7XG4gICAgICAgIGlmIChkaXIgPT0gMClcbiAgICAgICAgICAgIHN0cl9lbmRfemVybyA9IFwiMFwiO1xuICAgICAgICBlbHNlXG4gICAgICAgICAgICBzdHJfcHJlX3plcm8gPSBcIjBcIjtcblxuICAgICAgICBpZiAoX2xlbiA8IGxlbikge1xuICAgICAgICAgICAgbGV0IGk6IG51bWJlciA9IDA7XG4gICAgICAgICAgICB3aGlsZSAoaSA8IGxlbiAtIF9sZW4pIHtcbiAgICAgICAgICAgICAgICBfc3RyID0gc3RyX3ByZV96ZXJvICsgX3N0ciArIHN0cl9lbmRfemVybztcbiAgICAgICAgICAgICAgICArK2k7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiBfc3RyICsgc3RyO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHN0cjtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDljrvpmaTlt6blj7PnqbrmoLxcbiAgICAgKiBAcGFyYW0gaW5wdXRcbiAgICAgKiBAcmV0dXJuXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyB0cmltKGlucHV0OiBzdHJpbmcpOiBzdHJpbmcge1xuICAgICAgICBpZiAoaW5wdXQgPT0gbnVsbCkge1xuICAgICAgICAgICAgcmV0dXJuIFwiXCI7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGlucHV0LnJlcGxhY2UoL15cXHMrfFxccyskXCJcIl5cXHMrfFxccyskL2csIFwiXCIpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOWOu+mZpOW3puS+p+epuuagvFxuICAgICAqIEBwYXJhbSBpbnB1dFxuICAgICAqIEByZXR1cm5cbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIHRyaW1MZWZ0KGlucHV0OiBzdHJpbmcpOiBzdHJpbmcge1xuICAgICAgICBpZiAoaW5wdXQgPT0gbnVsbCkge1xuICAgICAgICAgICAgcmV0dXJuIFwiXCI7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGlucHV0LnJlcGxhY2UoL15cXHMrXCJcIl5cXHMrLywgXCJcIik7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5Y676Zmk5Y+z5L6n56m65qC8XG4gICAgICogQHBhcmFtIGlucHV0XG4gICAgICogQHJldHVyblxuICAgICAqL1xuICAgIHB1YmxpYyBzdGF0aWMgdHJpbVJpZ2h0KGlucHV0OiBzdHJpbmcpOiBzdHJpbmcge1xuICAgICAgICBpZiAoaW5wdXQgPT0gbnVsbCkge1xuICAgICAgICAgICAgcmV0dXJuIFwiXCI7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGlucHV0LnJlcGxhY2UoL1xccyskXCJcIlxccyskLywgXCJcIik7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5YiG6ZKf5LiO56eS5qC85byPKOWmgi0+IDQwOjE1KVxuICAgICAqIEBwYXJhbSBzZWNvbmRzIOenkuaVsFxuICAgICAqIEByZXR1cm5cbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIG1pbnV0ZUZvcm1hdChzZWNvbmRzOiBudW1iZXIpOiBzdHJpbmcge1xuICAgICAgICBsZXQgbWluOiBudW1iZXIgPSBNYXRoLmZsb29yKHNlY29uZHMgLyA2MCk7XG4gICAgICAgIGxldCBzZWM6IG51bWJlciA9IE1hdGguZmxvb3Ioc2Vjb25kcyAlIDYwKTtcblxuICAgICAgICBsZXQgbWluX3N0cjogc3RyaW5nID0gbWluIDwgMTAgPyAoXCIwXCIgKyBtaW4udG9TdHJpbmcoKSkgOiAobWluLnRvU3RyaW5nKCkpO1xuICAgICAgICBsZXQgc2VjX3N0cjogc3RyaW5nID0gc2VjIDwgMTAgPyAoXCIwXCIgKyBzZWMudG9TdHJpbmcoKSkgOiAoc2VjLnRvU3RyaW5nKCkpO1xuXG4gICAgICAgIHJldHVybiBtaW5fc3RyICsgXCI6XCIgKyBzZWNfc3RyO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOaXtuWIhuenkuagvOW8jyjlpoItPiAwNTozMjoyMClcbiAgICAgKiBAcGFyYW0gc2Vjb25kcyjnp5IpXG4gICAgICogQHJldHVyblxuICAgICAqL1xuICAgIHB1YmxpYyBzdGF0aWMgaG91ckZvcm1hdChzZWNvbmRzOiBudW1iZXIpOiBzdHJpbmcge1xuICAgICAgICBsZXQgaG91cjogbnVtYmVyID0gTWF0aC5mbG9vcihzZWNvbmRzIC8gMzYwMCk7XG4gICAgICAgIGxldCBob3VyX3N0cjogU3RyaW5nID0gaG91ciA8IDEwID8gKFwiMFwiICsgaG91ci50b1N0cmluZygpKSA6IChob3VyLnRvU3RyaW5nKCkpO1xuICAgICAgICByZXR1cm4gaG91cl9zdHIgKyBcIjpcIiArIFV0aWxTdHJpbmcubWludXRlRm9ybWF0KHNlY29uZHMgJSAzNjAwKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDmoLzlvI/ljJblrZfnrKbkuLJcbiAgICAgKiBAcGFyYW0gc3RyIOmcgOimgeagvOW8j+WMlueahOWtl+espuS4su+8jOOAkFwi5p2w5Y2r77yM6L+Z6YeM5pyJezB95Liq6Iu55p6c77yM5ZKMezF95Liq6aaZ6JWJ77yBXCIsIDUsMTDjgJFcbiAgICAgKiBAcGFyYW0gYXJncyDlj4LmlbDliJfooahcbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIGZvcm1hdChzdHI6IHN0cmluZywgLi4uYXJncyk6IHN0cmluZyB7XG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgYXJncy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgc3RyID0gc3RyLnJlcGxhY2UobmV3IFJlZ0V4cChcIlxcXFx7XCIgKyBpICsgXCJcXFxcfVwiLCBcImdtXCIpLCBhcmdzW2ldKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gc3RyO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOS7peaMh+WumuWtl+espuW8gOWni1xuICAgICAqL1xuICAgIHB1YmxpYyBzdGF0aWMgYmVnaW5zV2l0aChpbnB1dDogc3RyaW5nLCBwcmVmaXg6IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gcHJlZml4ID09IGlucHV0LnN1YnN0cmluZygwLCBwcmVmaXgubGVuZ3RoKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDku6XmjIflrprlrZfnrKbnu5PmnZ9cbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIGVuZHNXaXRoKGlucHV0OiBzdHJpbmcsIHN1ZmZpeDogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiBzdWZmaXggPT0gaW5wdXQuc3Vic3RyaW5nKGlucHV0Lmxlbmd0aCAtIHN1ZmZpeC5sZW5ndGgpO1xuICAgIH1cblxuICAgIC8qKmd1aWQqL1xuICAgIHB1YmxpYyBzdGF0aWMgZ2V0R1VJRFN0cmluZygpOiBzdHJpbmcge1xuICAgICAgICBsZXQgZCA9IERhdGUubm93KCk7XG4gICAgICAgIGlmICh3aW5kb3cucGVyZm9ybWFuY2UgJiYgdHlwZW9mIHdpbmRvdy5wZXJmb3JtYW5jZS5ub3cgPT09IFwiZnVuY3Rpb25cIikge1xuICAgICAgICAgICAgZCArPSBwZXJmb3JtYW5jZS5ub3coKTsgLy91c2UgaGlnaC1wcmVjaXNpb24gdGltZXIgaWYgYXZhaWxhYmxlXG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuICd4eHh4eHh4eC14eHh4LTR4eHgteXh4eC14eHh4eHh4eHh4eHgnLnJlcGxhY2UoL1t4eV0vZywgKGMpID0+IHtcbiAgICAgICAgICAgIGxldCByID0gKGQgKyBNYXRoLnJhbmRvbSgpICogMTYpICUgMTYgfCAwO1xuICAgICAgICAgICAgZCA9IE1hdGguZmxvb3IoZCAvIDE2KTtcbiAgICAgICAgICAgIHJldHVybiAoYyA9PSAneCcgPyByIDogKHIgJiAweDMgfCAweDgpKS50b1N0cmluZygxNik7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOmmluWtl+avjeWkp+WtplxuICAgICAqL1xuICAgIHB1YmxpYyBzdGF0aWMgZmlyc3RVcHBlckNhc2Uod29yZDogc3RyaW5nKTogc3RyaW5nIHtcbiAgICAgICAgcmV0dXJuIHdvcmQuY2hhckF0KDApLnRvVXBwZXJDYXNlKCkgKyB3b3JkLnNsaWNlKDEpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOagvOW8j+WMluS4i+WIkue6v+eahOWNleivjVxuICAgICAqL1xuICAgIHB1YmxpYyBzdGF0aWMgZm9ybWF0RGFzaFdvcmQod29yZDogc3RyaW5nLCBjYXBGaXJzdDogYm9vbGVhbiA9IGZhbHNlKSB7XG4gICAgICAgIGxldCBmaXJzdCA9IHRydWU7XG4gICAgICAgIGxldCByZXN1bHQgPSBcIlwiO1xuICAgICAgICB3b3JkLnNwbGl0KCdfJykuZm9yRWFjaCgoc2VjOiBzdHJpbmcpID0+IHtcbiAgICAgICAgICAgIGlmIChmaXJzdCkge1xuICAgICAgICAgICAgICAgIGlmIChjYXBGaXJzdCkge1xuICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSBVdGlsU3RyaW5nLmZpcnN0VXBwZXJDYXNlKHNlYyk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0gc2VjO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBmaXJzdCA9IGZhbHNlO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICByZXN1bHQgPSByZXN1bHQgKyBVdGlsU3RyaW5nLmZpcnN0VXBwZXJDYXNlKHNlYyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOaIquWPluWtl+espuS4slxuICAgICAqIEBwYXJhbSBzdHIg5a2X56ym5LiyXG4gICAgICogQHBhcmFtIHN0YXJ0IOW8gOWni+S9jee9rlxuICAgICAqIEBwYXJhbSBlbmQg57uT5p2f5L2N572uXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyBzdWJzdHJpbmcoc3RyOnN0cmluZyxzdGFydDpudW1iZXIsZW5kOm51bWJlcik6c3RyaW5nXG4gICAge1xuICAgICAgICByZXR1cm4gc3RyLnN1YnN0cmluZyhzdGFydCxlbmQpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOaIquWPluWtl+espuS4slxuICAgICAqIEBwYXJhbSBzdHIg5a2X56ym5LiyXG4gICAgICogQHBhcmFtIHN0YXJ0IOW8gOWni+S9jee9rlxuICAgICAqIEBwYXJhbSBsb25nIOaIquWPlumVv+W6plxuICAgICAqL1xuICAgIHB1YmxpYyBzdGF0aWMgc3Vic3RyKHN0cjpzdHJpbmcsc3RhcnQ6bnVtYmVyLGxvbmc6bnVtYmVyKTpzdHJpbmdcbiAgICB7XG4gICAgICAgIHJldHVybiBzdHIuc3Vic3RyKHN0YXJ0LGxvbmcpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOWtl+espuS4sui9rOWvueixoVxuICAgICAqIEBwYXJhbSBzdHJcbiAgICAgKi9cbiAgICBwdWJsaWMgc3RhdGljIHN0clRvT2JqZWN0KHN0cjpzdHJpbmcpXG4gICAge1xuICAgICAgICBjb25zdCBzdHJUb09iaiA9IEpTT04ucGFyc2Uoc3RyKTtcbiAgICAgICAgcmV0dXJuIHN0clRvT2JqO1xuICAgIH1cblxuXG4gICAgLyoqXG4gICAgICog5a+56LGh6L2s5a2X56ym5LiyXG4gICAgICogQHBhcmFtIHN0clxuICAgICAqL1xuICAgIHB1YmxpYyBzdGF0aWMgb2JqVG9TdHIob2JqOk9iamVjdCk6c3RyaW5nXG4gICAge1xuICAgICAgICBjb25zdCBvYmpUb1N0ciA9IEpTT04uc3RyaW5naWZ5KG9iailcbiAgICAgICAgcmV0dXJuIG9ialRvU3RyO1xuICAgIH1cbn1cbiIsIi8qKlxuICogQGF1dGhvciBTdW5cbiAqIEB0aW1lIDIwMTktMDgtMDkgMTk6MThcbiAqIEBwcm9qZWN0IFNGcmFtZXdvcmtfTGF5YUFpclxuICogQGRlc2NyaXB0aW9uICDml7bpl7Tlt6XlhbdcbiAqXG4gKi9cbmV4cG9ydCBjbGFzcyBVdGlsVGltZSB7XG5cbiAgICBwcml2YXRlIHN0YXRpYyBtX1N0YXJ0VGltZTogbnVtYmVyID0gMDtcblxuICAgIHB1YmxpYyBzdGF0aWMgc3RhcnQoKSB7XG4gICAgICAgIHRoaXMubV9TdGFydFRpbWUgPSBMYXlhLnRpbWVyLmN1cnJUaW1lcjtcbiAgICB9XG5cbiAgICAvKirkuKTluKfkuYvpl7TnmoTml7bpl7Tpl7TpmpQs5Y2V5L2N56eSKi9cbiAgICBwdWJsaWMgc3RhdGljIGdldCBkZWx0YVRpbWUoKTogbnVtYmVyIHtcbiAgICAgICAgcmV0dXJuIExheWEudGltZXIuZGVsdGEgKiAwLjAwMTtcbiAgICB9XG5cbiAgICAvKirlm7rlrprkuKTluKfkuYvpl7TnmoTml7bpl7Tpl7TpmpQqL1xuICAgIHB1YmxpYyBzdGF0aWMgZ2V0IGZpeGVkRGVsdGFUaW1lKCk6IG51bWJlciB7XG4gICAgICAgIHJldHVybiAwO1xuICAgIH1cblxuICAgIC8qKuW9k+WJjeaXtumXtO+8jOebuOWvuXh4eHjlubTlvIDlp4vnu4/ov4fnmoTmr6vnp5LmlbAqL1xuICAgIHB1YmxpYyBzdGF0aWMgZ2V0IHRpbWUoKTogbnVtYmVyIHtcbiAgICAgICAgcmV0dXJuIExheWEudGltZXIuY3VyclRpbWVyO1xuICAgIH1cblxuICAgIC8qKua4uOaIj+WQr+WKqOWIsOeOsOWcqOeahOaXtumXtCzljZXkvY3mr6vnp5IqL1xuICAgIHB1YmxpYyBzdGF0aWMgZ2V0IHRpbWVTaW5jZVN0YXJ0dXAoKTogbnVtYmVyIHtcbiAgICAgICAgcmV0dXJuIChMYXlhLnRpbWVyLmN1cnJUaW1lciAtIHRoaXMubV9TdGFydFRpbWUpO1xuICAgIH1cblxuICAgIC8qKua4uOaIj+WQr+WKqOWQju+8jOe7j+i/h+eahOW4p+aVsCovXG4gICAgcHVibGljIHN0YXRpYyBnZXQgZnJhbWVDb3VudCgpOiBudW1iZXIge1xuICAgICAgICByZXR1cm4gTGF5YS50aW1lci5jdXJyRnJhbWU7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBnZXQgdGltZVNjYWxlKCk6IG51bWJlciB7XG4gICAgICAgIHJldHVybiBMYXlhLnRpbWVyLnNjYWxlO1xuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgc2V0IHRpbWVTY2FsZShzY2FsZTogbnVtYmVyKSB7XG4gICAgICAgIExheWEudGltZXIuc2NhbGUgPSBzY2FsZTtcbiAgICB9XG59XG4iLCIvKipUaGlzIGNsYXNzIGlzIGF1dG9tYXRpY2FsbHkgZ2VuZXJhdGVkIGJ5IExheWFBaXJJREUsIHBsZWFzZSBkbyBub3QgbWFrZSBhbnkgbW9kaWZpY2F0aW9ucy4gKi9cbmltcG9ydCBTY2VuZT1MYXlhLlNjZW5lO1xuaW1wb3J0IFZpZXc9TGF5YS5WaWV3O1xuaW1wb3J0IERpYWxvZz1MYXlhLkRpYWxvZztcbmltcG9ydCBCb3g9TGF5YS5Cb3g7XG5pbXBvcnQgVGFwPUxheWEuVGFiO1xuaW1wb3J0IENsaXA9TGF5YS5DbGlwO1xuaW1wb3J0IExpc3Q9TGF5YS5MaXN0O1xuaW1wb3J0IEltYWdlPUxheWEuSW1hZ2U7XG5pbXBvcnQgTGFiZWw9TGF5YS5MYWJlbDtcbmltcG9ydCBQYW5lbD1MYXlhLlBhbmVsO1xuaW1wb3J0IFNwcml0ZT1MYXlhLlNwcml0ZTtcbmltcG9ydCBCdXR0b249TGF5YS5CdXR0b247XG5pbXBvcnQgQ2hlY2tCb3g9TGF5YS5DaGVja0JveDtcbmltcG9ydCBIU2xpZGVyPUxheWEuSFNsaWRlcjtcbmltcG9ydCBTbGlkZXI9TGF5YS5WU2xpZGVyO1xuaW1wb3J0IFZpZXdTdGFjaz1MYXlhLlZpZXdTdGFjaztcbmltcG9ydCBBbmltYXRpb249TGF5YS5BbmltYXRpb247XG5pbXBvcnQgUHJvZ3Jlc3NCYXI9TGF5YS5Qcm9ncmVzc0JhcjtcbmltcG9ydCBGcmFtZUFuaW1hdGlvbj1MYXlhLkZyYW1lQW5pbWF0aW9uO1xuaW1wb3J0IHtDdXN0b21WaWV3fSBmcm9tIFwiLi4vZnJhbWV3b3JrL21hbmFnZXIvdWkvdmlldy1iYXNlXCI7XG5pbXBvcnQge0N1c3RvbURpYWxvZ30gZnJvbSBcIi4uL2ZyYW1ld29yay9tYW5hZ2VyL3VpL2RpYWxvZy1iYXNlXCI7XG5pbXBvcnQgRGlhbG9nQmFzZSA9IEN1c3RvbURpYWxvZy5EaWFsb2dCYXNlO1xuaW1wb3J0IFZpZXdCYXNlID0gQ3VzdG9tVmlldy5WaWV3QmFzZTtcbnZhciBSRUc6IEZ1bmN0aW9uID0gTGF5YS5DbGFzc1V0aWxzLnJlZ0NsYXNzO1xuZXhwb3J0IG1vZHVsZSB1aS52aWV3LmNvbSB7XHJcbiAgICBleHBvcnQgY2xhc3MgZGF5N3NVSSBleHRlbmRzIERpYWxvZ0Jhc2Uge1xyXG4gICAgICAgIHB1YmxpYyBzdGF0aWMgIHVpVmlldzphbnkgPXtcInR5cGVcIjpcIkRpYWxvZ0Jhc2VcIixcInByb3BzXCI6e1wid2lkdGhcIjo3NTAsXCJoZWlnaHRcIjoxMzM0fSxcImNvbXBJZFwiOjIsXCJsb2FkTGlzdFwiOltdLFwibG9hZExpc3QzRFwiOltdfTtcclxuICAgICAgICBjb25zdHJ1Y3RvcigpeyBzdXBlcigpfVxyXG4gICAgICAgIGNyZWF0ZUNoaWxkcmVuKCk6dm9pZCB7XHJcbiAgICAgICAgICAgIHN1cGVyLmNyZWF0ZUNoaWxkcmVuKCk7XHJcbiAgICAgICAgICAgIHRoaXMuY3JlYXRlVmlldyhkYXk3c1VJLnVpVmlldyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgUkVHKFwidWkudmlldy5jb20uZGF5N3NVSVwiLGRheTdzVUkpO1xyXG4gICAgZXhwb3J0IGNsYXNzIGludml0ZVVJIGV4dGVuZHMgRGlhbG9nQmFzZSB7XHJcbiAgICAgICAgcHVibGljIHN0YXRpYyAgdWlWaWV3OmFueSA9e1widHlwZVwiOlwiRGlhbG9nQmFzZVwiLFwicHJvcHNcIjp7XCJ3aWR0aFwiOjc1MCxcImhlaWdodFwiOjEzMzR9LFwiY29tcElkXCI6MixcImxvYWRMaXN0XCI6W10sXCJsb2FkTGlzdDNEXCI6W119O1xyXG4gICAgICAgIGNvbnN0cnVjdG9yKCl7IHN1cGVyKCl9XHJcbiAgICAgICAgY3JlYXRlQ2hpbGRyZW4oKTp2b2lkIHtcclxuICAgICAgICAgICAgc3VwZXIuY3JlYXRlQ2hpbGRyZW4oKTtcclxuICAgICAgICAgICAgdGhpcy5jcmVhdGVWaWV3KGludml0ZVVJLnVpVmlldyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgUkVHKFwidWkudmlldy5jb20uaW52aXRlVUlcIixpbnZpdGVVSSk7XHJcbiAgICBleHBvcnQgY2xhc3MgbG90dGVyeVVJIGV4dGVuZHMgRGlhbG9nQmFzZSB7XHJcblx0XHRwdWJsaWMgaWRsZTpGcmFtZUFuaW1hdGlvbjtcblx0XHRwdWJsaWMgaW1nQ29udGV4dDpJbWFnZTtcblx0XHRwdWJsaWMgYnRuQ29uZmlybTpCdXR0b247XG5cdFx0cHVibGljIGJ0bkNsb3NlOkJ1dHRvbjtcbiAgICAgICAgcHVibGljIHN0YXRpYyAgdWlWaWV3OmFueSA9e1widHlwZVwiOlwiRGlhbG9nQmFzZVwiLFwicHJvcHNcIjp7XCJ5XCI6MCxcInhcIjowLFwid2lkdGhcIjo3NTAsXCJoZWlnaHRcIjoxMzM0fSxcImNvbXBJZFwiOjIsXCJjaGlsZFwiOlt7XCJ0eXBlXCI6XCJJbWFnZVwiLFwicHJvcHNcIjp7XCJ5XCI6NjE1LFwieFwiOjM3NSxcInNraW5cIjpcInJlcy9jb20vaW1nX2xvdHRlcnlfYm9yZGVyLnBuZ1wiLFwiYW5jaG9yWVwiOjAuNSxcImFuY2hvclhcIjowLjV9LFwiY29tcElkXCI6NDUsXCJjaGlsZFwiOlt7XCJ0eXBlXCI6XCJJbWFnZVwiLFwicHJvcHNcIjp7XCJ5XCI6MzE0LFwieFwiOjMxNCxcInZhclwiOlwiaW1nQ29udGV4dFwiLFwic2tpblwiOlwicmVzL2NvbS9pbWdfbG90dGVyeV9jb250ZW50LnBuZ1wiLFwiYW5jaG9yWVwiOjAuNSxcImFuY2hvclhcIjowLjV9LFwiY29tcElkXCI6NDZ9LHtcInR5cGVcIjpcIkltYWdlXCIsXCJwcm9wc1wiOntcInlcIjotNjYsXCJ4XCI6MjUzLFwic2tpblwiOlwicmVzL2NvbS9pbWdfemhlbi5wbmdcIn0sXCJjb21wSWRcIjo0N30se1widHlwZVwiOlwiQnV0dG9uXCIsXCJwcm9wc1wiOntcInlcIjo3ODAsXCJ4XCI6MzE0LFwid2lkdGhcIjoyNTgsXCJ2YXJcIjpcImJ0bkNvbmZpcm1cIixcInN0YXRlTnVtXCI6MSxcInNraW5cIjpcInJlcy9tYWluL2VmZmVjdC9idG5fY29tbW9uXzIucG5nXCIsXCJoZWlnaHRcIjoxMzAsXCJhbmNob3JZXCI6MC41LFwiYW5jaG9yWFwiOjAuNX0sXCJjb21wSWRcIjo0OCxcImNoaWxkXCI6W3tcInR5cGVcIjpcIkxhYmVsXCIsXCJwcm9wc1wiOntcInZhbGlnblwiOlwibWlkZGxlXCIsXCJ0b3BcIjowLFwidGV4dFwiOlwi5oq95aWWXCIsXCJyaWdodFwiOjAsXCJsZWZ0XCI6MCxcImZvbnRTaXplXCI6NjAsXCJib3R0b21cIjowLFwiYm9sZFwiOnRydWUsXCJhbGlnblwiOlwiY2VudGVyXCJ9LFwiY29tcElkXCI6NDl9XX0se1widHlwZVwiOlwiQnV0dG9uXCIsXCJwcm9wc1wiOntcInlcIjotMTk0LFwieFwiOjU4NyxcInZhclwiOlwiYnRuQ2xvc2VcIixcInN0YXRlTnVtXCI6MSxcInNraW5cIjpcInJlcy9tYWluL2VmZmVjdC9idG5fY2xvc2UucG5nXCIsXCJhbmNob3JZXCI6MC41LFwiYW5jaG9yWFwiOjAuNX0sXCJjb21wSWRcIjo1MH1dfV0sXCJhbmltYXRpb25zXCI6W3tcIm5vZGVzXCI6W3tcInRhcmdldFwiOjM0LFwia2V5ZnJhbWVzXCI6e1wieFwiOlt7XCJ2YWx1ZVwiOjM2NyxcInR3ZWVuTWV0aG9kXCI6XCJsaW5lYXJOb25lXCIsXCJ0d2VlblwiOnRydWUsXCJ0YXJnZXRcIjozNCxcImtleVwiOlwieFwiLFwiaW5kZXhcIjowfSx7XCJ2YWx1ZVwiOjM2NyxcInR3ZWVuTWV0aG9kXCI6XCJsaW5lYXJOb25lXCIsXCJ0d2VlblwiOnRydWUsXCJ0YXJnZXRcIjozNCxcImtleVwiOlwieFwiLFwiaW5kZXhcIjoxMH0se1widmFsdWVcIjozNjcsXCJ0d2Vlbk1ldGhvZFwiOlwibGluZWFyTm9uZVwiLFwidHdlZW5cIjp0cnVlLFwidGFyZ2V0XCI6MzQsXCJrZXlcIjpcInhcIixcImluZGV4XCI6MjV9XSxcInZpc2libGVcIjpbe1widmFsdWVcIjp0cnVlLFwidHdlZW5NZXRob2RcIjpcImxpbmVhck5vbmVcIixcInR3ZWVuXCI6ZmFsc2UsXCJ0YXJnZXRcIjozNCxcImtleVwiOlwidmlzaWJsZVwiLFwiaW5kZXhcIjowfSx7XCJ2YWx1ZVwiOmZhbHNlLFwidHdlZW5NZXRob2RcIjpcImxpbmVhck5vbmVcIixcInR3ZWVuXCI6ZmFsc2UsXCJ0YXJnZXRcIjozNCxcImtleVwiOlwidmlzaWJsZVwiLFwiaW5kZXhcIjoxMH0se1widmFsdWVcIjp0cnVlLFwidHdlZW5NZXRob2RcIjpcImxpbmVhck5vbmVcIixcInR3ZWVuXCI6ZmFsc2UsXCJ0YXJnZXRcIjozNCxcImtleVwiOlwidmlzaWJsZVwiLFwiaW5kZXhcIjoxNX0se1widmFsdWVcIjpmYWxzZSxcInR3ZWVuTWV0aG9kXCI6XCJsaW5lYXJOb25lXCIsXCJ0d2VlblwiOmZhbHNlLFwidGFyZ2V0XCI6MzQsXCJrZXlcIjpcInZpc2libGVcIixcImluZGV4XCI6MjV9LHtcInZhbHVlXCI6dHJ1ZSxcInR3ZWVuTWV0aG9kXCI6XCJsaW5lYXJOb25lXCIsXCJ0d2VlblwiOmZhbHNlLFwidGFyZ2V0XCI6MzQsXCJrZXlcIjpcInZpc2libGVcIixcImluZGV4XCI6MzB9XSxcInJvdGF0aW9uXCI6W3tcInZhbHVlXCI6MCxcInR3ZWVuTWV0aG9kXCI6XCJsaW5lYXJOb25lXCIsXCJ0d2VlblwiOnRydWUsXCJ0YXJnZXRcIjozNCxcImtleVwiOlwicm90YXRpb25cIixcImluZGV4XCI6MH0se1widmFsdWVcIjowLFwidHdlZW5NZXRob2RcIjpcImxpbmVhck5vbmVcIixcInR3ZWVuXCI6dHJ1ZSxcInRhcmdldFwiOjM0LFwia2V5XCI6XCJyb3RhdGlvblwiLFwiaW5kZXhcIjoxMH0se1widmFsdWVcIjo3LFwidHdlZW5NZXRob2RcIjpcImxpbmVhck5vbmVcIixcInR3ZWVuXCI6dHJ1ZSxcInRhcmdldFwiOjM0LFwia2V5XCI6XCJyb3RhdGlvblwiLFwiaW5kZXhcIjoxNX0se1widmFsdWVcIjo3LFwidHdlZW5NZXRob2RcIjpcImxpbmVhck5vbmVcIixcInR3ZWVuXCI6dHJ1ZSxcInRhcmdldFwiOjM0LFwia2V5XCI6XCJyb3RhdGlvblwiLFwiaW5kZXhcIjoyNX0se1widmFsdWVcIjowLFwidHdlZW5NZXRob2RcIjpcImxpbmVhck5vbmVcIixcInR3ZWVuXCI6dHJ1ZSxcInRhcmdldFwiOjM0LFwia2V5XCI6XCJyb3RhdGlvblwiLFwiaW5kZXhcIjozMH1dfX1dLFwibmFtZVwiOlwiaWRsZVwiLFwiaWRcIjoxLFwiZnJhbWVSYXRlXCI6MjQsXCJhY3Rpb25cIjowfV0sXCJsb2FkTGlzdFwiOltcInJlcy9jb20vaW1nX2xvdHRlcnlfYm9yZGVyLnBuZ1wiLFwicmVzL2NvbS9pbWdfbG90dGVyeV9jb250ZW50LnBuZ1wiLFwicmVzL2NvbS9pbWdfemhlbi5wbmdcIixcInJlcy9tYWluL2VmZmVjdC9idG5fY29tbW9uXzIucG5nXCIsXCJyZXMvbWFpbi9lZmZlY3QvYnRuX2Nsb3NlLnBuZ1wiXSxcImxvYWRMaXN0M0RcIjpbXX07XHJcbiAgICAgICAgY29uc3RydWN0b3IoKXsgc3VwZXIoKX1cclxuICAgICAgICBjcmVhdGVDaGlsZHJlbigpOnZvaWQge1xyXG4gICAgICAgICAgICBzdXBlci5jcmVhdGVDaGlsZHJlbigpO1xyXG4gICAgICAgICAgICB0aGlzLmNyZWF0ZVZpZXcobG90dGVyeVVJLnVpVmlldyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgUkVHKFwidWkudmlldy5jb20ubG90dGVyeVVJXCIsbG90dGVyeVVJKTtcclxuICAgIGV4cG9ydCBjbGFzcyByYW5rVUkgZXh0ZW5kcyBEaWFsb2dCYXNlIHtcclxuICAgICAgICBwdWJsaWMgc3RhdGljICB1aVZpZXc6YW55ID17XCJ0eXBlXCI6XCJEaWFsb2dCYXNlXCIsXCJwcm9wc1wiOntcIndpZHRoXCI6NzUwLFwiaGVpZ2h0XCI6MTMzNH0sXCJjb21wSWRcIjoyLFwiY2hpbGRcIjpbe1widHlwZVwiOlwiV1hPcGVuRGF0YVZpZXdlclwiLFwicHJvcHNcIjp7XCJ5XCI6MzgxLFwieFwiOjExNixcIndpZHRoXCI6NTI0LFwibW91c2VUaHJvdWdoXCI6dHJ1ZSxcImljb25TaWduXCI6XCJ3eFwiLFwiaGVpZ2h0XCI6ODU4LFwicnVudGltZVwiOlwiTGF5YS5XWE9wZW5EYXRhVmlld2VyXCJ9LFwiY29tcElkXCI6M31dLFwibG9hZExpc3RcIjpbXSxcImxvYWRMaXN0M0RcIjpbXX07XHJcbiAgICAgICAgY29uc3RydWN0b3IoKXsgc3VwZXIoKX1cclxuICAgICAgICBjcmVhdGVDaGlsZHJlbigpOnZvaWQge1xyXG4gICAgICAgICAgICBzdXBlci5jcmVhdGVDaGlsZHJlbigpO1xyXG4gICAgICAgICAgICB0aGlzLmNyZWF0ZVZpZXcocmFua1VJLnVpVmlldyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgUkVHKFwidWkudmlldy5jb20ucmFua1VJXCIscmFua1VJKTtcclxuICAgIGV4cG9ydCBjbGFzcyBzaG9wVUkgZXh0ZW5kcyBEaWFsb2dCYXNlIHtcclxuICAgICAgICBwdWJsaWMgc3RhdGljICB1aVZpZXc6YW55ID17XCJ0eXBlXCI6XCJEaWFsb2dCYXNlXCIsXCJwcm9wc1wiOntcIndpZHRoXCI6NzUwLFwibW91c2VUaHJvdWdoXCI6dHJ1ZSxcImhlaWdodFwiOjEzMzR9LFwiY29tcElkXCI6MixcImxvYWRMaXN0XCI6W10sXCJsb2FkTGlzdDNEXCI6W119O1xyXG4gICAgICAgIGNvbnN0cnVjdG9yKCl7IHN1cGVyKCl9XHJcbiAgICAgICAgY3JlYXRlQ2hpbGRyZW4oKTp2b2lkIHtcclxuICAgICAgICAgICAgc3VwZXIuY3JlYXRlQ2hpbGRyZW4oKTtcclxuICAgICAgICAgICAgdGhpcy5jcmVhdGVWaWV3KHNob3BVSS51aVZpZXcpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIFJFRyhcInVpLnZpZXcuY29tLnNob3BVSVwiLHNob3BVSSk7XHJcbn1cclxuZXhwb3J0IG1vZHVsZSB1aS52aWV3Lm1haW4ge1xyXG4gICAgZXhwb3J0IGNsYXNzIGJnVUkgZXh0ZW5kcyBWaWV3QmFzZSB7XHJcblx0XHRwdWJsaWMgaW1nQmc6SW1hZ2U7XG4gICAgICAgIHB1YmxpYyBzdGF0aWMgIHVpVmlldzphbnkgPXtcInR5cGVcIjpcIlZpZXdCYXNlXCIsXCJwcm9wc1wiOntcIndpZHRoXCI6NzUwLFwiaGVpZ2h0XCI6MTMzNH0sXCJjb21wSWRcIjoyLFwiY2hpbGRcIjpbe1widHlwZVwiOlwiSW1hZ2VcIixcInByb3BzXCI6e1widmFyXCI6XCJpbWdCZ1wiLFwidG9wXCI6MCxcInNraW5cIjpcInJlcy9tYWluL2JnL2JnLnBuZ1wiLFwicmlnaHRcIjowLFwibGVmdFwiOjAsXCJib3R0b21cIjowfSxcImNvbXBJZFwiOjV9XSxcImxvYWRMaXN0XCI6W1wicmVzL21haW4vYmcvYmcucG5nXCJdLFwibG9hZExpc3QzRFwiOltdfTtcclxuICAgICAgICBjb25zdHJ1Y3RvcigpeyBzdXBlcigpfVxyXG4gICAgICAgIGNyZWF0ZUNoaWxkcmVuKCk6dm9pZCB7XHJcbiAgICAgICAgICAgIHN1cGVyLmNyZWF0ZUNoaWxkcmVuKCk7XHJcbiAgICAgICAgICAgIHRoaXMuY3JlYXRlVmlldyhiZ1VJLnVpVmlldyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgUkVHKFwidWkudmlldy5tYWluLmJnVUlcIixiZ1VJKTtcclxuICAgIGV4cG9ydCBjbGFzcyBkM1VJIGV4dGVuZHMgVmlld0Jhc2Uge1xyXG4gICAgICAgIHB1YmxpYyBzdGF0aWMgIHVpVmlldzphbnkgPXtcInR5cGVcIjpcIlZpZXdCYXNlXCIsXCJwcm9wc1wiOntcIndpZHRoXCI6NzUwLFwiaGVpZ2h0XCI6MTMzNH0sXCJjb21wSWRcIjoyLFwibG9hZExpc3RcIjpbXSxcImxvYWRMaXN0M0RcIjpbXX07XHJcbiAgICAgICAgY29uc3RydWN0b3IoKXsgc3VwZXIoKX1cclxuICAgICAgICBjcmVhdGVDaGlsZHJlbigpOnZvaWQge1xyXG4gICAgICAgICAgICBzdXBlci5jcmVhdGVDaGlsZHJlbigpO1xyXG4gICAgICAgICAgICB0aGlzLmNyZWF0ZVZpZXcoZDNVSS51aVZpZXcpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIFJFRyhcInVpLnZpZXcubWFpbi5kM1VJXCIsZDNVSSk7XHJcbiAgICBleHBvcnQgY2xhc3MgZWZmZWN0VUkgZXh0ZW5kcyBWaWV3QmFzZSB7XHJcblx0XHRwdWJsaWMgYnRuTHVja3k6QnV0dG9uO1xuXHRcdHB1YmxpYyBidG5SYW5rOkJ1dHRvbjtcblx0XHRwdWJsaWMgYnRuSW52aXRlOkJ1dHRvbjtcblx0XHRwdWJsaWMgYnRuU2V0dGluZzpCdXR0b247XG4gICAgICAgIHB1YmxpYyBzdGF0aWMgIHVpVmlldzphbnkgPXtcInR5cGVcIjpcIlZpZXdCYXNlXCIsXCJwcm9wc1wiOntcIndpZHRoXCI6NzUwLFwiaGVpZ2h0XCI6MTMzNH0sXCJjb21wSWRcIjoyLFwiY2hpbGRcIjpbe1widHlwZVwiOlwiSW1hZ2VcIixcInByb3BzXCI6e1wieVwiOjY0LFwieFwiOjcyLFwid2lkdGhcIjoyMTMsXCJza2luXCI6XCJyZXMvbWFpbi9lZmZlY3QvaW1hZ2Vfc3RhdHVzLnBuZ1wiLFwiaGVpZ2h0XCI6NDZ9LFwiY29tcElkXCI6M30se1widHlwZVwiOlwiSW1hZ2VcIixcInByb3BzXCI6e1wieVwiOjY0LFwieFwiOjQ1OSxcIndpZHRoXCI6MjEzLFwic2tpblwiOlwicmVzL21haW4vZWZmZWN0L2ltYWdlX3N0YXR1cy5wbmdcIixcImhlaWdodFwiOjQ2fSxcImNvbXBJZFwiOjR9LHtcInR5cGVcIjpcIkltYWdlXCIsXCJwcm9wc1wiOntcInlcIjo0OCxcInhcIjo0MDMsXCJza2luXCI6XCJyZXMvbWFpbi9lZmZlY3QvaW1nX2RpYW1vbmQucG5nXCJ9LFwiY29tcElkXCI6NX0se1widHlwZVwiOlwiSW1hZ2VcIixcInByb3BzXCI6e1wieVwiOjQ0LFwieFwiOjMwLFwic2tpblwiOlwicmVzL21haW4vZWZmZWN0L2ltZ19nbG9kLnBuZ1wifSxcImNvbXBJZFwiOjZ9LHtcInR5cGVcIjpcIkJ1dHRvblwiLFwicHJvcHNcIjp7XCJ5XCI6MjgyLFwieFwiOjM3NSxcIndpZHRoXCI6MjA3LFwidmFyXCI6XCJidG5MdWNreVwiLFwic3RhdGVOdW1cIjoxLFwic2tpblwiOlwicmVzL21haW4vZWZmZWN0L2J0bl9jb21tb25fMS5wbmdcIixcImhlaWdodFwiOjEwNCxcImFuY2hvcllcIjowLjUsXCJhbmNob3JYXCI6MC41fSxcImNvbXBJZFwiOjcsXCJjaGlsZFwiOlt7XCJ0eXBlXCI6XCJMYWJlbFwiLFwicHJvcHNcIjp7XCJ2YWxpZ25cIjpcIm1pZGRsZVwiLFwidG9wXCI6MCxcInRleHRcIjpcIui9rOebmFwiLFwicmlnaHRcIjowLFwibGVmdFwiOjAsXCJmb250U2l6ZVwiOjQwLFwiYm90dG9tXCI6MCxcImJvbGRcIjp0cnVlLFwiYWxpZ25cIjpcImNlbnRlclwifSxcImNvbXBJZFwiOjExfV19LHtcInR5cGVcIjpcIkJ1dHRvblwiLFwicHJvcHNcIjp7XCJ5XCI6NDM5LFwieFwiOjM3NSxcIndpZHRoXCI6MjA3LFwidmFyXCI6XCJidG5SYW5rXCIsXCJzdGF0ZU51bVwiOjEsXCJza2luXCI6XCJyZXMvbWFpbi9lZmZlY3QvYnRuX2NvbW1vbl8yLnBuZ1wiLFwiaGVpZ2h0XCI6MTA0LFwiYW5jaG9yWVwiOjAuNSxcImFuY2hvclhcIjowLjV9LFwiY29tcElkXCI6OCxcImNoaWxkXCI6W3tcInR5cGVcIjpcIkxhYmVsXCIsXCJwcm9wc1wiOntcInZhbGlnblwiOlwibWlkZGxlXCIsXCJ0b3BcIjowLFwidGV4dFwiOlwi5o6S6KGMXCIsXCJyaWdodFwiOjAsXCJsZWZ0XCI6MCxcImZvbnRTaXplXCI6NDAsXCJib3R0b21cIjowLFwiYm9sZFwiOnRydWUsXCJhbGlnblwiOlwiY2VudGVyXCJ9LFwiY29tcElkXCI6MTJ9XX0se1widHlwZVwiOlwiQnV0dG9uXCIsXCJwcm9wc1wiOntcInlcIjo2MDYsXCJ4XCI6Mzc1LFwid2lkdGhcIjoyMDcsXCJ2YXJcIjpcImJ0bkludml0ZVwiLFwic3RhdGVOdW1cIjoxLFwic2tpblwiOlwicmVzL21haW4vZWZmZWN0L2J0bl9jb21tb25fMy5wbmdcIixcImhlaWdodFwiOjEwNCxcImFuY2hvcllcIjowLjUsXCJhbmNob3JYXCI6MC41fSxcImNvbXBJZFwiOjksXCJjaGlsZFwiOlt7XCJ0eXBlXCI6XCJMYWJlbFwiLFwicHJvcHNcIjp7XCJ2YWxpZ25cIjpcIm1pZGRsZVwiLFwidG9wXCI6MCxcInRleHRcIjpcIumCgOivt1wiLFwicmlnaHRcIjowLFwibGVmdFwiOjAsXCJmb250U2l6ZVwiOjQwLFwiYm90dG9tXCI6MCxcImJvbGRcIjp0cnVlLFwiYWxpZ25cIjpcImNlbnRlclwifSxcImNvbXBJZFwiOjEzfV19LHtcInR5cGVcIjpcIkJ1dHRvblwiLFwicHJvcHNcIjp7XCJ5XCI6Nzc2LFwieFwiOjM3NSxcIndpZHRoXCI6MjA3LFwidmFyXCI6XCJidG5TZXR0aW5nXCIsXCJzdGF0ZU51bVwiOjEsXCJza2luXCI6XCJyZXMvbWFpbi9lZmZlY3QvYnRuX2NvbW1vbl80LnBuZ1wiLFwiaGVpZ2h0XCI6MTA0LFwiYW5jaG9yWVwiOjAuNSxcImFuY2hvclhcIjowLjV9LFwiY29tcElkXCI6MTAsXCJjaGlsZFwiOlt7XCJ0eXBlXCI6XCJMYWJlbFwiLFwicHJvcHNcIjp7XCJ2YWxpZ25cIjpcIm1pZGRsZVwiLFwidG9wXCI6MCxcInRleHRcIjpcIuiuvue9rlwiLFwicmlnaHRcIjowLFwibGVmdFwiOjAsXCJmb250U2l6ZVwiOjQwLFwiYm90dG9tXCI6MCxcImJvbGRcIjp0cnVlLFwiYWxpZ25cIjpcImNlbnRlclwifSxcImNvbXBJZFwiOjE0fV19XSxcImxvYWRMaXN0XCI6W1wicmVzL21haW4vZWZmZWN0L2ltYWdlX3N0YXR1cy5wbmdcIixcInJlcy9tYWluL2VmZmVjdC9pbWdfZGlhbW9uZC5wbmdcIixcInJlcy9tYWluL2VmZmVjdC9pbWdfZ2xvZC5wbmdcIixcInJlcy9tYWluL2VmZmVjdC9idG5fY29tbW9uXzEucG5nXCIsXCJyZXMvbWFpbi9lZmZlY3QvYnRuX2NvbW1vbl8yLnBuZ1wiLFwicmVzL21haW4vZWZmZWN0L2J0bl9jb21tb25fMy5wbmdcIixcInJlcy9tYWluL2VmZmVjdC9idG5fY29tbW9uXzQucG5nXCJdLFwibG9hZExpc3QzRFwiOltdfTtcclxuICAgICAgICBjb25zdHJ1Y3RvcigpeyBzdXBlcigpfVxyXG4gICAgICAgIGNyZWF0ZUNoaWxkcmVuKCk6dm9pZCB7XHJcbiAgICAgICAgICAgIHN1cGVyLmNyZWF0ZUNoaWxkcmVuKCk7XHJcbiAgICAgICAgICAgIHRoaXMuY3JlYXRlVmlldyhlZmZlY3RVSS51aVZpZXcpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIFJFRyhcInVpLnZpZXcubWFpbi5lZmZlY3RVSVwiLGVmZmVjdFVJKTtcclxuICAgIGV4cG9ydCBjbGFzcyBnYW1lVUkgZXh0ZW5kcyBWaWV3QmFzZSB7XHJcblx0XHRwdWJsaWMgYW5pX2dyYXA6RnJhbWVBbmltYXRpb247XG5cdFx0cHVibGljIGFuaV9sdWNrQkw6RnJhbWVBbmltYXRpb247XG4gICAgICAgIHB1YmxpYyBzdGF0aWMgIHVpVmlldzphbnkgPXtcInR5cGVcIjpcIlZpZXdCYXNlXCIsXCJwcm9wc1wiOntcIndpZHRoXCI6NzUwLFwiaGVpZ2h0XCI6MTMzNH0sXCJjb21wSWRcIjoyLFwiYW5pbWF0aW9uc1wiOlt7XCJub2Rlc1wiOlt7XCJ0YXJnZXRcIjo0MTMsXCJrZXlmcmFtZXNcIjp7XCJ2aXNpYmxlXCI6W3tcInZhbHVlXCI6ZmFsc2UsXCJ0d2Vlbk1ldGhvZFwiOlwibGluZWFyTm9uZVwiLFwidHdlZW5cIjpmYWxzZSxcInRhcmdldFwiOjQxMyxcImtleVwiOlwidmlzaWJsZVwiLFwiaW5kZXhcIjowfSx7XCJ2YWx1ZVwiOnRydWUsXCJ0d2Vlbk1ldGhvZFwiOlwibGluZWFyTm9uZVwiLFwidHdlZW5cIjpmYWxzZSxcInRhcmdldFwiOjQxMyxcImtleVwiOlwidmlzaWJsZVwiLFwiaW5kZXhcIjoyfSx7XCJ2YWx1ZVwiOmZhbHNlLFwidHdlZW5NZXRob2RcIjpcImxpbmVhck5vbmVcIixcInR3ZWVuXCI6ZmFsc2UsXCJ0YXJnZXRcIjo0MTMsXCJrZXlcIjpcInZpc2libGVcIixcImluZGV4XCI6NH0se1widmFsdWVcIjp0cnVlLFwidHdlZW5NZXRob2RcIjpcImxpbmVhck5vbmVcIixcInR3ZWVuXCI6ZmFsc2UsXCJ0YXJnZXRcIjo0MTMsXCJrZXlcIjpcInZpc2libGVcIixcImluZGV4XCI6Nn0se1widmFsdWVcIjpmYWxzZSxcInR3ZWVuTWV0aG9kXCI6XCJsaW5lYXJOb25lXCIsXCJ0d2VlblwiOmZhbHNlLFwidGFyZ2V0XCI6NDEzLFwia2V5XCI6XCJ2aXNpYmxlXCIsXCJpbmRleFwiOjh9LHtcInZhbHVlXCI6dHJ1ZSxcInR3ZWVuTWV0aG9kXCI6XCJsaW5lYXJOb25lXCIsXCJ0d2VlblwiOmZhbHNlLFwidGFyZ2V0XCI6NDEzLFwia2V5XCI6XCJ2aXNpYmxlXCIsXCJpbmRleFwiOjEwfSx7XCJ2YWx1ZVwiOmZhbHNlLFwidHdlZW5NZXRob2RcIjpcImxpbmVhck5vbmVcIixcInR3ZWVuXCI6ZmFsc2UsXCJ0YXJnZXRcIjo0MTMsXCJrZXlcIjpcInZpc2libGVcIixcImluZGV4XCI6MTJ9XX19LHtcInRhcmdldFwiOjMyNCxcImtleWZyYW1lc1wiOntcInZpc2libGVcIjpbe1widmFsdWVcIjp0cnVlLFwidHdlZW5NZXRob2RcIjpcImxpbmVhck5vbmVcIixcInR3ZWVuXCI6ZmFsc2UsXCJ0YXJnZXRcIjozMjQsXCJrZXlcIjpcInZpc2libGVcIixcImluZGV4XCI6MH0se1widmFsdWVcIjpmYWxzZSxcInR3ZWVuTWV0aG9kXCI6XCJsaW5lYXJOb25lXCIsXCJ0d2VlblwiOmZhbHNlLFwidGFyZ2V0XCI6MzI0LFwia2V5XCI6XCJ2aXNpYmxlXCIsXCJpbmRleFwiOjJ9LHtcInZhbHVlXCI6dHJ1ZSxcInR3ZWVuTWV0aG9kXCI6XCJsaW5lYXJOb25lXCIsXCJ0d2VlblwiOmZhbHNlLFwidGFyZ2V0XCI6MzI0LFwia2V5XCI6XCJ2aXNpYmxlXCIsXCJpbmRleFwiOjR9LHtcInZhbHVlXCI6ZmFsc2UsXCJ0d2Vlbk1ldGhvZFwiOlwibGluZWFyTm9uZVwiLFwidHdlZW5cIjpmYWxzZSxcInRhcmdldFwiOjMyNCxcImtleVwiOlwidmlzaWJsZVwiLFwiaW5kZXhcIjo2fSx7XCJ2YWx1ZVwiOnRydWUsXCJ0d2Vlbk1ldGhvZFwiOlwibGluZWFyTm9uZVwiLFwidHdlZW5cIjpmYWxzZSxcInRhcmdldFwiOjMyNCxcImtleVwiOlwidmlzaWJsZVwiLFwiaW5kZXhcIjo4fSx7XCJ2YWx1ZVwiOmZhbHNlLFwidHdlZW5NZXRob2RcIjpcImxpbmVhck5vbmVcIixcInR3ZWVuXCI6ZmFsc2UsXCJ0YXJnZXRcIjozMjQsXCJrZXlcIjpcInZpc2libGVcIixcImluZGV4XCI6MTB9LHtcInZhbHVlXCI6dHJ1ZSxcInR3ZWVuTWV0aG9kXCI6XCJsaW5lYXJOb25lXCIsXCJ0d2VlblwiOmZhbHNlLFwidGFyZ2V0XCI6MzI0LFwia2V5XCI6XCJ2aXNpYmxlXCIsXCJpbmRleFwiOjEyfV19fV0sXCJuYW1lXCI6XCJhbmlfZ3JhcFwiLFwiaWRcIjoyOSxcImZyYW1lUmF0ZVwiOjI0LFwiYWN0aW9uXCI6MH0se1wibm9kZXNcIjpbe1widGFyZ2V0XCI6NDY4LFwia2V5ZnJhbWVzXCI6e1wicm90YXRpb25cIjpbe1widmFsdWVcIjowLFwidHdlZW5NZXRob2RcIjpcImxpbmVhck5vbmVcIixcInR3ZWVuXCI6dHJ1ZSxcInRhcmdldFwiOjQ2OCxcImtleVwiOlwicm90YXRpb25cIixcImluZGV4XCI6MH0se1widmFsdWVcIjozNjAsXCJ0d2Vlbk1ldGhvZFwiOlwibGluZWFyTm9uZVwiLFwidHdlZW5cIjp0cnVlLFwidGFyZ2V0XCI6NDY4LFwia2V5XCI6XCJyb3RhdGlvblwiLFwiaW5kZXhcIjoyMDB9XSxcImFscGhhXCI6W3tcInZhbHVlXCI6MSxcInR3ZWVuTWV0aG9kXCI6XCJsaW5lYXJOb25lXCIsXCJ0d2VlblwiOnRydWUsXCJ0YXJnZXRcIjo0NjgsXCJrZXlcIjpcImFscGhhXCIsXCJpbmRleFwiOjB9LHtcInZhbHVlXCI6MC41LFwidHdlZW5NZXRob2RcIjpcImxpbmVhck5vbmVcIixcInR3ZWVuXCI6dHJ1ZSxcInRhcmdldFwiOjQ2OCxcImtleVwiOlwiYWxwaGFcIixcImluZGV4XCI6NTB9LHtcInZhbHVlXCI6MSxcInR3ZWVuTWV0aG9kXCI6XCJsaW5lYXJOb25lXCIsXCJ0d2VlblwiOnRydWUsXCJ0YXJnZXRcIjo0NjgsXCJrZXlcIjpcImFscGhhXCIsXCJpbmRleFwiOjEwMH0se1widmFsdWVcIjowLjUsXCJ0d2Vlbk1ldGhvZFwiOlwibGluZWFyTm9uZVwiLFwidHdlZW5cIjp0cnVlLFwidGFyZ2V0XCI6NDY4LFwia2V5XCI6XCJhbHBoYVwiLFwiaW5kZXhcIjoxNTB9LHtcInZhbHVlXCI6MSxcInR3ZWVuTWV0aG9kXCI6XCJsaW5lYXJOb25lXCIsXCJ0d2VlblwiOnRydWUsXCJ0YXJnZXRcIjo0NjgsXCJrZXlcIjpcImFscGhhXCIsXCJpbmRleFwiOjIwMH1dfX0se1widGFyZ2V0XCI6NDY5LFwia2V5ZnJhbWVzXCI6e1wicm90YXRpb25cIjpbe1widmFsdWVcIjowLFwidHdlZW5NZXRob2RcIjpcImxpbmVhck5vbmVcIixcInR3ZWVuXCI6dHJ1ZSxcInRhcmdldFwiOjQ2OSxcImtleVwiOlwicm90YXRpb25cIixcImluZGV4XCI6MH0se1widmFsdWVcIjotMzYwLFwidHdlZW5NZXRob2RcIjpcImxpbmVhck5vbmVcIixcInR3ZWVuXCI6dHJ1ZSxcInRhcmdldFwiOjQ2OSxcImtleVwiOlwicm90YXRpb25cIixcImluZGV4XCI6MjAwfV0sXCJhbHBoYVwiOlt7XCJ2YWx1ZVwiOjAuNSxcInR3ZWVuTWV0aG9kXCI6XCJsaW5lYXJOb25lXCIsXCJ0d2VlblwiOnRydWUsXCJ0YXJnZXRcIjo0NjksXCJrZXlcIjpcImFscGhhXCIsXCJpbmRleFwiOjB9LHtcInZhbHVlXCI6MSxcInR3ZWVuTWV0aG9kXCI6XCJsaW5lYXJOb25lXCIsXCJ0d2VlblwiOnRydWUsXCJ0YXJnZXRcIjo0NjksXCJrZXlcIjpcImFscGhhXCIsXCJpbmRleFwiOjUwfSx7XCJ2YWx1ZVwiOjAuNSxcInR3ZWVuTWV0aG9kXCI6XCJsaW5lYXJOb25lXCIsXCJ0d2VlblwiOnRydWUsXCJ0YXJnZXRcIjo0NjksXCJrZXlcIjpcImFscGhhXCIsXCJpbmRleFwiOjEwMH0se1widmFsdWVcIjoxLFwidHdlZW5NZXRob2RcIjpcImxpbmVhck5vbmVcIixcInR3ZWVuXCI6dHJ1ZSxcInRhcmdldFwiOjQ2OSxcImtleVwiOlwiYWxwaGFcIixcImluZGV4XCI6MTUwfV19fV0sXCJuYW1lXCI6XCJhbmlfbHVja0JMXCIsXCJpZFwiOjMwLFwiZnJhbWVSYXRlXCI6MjQsXCJhY3Rpb25cIjowfV0sXCJsb2FkTGlzdFwiOltdLFwibG9hZExpc3QzRFwiOltdfTtcclxuICAgICAgICBjb25zdHJ1Y3RvcigpeyBzdXBlcigpfVxyXG4gICAgICAgIGNyZWF0ZUNoaWxkcmVuKCk6dm9pZCB7XHJcbiAgICAgICAgICAgIHN1cGVyLmNyZWF0ZUNoaWxkcmVuKCk7XHJcbiAgICAgICAgICAgIHRoaXMuY3JlYXRlVmlldyhnYW1lVUkudWlWaWV3KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBSRUcoXCJ1aS52aWV3Lm1haW4uZ2FtZVVJXCIsZ2FtZVVJKTtcclxuICAgIGV4cG9ydCBjbGFzcyBsb2FkaW5nVUkgZXh0ZW5kcyBWaWV3QmFzZSB7XHJcblx0XHRwdWJsaWMgaW1nX2JnOkltYWdlO1xuXHRcdHB1YmxpYyBib3hfYnRtOkJveDtcblx0XHRwdWJsaWMgcHJvX0xvYWRpbmc6UHJvZ3Jlc3NCYXI7XG5cdFx0cHVibGljIGxibExvYWRpbmc6TGFiZWw7XG5cdFx0cHVibGljIGxibF9wOkxhYmVsO1xuICAgICAgICBwdWJsaWMgc3RhdGljICB1aVZpZXc6YW55ID17XCJ0eXBlXCI6XCJWaWV3QmFzZVwiLFwicHJvcHNcIjp7XCJ3aWR0aFwiOjc1MCxcImhlaWdodFwiOjEzMzR9LFwiY29tcElkXCI6MixcImNoaWxkXCI6W3tcInR5cGVcIjpcIkltYWdlXCIsXCJwcm9wc1wiOntcInlcIjowLFwieFwiOjAsXCJ2YXJcIjpcImltZ19iZ1wiLFwidG9wXCI6MCxcInNraW5cIjpcInJlcy9sb2FkaW5nL2ltZ19sb2FkaW5nX2JnLnBuZ1wiLFwicmlnaHRcIjowLFwibGVmdFwiOjAsXCJib3R0b21cIjowfSxcImNvbXBJZFwiOjN9LHtcInR5cGVcIjpcIkJveFwiLFwicHJvcHNcIjp7XCJ5XCI6MCxcInhcIjowLFwid2lkdGhcIjo0OTMsXCJ2YXJcIjpcImJveF9idG1cIixcInBpdm90WVwiOjE0OSxcInBpdm90WFwiOjI0OSxcImhlaWdodFwiOjE0OSxcImNlbnRlclhcIjowLFwiYm90dG9tXCI6MH0sXCJjb21wSWRcIjo1LFwiY2hpbGRcIjpbe1widHlwZVwiOlwiUHJvZ3Jlc3NCYXJcIixcInByb3BzXCI6e1wieVwiOjIwLFwieFwiOjI0NyxcInZhclwiOlwicHJvX0xvYWRpbmdcIixcInNraW5cIjpcInJlcy9sb2FkaW5nL3Byb2dyZXNzX2xvYWRpbmcucG5nXCIsXCJwaXZvdFlcIjoxMixcInBpdm90WFwiOjE3NX0sXCJjb21wSWRcIjo2fSx7XCJ0eXBlXCI6XCJMYWJlbFwiLFwicHJvcHNcIjp7XCJ5XCI6MjAsXCJ3aWR0aFwiOjIzOCxcInZhclwiOlwibGJsTG9hZGluZ1wiLFwidmFsaWduXCI6XCJtaWRkbGVcIixcInRleHRcIjpcIjEwMCVcIixcInN0cm9rZUNvbG9yXCI6XCIjZmZmZmZmXCIsXCJzdHJva2VcIjo0LFwicGl2b3RZXCI6MTYsXCJwaXZvdFhcIjoxMTksXCJoZWlnaHRcIjozMixcImZvbnRTaXplXCI6MjYsXCJmb250XCI6XCJBcmlhbFwiLFwiY29sb3JcIjpcIiM1OTIyMjJcIixcImNlbnRlclhcIjowLFwiYm9sZFwiOnRydWUsXCJhbGlnblwiOlwiY2VudGVyXCJ9LFwiY29tcElkXCI6N30se1widHlwZVwiOlwiSW1hZ2VcIixcInByb3BzXCI6e1wieVwiOjg1LFwieFwiOjI0NyxcIndpZHRoXCI6NDkzLFwic2tpblwiOlwicmVzL2xvYWRpbmcvaW1nXzhyLnBuZ1wiLFwicGl2b3RZXCI6MjAsXCJwaXZvdFhcIjoyNDcsXCJoZWlnaHRcIjozOX0sXCJjb21wSWRcIjo4fSx7XCJ0eXBlXCI6XCJMYWJlbFwiLFwicHJvcHNcIjp7XCJ5XCI6MTI4LFwieFwiOjI0NyxcIndpZHRoXCI6MjgzLFwidmFyXCI6XCJsYmxfcFwiLFwidmFsaWduXCI6XCJtaWRkbGVcIixcInRleHRcIjpcIlBvd2VyZWQgYnkgTGF5YUFpciBFbmdpbmVcIixcInBpdm90WVwiOjIxLFwicGl2b3RYXCI6MTQyLFwiaGVpZ2h0XCI6NDIsXCJmb250U2l6ZVwiOjE4LFwiY29sb3JcIjpcIiNmZmZmZmZcIixcImJvbGRcIjp0cnVlLFwiYWxpZ25cIjpcImNlbnRlclwifSxcImNvbXBJZFwiOjl9XX1dLFwibG9hZExpc3RcIjpbXCJyZXMvbG9hZGluZy9pbWdfbG9hZGluZ19iZy5wbmdcIixcInJlcy9sb2FkaW5nL3Byb2dyZXNzX2xvYWRpbmcucG5nXCIsXCJyZXMvbG9hZGluZy9pbWdfOHIucG5nXCJdLFwibG9hZExpc3QzRFwiOltdfTtcclxuICAgICAgICBjb25zdHJ1Y3RvcigpeyBzdXBlcigpfVxyXG4gICAgICAgIGNyZWF0ZUNoaWxkcmVuKCk6dm9pZCB7XHJcbiAgICAgICAgICAgIHN1cGVyLmNyZWF0ZUNoaWxkcmVuKCk7XHJcbiAgICAgICAgICAgIHRoaXMuY3JlYXRlVmlldyhsb2FkaW5nVUkudWlWaWV3KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBSRUcoXCJ1aS52aWV3Lm1haW4ubG9hZGluZ1VJXCIsbG9hZGluZ1VJKTtcclxufVxyIl19
